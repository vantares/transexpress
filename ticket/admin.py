# -*- coding: UTF-8 -*- 
from django.contrib import admin
from django.db import models
from django.conf import settings
from read_only import ReadOnlyAdmin
from ticket.models import *
from postal.admin import RawIdCustomAdmin
class ArchivoInline ( admin.TabularInline ):
    model = Archivo
    extra = 1

class ArchivoRO( ReadOnlyAdmin, admin.TabularInline ):
    model = Archivo
    extra = 0

class RespuestaInline( admin.TabularInline ):
    model = Respuesta
    extra = 1

class RespuestaRO( ReadOnlyAdmin, admin.TabularInline ):
    model = Respuesta
    extra = 0

class TicketAdmin( RawIdCustomAdmin, admin.ModelAdmin ):
    inlines = [ArchivoInline, RespuestaInline]
    raw_id_fields = ('cliente',)

class TicketRO( ReadOnlyAdmin, admin.ModelAdmin ):
    inlines = [ArchivoRO, RespuestaRO]

class EstadoTicketRO( ReadOnlyAdmin, admin.ModelAdmin):
    model = EstadoTicketRead

admin.site.register(Ticket,TicketAdmin)
admin.site.register(EstadoTicket)
admin.site.register(TicketRead, TicketRO)
admin.site.register(EstadoTicketRead, EstadoTicketRO)
