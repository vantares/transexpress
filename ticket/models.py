# -*- coding: UTF-8 -*- 
from django.db import models
from crm.models import Cliente
from nomina.models import Empleado

class EstadoTicket( models.Model ):
    estado = models.CharField(db_column="estado", null=True, blank=True, max_length=300, verbose_name="estado")  
    
    def __unicode__( self ):
        return self.estado

    class Meta:
        verbose_name = "Estado Ticket Browse"
        verbose_name_plural = "Estados Tickets Browse" 

class EstadoTicketRead( EstadoTicket ):
    class Meta:
        verbose_name = "Estado Ticket"
        verbose_name_plural = "Estados Tickets" 
        proxy = True

class Ticket( models.Model ):
    cliente = models.ForeignKey(Cliente, blank=True, null=True, verbose_name="cliente")
    descripcion = models.CharField(db_column="descripcion", null=False, blank=False, max_length=500, verbose_name=u"descripción")  
    estado = models.ForeignKey(EstadoTicket, blank=True, null=True, verbose_name="estado")
    empleado = models.ForeignKey(Empleado, blank=True, null=True, verbose_name="Empleado")
    detalle = models.TextField(db_column="detalle", null=True, blank=True, verbose_name=u"detalle")

    def __unicode__( self ):
        return self.descripcion

    class Meta:
        verbose_name = "Ticket Browse"
        verbose_name_plural = "Tickets Browse" 

class TicketRead( Ticket ):
    class Meta:
        verbose_name = "Ticket"
        verbose_name_plural = "Tickets" 
        proxy = True
    
class Archivo( models.Model ):
    ticket = models.ForeignKey(Ticket, blank=True, null=True, verbose_name="ticket")
    titulo = models.CharField(db_column="titulo", null=False, blank=False, max_length=300, verbose_name="titulo")  
    archivo = models.FileField(db_column="documento", upload_to = 'documents', null=False, blank=False, verbose_name="Archivo de documento")

    def __unicode__( self ):
        return self.titulo

class Respuesta( models.Model ):
    ticket = models.ForeignKey(Ticket, blank=True, null=True, verbose_name="ticket")
    texto = models.TextField(db_column="texto", null=False, blank=False, verbose_name=u"texto respuesta")
    
    def __unicode__( self ):
        return self.texto
