# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'EstadoTicket'
        db.create_table('ticket_estadoticket', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('estado', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, db_column='estado', blank=True)),
        ))
        db.send_create_signal('ticket', ['EstadoTicket'])

        # Adding model 'Ticket'
        db.create_table('ticket_ticket', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cliente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crm.Cliente'], null=True, blank=True)),
            ('descripcion', self.gf('django.db.models.fields.CharField')(max_length=500, db_column='descripcion')),
            ('estado', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ticket.EstadoTicket'], null=True, blank=True)),
            ('empleado', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nomina.Empleado'], null=True, blank=True)),
            ('detalle', self.gf('django.db.models.fields.TextField')(null=True, db_column='detalle', blank=True)),
        ))
        db.send_create_signal('ticket', ['Ticket'])

        # Adding model 'Archivo'
        db.create_table('ticket_archivo', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ticket', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ticket.Ticket'], null=True, blank=True)),
            ('titulo', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='titulo')),
            ('archivo', self.gf('django.db.models.fields.files.FileField')(max_length=100, db_column='documento')),
        ))
        db.send_create_signal('ticket', ['Archivo'])

        # Adding model 'Respuesta'
        db.create_table('ticket_respuesta', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ticket', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ticket.Ticket'], null=True, blank=True)),
            ('texto', self.gf('django.db.models.fields.TextField')(db_column='texto')),
        ))
        db.send_create_signal('ticket', ['Respuesta'])

    def backwards(self, orm):
        # Deleting model 'EstadoTicket'
        db.delete_table('ticket_estadoticket')

        # Deleting model 'Ticket'
        db.delete_table('ticket_ticket')

        # Deleting model 'Archivo'
        db.delete_table('ticket_archivo')

        # Deleting model 'Respuesta'
        db.delete_table('ticket_respuesta')

    models = {
        'crm.cliente': {
            'Meta': {'object_name': 'Cliente', '_ormbases': ['crm.Contacto']},
            'activo': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'activo'"}),
            'bloqueado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'clasificacion': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'clasificacion'"}),
            'codigo': ('django.db.models.fields.IntegerField', [], {}),
            'contacto_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['crm.Contacto']", 'unique': 'True', 'primary_key': 'True'}),
            'exonerado': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'exonerado'"}),
            'organization': ('django.db.models.fields.CharField', [], {'default': "'N/A'", 'max_length': '35', 'null': 'True', 'db_column': "'organization'", 'blank': 'True'}),
            'ruc': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'db_column': "'ruc'", 'blank': 'True'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.TipoCliente']", 'null': 'True', 'blank': 'True'}),
            'tipocobro': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.TipoCobro']", 'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '100', 'null': 'True', 'db_column': "'url'", 'blank': 'True'}),
            'zona': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.Ruta']", 'null': 'True', 'blank': 'True'})
        },
        'crm.contacto': {
            'Meta': {'object_name': 'Contacto'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'apellido'"}),
            'categoria': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'categoria'", 'blank': 'True'}),
            'cedula': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'db_column': "'cedula'", 'blank': 'True'}),
            'estadocivil': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'estadocivil'", 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'db_column': "'foto'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'licencia': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'licencia'", 'blank': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'db_column': "'logo'", 'blank': 'True'}),
            'nacimiento': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'nacimiento'", 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'nombre'"}),
            'nombrecorto': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'nombrecorto'", 'blank': 'True'}),
            'notas': ('django.db.models.fields.TextField', [], {'null': 'True', 'db_column': "'notas'", 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'title'", 'blank': 'True'})
        },
        'crm.ruta': {
            'Meta': {'object_name': 'Ruta'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ruta': ('django.db.models.fields.TextField', [], {'max_length': '300', 'db_column': "'ruta'"}),
            'zona': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'zona'"})
        },
        'crm.tipocliente': {
            'Meta': {'object_name': 'TipoCliente'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'nombre'"})
        },
        'crm.tipocobro': {
            'Meta': {'object_name': 'TipoCobro'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meses': ('django.db.models.fields.IntegerField', [], {}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'nomina.cargo': {
            'Meta': {'object_name': 'Cargo'},
            'cargo': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'cargo'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'nomina.empleado': {
            'Meta': {'object_name': 'Empleado', '_ormbases': ['crm.Contacto']},
            'activo': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'activo'"}),
            'cargo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomina.Cargo']"}),
            'codigoempleado': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'codigo'"}),
            'comisionista': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'comisionista'"}),
            'contacto_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['crm.Contacto']", 'unique': 'True', 'primary_key': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'fecha'", 'blank': 'True'}),
            'idseguro': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'seguro'"}),
            'madre': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'db_column': "'madre'", 'blank': 'True'}),
            'numerohijos': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'hijos'", 'blank': 'True'}),
            'padre': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'db_column': "'padre'", 'blank': 'True'}),
            'sueldo': ('django.db.models.fields.DecimalField', [], {'db_column': "'sueldo'", 'decimal_places': '2', 'max_digits': '10'})
        },
        'ticket.archivo': {
            'Meta': {'object_name': 'Archivo'},
            'archivo': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'db_column': "'documento'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ticket': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ticket.Ticket']", 'null': 'True', 'blank': 'True'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'titulo'"})
        },
        'ticket.estadoticket': {
            'Meta': {'object_name': 'EstadoTicket'},
            'estado': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'estado'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'ticket.respuesta': {
            'Meta': {'object_name': 'Respuesta'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'texto': ('django.db.models.fields.TextField', [], {'db_column': "'texto'"}),
            'ticket': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ticket.Ticket']", 'null': 'True', 'blank': 'True'})
        },
        'ticket.ticket': {
            'Meta': {'object_name': 'Ticket'},
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.Cliente']", 'null': 'True', 'blank': 'True'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '500', 'db_column': "'descripcion'"}),
            'detalle': ('django.db.models.fields.TextField', [], {'null': 'True', 'db_column': "'detalle'", 'blank': 'True'}),
            'empleado': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomina.Empleado']", 'null': 'True', 'blank': 'True'}),
            'estado': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ticket.EstadoTicket']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['ticket']