# Django settings for transexpress project.
from django.conf import global_settings as DEFAULT_SETTINGS
import os

PROJECT_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)),'..')
DEBUG = True 
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    #('Your Name', 'lbgm2011@gmail.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': '', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': '',                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

TIME_ZONE = 'America/Managua'
DEFAULT_REGION = "Resto Del Mundo"
LANGUAGE_CODE = 'es-ni'

SITE_ID = 1

USE_I18N = True

USE_L10N = True

MEDIA_ROOT = os.path.join(PROJECT_DIR, 'media')

MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join(PROJECT_DIR, 'static')

STATIC_URL = '/static/'

STATICFILES_DIRS = (
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

SECRET_KEY = '&ta2-bbj8mp^!!+-)9-=r_*8evrp9v#^1q#uqtflvwf)jgl+&g'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
    os.path.join(PROJECT_DIR, 'templates'),
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.request',
    'reportes.processors.noticias',
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    
    'mptt', 
    'reportes',
	'caja',
    'contabilidad',
	'facturacion',
    'postal',
    'hojac',
    'crm',
    'ticket', 
    'nomina',
    'tastypie',
    'celery',
    'djcelery',
    'gunicorn',
    'chosen',
    'south',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'file_packages':{
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(PROJECT_DIR,'logs','packages.log'),
        },
        'file_packagesdsp':{
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(PROJECT_DIR,'logs','packagesdsp.log'),
        },
        'file_contents':{
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(PROJECT_DIR,'logs','contents.log'),
            },
        'file_cobros':{
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(PROJECT_DIR,'logs','cobros.log'),
            },
        'file_status':{
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(PROJECT_DIR,'logs','status.log'),
            }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'packagessaves': {                  # define another logger
            'handlers': ['file_packages'],  # associate a different handler
            'level': 'DEBUG',                # specify the logging level
            'propagate': True,
        },
        'packagesdspsaves': {                  # define another logger
            'handlers': ['file_packagesdsp'],  # associate a different handler
            'level': 'DEBUG',                # specify the logging level
            'propagate': True,
        },
        'contentsaves': {
            'handlers': ['file_contents'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'cobrosaves': {
            'handlers': ['file_cobros'],
            'level': 'DEBUG',
            'propagate': True,
            },
        'statussaves': {
            'handlers': ['file_status'],
            'level': 'DEBUG',
            'propagate': True,
            },
     }
}
