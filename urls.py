from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.views.generic.simple import redirect_to
from postal.api import CourierResource, CartaResource, CasilleroResource
from crm.api import ClienteResource
from caja.api import ServicioResource
from contabilidad.api import CuentaResource
from facturacion.api import RequerimientoResource, FacturaResource, NotaDebitoResource
admin.autodiscover()

urlpatterns = patterns('',  	
    url(r'^caja/', include('caja.urls', namespace='caja')),	
    url(r'^contabilidad/', include('contabilidad.urls', namespace='contabilidad')),
    url(r'^crm/', include('crm.urls', namespace='crm')),
    url(r'^facturacion/', include('facturacion.urls', namespace='facturacion')),
    url(r'^fisco/', include('fisco.urls')),
    url(r'^nomina/', include('nomina.urls', namespace='nomina')),
    url(r'^postal/', include('postal.urls', namespace='postal')),
    url(r'^hojac/', include('hojac.urls', namespace='hojac')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', redirect_to, {'url': '/admin/'} ),        
   )

urlpatterns += patterns(
    (r'^api/', include(CourierResource().urls)),
    (r'^api/', include(CartaResource().urls)),
    (r'^api/', include(CasilleroResource().urls)),
    (r'^api/', include(ClienteResource().urls)),
    (r'^api/', include(ServicioResource().urls)),
    (r'^api/', include(CuentaResource().urls)),
    (r'^api/', include(FacturaResource().urls)),
    (r'^api/', include(NotaDebitoResource().urls)),
    (r'^api/', include(RequerimientoResource().urls)),)

if settings.DEBUG:
    urlpatterns += patterns('',
    (r'^media/(?P<path>.*)$', 'django.views.static.serve',
    {'document_root': settings.MEDIA_ROOT}),)

 
