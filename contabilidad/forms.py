# -*- coding: utf-8 -*-

from django import forms
from contabilidad.models import TipoDocumento, MESES, Cuenta, MovimientoContable
from django.contrib.admin import widgets
from postal.admin import RawIdWidget
from contabilidad.models import Cuenta
from moneyfmt import moneyfmt


from django.forms import ModelForm


class ChequeForm(forms.Form):
    numero = forms.CharField(label=u'Número', required=False)
    cliente = forms.CharField(label=u'Cliente', required=False)
    fecha_inicio = forms.DateField(widget=widgets.AdminDateWidget(),label='Desde', required=False)
    fecha_fin = forms.DateField(widget=widgets.AdminDateWidget(),label='Hasta', required=False)
    resumen = forms.BooleanField(label='Resumen', required=False)


class CuentaForm(forms.Form):
    TIPO_CUENTA=(
        ('0','------'),
        ('1','Debe'),
        ('2','Haber'),
        
    )
    POSTEO=(
        ('0','------'),
        ('1','Si'),
        ('2','No'),

    )
    codigo = forms.CharField(label=u'Codigo de cuenta', required=False)
    nombre = forms.CharField(label=u'Nombre', required=False)
    tipo = forms.ChoiceField(choices=TIPO_CUENTA, label='Tipo Cuenta', required=False)
    posteo = forms.ChoiceField(choices=POSTEO, label='Permite Posteo', required=False)
    
class LibroDiarioForm(forms.Form):
    TIPO_CUENTA=(
        ('0','------'),
        ('egreso','Egreso'),
        ('diario','Diario'), 
    )

    fecha_inicio = forms.DateField(widget=widgets.AdminDateWidget(),label='Desde', required=False)
    fecha_fin = forms.DateField(widget=widgets.AdminDateWidget(),label='Hasta', required=False)
    tipo = forms.ChoiceField(choices=TIPO_CUENTA, label='Tipo Cuenta', required=False)
    


class ReporteComparativoForm(forms.Form):
    mes_inicio = forms.ChoiceField(choices=MESES, label='Mes')
    anyo_inicio = forms.IntegerField(label=u'Año')
    mes_fin = forms.ChoiceField(choices=MESES, label='Mes')
    anyo_fin = forms.IntegerField(label=u'Año')

class EstadosForm(ReporteComparativoForm):
    anexos = forms.BooleanField(label=u'Generar Anexos', required=False)
    

class ReporteMensualForm(forms.Form):
    mes_inicio = forms.ChoiceField(choices=MESES, label='Mes')
    anyo_inicio = forms.IntegerField(label=u'Año')
    
class AuxiliarCuentaForm(ReporteMensualForm):
    FILTRO = (
              ('T','Todos'),
              ('M','Morosos')
              )
    mostrar = forms.ChoiceField(choices=FILTRO, initial='M')
    
    
class MayorAuxiliarForm(forms.Form):
    fecha_inicio = forms.DateField(widget=widgets.AdminDateWidget(),label='Desde', required=True)
    fecha_fin = forms.DateField(widget=widgets.AdminDateWidget(),label='Hasta', required=True)
            
    cuenta_inicio= forms.CharField(max_length=10,
        widget=RawIdWidget(Cuenta, 'parent',"/api/cuenta/"))
    
    cuenta_fin= forms.CharField(max_length=10,
        widget=RawIdWidget(Cuenta, 'parent',"/api/cuenta/"))
     