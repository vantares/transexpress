# -*- coding: UTF-8 -*-
from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from contabilidad.models import *
from contabilidad.forms import *
from contabilidad.moneyfmt import moneyfmt
from django.forms import ValidationError
from decimal import Decimal
from django.db.models.related  import RelatedObject
from django import forms
from django.conf import settings
from postal.admin import RawIdCustomAdmin

class MovimientoContableInlineFormset(forms.models.BaseInlineFormSet):
    def clean(self):
        # get forms that actually have valid data
        count = 0
        cargo = Decimal(0)
        abono = Decimal(0)
        for form in self.forms:
            
            try:
                                    
                if form.cleaned_data:
                    count += 1
                    cargo += form.cleaned_data['cargo'] if form.cleaned_data['cargo'] is not None else Decimal(0)
                    abono += form.cleaned_data['abono'] if form.cleaned_data['abono'] is not None else Decimal(0)
            except AttributeError:
                # annoyingly, if a subform is invalid Django explicity raises
                # an AttributeError for cleaned_data
                pass
        if count < 2:
            raise forms.ValidationError('Deben ser afectadas al menos dos cuentas contables')
        else:
            cargo = Decimal(str(round(cargo,2)))
            abono = Decimal(str(round(abono,2)))
            if cargo!= abono or (cargo ==0 and abono == 0):
                raise ValidationError("Los cargos y abonos deben estar balanceados.\n Cargo=%s \n Abono=%s \n Diferencia=%s"%(moneyfmt(cargo,2),moneyfmt(abono,2),moneyfmt(cargo-abono,2)))
            


class MovimientoContableInline( RawIdCustomAdmin, admin.TabularInline ):
    model = MovimientoContable
    formset =  MovimientoContableInlineFormset
    search_fields = ['cuenta']
    list_filter = ['cuenta']
    fields = ['cuenta','cargo','abono']
    raw_id_fields = ('cuenta',)
    extra = 2
    
    def get_form(self, request, obj=None, **kwargs):
        form = super(MovimientoContableInline, self).get_form(request, obj, **kwargs)
        form.base_fields['cuenta'].queryset=Cuenta.objects.filter(permite_posteo=True)
        return form

class AsientoAdmin( admin.ModelAdmin ):
    model = Asiento
    exclude = ["tipo_partida","actualizado"]
    list_display = ['fecha','comprobante',"tipo_partida",'concepto']
    inlines = [MovimientoContableInline]
    search_fields = ['comprobante']
    readonly_fields = ['cargo_','abono_']      


class SaldoInline( admin.TabularInline ):
    model = Saldo
    readonly_fields = ['saldo_anterior','cargo','abono']
    list_display = ['anyo','mes','cuenta']
    extra = 1

class SaldoAdmin( RawIdCustomAdmin, admin.ModelAdmin ):
    model = Saldo
    readonly_fields = ['saldo_anterior','cargo','abono']
    list_display = ['cuenta','anyo','mes','saldo_cuenta']
    raw_id_fields = ('cuenta',)
    extra = 1

class TipoDocumentoAdmin( admin.ModelAdmin ):
    model = TipoDocumento
    extra = 1


class CuentaInLine( admin.TabularInline ):
    model = Cuenta
    extra = 1
    
class CuentaAdmin( RawIdCustomAdmin, MPTTModelAdmin ):
    model = Cuenta
    raw_id_fields = ('parent',)
    inlines = [SaldoInline]
    list_display = [ 'codigo_cuenta', 'nombre','permite_posteo','parent' ]
    search_fields = ['codigo_cuenta', 'nombre']

class ConceptoAdmin( admin.ModelAdmin ):
    model = Concepto
    extra = 1

class ChequeAdmin( admin.ModelAdmin ):
    model = Cheque
    fields = ["comprobante","fecha","banco","a_nombre_de","monto","concepto"]
    list_display = ['banco', 'comprobante','fecha','a_nombre_de','monto','concepto']
    inlines = [MovimientoContableInline]
    search_fields = ['a_nombre_de']    
    date_hierarchy = 'fecha'

    
    def get_readonly_fields(self, request, obj=None):
        if obj is not None:
            field_names = obj._meta.get_all_field_names()[:]
            readonly_fields = list(super(ChequeAdmin, self).get_readonly_fields(request, obj))
            for name in field_names:
                if name != 'id'  and not isinstance(obj._meta.get_field_by_name(name)[0], RelatedObject):
                    readonly_fields.append(name)

            return readonly_fields

        return ()

class BancoAdmin( RawIdCustomAdmin, admin.ModelAdmin ):
    raw_id_fields = ('cuenta',)


class PeriodoAdmin( admin.ModelAdmin ):
    model = Periodo
    list_display = ['inicio', 'fin','contador','gerente','activo']



admin.site.register(Asiento, AsientoAdmin)
admin.site.register(Periodo,PeriodoAdmin)
admin.site.register(Cheque, ChequeAdmin)
admin.site.register(Cuenta,CuentaAdmin)
admin.site.register(Saldo, SaldoAdmin)
admin.site.register(Banco, BancoAdmin)
admin.site.register(Concepto, ConceptoAdmin)

