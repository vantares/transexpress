from django.conf.urls.defaults import patterns, url
from contabilidad.views import *

urlpatterns = patterns('contabilidad.views',
    url(r'^$', 'index'),
    url('^cuentas/$', CuentaView.as_view(), name='cuentas'),
    url('^balancecomparativo/$', BalanceComparativoView.as_view(), name='balancecomparativo'),
    url('^estadoresultado/$', EstadoResultadoView.as_view(), name='estadoresultado'),
    url('^balancecomprobacion/$', BalanceComprobacionView.as_view(), name='balancecomprobacion'),
    url('^situacionfinanciera/$', SituacionFinancieraView.as_view(), name='situacionfinanciera'),
    url('^auxiliarcuentas/$', AuxiliarCuentaView.as_view(), name='auxiliarcuentas'),
    url('^estadocomparativo/$', BalanceComprobacionView.as_view(), name='estadocomparativo'),
    url('^librodiario/$', LibroDiarioView.as_view(), name='librodiario'),
    url('^mayorauxiliar/$', MayorAuxiliarView.as_view(), name='mayorauxiliar'),
    url('^mayorgeneral/$', MayorGeneralView.as_view(), name='mayorgeneral'),
    url('^cheque/$', ChequeView.as_view(), name='cheque'),
    url(r'^imprimircheque/$', ChequeImprimirView.as_view(), name='imprimircheque'),
    url(r'^imprimirasiento/$', ImprimirAsientoView.as_view(), name='imprimirasiento'),
    url('^obtener_nombre/$', 'obtener_nombre', name='obtener_nombre'),
    
    
    
)
