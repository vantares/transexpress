# -*- coding: utf-8 -*-
from geraldo import Report, ReportBand, DetailBand, SystemField, Label, ObjectValue, FIELD_ACTION_COUNT, FIELD_ACTION_SUM, BAND_WIDTH, Line, ReportGroup, SubReport
from geraldo.utils import cm, BAND_WIDTH, TA_CENTER, TA_RIGHT, TA_LEFT
from reportlab.lib.enums import TA_JUSTIFY
from geraldo.graphics import Line
from reportlab.lib.colors import navy, red
from moneyfmt import moneyfmt
from decimal import Decimal
from reportlab.lib.pagesizes import LETTER,landscape
from settings.settings import PROJECT_DIR
import os

CODIGO = 1.5*cm
NOMBRE = 4*cm
POSTEO = 14.5*cm
TIPO = 16*cm
letter_width = landscape(LETTER)[0] - 0.5*cm



class PercentObjectValue(ObjectValue):                       
    def _text(self):      
        if 'mes1' in self.expression:
            population =  self.generator.variables['total1']
        elif 'mes2' in self.expression:
            population =  self.generator.variables['total2']
        else:
            return '0.00%'
            
          
        if population == 0:
            return '0.00%'
        fraction = super(PercentObjectValue,self)._text()
        try:
            return "%.2f%%" % ((float(fraction) / float(population)) * 100)
        except ValueError:
            return '0.00%'        
        

class DecimalObjectValue(ObjectValue):
    def _text(self):
        text = super(DecimalObjectValue,self)._text()
        try:
            return  moneyfmt(text,2,'')
        except Exception as ins:            
            try:
                return  moneyfmt(Decimal(text),2,'')
            except Exception as ins2:
                return  ''
        
class ChequeReport(Report):
    additional_fonts = { 'Calibri': os.path.join(PROJECT_DIR,'static/fonts/Calibri.ttf'),}

    default_style = {'fontName':'Calibri', 'fontSize': 10}
    title = ''   
    
    class band_detail(DetailBand):
        height = 10*cm

        elements = [
#            Label(text="TRANS-EXPRESS,S.A ", top=0, left=8*cm, style={'fontName': 'Courier-Bold'}),
#            Label(text="RUC No. J0310000012008", top=0.5*cm, left=7.5*cm),               
            
            ObjectValue(expression='a_nombre_de', top=1.5*cm, left=1*cm,width=14*cm),
            ObjectValue(expression='monto_to_words', top=2.3*cm, left=1*cm,width=15*cm),

            ObjectValue(expression='fecha_formato', top=0.5*cm, left=14*cm),            
            
            DecimalObjectValue(expression='monto', top=1.5*cm, left=16.5*cm ),
            ObjectValue(expression='concepto', top=7.5*cm, left=1*cm,width=18*cm, style={'fontSize': 10, 'alignment': TA_JUSTIFY} ),
                            
            
        ]

    class band_page_footer(ReportBand):
        pass
      
    subreports = [
        SubReport(
                  queryset_string = '%(object)s.movimientos.all()',
                  band_detail = ReportBand(
                        height=0.5*cm,
                        elements=[
                            ObjectValue(attribute_name='cuenta.codigo_cuenta', top=0.5*cm, left=1*cm),
                            ObjectValue(attribute_name='cuenta.nombre', top=0.5*cm, left=4*cm, width= 9*cm),
                            
                            ObjectValue(attribute_name='cargo', top=0.5*cm, left=14*cm , width=2*cm, style = {'alignment': TA_RIGHT} ,get_value=lambda inst: moneyfmt(inst.cargo,2,'') if inst.cargo is not None else ''),
                            ObjectValue(attribute_name='abono', top=0.5*cm, left=17*cm ,  width=2*cm,  style = {'alignment': TA_RIGHT} , get_value=lambda inst: moneyfmt(inst.abono,2,'') if inst.abono is not None else ''),
                        ],
#                        borders = {'left':True,'right':True}
                    ),
                    band_footer = ReportBand(
                        height=2*cm,
                        elements=[
                                  Label(text="Totales", top=3.5*cm, left=11*cm),
                                  Label(text="-------------", top=3.2*cm, left=14*cm, width=2*cm , style = {'alignment': TA_RIGHT} ),
                                  Label(text="-------------", top=3.2*cm, left=17*cm, width=2*cm , style = {'alignment': TA_RIGHT} ),
                                  DecimalObjectValue(expression='sum(cargo)', top=3.5*cm,left=14*cm, width=2*cm, style = {'alignment': TA_RIGHT} ),
                                  DecimalObjectValue(expression='sum(abono)',top=3.5*cm, left=17*cm, width=2*cm, style = {'alignment': TA_RIGHT} ),
                            ],
#                        borders = {'bottom':True,'left':True,'right':True}                        
                    ),                             
                  
        ),
                  
    ]


class AsientoReport(Report):
    default_style = {'fontName':'Courier', 'fontSize': 10}
    title = ''
    default_style = {'fontName':'Courier'}
    fuente = {'fontName': 'Courier', 'fontSize': 10}   

    class band_page_header(ReportBand):
        height = 1.3*cm
        elements = [
            Label(text="TRANS-EXPRESS,S.A ", top=0.1*cm, left=0, width=BAND_WIDTH,
                style={'fontName': 'Courier-Bold', 'fontSize': 14, 'alignment': TA_CENTER}),
            Label(text="RUC No. J0310000012008", top=0.6*cm, left=0*cm, width=BAND_WIDTH,
                style={'fontName': 'Courier', 'fontSize': 10, 'alignment': TA_CENTER}),
            SystemField(expression=u'Página %(page_number)d de %(page_count)d', top=0.1*cm,
                width=BAND_WIDTH, style={'alignment': TA_RIGHT}),
                    
        ]
    
    class band_detail(DetailBand):
        height = 3*cm

        elements = [
            Label(text="Asiento No.", top=0.5*cm, left=15*cm),
            ObjectValue(expression='comprobante', top=0.5*cm, left=18*cm,width=11*cm),
            Label(text="Fecha", top=1*cm, left=16*cm),                                                      
            ObjectValue(expression='fecha_formato', top=1*cm, left=18*cm),
            Label(text="Concepto", top=2*cm, left=0, style={'fontName': 'Courier-Bold'}),               
            ObjectValue(expression='concepto', top=2.5*cm, left=0*cm,width=18*cm, style={'fontName': 'Courier', 'fontSize': 10, 'alignment': TA_JUSTIFY} ),                            
            
        ]

    class band_page_footer(ReportBand):
        pass
      
    subreports = [
        SubReport(
                  queryset_string = '%(object)s.movimientos.all()',
                  band_detail = ReportBand(
                        height=0.5*cm,
                        elements=[
                            ObjectValue(attribute_name='cuenta.codigo_cuenta', top=0.5*cm, left=1*cm),
                            ObjectValue(attribute_name='cuenta.nombre', top=0.5*cm, left=4*cm, width= 9*cm),
                            
                            ObjectValue(attribute_name='cargo', top=0.5*cm, left=13.5*cm , width=2*cm, style={'alignment': TA_RIGHT},get_value=lambda inst: moneyfmt(inst.cargo,2,'') if inst.cargo is not None else ''),
                            ObjectValue(attribute_name='abono', top=0.5*cm, left=17*cm, width=2*cm, style={'alignment': TA_RIGHT},get_value=lambda inst: moneyfmt(inst.abono,2,'') if inst.abono is not None else ''),
                        ],
#                        borders = {'left':True,'right':True}
                    ),
                    band_footer = ReportBand(
                        height=2*cm,
                        default_style = {'fontName': 'Courier-Bold','alignment': TA_CENTER},
                        elements=[
                                  Label(text="Totales", top=3.5*cm, left=8*cm),
                                  Label(text="----------", top=3.2*cm, left=13.5*cm, width=2*cm),
                                  Label(text="----------", top=3.2*cm, left=17*cm, width=2*cm),                                  
                                  DecimalObjectValue(expression='sum(cargo)', top=3.5*cm,left=13.5*cm, width=2*cm, style={'alignment': TA_RIGHT}),
                                  DecimalObjectValue(expression='sum(abono)',top=3.5*cm, left=17*cm, width=2*cm, style={'alignment': TA_RIGHT}),
                            ],
#                        borders = {'bottom':True,'left':True,'right':True}                        
                    ),
                  
                    band_header = ReportBand(
                        height=1*cm,
                        default_style = {'fontName': 'Courier-Bold'},
                        elements=[
                                  Label(text="Codigo", top=1*cm, left=1*cm),
                                  Label(text=u"Descripción", top=1*cm, left=4*cm),
                                  Label(text="Cargo", top=1*cm, left=13.5*cm, width=2*cm, style={'alignment': TA_CENTER}),
                                  Label(text="Abono", top=1*cm, left=17*cm, width=2*cm, style={'alignment': TA_CENTER})
                            ],
#                        borders = {'bottom':True,'left':True,'right':True}                        
                    ),                                               
                  
        ),
                  
    ]

class CuentaReport(Report):   
    default_style = {'fontName':'Courier'}
    class band_detail(DetailBand):
        height = 0.7*cm
        elements = [
            ObjectValue(expression='codigo_cuenta', left=CODIGO, width = 3*cm),
            ObjectValue(expression='nombre', left=NOMBRE, width = 10*cm),
            ObjectValue(expression='permite_posteo', left=POSTEO),
            ObjectValue(expression='tipo_cuenta', left=TIPO),
        ]
#        borders = {'bottom': True, 'left': True, 'right': True}

    class band_page_header(ReportBand):
        height = 1.3*cm
        elements = [
            SystemField(expression='%(report_title)s', top=0.1*cm, left=0, width=BAND_WIDTH,
                style={'fontName': 'Courier', 'fontSize': 14, 'alignment': TA_CENTER}),
            SystemField(expression=u'Página %(page_number)d de %(page_count)d', top=0.1*cm,
                width=BAND_WIDTH, style={'alignment': TA_RIGHT}),
            Label(text="Codigo", top=0.8*cm, left=CODIGO, style={'fontName': 'Courier'}),
            Label(text="Nombre", top=0.8*cm, left=NOMBRE, style={'fontName': 'Courier'}),
            Label(text="Posteo", top=0.8*cm, left=POSTEO, style={'fontName': 'Courier'}),
            Label(text="Tipo", top=0.8*cm, left=TIPO, style={'fontName': 'Courier'}),
        ]
#        borders = {'all': True}

    class band_summary(ReportBand):
        height = 0.5*cm
        elements = [
            Label(text='Totales:'),
            ObjectValue(expression='count(codigo_cuenta)', left=CODIGO, style={'fontName': 'Courier'}),
            ]
        borders = {'top': True}


WIDTH = [0*cm,2*cm, 10*cm, 13*cm,16*cm]
class BalanceComparativoReport(Report):
    title = 'Cuentas Contables'
    default_style = {'fontName':'Courier', 'fontSize': 8}

    class band_detail(DetailBand):
        height = 0.7*cm
        default_style = {'alignment':TA_RIGHT}
        auto_expand_height = True
        elements = [
            ObjectValue(expression='codigo_cuenta', left=WIDTH[0], style = {'alignment':TA_LEFT}),
            ObjectValue(expression='nombre', left=WIDTH[1], width = 10*cm, style = {'alignment':TA_LEFT}),
            DecimalObjectValue(expression='mes1', left=WIDTH[2] , width=2*cm),
            DecimalObjectValue(expression='mes2', left=WIDTH[3], width=2*cm),
            DecimalObjectValue(expression='diferencia', left=WIDTH[4], width=2*cm),

        ]

    class band_page_header(ReportBand):
        height = 3*cm
        elements = [
            Label(text="TRANS-EXPRESS,S.A ", top=0.1*cm, left=0, width=BAND_WIDTH, style= {'alignment': TA_CENTER}),            
            SystemField(expression='%(report_title)s' , top=0.5*cm, left=0, width=BAND_WIDTH, style= {'alignment': TA_CENTER}),
            SystemField(expression='%(var:periodo)s' , top=1*cm, left=0, width=BAND_WIDTH, style= {'alignment': TA_CENTER}),                       
            SystemField(expression=u'Página %(page_number)d de %(page_count)d', top=0.1*cm, width=BAND_WIDTH, style={'alignment': TA_RIGHT}), 
            Line(left=0*cm, top=2.8*cm, right=letter_width , bottom=2.8*cm),
            Label(text="Codigo", top=2*cm, left=WIDTH[0] , style = {'alignment':TA_LEFT}),
            Label(text="Nombre", top=2*cm, left=WIDTH[1] , style = {'alignment':TA_LEFT}),
            SystemField(expression='%(var:mes1)s %(var:anho1)s', top=2*cm, left=WIDTH[2] , width=2*cm, style = {'alignment':TA_CENTER}),
            SystemField(expression='%(var:mes2)s %(var:anho2)s', top=2*cm, left=WIDTH[3] , width=2*cm, style = {'alignment':TA_CENTER}),
            Label(text="Inc./ Dec.", top=2*cm, left=WIDTH[4] , width=2*cm, style = {'alignment':TA_CENTER} ),
        ]

    class band_summary(ReportBand):
        height = 0.5*cm
        default_style = {'alignment':TA_RIGHT}
        elements = [
            Label(text='Totales:',left=WIDTH[0]),
            ObjectValue(expression='count(nombre)', left=WIDTH[1]+2*cm , width=2*cm ),
            DecimalObjectValue(expression='sum(mes1)', left=WIDTH[2] , width=2*cm),
            DecimalObjectValue(expression='sum(mes2)', left=WIDTH[3] , width=2*cm),
            DecimalObjectValue(expression='sum(diferencia)', left=WIDTH[4] , width=2*cm ),
            Label(text='Elaborado Por:',left=2*cm,top=2.5*cm ),
            Label(text='Autorizado Por:',left=11*cm,top=2.5*cm ),
            ]
        borders = {'top': True}


COLS = [0*cm,2*cm, 8*cm, 10*cm,12*cm,14*cm,16*cm,18*cm]
class AnexoBalanceReport(Report):
    title = 'Cuentas Contables'
    default_style = {'fontName':'Courier', 'fontSize': 8}
    page_size = LETTER            
    groups = [             
        ReportGroup(attribute_name='clasificacion',

            band_header = ReportBand(
                height=0.5*cm,
                elements=[
                    
                    ObjectValue(expression='clasificacion', left=COLS[0],  style={'fontName': 'Courier-Bold'}),      
                    
                    ],

            ),            
            band_footer = ReportBand(
                height=1*cm,
                elements=[
                    Label(text="TOTAL ", left=WIDTH[0] ,style={'fontName': 'Courier-Bold'}),
                    ObjectValue(expression='clasificacion', left=COLS[1],  style={'fontName': 'Courier-Bold'}),      
                    DecimalObjectValue(expression='sum(mes1)', left=COLS[4], width = 2*cm,style={'fontName': 'Courier-Bold','alignment': TA_RIGHT},get_value=lambda inst: inst['mes1'] if inst['level'] ==1 else 0),
                    DecimalObjectValue(expression='sum(mes2)', left=COLS[7],width = 2*cm, style={'fontName': 'Courier-Bold' ,'alignment': TA_RIGHT},get_value=lambda inst: inst['mes2'] if inst['level'] ==1 else 0),
                    ],

            ),
        ),]

    class band_detail(DetailBand):
        height = 0.5*cm
        auto_expand_height = True
        default_style = {'fontName':'Courier', 'fontSize': 8,'alignment': TA_RIGHT}
        elements = [
            ObjectValue(expression='codigo_cuenta', left=COLS[0] , width = 1.5*cm, style={'alignment': TA_LEFT}),
            ObjectValue(expression='nombre', style={'alignment': TA_LEFT}, left=COLS[1], width = 6*cm),
            DecimalObjectValue(attribute_name='mes1', top=0, left=COLS[2], width = 2*cm,get_value=lambda inst: inst['mes1'] if inst['level'] ==3 else ''),
            DecimalObjectValue(attribute_name='mes1', top=0, left=COLS[3],width = 2*cm,get_value=lambda inst: inst['mes1'] if inst['level'] ==2 else ''),
            DecimalObjectValue(attribute_name='mes1', top=0, left=COLS[4],width = 2*cm,get_value=lambda inst: inst['mes1'] if inst['level'] ==1 else ''),
            DecimalObjectValue(attribute_name='mes2', top=0, left=COLS[5],width = 2*cm,get_value=lambda inst: inst['mes2'] if inst['level'] ==3 else ''),
            DecimalObjectValue(attribute_name='mes2', top=0, left=COLS[6],width = 2*cm,get_value=lambda inst: inst['mes2'] if inst['level'] ==2 else ''),
            DecimalObjectValue(attribute_name='mes2', top=0, left=COLS[7],width = 2*cm,get_value=lambda inst: inst['mes2'] if inst['level'] ==1 else ''),

            ]
#        borders = {'bottom': True, 'left': True, 'right': True}

    class band_page_header(ReportBand):
        height = 1.5*cm
        default_style = {'alignment': TA_CENTER}        
        elements = [
            SystemField(expression='%(report_title)s', top=0.1*cm, left=0, width=BAND_WIDTH,
                style={'fontName': 'Courier', 'fontSize': 14, 'alignment': TA_CENTER}),
            SystemField(expression=u'Página %(page_number)d de %(page_count)d', top=0.1*cm,
                width=BAND_WIDTH, style={'alignment': TA_RIGHT}),
            Label(text="Codigo", top=1*cm, left=COLS[0], width = 2*cm, style={'fontName': 'Courier','alignment': TA_LEFT}),
            Label(text="Nombre", top=1*cm, left=COLS[1],width = 6*cm, style={'fontName': 'Courier','alignment': TA_LEFT}),
            SystemField(expression='%(var:mes1)s %(var:anho1)s', top=0.7*cm, left=COLS[2], width = 6*cm, style={'fontName': 'Courier','alignment': TA_LEFT}),
            Label(text="Nivel III", top=1*cm, left=COLS[2],width = 2*cm, style={'fontName': 'Courier'}),
            Label(text="Nivel II", top=1*cm, left=COLS[3],width = 2*cm, style={'fontName': 'Courier'}),
            Label(text="Nivel I", top=1*cm, left=COLS[4],width = 2*cm, style={'fontName': 'Courier'}),
            SystemField(expression='%(var:mes2)s %(var:anho2)s', top=0.7*cm, left=COLS[5], width = 6*cm, style={'fontName': 'Courier','alignment': TA_LEFT}),
            Label(text="Nivel III", top=1*cm, left=COLS[5],width = 2*cm, style={'fontName': 'Courier'}),
            Label(text="Nivel II", top=1*cm, left=COLS[6], width = 2*cm,style={'fontName': 'Courier'}),
            Label(text="Nivel I", top=1*cm, left=COLS[7],width = 2*cm, style={'fontName': 'Courier'}),
            ]
    

#    class band_summary(ReportBand):
#        height = 0.5*cm
#        elements = [
#            Label(text='Totales:'),
#            ObjectValue(expression='count(nombre)', left=WIDTH[1], style={'fontName': 'Courier'}),
#            DecimalObjectValue(expression='sum(mes1)', left=WIDTH[4], style={'fontName': 'Courier'}),
#            DecimalObjectValue(expression='sum(mes2)', left=WIDTH[7], style={'fontName': 'Courier'}),
#
#
#            ]
#        borders = {'top': True}

WIDTH = [0.5*cm,9*cm, 12*cm, 15*cm,18*cm,21*cm, 24*cm]
class SituacionFinancieraReport(Report):
    title = 'Estado de situacion Financiera'
    default_style = {'fontName':'Courier'}
    page_size = landscape(LETTER)
    
    groups = [             
        ReportGroup(attribute_name='clasificacion',

            band_header = ReportBand(
                height=0.5*cm,
                elements=[
                    
                    ObjectValue(expression='clasificacion', left=WIDTH[0],  style={'fontName': 'Courier-Bold'}),      
                    
                    ],

            ),            
            band_footer = ReportBand(
                height=1*cm,
                elements=[
                    Label(text="TOTAL ", left=WIDTH[0] ,style={'fontName': 'Courier-Bold'}),
                    ObjectValue(expression='clasificacion', left=WIDTH[0]+ 2*cm,  style={'fontName': 'Courier-Bold'}),      
                    DecimalObjectValue(expression='sum(mes1)', left=WIDTH[2], width=2.5*cm, style={'fontName': 'Courier-Bold','alignment':TA_RIGHT}),
                    DecimalObjectValue(expression='sum(mes2)', left=WIDTH[5], width=2.5*cm, style={'fontName': 'Courier-Bold','alignment':TA_RIGHT}),
                PercentObjectValue(expression='sum(mes1)', left=WIDTH[3]),
                PercentObjectValue(expression='sum(mes2)', left=WIDTH[6]),
                    ],

            ),
        ),
        ReportGroup(attribute_name='tipo',
            band_header = ReportBand(
                            height=0.5*cm,
                            elements=[
                                
                                ObjectValue(expression='tipo', left=WIDTH[0],  style={'fontName': 'Courier-Bold'}),      
                                
                                ],

            ),                        
            band_footer = ReportBand(
                height=1*cm,
                elements=[
                    Label(text="TOTAL ", left=WIDTH[0] , style={'fontName': 'Courier-Bold'}),
                    ObjectValue(expression='tipo', left=WIDTH[0]+2*cm,  style={'fontName': 'Courier-Bold'}),      
                    DecimalObjectValue(expression='sum(mes1)', left=WIDTH[2],  width=2.5*cm, style={'fontName': 'Courier-Bold','alignment':TA_RIGHT}),
                    DecimalObjectValue(expression='sum(mes2)', left=WIDTH[5],  width=2.5*cm, style={'fontName': 'Courier-Bold','alignment':TA_RIGHT}),
                PercentObjectValue(expression='sum(mes1)', left=WIDTH[3]),
                PercentObjectValue(expression='sum(mes2)', left=WIDTH[6]),                                        
                    ],
            ),

        ), 
        ReportGroup(attribute_name='categoria',
            
            band_header = ReportBand(
                height=0.5*cm,
                elements=[

                    ObjectValue(expression='categoria', left=WIDTH[0], width = 8*cm, style={'fontName': 'Courier-Bold'}),      
                    DecimalObjectValue(expression='sum(mes1)', left=WIDTH[2],  width=2.5*cm, style={'fontName': 'Courier-Bold','alignment':TA_RIGHT}),
                    DecimalObjectValue(expression='sum(mes2)', left=WIDTH[5],  width=2.5*cm, style={'fontName': 'Courier-Bold','alignment':TA_RIGHT}),
                PercentObjectValue(expression='sum(mes1)', left=WIDTH[3]),
                PercentObjectValue(expression='sum(mes2)', left=WIDTH[6]),
                    ],

            ),
            band_footer = ReportBand(
                height=0.5*cm)
        ),                             
    ]
    
    class band_detail(DetailBand):
        height = 0.5*cm
        elements = [
            ObjectValue(expression='concepto', left=WIDTH[0],width = 8*cm),
            DecimalObjectValue(expression='mes1', left=WIDTH[1],  width=2.5*cm, style={'alignment':TA_RIGHT}),

            DecimalObjectValue(expression='mes2', left=WIDTH[4] ,  width=2.5*cm, style={'alignment':TA_RIGHT}),
            
            
            

        ]
        
    class band_page_header(ReportBand):
        height = 2.8*cm
        letter_width = landscape(LETTER)[0] - 2*cm
        elements = [
            Label(text="TRANS-EXPRESS,S.A ", top=0.1*cm, left=0, width=BAND_WIDTH,
                        style= {'alignment': TA_CENTER}),            
            SystemField(expression='%(report_title)s', top=0.6*cm, left=0, width=BAND_WIDTH,
                        style= {'alignment': TA_CENTER}),
            SystemField(expression='Al %(var:fin_mes2)s %(var:mes2)s %(var:anho2)s (Cordobas)', 
                        top=1.1*cm, left=0,width=BAND_WIDTH, style={'fontName': 'Courier','alignment': TA_CENTER}),
            
            Line(left=0*cm, top=1.8*cm, right=letter_width , bottom=1.8*cm),
                
            SystemField(expression=u'Página %(page_number)d de %(page_count)d', top=0.1*cm,
                width=BAND_WIDTH, style={'alignment': TA_RIGHT}),
            Label(text="Concepto", top=1.8*cm, left=WIDTH[0], style={'fontName': 'Courier-Bold'}),            
            SystemField(expression='%(var:mes1)s %(var:anho1)s', top=1.8*cm, left=WIDTH[1], style={'fontName': 'Courier-Bold'}),
            Label(text="Parcial", top=2.3*cm, left=WIDTH[1], style={'fontName': 'Courier-Bold'}),
            Label(text="Rubro", top=2.3*cm, left=WIDTH[2], style={'fontName': 'Courier-Bold'}),        
            Label(text="%", top=2.3*cm, left=WIDTH[3], style={'fontName': 'Courier-Bold'}),
            SystemField(expression='%(var:mes2)s %(var:anho2)s', top=1.8*cm, left=WIDTH[4], style={'fontName': 'Courier-Bold'}),
            Label(text="Parcial", top=2.3*cm, left=WIDTH[4], style={'fontName': 'Courier-Bold'}),
            Label(text="Rubro", top=2.3*cm, left=WIDTH[5], style={'fontName': 'Courier-Bold'}),
            Label(text="%", top=2.3*cm, left=WIDTH[6], style={'fontName': 'Courier-Bold'}),
        ]
                

    class band_summary(ReportBand):
        height = 3*cm
        letter_width = landscape(LETTER)[0] - 0.5*cm
        
        
        elements = [
                    Label(text='TOTAL PASIVO + CAPITAL', top =0, left = WIDTH[0],style={'fontName': 'Courier-Bold', 'alignment': TA_CENTER}),
                    SystemField(expression='%(var:total1_)s' ,left = WIDTH[2], width=2.5*cm, style={'fontName': 'Courier-Bold','alignment':TA_RIGHT}),
                    SystemField(expression='%(var:total2_)s' ,left = WIDTH[5], width=2.5*cm, style={'fontName': 'Courier-Bold','alignment':TA_RIGHT}),                    
                Label(text='100.00%', left=WIDTH[3]),
                Label(text='100.00%', left=WIDTH[6]),
                    
                    Label(text='CONTADOR GENERAL',width=letter_width/4,top=2.5*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),
                    SystemField(expression='%(var:contador)s' ,width=letter_width/4,top=3*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),
                    
                    Label(text='ASESOR FINANCIERO',left=letter_width/3,width=letter_width/3,top=2.5*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),
                    
                    
                    
                    Label(text='GERENTE GENERAL',left=letter_width*2/3,width=letter_width/4,top=2.5*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),
                    SystemField(expression='%(var:gerente)s' ,left=letter_width*2/3, width = letter_width/4,top=3*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),
                        
#            Label(text='ARMANDO GONZALEZ CORRALES',left=1.5*cm,top=3*cm, width= 10*cm,style={'fontName': 'Courier'}),        
#            Label(text='MARIO ROCHA MALTEZ',left=15.5*cm,top=3*cm,style={'fontName': 'Courier'}),
#            
#            
#            Label(text='Elaborado Por:',width=letter_width/2,top=1.5*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),
#            SystemField(expression='%(var:contador)s' ,width=letter_width/2,top=2*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),
#            Label(text='CONTADOR',width=letter_width/2, top=2.5*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),
#            Label(text='Autorizado Por:',left=letter_width/2, width = letter_width/2,top=1.5*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),
#            SystemField(expression='%(var:gerente)s' ,left=letter_width/2, width = letter_width/2,top=2*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),
#            Label(text='GERENTE GENERAL',left=letter_width/2, width = letter_width/2,top=2.5*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),            
        ]        






WIDTH = [0.5*cm,6.5*cm, 9*cm, 11*cm,13.5*cm,15.5*cm, 18*cm]
class EstadoResultadoReport(Report):
    title = 'Estado de Resultado'
    default_style = {'fontName':'Courier', 'fontSize': 8,'alignment': TA_RIGHT }    

    class band_detail(DetailBand):
        height = 0.7*cm
        elements = [
            ObjectValue(expression='concepto', left=WIDTH[0],width = 6.5*cm,style= {'alignment': TA_LEFT }),
            ObjectValue(expression='mes1', left=WIDTH[1], width = 2*cm),
            ObjectValue(expression='porcentaje_mes1', left=WIDTH[2], width = 1*cm),
            ObjectValue(expression='mes2', left=WIDTH[3], width = 2*cm),
            ObjectValue(expression='porcentaje_mes2', left=WIDTH[4], width = 1*cm),
            ObjectValue(expression='acumulado', left=WIDTH[5], width = 2*cm),
            ObjectValue(expression='porcentaje_acumulado', left=WIDTH[6], width = 1*cm),

        ]
        

    class band_page_header(ReportBand):
        height = 3*cm
        letter_width = LETTER[0] - 0.5*cm
        elements = [
            Label(text="TRANS-EXPRESS,S.A ", top=0.1*cm, left=0, width=BAND_WIDTH,
                        style= {'alignment': TA_CENTER}),            
            SystemField(expression='%(report_title)s', top=0.6*cm, left=0, width=BAND_WIDTH,
                        style= {'alignment': TA_CENTER}),
            SystemField(expression='%(var:periodo)s',
                         
                        top=1.1*cm, left=0,width=BAND_WIDTH, style={'fontName': 'Courier','alignment': TA_CENTER}),
            
            
                
            SystemField(expression=u'Página %(page_number)d de %(page_count)d', top=0.1*cm,
                width=BAND_WIDTH, style={'alignment': TA_RIGHT}),
            Label(text="Concepto", top=1.8*cm, left=WIDTH[0], style= {'alignment': TA_LEFT }),            
            SystemField(expression='%(var:mes1)s', top=1.8*cm, left=WIDTH[1], width=2*cm, style={'alignment': TA_CENTER}),
            SystemField(expression='%(var:anho1)s', top=2.3*cm, left=WIDTH[1], width=2*cm, style={'alignment': TA_CENTER}),
            Label(text="%", top=1.8*cm, left=WIDTH[2], width=2*cm, style={'alignment': TA_CENTER}),
            SystemField(expression='%(var:mes2)s', top=1.8*cm, left=WIDTH[3],  width=2*cm,style={'alignment': TA_CENTER}),
            SystemField(expression='%(var:anho2)s', top=2.3*cm, left=WIDTH[3], width=2*cm,style={'alignment': TA_CENTER}),
            Label(text="%", top=1.8*cm, left=WIDTH[4],  width=2*cm,style={'alignment': TA_CENTER}),
            SystemField(expression='Acumulado', top=1.8*cm, width=2*cm, left=WIDTH[5], style={'alignment': TA_CENTER}),
            Label(text="%", top=1.8*cm, left=WIDTH[6],  width=2*cm,style={'alignment': TA_CENTER}),
        ]
        borders = {'bottom': True}
        
        

        

    class band_summary(ReportBand):
        borders = {'top': True}
        letter_width = LETTER[0] - 0.5*cm
        elements = [
                    
                    Label(text='CONTADOR GENERAL',width=letter_width/4,top=2.5*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),
                    SystemField(expression='%(var:contador)s' ,width=letter_width/4,top=3*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),
                    
                    Label(text='ASESOR FINANCIERO',left=letter_width/3,width=letter_width/4,top=2.5*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),
                    
                    
                    
                    Label(text='GERENTE GENERAL',left=letter_width*2/3,width=letter_width/4,top=2.5*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),
                    SystemField(expression='%(var:gerente)s' ,left=letter_width*2/3, width = letter_width/4,top=3*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),
                                         
#                    Label(text='CONTADOR GENERAL',left=2*cm,top=2.5*cm,style={'fontName': 'Courier'}),
#                    Label(text='ASESOR FINANCIERO',left=9*cm,top=2.5*cm,style={'fontName': 'Courier'}),
#            Label(text='GERENTE GENERAL',left=16*cm,top=2.5*cm,style={'fontName': 'Courier'}),
#            Label(text='ARMANDO GONZALEZ CORRALES',left=1.5*cm,top=3*cm, width= 10*cm,style={'fontName': 'Courier'}),        
#            Label(text='MARIO ROCHA MALTEZ',left=15.5*cm,top=3*cm,style={'fontName': 'Courier'}),
        ]


        
COLUMNAS = [0*cm,2*cm,11*cm,14*cm,17*cm]
class GastosReport(Report):
    page_size = LETTER
    default_style = {'fontName':'Courier', 'fontSize': 8,'alignment': TA_RIGHT}
        
    class band_detail(DetailBand):
        height = 0.5*cm
        auto_expand_height = True
        
        elements = [
            ObjectValue(expression='codigo', left=COLUMNAS[0], style={'alignment': TA_LEFT}, width=2*cm),
            ObjectValue(expression='nombre', left=COLUMNAS[1], width = 10*cm, style={'alignment': TA_LEFT}),
            DecimalObjectValue(expression='mes1', left=COLUMNAS[2],width=2*cm),
            DecimalObjectValue(expression='mes2', left=COLUMNAS[3],width=2*cm),
            DecimalObjectValue(expression='acumulado', left= COLUMNAS[4], width=2*cm),

        ]

    class band_page_header(ReportBand):
        height = 2.5*cm
        elements = [
            Label(text="TRANS-EXPRESS,S.A ", top=0.1*cm, left=0, width=BAND_WIDTH, style= {'alignment': TA_CENTER}),            
            SystemField(expression='%(report_title)s', top=0.5*cm, left=0, width=BAND_WIDTH, style= {'alignment': TA_CENTER}),
            SystemField(expression='%(var:periodo)s' , top=1*cm, left=0, width=BAND_WIDTH, style= {'alignment': TA_CENTER}),
            SystemField(expression=u'Página %(page_number)d de %(page_count)d', top=0.1*cm, width=BAND_WIDTH, style={'alignment': TA_RIGHT}), 
#            Line(left=0*cm, top=2.8*cm, right=letter_width , bottom=2.8*cm),
                
            Label(text="Codigo", top=2*cm, left=COLUMNAS[0],  style={'alignment': TA_LEFT}, width=2*cm),
            Label(text="Nombre", top=2*cm, left=COLUMNAS[1], style={'alignment': TA_LEFT}, width=2*cm),
            SystemField(expression='%(var:mes1)s %(var:anho1)s', top=2*cm, width=2*cm, left=COLUMNAS[2], style={'fontName': 'Courier', 'alignment': TA_CENTER}),
            SystemField(expression='%(var:mes2)s %(var:anho2)s', top=2*cm, width=2*cm, left=COLUMNAS[3], style={'fontName': 'Courier', 'alignment': TA_CENTER}),
            Label(text="Acumulado", top=2*cm,left = COLUMNAS[4], width=2*cm, style={'fontName': 'Courier', 'alignment': TA_CENTER}),
        ]
        borders = {'bottom': True}

    class band_summary(ReportBand):
        height = 0.5*cm
        elements = [
            Label(text='Totales:'),
            ObjectValue(expression='count(nombre)', left=5*cm, style={'fontName': 'Courier'}),
            DecimalObjectValue(expression='sum(mes1)', left=COLUMNAS[2], style={'alignment': TA_RIGHT}, width=2*cm),
            DecimalObjectValue(expression='sum(mes2)', left=COLUMNAS[3], style={'alignment': TA_RIGHT}, width=2*cm),
            DecimalObjectValue(expression='sum(acumulado)', left=COLUMNAS[4], style={'alignment': TA_RIGHT}, width=2*cm),
            
            
            Label(text='Elaborado Por:',width=5*cm, left = COLUMNAS[1],top=1.5*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),
            SystemField(expression='%(var:contador)s' ,width=5*cm, left = COLUMNAS[1], top=2*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),                    
            Label(text='CONTADOR',width=5*cm, left = COLUMNAS[1], top=2.5*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),
            
            Label(text='Autorizado Por:',width=5*cm, left = COLUMNAS[3],top=1.5*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),
            SystemField(expression='%(var:gerente)s' ,width=5*cm, left = COLUMNAS[3],top=2*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),
            Label(text='GERENTE GENERAL',width=5*cm, left = COLUMNAS[3],top=2.5*cm,style={'fontName': 'Courier', 'alignment': TA_CENTER}),
            ]
        borders = {'top': True}
        
        

WIDTH = [0*cm,1.5*cm, 7.5*cm, 9.5*cm,12*cm,14.5*cm,17*cm]
class BalanceComprobacionReport(Report):
    page_size = LETTER
        
    
    class band_detail(DetailBand):
        height = 0.5*cm
        auto_expand_height = True
        default_style = {'fontName':'Courier', 'fontSize': 8}
        elements = [
            ObjectValue(expression='codigo_cuenta', left=WIDTH[0] ),
            ObjectValue(expression='nombre', left=WIDTH[1], width = 6*cm ),
            DecimalObjectValue(expression='saldo_anterior', left=WIDTH[2], style= {'alignment': TA_RIGHT}, width= 2*cm ),
            DecimalObjectValue(expression='debe', left=WIDTH[3], style= {'alignment': TA_RIGHT}, width= 2.5*cm ),
            DecimalObjectValue(expression='haber', left=WIDTH[4] , style= {'alignment': TA_RIGHT}, width= 2.5*cm),
            DecimalObjectValue(expression='saldo', left=WIDTH[5] , style= {'alignment': TA_RIGHT}, width= 2.5*cm),
            DecimalObjectValue(expression='acumulado', left=WIDTH[6] , style= {'alignment': TA_RIGHT}, width= 2.5*cm),
        ]        
    class band_page_header(ReportBand):
        height = 3*cm
        default_style = {'fontName':'Courier', 'fontSize': 8}
        letter_width = landscape(LETTER)[0] - 0.5*cm
        elements = [
            Label(text="TRANS-EXPRESS,S.A ", top=0.1*cm, left=0, width=BAND_WIDTH, style= {'alignment': TA_CENTER}),            
            SystemField(expression='%(report_title)s', top=0.6*cm, left=0, width=BAND_WIDTH, style= {'alignment': TA_CENTER}), 
            SystemField(expression=u'Página %(page_number)d de %(page_count)d', top=0.1*cm, width=BAND_WIDTH, style={'alignment': TA_RIGHT}), 
            Line(left=0*cm, top=1.8*cm, right=letter_width - 1.5*cm , bottom=1.8*cm),

            Label(text="Codigo", top=1*cm, left=WIDTH[0]),
            Label(text="Cuenta Contable", top=1*cm, left=WIDTH[1]),
            Label(text="Saldo Anterior", top=1*cm, left=WIDTH[2], width=2*cm, style= {'alignment': TA_CENTER} ),
            Label(text="Debe", top=1*cm, left=WIDTH[3] , style= {'alignment': TA_CENTER}, width= 2.5*cm),
            Label(text="Haber", top=1*cm, left=WIDTH[4] , style= {'alignment': TA_CENTER}, width= 2.5*cm),
            Label(text="Saldo del mes", top=1*cm, left=WIDTH[5] , style= {'alignment': TA_CENTER}, width= 2.5*cm),
            SystemField(expression='Acumulado a %(var:fin_periodo)s', top=1*cm, left=WIDTH[6] , style= {'alignment': TA_CENTER}, width= 2.5*cm),
        ]

    class band_summary(ReportBand):
        default_style = {'fontName':'Courier-Bold', 'fontSize': 8}
        height = 0.5*cm
        elements = [
            Label(text='Totales:'),
            ObjectValue(expression='count(nombre)', left=WIDTH[1] , style= {'alignment': TA_CENTER}, width= 2*cm),
            DecimalObjectValue(expression='sum(saldo_anterior)',left=WIDTH[2], style= {'alignment': TA_RIGHT}, width= 2*cm  ),
            DecimalObjectValue(expression='sum(debe)', left=WIDTH[3], style= {'alignment': TA_RIGHT}, width= 2.5*cm, get_value=lambda inst: inst.debe if inst.level == 0 else 0 ),
            DecimalObjectValue(expression='sum(haber)', left=WIDTH[4], style= {'alignment': TA_RIGHT}, width= 2.5*cm, get_value=lambda inst: inst.haber if inst.level == 0 else 0),
            DecimalObjectValue(expression='sum(saldo)', left=WIDTH[5], style= {'alignment': TA_RIGHT}, width= 2.5*cm),
            DecimalObjectValue(expression='sum(acumulado)', left=WIDTH[6], style= {'alignment': TA_RIGHT}, width= 2.5*cm),
            Label(text='Elaborado Por:',left=2*cm,top=2*cm ),
            Label(text='Autorizado Por:',left=11*cm,top=2*cm ),
            ]
        borders = {'top': True}

class AuxiliarCuentaReport(BalanceComprobacionReport):            
    class band_summary(ReportBand):
        height = 0.5*cm
        elements = [
            Label(text='Totales:'),
            ObjectValue(expression='count(nombre)', left=WIDTH[1] , style= {'alignment': TA_CENTER}, width= 2*cm),
            DecimalObjectValue(expression='sum(saldo_anterior)',left=WIDTH[2], style= {'alignment': TA_RIGHT}, width= 2*cm  ),
            DecimalObjectValue(expression='sum(debe)', left=WIDTH[3], style= {'alignment': TA_RIGHT}, width= 2.5*cm),
            DecimalObjectValue(expression='sum(haber)', left=WIDTH[4], style= {'alignment': TA_RIGHT}, width= 2.5*cm),
            DecimalObjectValue(expression='sum(saldo)', left=WIDTH[5], style= {'alignment': TA_RIGHT}, width= 2.5*cm),
            DecimalObjectValue(expression='sum(acumulado)', left=WIDTH[6], style= {'alignment': TA_RIGHT}, width= 2.5*cm),
            Label(text='Elaborado Por:',left=2*cm,top=2*cm ),
            Label(text='Autorizado Por:',left=11*cm,top=2*cm ),
            ]
        borders = {'top': True}        


class LibroDiarioReport(Report):
    title = ''
    default_style = {'fontName':'Courier'}
    class band_summary(ReportBand):
        height = 0.5*cm
        elements = [
            Label(text='Total General', left=3*cm),            
            DecimalObjectValue(expression='sum(cargo)', left=12*cm, style={'fontName': 'Courier'}),
            DecimalObjectValue(expression='sum(abono)', left=15*cm, style={'fontName': 'Courier'}),
            ]
        borders = {'top': Line(stroke_color=red, stroke_width=3)}
        
    class band_detail(DetailBand):
        height = 1.5*cm
        elements = [           
            Label(text="Comprobante No", top=1*cm, left=0*cm, style={'fontName': 'Courier'}),
            ObjectValue(expression='comprobante', top=1*cm, left=3*cm, style={'fontName': 'Courier'}),
            ObjectValue(expression='tipo_partida', top=1*cm, left=4.5*cm, style={'fontName': 'Courier'}),
            Label(text="Fecha", top=1*cm, left=7.5*cm, style={'fontName': 'Courier'}),
            ObjectValue(expression='fecha_formato', top=1*cm, left=9*cm, style={'fontName': 'Courier'}),
        ]

    groups = [
        ReportGroup(attribute_name='fecha',
            
            band_footer = ReportBand(
                height=0.5*cm,
                elements=[
                    Label(text="Total Fecha ", left=3*cm, style={'fontName': 'Courier'}),
                    ObjectValue(expression='fecha_formato', left=6*cm, style={'fontName': 'Courier'}),      
                    DecimalObjectValue(expression='sum(cargo)', left=12*cm,style={'fontName': 'Courier'}),
                    DecimalObjectValue(expression='sum(abono)', left=15*cm,style={'fontName': 'Courier'}),
                    ],
                borders = {'top':True}
            ),
        ),
    ]
      
    subreports = [
        SubReport(
                  queryset_string = '%(object)s.movimientos.all()',
                  band_detail = ReportBand(
                        height=0.5*cm,
                        elements=[
                              ObjectValue(attribute_name='cuenta.codigo_cuenta', top=0, left=0*cm, width= 9*cm),
                            ObjectValue(attribute_name='cuenta', top=0, left=3*cm, width= 9*cm),
                            DecimalObjectValue(attribute_name='cargo', top=0, left=12*cm, style={'alignment': TA_RIGHT} , width=2*cm),
                            DecimalObjectValue(attribute_name='abono', top=0, left=15*cm, style={'alignment': TA_RIGHT} , width=2*cm),
                        ],
                    ),
                    band_footer = ReportBand(
                        default_style = {'fontName': 'Courier-Bold'},
                        height=0.5*cm,
                        elements=[
                            Label(text="Total Comprobante ", left=3*cm),
                            ObjectValue(expression='comprobante', left=8*cm),      
                            DecimalObjectValue(expression='sum(cargo)', left=12*cm, style={'alignment': TA_RIGHT} , width=2*cm),
                            DecimalObjectValue(expression='sum(abono)', left=15*cm, style={'alignment': TA_RIGHT} , width=2*cm),
                            ],
                        
                    ),                             
                  
        ),
                  
    ]

    class band_page_header(ReportBand):
        height = 1*cm
        default_style = {'fontName': 'Courier-Bold'}
        elements = [
            Label(text="TRANS-EXPRESS,S.A ", top=0.1*cm, left=0, width=BAND_WIDTH, style= {'alignment': TA_CENTER}),            
            SystemField(expression='%(report_title)s', top=0.6*cm, left=0, width=BAND_WIDTH, style= {'alignment': TA_CENTER}), 
            SystemField(expression=u'Página %(page_number)d de %(page_count)d', top=0.1*cm, width=BAND_WIDTH, style={'alignment': TA_RIGHT}),
            
          Label(text="Codigo", top=1*cm, left=0*cm, width=2*cm),
          Label(text=u"Descripción", top=1*cm, left=3*cm),
          Label(text="Cargo", top=1*cm, left=12*cm, width=2*cm, style={'alignment': TA_CENTER}),
          Label(text="Abono", top=1*cm, left=15*cm, width=2*cm, style={'alignment': TA_CENTER})                        

        ]
        

COLUMNS = [0*cm,2*cm,8*cm, 8.5*cm,11*cm,13.5*cm, 16*cm, 18*cm]
class MayorAuxiliarReport(Report):
    title = ''
    default_style = {'fontName':'Courier'}
    
    page_size = LETTER
    default_style = {'fontName':'Courier', 'fontSize': 8}

    class band_page_header(ReportBand):
        height = 3*cm
        letter_width = landscape(LETTER)[0] - 0.5*cm
        default_style = {'fontName':'Courier', 'alignment': TA_RIGHT}    
        elements = [
            Label(text="TRANS-EXPRESS,S.A ", top=0.1*cm, left=0, width=BAND_WIDTH, style= {'alignment': TA_CENTER}),            
            SystemField(expression='%(report_title)s', top=0.6*cm, left=0, width=BAND_WIDTH, style= {'alignment': TA_CENTER}), 
            SystemField(expression=u'Página %(page_number)d de %(page_count)d', top=0.1*cm, width=BAND_WIDTH, style={'alignment': TA_RIGHT}), 
            Line(left=0*cm, top=1.8*cm, right=letter_width , bottom=1.8*cm),

            Label(text="Codigo", top=1*cm, left=COLUMNS[0] , width=2*cm, style={'fontName':'Courier','alignment':TA_LEFT} ),
            Label(text="Cuenta Contable", top=1*cm, left=COLUMNS[1] , style={'fontName':'Courier','alignment':TA_LEFT}),
            Label(text="Fecha", top=1*cm, left=COLUMNS[2]-1.5*cm , style={'fontName':'Courier','alignment':TA_LEFT}),            
            Label(text="Comp.", top=1*cm, left=COLUMNS[2] , width=0.5*cm , style={'fontName':'Courier','alignment':TA_LEFT} ),            
            Label(text="Saldo Anterior", top=1*cm, left=COLUMNS[3], width=2*cm , style={'fontName':'Courier','alignment':TA_CENTER}),
            Label(text="Debe", top=1*cm, left=COLUMNS[4] , width=2*cm , style={'fontName':'Courier','alignment':TA_CENTER}),
            Label(text="Haber", top=1*cm, left=COLUMNS[5], width=2*cm , style={'fontName':'Courier','alignment':TA_CENTER}),
            Label(text="Saldo del Mes", top=1*cm, left=COLUMNS[6] , width=2*cm , style={'fontName':'Courier','alignment':TA_CENTER}),
            Label(text="Saldo a la fecha", top=1*cm, left=COLUMNS[7] , width=2*cm , style={'fontName':'Courier','alignment':TA_CENTER}),
        ]

    class band_detail(DetailBand):
        height = 0.5*cm
        
        elements = [
            ObjectValue(expression='codigo_cuenta', left=COLUMNS[0] ),
            ObjectValue(expression='nombre', left=COLUMNS[1], width = 6*cm ),
            DecimalObjectValue(expression='saldo_anterior', left=COLUMNS[3] , width = 2*cm , style = {'fontName':'Courier','alignment':TA_RIGHT}),
            
        ]
    
    
    subreports = [
        SubReport(
                  queryset_string = '%(object)s.movimientos_contables.all().order_by("asiento__fecha","asiento__comprobante")',
                  band_detail = ReportBand(
                        height=0.5*cm,
                        default_style = {'fontName':'Courier','alignment':TA_RIGHT},
                        elements=[
                            ObjectValue(attribute_name='asiento.fecha', top=0, left=COLUMNS[1], ),
                            
                            ObjectValue(attribute_name='asiento.numero', top=0, left=COLUMNS[2], width= 0.5*cm),
                            DecimalObjectValue(attribute_name='cargo', top=0, left=COLUMNS[4] , width=2*cm ),
                            DecimalObjectValue(attribute_name='abono', top=0, left=COLUMNS[5] , width=2*cm ),
#                            DecimalObjectValue(attribute_name='saldo', top=0, left=WIDTH[5],get_value=lambda inst:  (inst.cargo if inst.cargo is not None else 0) - (inst.abono if inst.abono is not None else 0) ),
                        ],
                    ),
                    band_footer = ReportBand(
                        height=1*cm,
                        default_style = {'fontName':'Courier','alignment':TA_RIGHT},
                        elements=[
                            Label(text="Total Cuenta ", left=3*cm, style={'fontName': 'Courier'}),
                            
                            DecimalObjectValue(attribute_name='saldo_anterior', left=COLUMNS[3], width=2*cm ),      
                            DecimalObjectValue(expression='debe', left=COLUMNS[4] , width=2*cm ),
                            DecimalObjectValue(expression='haber', left=COLUMNS[5] , width=2*cm ),
                            DecimalObjectValue(attribute_name='saldo', left=COLUMNS[6], width=2*cm ),
                            DecimalObjectValue(attribute_name='saldo_mes', left=COLUMNS[7], width=2*cm ),
                            ],                        
                    ),                             
                  
        ),
                  
    ]
    

WIDTH = [0*cm,1.5*cm, 8*cm, 10*cm,12*cm,14*cm,16*cm,19*cm]
class MayorGeneralReport(Report):
    default_style = {'fontName':'Courier'}
    class band_detail(DetailBand):
        height = 0.7*cm
        elements = [
            ObjectValue(expression='codigo', left=WIDTH[0],style={'fontName': 'Courier', 'fontSize': 8}),
            ObjectValue(expression='nombre', left=WIDTH[1], width = 5*cm, style={'fontName': 'Courier', 'fontSize': 8}),
            ObjectValue(expression='saldo_anterior', left=WIDTH[2],style={'fontName': 'Courier', 'fontSize': 8}),
            ObjectValue(expression='debe', left=WIDTH[3],style={'fontName': 'Courier', 'fontSize': 8}),
            ObjectValue(expression='haber', left=WIDTH[4],style={'fontName': 'Courier', 'fontSize': 8}),
            ObjectValue(expression='saldo_mes', left=WIDTH[5],style={'fontName': 'Courier', 'fontSize': 8}),
            ObjectValue(expression='acumulado', left=WIDTH[6],style={'fontName': 'Courier', 'fontSize': 8}),
            ]


    class band_page_header(ReportBand):
        height = 1.3*cm
        elements = [
            SystemField(expression='%(report_title)s', top=0.1*cm, left=0, width=BAND_WIDTH,
                style={'fontName': 'Courier', 'fontSize': 10, 'alignment': TA_CENTER}),
            SystemField(expression=u'Página %(page_number)d de %(page_count)d', top=0.1*cm,
                width=BAND_WIDTH, style={'alignment': TA_RIGHT}),
            Label(text="Codigo", top=0.8*cm, left=WIDTH[0], style={'fontName': 'Courier'}),
            Label(text="Cuenta Contable", top=0.8*cm, left=WIDTH[1], style={'fontName': 'Courier','fontSize': 8,}),
            Label(text="Saldo Anterior", top=0.8*cm, left=WIDTH[2]-1*cm, width=3*cm , style={'fontName': 'Courier','fontSize': 8,}),
            Label(text="Debe", top=0.8*cm, left=WIDTH[3]-0.5*cm, style={'fontName': 'Courier','fontSize': 8,}),
            Label(text="Haber", top=0.8*cm, left=WIDTH[4]-0.5*cm, style={'fontName': 'Courier','fontSize': 8,}),
            Label(text="Saldo del Mes", top=0.8*cm, left=WIDTH[5]-1*cm, style={'fontName': 'Courier','fontSize': 8,}),
            Label(text="Saldo a la fecha", top=0.8*cm, left=WIDTH[6]-1*cm, style={'fontName': 'Courier','fontSize': 8,}),
            ]
        

    class band_summary(ReportBand):
        height = 0.5*cm
        elements = [
            Label(text='Totales:'),
            ObjectValue(expression='count(nombre)', left=WIDTH[1], style={'fontName': 'Courier','fontSize': 8}),
            ObjectValue(expression='sum(saldo_anterior)', left=WIDTH[2]-1*cm, style={'fontName': 'Courier','fontSize': 8}),
            ObjectValue(expression='sum(debe)', left=WIDTH[3]-0.5*cm, style={'fontName': 'Courier','fontSize': 8}),
            ObjectValue(expression='sum(haber)', left=WIDTH[4]-0.5*cm, style={'fontName': 'Courier','fontSize': 8}),
            ObjectValue(expression='sum(saldo_mes)', left=WIDTH[5]-0.5*cm, style={'fontName': 'Courier','fontSize': 8}),
            ObjectValue(expression='sum(acumulado)', left=WIDTH[6]-0.5*cm, style={'fontName': 'Courier','fontSize': 8}),
            Label(text='Elaborado Por:',left=2*cm,top=2.5*cm,style={'fontName': 'Courier','fontSize': 8}),
            Label(text='Autorizado Por:',left=11*cm,top=2.5*cm,style={'fontName': 'Courier','fontSize': 8}),
            ]
        borders = {'top': True}

