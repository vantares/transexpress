# -*- coding: UTF-8 -*-
from django.core.management.base import BaseCommand
import datetime
from django.db import transaction
from contabilidad.models import Cuenta

import logging
FORMAT = '%(levelname)s:%(asctime)s:%(message)s'
logging.basicConfig(filename='logs/cuentas.log',level=logging.INFO,format = FORMAT)


class Command(BaseCommand):
    
    @transaction.commit_manually
    def handle(self, *args, **options):
        
        try:            
            data = Cuenta.objects.all().update(ordenado=False)
            data = Cuenta.objects.filter(level=0).order_by('codigo_cuenta')

            tree_count = 1
            logging.info("============> Inicia Ordenamiento")
            for record in data:                
                record.parent = None
                record.tree_id = tree_count
                record.lft = 1
                actualizar(record,0)                            
                tree_count +=1
                
                
            print "%d cuentas"%len(data) 
        except Exception as inst:
            
            transaction.rollback()
            logging.critical( "Error: %s" %(unicode(inst)))
                              
        else:
            transaction.commit()
            logging.info("<============FIN")
                    
        

        

def actualizar(padre,level):
    nodos = Cuenta.objects.exclude(pk=padre.pk).filter(codigo_cuenta__startswith=padre.codigo_cuenta).order_by('codigo_cuenta')        
    padre.rght = padre.lft + (len(nodos)*2) + 1
    
    padre.level = level

    rght = padre.lft
    logging.info( "id    lft    rght    level    tree_id    parent    codigo")
    logging.info( "%s     %s     %s     %s        %s         %s    %s"%(padre.id,padre.lft,padre.rght,padre.level,padre.tree_id,padre.parent.id if padre.parent is not None else None,padre.codigo_cuenta))
     

    for row in nodos:
        record = Cuenta.objects.get(pk=row.pk)
        if not record.ordenado:
            rght += 1
            record.lft = rght 
            record.tree_id = padre.tree_id
            actualizar(record,padre.level+1)
            rght = record.rght        
            record.save()
            

    
    padre.ordenado = True
    padre.save()
        
    
