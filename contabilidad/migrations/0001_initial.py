# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Cuenta'
        db.create_table('contabilidad_cuenta', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('codigo_cuenta', self.gf('django.db.models.fields.CharField')(unique=True, max_length=20, db_column='codigo_cuenta')),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=70, db_column='nombre')),
            ('permite_posteo', self.gf('django.db.models.fields.BooleanField')(default=False, db_column='permite_posteo')),
            ('tipo_cuenta', self.gf('django.db.models.fields.CharField')(max_length=20, db_column='tipo_cuenta')),
            ('parent', self.gf('mptt.fields.TreeForeignKey')(blank=True, related_name='children', null=True, db_column='parent', to=orm['contabilidad.Cuenta'])),
            ('lft', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('rght', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('tree_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('level', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
        ))
        db.send_create_signal('contabilidad', ['Cuenta'])

        # Adding model 'Banco'
        db.create_table('contabilidad_banco', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('codigo', self.gf('django.db.models.fields.CharField')(max_length=20, db_column='codigo')),
            ('descripcion', self.gf('django.db.models.fields.CharField')(max_length=50, db_column='descripcion')),
            ('cuenta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contabilidad.Cuenta'], db_column='cuenta')),
        ))
        db.send_create_signal('contabilidad', ['Banco'])

        # Adding model 'TipoDocumento'
        db.create_table('contabilidad_tipodocumento', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=20, db_column='nombre')),
            ('comportamiento', self.gf('django.db.models.fields.CharField')(max_length=20, db_column='comportamiento')),
        ))
        db.send_create_signal('contabilidad', ['TipoDocumento'])

        # Adding model 'Concepto'
        db.create_table('contabilidad_concepto', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=80, db_column='nombre')),
        ))
        db.send_create_signal('contabilidad', ['Concepto'])

        # Adding model 'Asiento'
        db.create_table('contabilidad_asiento', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('comprobante', self.gf('django.db.models.fields.CharField')(max_length=20, db_column='comprobante')),
            ('fecha', self.gf('django.db.models.fields.DateField')(db_column='fecha')),
            ('concepto', self.gf('django.db.models.fields.TextField')(max_length=300, db_column='concepto')),
            ('tipo_partida', self.gf('django.db.models.fields.CharField')(default='diario', max_length=20, db_column='tipo_partida')),
            ('actualizado', self.gf('django.db.models.fields.BooleanField')(default=True, db_column='actualizado')),
        ))
        db.send_create_signal('contabilidad', ['Asiento'])

        # Adding model 'Cheque'
        db.create_table('contabilidad_cheque', (
            ('asiento_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['contabilidad.Asiento'], unique=True, primary_key=True)),
            ('banco', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contabilidad.Banco'], db_column='banco')),
            ('a_nombre_de', self.gf('django.db.models.fields.CharField')(max_length=45, db_column='a_nombre_de')),
            ('concepto_pago', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contabilidad.Concepto'], db_column='concepto')),
            ('monto', self.gf('django.db.models.fields.DecimalField')(db_column='monto', decimal_places=4, max_digits=12)),
            ('impreso', self.gf('django.db.models.fields.BooleanField')(default=False, db_column='impreso')),
            ('cobrado', self.gf('django.db.models.fields.BooleanField')(default=False, db_column='cobrado')),
            ('tipo_doc', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contabilidad.TipoDocumento'], null=True, db_column='tipo_doc', blank=True)),
        ))
        db.send_create_signal('contabilidad', ['Cheque'])

        # Adding model 'MovimientoContable'
        db.create_table('contabilidad_movimientocontable', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('asiento', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contabilidad.Asiento'], db_column='asiento')),
            ('cuenta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contabilidad.Cuenta'], db_column='cuenta')),
            ('tipo_partida', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, db_column='tipo_partida', blank=True)),
            ('cargo', self.gf('django.db.models.fields.DecimalField')(blank=True, null=True, db_column='cargo', decimal_places=4, max_digits=12)),
            ('abono', self.gf('django.db.models.fields.DecimalField')(blank=True, null=True, db_column='abono', decimal_places=4, max_digits=12)),
            ('concepto', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, db_column='concepto', blank=True)),
        ))
        db.send_create_signal('contabilidad', ['MovimientoContable'])

        # Adding model 'Saldo'
        db.create_table('contabilidad_saldo', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('anyo', self.gf('django.db.models.fields.IntegerField')(db_column='anyo')),
            ('mes', self.gf('django.db.models.fields.IntegerField')(db_column='mes')),
            ('saldo_anterior', self.gf('django.db.models.fields.DecimalField')(db_column='saldo_anterior', decimal_places=4, max_digits=12)),
            ('cargo', self.gf('django.db.models.fields.DecimalField')(db_column='cargo', decimal_places=4, max_digits=12)),
            ('abono', self.gf('django.db.models.fields.DecimalField')(db_column='abono', decimal_places=4, max_digits=12)),
            ('cuenta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contabilidad.Cuenta'], db_column='cuenta')),
        ))
        db.send_create_signal('contabilidad', ['Saldo'])

        # Adding unique constraint on 'Saldo', fields ['anyo', 'mes', 'cuenta']
        db.create_unique('contabilidad_saldo', ['anyo', 'mes', 'cuenta'])

    def backwards(self, orm):
        # Removing unique constraint on 'Saldo', fields ['anyo', 'mes', 'cuenta']
        db.delete_unique('contabilidad_saldo', ['anyo', 'mes', 'cuenta'])

        # Deleting model 'Cuenta'
        db.delete_table('contabilidad_cuenta')

        # Deleting model 'Banco'
        db.delete_table('contabilidad_banco')

        # Deleting model 'TipoDocumento'
        db.delete_table('contabilidad_tipodocumento')

        # Deleting model 'Concepto'
        db.delete_table('contabilidad_concepto')

        # Deleting model 'Asiento'
        db.delete_table('contabilidad_asiento')

        # Deleting model 'Cheque'
        db.delete_table('contabilidad_cheque')

        # Deleting model 'MovimientoContable'
        db.delete_table('contabilidad_movimientocontable')

        # Deleting model 'Saldo'
        db.delete_table('contabilidad_saldo')

    models = {
        'contabilidad.asiento': {
            'Meta': {'object_name': 'Asiento'},
            'actualizado': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_column': "'actualizado'"}),
            'comprobante': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'comprobante'"}),
            'concepto': ('django.db.models.fields.TextField', [], {'max_length': '300', 'db_column': "'concepto'"}),
            'fecha': ('django.db.models.fields.DateField', [], {'db_column': "'fecha'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo_partida': ('django.db.models.fields.CharField', [], {'default': "'diario'", 'max_length': '20', 'db_column': "'tipo_partida'"})
        },
        'contabilidad.banco': {
            'Meta': {'object_name': 'Banco'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'codigo'"}),
            'cuenta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contabilidad.Cuenta']", 'db_column': "'cuenta'"}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'descripcion'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contabilidad.cheque': {
            'Meta': {'object_name': 'Cheque', '_ormbases': ['contabilidad.Asiento']},
            'a_nombre_de': ('django.db.models.fields.CharField', [], {'max_length': '45', 'db_column': "'a_nombre_de'"}),
            'asiento_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['contabilidad.Asiento']", 'unique': 'True', 'primary_key': 'True'}),
            'banco': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contabilidad.Banco']", 'db_column': "'banco'"}),
            'cobrado': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'cobrado'"}),
            'concepto_pago': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contabilidad.Concepto']", 'db_column': "'concepto'"}),
            'impreso': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'impreso'"}),
            'monto': ('django.db.models.fields.DecimalField', [], {'db_column': "'monto'", 'decimal_places': '4', 'max_digits': '12'}),
            'tipo_doc': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contabilidad.TipoDocumento']", 'null': 'True', 'db_column': "'tipo_doc'", 'blank': 'True'})
        },
        'contabilidad.concepto': {
            'Meta': {'object_name': 'Concepto'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '80', 'db_column': "'nombre'"})
        },
        'contabilidad.cuenta': {
            'Meta': {'object_name': 'Cuenta'},
            'codigo_cuenta': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20', 'db_column': "'codigo_cuenta'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '70', 'db_column': "'nombre'"}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'db_column': "'parent'", 'to': "orm['contabilidad.Cuenta']"}),
            'permite_posteo': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'permite_posteo'"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tipo_cuenta': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'tipo_cuenta'"}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'contabilidad.movimientocontable': {
            'Meta': {'object_name': 'MovimientoContable'},
            'abono': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'null': 'True', 'db_column': "'abono'", 'decimal_places': '4', 'max_digits': '12'}),
            'asiento': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contabilidad.Asiento']", 'db_column': "'asiento'"}),
            'cargo': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'null': 'True', 'db_column': "'cargo'", 'decimal_places': '4', 'max_digits': '12'}),
            'concepto': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'db_column': "'concepto'", 'blank': 'True'}),
            'cuenta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contabilidad.Cuenta']", 'db_column': "'cuenta'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo_partida': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'tipo_partida'", 'blank': 'True'})
        },
        'contabilidad.saldo': {
            'Meta': {'unique_together': "(('anyo', 'mes', 'cuenta'),)", 'object_name': 'Saldo'},
            'abono': ('django.db.models.fields.DecimalField', [], {'db_column': "'abono'", 'decimal_places': '4', 'max_digits': '12'}),
            'anyo': ('django.db.models.fields.IntegerField', [], {'db_column': "'anyo'"}),
            'cargo': ('django.db.models.fields.DecimalField', [], {'db_column': "'cargo'", 'decimal_places': '4', 'max_digits': '12'}),
            'cuenta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contabilidad.Cuenta']", 'db_column': "'cuenta'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mes': ('django.db.models.fields.IntegerField', [], {'db_column': "'mes'"}),
            'saldo_anterior': ('django.db.models.fields.DecimalField', [], {'db_column': "'saldo_anterior'", 'decimal_places': '4', 'max_digits': '12'})
        },
        'contabilidad.tipodocumento': {
            'Meta': {'object_name': 'TipoDocumento'},
            'comportamiento': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'comportamiento'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'nombre'"})
        }
    }

    complete_apps = ['contabilidad']