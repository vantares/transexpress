# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Cheque.concepto_pago'
        db.alter_column('contabilidad_cheque', 'concepto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contabilidad.Concepto'], null=True, db_column='concepto'))
    def backwards(self, orm):

        # Changing field 'Cheque.concepto_pago'
        db.alter_column('contabilidad_cheque', 'concepto', self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['contabilidad.Concepto'], db_column='concepto'))
    models = {
        'contabilidad.asiento': {
            'Meta': {'object_name': 'Asiento'},
            'actualizado': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_column': "'actualizado'"}),
            'comprobante': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'comprobante'"}),
            'concepto': ('django.db.models.fields.TextField', [], {'max_length': '300', 'db_column': "'concepto'", 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {'db_column': "'fecha'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo_partida': ('django.db.models.fields.CharField', [], {'default': "'diario'", 'max_length': '20', 'db_column': "'tipo_partida'"})
        },
        'contabilidad.banco': {
            'Meta': {'object_name': 'Banco'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'codigo'"}),
            'cuenta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contabilidad.Cuenta']", 'db_column': "'cuenta'"}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'descripcion'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contabilidad.cheque': {
            'Meta': {'object_name': 'Cheque', '_ormbases': ['contabilidad.Asiento']},
            'a_nombre_de': ('django.db.models.fields.CharField', [], {'max_length': '45', 'db_column': "'a_nombre_de'"}),
            'asiento_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['contabilidad.Asiento']", 'unique': 'True', 'primary_key': 'True'}),
            'banco': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contabilidad.Banco']", 'db_column': "'banco'"}),
            'cobrado': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'cobrado'"}),
            'concepto_pago': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contabilidad.Concepto']", 'null': 'True', 'db_column': "'concepto'", 'blank': 'True'}),
            'impreso': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'impreso'"}),
            'monto': ('django.db.models.fields.DecimalField', [], {'db_column': "'monto'", 'decimal_places': '4', 'max_digits': '12'}),
            'tipo_doc': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contabilidad.TipoDocumento']", 'null': 'True', 'db_column': "'tipo_doc'", 'blank': 'True'})
        },
        'contabilidad.concepto': {
            'Meta': {'object_name': 'Concepto'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '80', 'db_column': "'nombre'"})
        },
        'contabilidad.cuenta': {
            'Meta': {'object_name': 'Cuenta'},
            'codigo_cuenta': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20', 'db_column': "'codigo_cuenta'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '70', 'db_column': "'nombre'"}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'db_column': "'parent'", 'to': "orm['contabilidad.Cuenta']"}),
            'permite_posteo': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'permite_posteo'"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tipo_cuenta': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'tipo_cuenta'"}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'contabilidad.movimientocontable': {
            'Meta': {'object_name': 'MovimientoContable'},
            'abono': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'null': 'True', 'db_column': "'abono'", 'decimal_places': '4', 'max_digits': '12'}),
            'asiento': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contabilidad.Asiento']", 'db_column': "'asiento'"}),
            'cargo': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'null': 'True', 'db_column': "'cargo'", 'decimal_places': '4', 'max_digits': '12'}),
            'concepto': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'db_column': "'concepto'", 'blank': 'True'}),
            'cuenta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contabilidad.Cuenta']", 'db_column': "'cuenta'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo_partida': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'tipo_partida'", 'blank': 'True'})
        },
        'contabilidad.saldo': {
            'Meta': {'unique_together': "(('anyo', 'mes', 'cuenta'),)", 'object_name': 'Saldo'},
            'abono': ('django.db.models.fields.DecimalField', [], {'db_column': "'abono'", 'decimal_places': '4', 'max_digits': '12'}),
            'anyo': ('django.db.models.fields.IntegerField', [], {'db_column': "'anyo'"}),
            'cargo': ('django.db.models.fields.DecimalField', [], {'db_column': "'cargo'", 'decimal_places': '4', 'max_digits': '12'}),
            'cuenta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contabilidad.Cuenta']", 'db_column': "'cuenta'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mes': ('django.db.models.fields.IntegerField', [], {'db_column': "'mes'"}),
            'saldo_anterior': ('django.db.models.fields.DecimalField', [], {'db_column': "'saldo_anterior'", 'decimal_places': '4', 'max_digits': '12'})
        },
        'contabilidad.tipodocumento': {
            'Meta': {'object_name': 'TipoDocumento'},
            'comportamiento': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'comportamiento'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'nombre'"})
        }
    }

    complete_apps = ['contabilidad']