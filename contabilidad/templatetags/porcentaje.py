from django import template
from datetime import date
from reportes.views import obtener_periodo
from contabilidad.models import MESES

register = template.Library()

@register.filter
def get_final_date(data):
    periodo  =obtener_periodo()
    fin_periodo =  periodo.fin if periodo else date.today() 
    return '%s-%s-%s'%(fin_periodo.day, MESES[fin_periodo.month][1], fin_periodo.year)


@register.filter(name='percentage')
def percentage(fraction, population):
    if population == 0:
        return ''
    try:
        return "%.2f%%" % ((float(fraction) / float(population)) * 100)
    except ValueError:
        return ''

@register.filter(name='pdb')
def pdb_filter(arg):
    import pdb; pdb.set_trace()
    

@register.filter
def decimal_format(value):
    try:
        value = Decimal(str(value))
        return moneyfmt(value,2,'')
    except:
        return '-'

@register.filter
def total_sum(list,col):
    try:
        m = 0
        for l in list:
            m+= l[col]
            
        return m
    except:
        return 0

@register.filter
def level_sum(list,col):
#    try:
        return sum([l[col] for l in list if l['level'] == 1])
#    except:
#        return 0

from decimal import Decimal    
def moneyfmt( value, places = 4, curr = '', sep = ',', dp = '.', pos = '', neg = '-', trailneg = '' ):
    q = Decimal( 10 ) ** -places      # 2 places --> '0.01'
    sign, digits, _exp = value.quantize( q ).as_tuple()
    result = []
    digits = map( str, digits )
    build, next = result.append, digits.pop
    if sign:
        build( trailneg )
    for _i in range( places ):
        build( next() if digits else '0' )
    build( dp )
    if not digits:
        build( '0' )
    i = 0
    while digits:
        build( next() )
        i += 1
        if i == 3 and digits:
            i = 0
            build( sep )
    build( curr )
    build( neg if sign else pos )
    return ''.join( reversed( result ) )
    