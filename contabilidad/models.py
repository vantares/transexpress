# -*- coding: UTF-8 -*-
from django.db import models, transaction
from mptt.models import MPTTModel,TreeForeignKey
from moneyfmt import moneyfmt
from django.db.models import Sum
from decimal import Decimal
from crm.models import Cliente
from django.forms import ModelForm, ModelChoiceField
from django.forms import ValidationError
from django.db.models.related  import RelatedObject
from contabilidad.moneyfmt import moneyfmt, number_to_words
from datetime import date, timedelta
import calendar
from django.db import connection
from model_utils.managers import QueryManager

MESES = [
         ('', '-----'),
        (1,'Enero'),
        (2,'Febrero'),
        (3,'Marzo'),
        (4,'Abril'),
        (5,'Mayo'),
        (6,'Junio'),
        (7,'Julio'),
        (8,'Agosto'),
        (9,'Septiembre'),
        (10,'Octubre'),
        (11,'Noviembre'),
        (12,'Diciembre'),
    ]




class Periodo( models.Model ):
    inicio = models.DateField( null=False, blank=False, verbose_name='Fecha de Inicio')    
    fin = models.DateField( null=False, blank=False, verbose_name='Fecha de Fin')
    contador = models.CharField(null=False, blank=False, max_length=100, verbose_name="Nombre del Contador Actual")
    gerente = models.CharField(null=False, blank=False, max_length=100, verbose_name="Nombre del Gerente General")     
    activo = models.BooleanField(default = False)

    class Meta:
        verbose_name = u"Período Contable"
        verbose_name_plural = u"Períodos Contables"
        
    def __unicode__( self ):    
        return 'Del ' + str(self.inicio) +' al '+ str(self.inicio)
    

    def save(self, force_insert=False, force_update=False, using=None):        
            super(Periodo, self).save(force_insert=False, force_update=False, using=None)
            if self.activo:
                self.validar_periodo()
    @property
    def fecha_inicio(self):
        return  "%s de %s %s" %(self.inicio.day, MESES[self.inicio.month][1] , self.inicio.year)
    
    @property
    def fecha_fin(self):
        return  "%s de %s %s" %(self.fin.day, MESES[self.fin.month][1] , self.fin.year)    
            
            
    @transaction.commit_manually        
    def validar_periodo(self):
        try:
            for p in Periodo.objects.exclude(id = self.id).filter(activo = True):
                p.activo = False
                p.save()
        except:
            transaction.rollback()
        else:
            transaction.commit()


#==========================================================
#	Cuenta
#==========================================================
class Cuenta( MPTTModel ):
    TIPO_CUENTA=(
        ('debe','Deudor'),
        ('haber','Acreedor'),
    )
    codigo_cuenta = models.CharField(db_column="codigo_cuenta", unique=True, null=False, blank=False, max_length=20, verbose_name="Codigo")
    nombre = models.CharField(db_column="nombre", null=False, blank=False, max_length=70, verbose_name="Nombre")
    permite_posteo = models.BooleanField(db_column="permite_posteo", null=False, blank=False, verbose_name="Permite Posteo")
    tipo_cuenta = models.CharField(db_column="tipo_cuenta",choices=TIPO_CUENTA, null=False, blank=False, max_length=20, verbose_name="Tipo de Cuenta")
    parent = TreeForeignKey('self',db_column="parent", null=True, blank=True, related_name='children')
    ordenado = models.BooleanField(default=False, editable=False)
    visible = models.BooleanField(default=True, editable=False)
    
    objects = QueryManager(visible=True)
    allobjects = models.Manager()
    
    
    __saldo_anterior = None
    __debe = None
    __haber = None
    fecha_inicio = None
    fecha_fin = None

    class MPTTMeta:
        order_insertion_by = ['codigo_cuenta']

    class Meta:
        verbose_name = "Cuenta"
        verbose_name_plural = "Cuentas"

    def __unicode__( self ):
        return self.nombre

    def delete(self, force=False, *args, **kwargs):
        if force:
            super(Cuenta, self).delete(*args, **kwargs)
        else:
            self.visible = False
            self.save()

    def clean(self):
        try:
            c = Cuenta.allobjects.exclude(pk=self.pk).get(codigo_cuenta=self.codigo_cuenta)

        except:
            c = None
        if c is not None:
            raise ValidationError('El codigo {0} fue utilizado por la cuenta {1} y no puede utilizarse nuevamente, por favor elija un codigo unico'.format(self.codigo_cuenta,c.nombre))
    def validate_if_exists(self):
        try:
            c = Cuenta.allobjects.get(codigo_cuenta=self.codigo_cuenta)
            self.pk = c.pk
            self.visible = True
        except:
            pass    

    def save(self, force_insert=False, force_update=False, using=None):
        if self.pk is None:
            self.validate_if_exists()
        
        if self.parent is not None:
            self.tipo_cuenta = self.parent.tipo_cuenta
        return super(Cuenta,self).save( force_insert=False, force_update=False, using=None)
    
    @property
    def js_info(self):
        return self.codigo_cuenta,self.nombre

    @property
    def movimientos_contables(self):
        mov = MovimientoContable.objects.filter(cuenta=self)                 
        if self.fecha_inicio is not None:
            mov = mov.filter(asiento__fecha__gte=self.fecha_inicio)
                                
        if self.fecha_fin is not None:
            mov = mov.filter(asiento__fecha__lte=self.fecha_fin)                    
        
        mov.order_by('asiento__fecha')
        return mov
    
    
    @property
    def debe(self):
        """
        
        """
        
        if self.__debe is None:
            self.movimientos()
            
        return self.__debe 

    @property
    def haber(self):
        if self.__haber is None:
            self.movimientos()
        return self.__haber

    @property
    def saldo(self):
        """
        Diferencia entre el debe y el haber
        """
        if self.__debe is None or self.__haber is None:
            self.movimientos()        
        valor = ((self.__debe - self.__haber)) #* (-1 if self.tipo_cuenta =='haber' else 1 ))
        return valor  

    @property
    def saldo_anterior(self):
        """ 
        """
        if self.__saldo_anterior is None:
            self.movimientos()            
        return self.__saldo_anterior 

    @property
    def acumulado(self):
        """
        Devuelve el acumulado del periodo
        """
        if self.__acumulado is None:
            self.movimientos()            
        return self.__acumulado 

    @property
    def saldo_mes(self):
        """
        Suma el saldo anterior, el debe menos el haber
        """
        return self.saldo_anterior + ((self.debe - self.haber))#* (-1 if self.tipo_cuenta =='haber' else 1 ))    

    def movimientos(self,anyo=None,mes=None):
        from reportes.views import obtener_periodo
        
        cuentas = self.get_descendants(include_self=True).filter(movimientocontable__isnull=False)
        
        
        periodo  =obtener_periodo()
        fin_periodo =  periodo.fin if periodo else date.today() 
        
        if anyo is not None and mes is not None:
            self.fecha_inicio = date(anyo,mes,1)
            self.fecha_fin = self.fecha_inicio + timedelta(days=calendar.monthrange(self.fecha_inicio.year,self.fecha_inicio.month)[1] -1)      
                
        if self.fecha_inicio is not None:
            cuentas = cuentas.filter(movimientocontable__asiento__fecha__gte=self.fecha_inicio)
                                
        if self.fecha_fin is not None:
            cuentas = cuentas.filter(movimientocontable__asiento__fecha__lte=self.fecha_fin)  
                              
        query = """
            SELECT 
            COALESCE(sum(CASE WHEN asiento.fecha < %(fecha_inicio)s THEN COALESCE(cargo,0) - COALESCE(abono,0) ELSE 0 END),0) AS saldo_anterior,
            COALESCE(sum(CASE WHEN asiento.fecha >= %(fecha_inicio)s AND asiento.fecha <= %(fecha_fin)s THEN COALESCE(cargo,0) ELSE 0 END),0) as totalcargo,
            COALESCE(sum(CASE WHEN asiento.fecha >= %(fecha_inicio)s AND asiento.fecha <= %(fecha_fin)s THEN COALESCE(abono,0) ELSE 0 END),0) as totalabono,
            COALESCE(sum(CASE WHEN asiento.fecha <= %(fecha_fin)s THEN COALESCE(cargo,0) - COALESCE(abono,0) ELSE 0 END),0) as saldo,
            COALESCE(sum(CASE WHEN asiento.fecha <= %(fecha_fin_periodo)s THEN COALESCE(cargo,0) - COALESCE(abono,0) ELSE 0 END),0) as acumulado
            FROM 
            contabilidad_movimientocontable mov
            JOIN contabilidad_asiento asiento ON mov.asiento = asiento.id
            JOIN contabilidad_cuenta cuenta ON mov.cuenta = cuenta.id
            WHERE cuenta.lft >= %(cuenta_left)s AND cuenta.rght <= %(cuenta_right)s AND cuenta.tree_id = %(cuenta_tree)s        
        """
        parameters = {
                      'fecha_inicio':self.fecha_inicio,
                      'fecha_fin':self.fecha_fin, 
                      'fecha_fin_periodo':fin_periodo,
                      'cuenta_left':self.lft, 
                      'cuenta_right':self.rght, 
                      'cuenta_tree':self.tree_id}
        cursor = connection.cursor()
        cursor.execute(query , parameters)

        desc = cursor.description
        mov = [dict(zip([col[0] for col in desc], row)) for row in cursor.fetchall()][0]
        
#        mov = cuentas.aggregate(totalcargo=Sum('movimientocontable__cargo'),totalabono=Sum('movimientocontable__abono'))
        
        saldo_anterior = mov['saldo_anterior']
        cargo = mov['totalcargo']
        abono = mov['totalabono']
        saldo_final = mov['saldo']
        acumulado = mov['acumulado']

        saldo_anterior = saldo_anterior if saldo_anterior is not None else Decimal(0)
        cargo = cargo if cargo is not None else Decimal(0)
        abono = abono if abono is not None else Decimal(0)        
        saldo_final = saldo_final if saldo_final is not None else Decimal(0)
        acumulado = acumulado if acumulado is not None else Decimal(0)
        
        self.__debe = cargo
        self.__haber = abono   
        self.__saldo_anterior = saldo_anterior #* (-1 if self.tipo_cuenta =='haber' else 1 ) 
        self.__acumulado =    acumulado #* (-1 if self.tipo_cuenta =='haber' else 1 )
        
        return [cargo,abono, saldo_anterior, saldo_final]        

    def get_saldo_cierre(self,a,m):
        saldo_cierre = Saldo.objects.filter(cuenta=self).filter(anyo=a).filter(mes=m)
        valor = saldo_cierre.saldo
        valor = valor if valor is not None else Decimal(0)
        return valor

    def values(self):
        return [self.codigo_cuenta, (self.nombre if self.nombre.find(",") == -1 else "\"" + self.nombre +  "\""),self.permite_posteo,self.tipo_cuenta]

#==========================================================
#	Banco
#==========================================================
class Banco( models.Model ):
    codigo = models.CharField(db_column="codigo", null=False, blank=False, max_length=20, verbose_name="Codigo")
    descripcion = models.CharField(db_column="descripcion", null=False, blank=False, max_length=50, verbose_name="Descripcion Banco")
    cuenta = models.ForeignKey(Cuenta, db_column="cuenta", null=False,blank=False)
    class Meta:
        verbose_name = "Banco"
        verbose_name_plural = "Bancos"

    def __unicode__( self ):
        return self.descripcion
#==========================================================
#	Tipo Documento
#==========================================================
class TipoDocumento( models.Model ):
    TIPO_CUENTA=(
        ('debe','Debe'),
        ('haber','Haber'),
    )
    nombre = models.CharField(db_column="nombre", null=False, blank=False, max_length=20, verbose_name="Nombre")
    comportamiento = models.CharField(db_column="comportamiento",choices=TIPO_CUENTA, null=False, blank=False, max_length=20, verbose_name="Comportamiento")
    class Meta:
        verbose_name = "Tipo de Documento"
        verbose_name_plural = "Tipos de Documentos"

    def __unicode__( self ):
        return self.nombre


#==========================================================
#    Concepto Del Documento
#==========================================================
class Concepto( models.Model ):
    nombre = models.CharField(db_column="nombre", null=False, blank=False, max_length=80, verbose_name="Concepto de Cheque")

    class Meta:
        verbose_name = "Concepto de Cheques"
        verbose_name_plural = "Conceptos de Cheques"

    def __unicode__( self ):
        return self.nombre


#==========================================================
#	Asiento
#==========================================================
class Asiento( models.Model ):
    comprobante = models.CharField(db_column="comprobante", null=False, blank=False, max_length=20, verbose_name=u"Número")
    fecha = models.DateField(db_column="fecha", null=False, blank=False, verbose_name="Fecha")
    concepto = models.TextField(db_column="concepto", null=False, blank=True, max_length=300, verbose_name="Concepto y Observaciones")
    tipo_partida = models.CharField(db_column="tipo_partida",null=False, blank=False, default="diario", max_length=20, verbose_name="Tipo Partida")
    actualizado = models.BooleanField(db_column="actualizado", default=True, null=False, blank=False, verbose_name="Actualizado")
    validar = False

    class Meta:
        verbose_name = "Asiento"
        verbose_name_plural = "Asientos"

    def __unicode__( self ):
        return str(self.fecha) + ': ' + self.concepto
    
    @property
    def numero(self):
        return self.comprobante + ('D' if self.tipo_partida == 'diario' else 'E')
    
    @property
    def cargo(self):
        valor = MovimientoContable.objects.filter(asiento=self).aggregate(Sum('cargo'))['cargo__sum']
        valor = valor if valor is not None else Decimal(0)
        return valor
    
    @property
    def cargo_(self):
        return moneyfmt(self.cargo,2)
    
    @property
    def abono(self):
        valor = MovimientoContable.objects.filter(asiento=self).aggregate(Sum('abono'))['abono__sum']
        valor = valor if valor is not None else Decimal(0)
        return valor
    
    @property
    def abono_(self):
        return moneyfmt(self.abono,2)

    def values(self):
        return [self.comprobante,self.tipo_partida,self.fecha, self.concepto, self.cargo, self.abono] 
    
    def fecha_formato(self):
        return format(self.fecha,"%d/%m/%y")
    @property
    def movimientos(self):
        return MovimientoContable.objects.filter(asiento=self)  

#==========================================================
#    Cheque
#==========================================================
class Cheque( Asiento ):    
    banco = models.ForeignKey(Banco, db_column="banco", null=False, blank=False)
    a_nombre_de = models.CharField(db_column="a_nombre_de", null=False, blank=False, max_length=45, verbose_name="A Nombre De")
    concepto_pago = models.ForeignKey(Concepto, db_column="concepto", null=True, blank=True,verbose_name="Concepto del Cheque")
    monto = models.DecimalField(db_column="monto", null=False, blank=False, max_digits=12, decimal_places=4, verbose_name="Monto")
    impreso = models.BooleanField(db_column="impreso", null=False, blank=False)
    cobrado = models.BooleanField(db_column="cobrado", null=False, blank=False)
    tipo_doc = models.ForeignKey(TipoDocumento, db_column="tipo_doc", null=True, blank=True)
    moneda = " DOLARES"
    
    
    class Meta:
        verbose_name = "Cheque"
        verbose_name_plural = "Cheques"

    def __unicode__( self ):
        return unicode (self.comprobante)

    @property
    def values(self):
        return [self.comprobante, self.fecha,("\"" + self.a_nombre_de + "\"" ),self.monto,("\"" + self.concepto+ "\"" )] 
    
    def save(self, force_insert=False, force_update=False, using=None):
        self.tipo_partida = "egreso"
        super(Cheque,self).save(force_insert=False, force_update=False, using=None)
    
    @property    
    def movimientos(self):
        return MovimientoContable.objects.filter(asiento=self)
    
    def monto_to_words(self):
        return number_to_words(self.monto,'')
    
    def fecha_formato(self):
        return  "%s de %s, %s" %(self.fecha.day, MESES[self.fecha.month][1] , self.fecha.year)
    

#==========================================================
#	Movimiento Contable
#==========================================================
class MovimientoContable( models.Model ):
    TIPO_CUENTA=(
        ('debe','Debe'),
        ('haber','Haber'),
    )
    asiento = models.ForeignKey(Asiento, db_column="asiento", null=False, blank=False, verbose_name="Asiento")
    cuenta = models.ForeignKey(Cuenta, db_column="cuenta",null=False, blank=False, verbose_name="Cuenta")
    tipo_partida = models.CharField(db_column="tipo_partida",choices=TIPO_CUENTA, null=True, blank=True, max_length=20, verbose_name="Tipo Partida")
    cargo = models.DecimalField(db_column="cargo", null=True, blank=True, max_digits=12, decimal_places=4, verbose_name="Cargo")
    abono = models.DecimalField(db_column="abono", null=True, blank=True, max_digits=12, decimal_places=4, verbose_name="Abono")
    #concepto = models.CharField(db_column="concepto", null=True , blank=True, max_length=50, verbose_name="Concepto")

    class Meta:
        verbose_name = "Movimiento Contable"
        verbose_name_plural = "Movimientos Contables"

    def __unicode__( self ):
        return self.cuenta.nombre

    def clean(self):
        if (self.cargo is None or self.cargo == 0) and ( self.abono is None or self.abono == 0):
            raise ValidationError('Por favor especifique el monto del movimiento')
        
        models.Model.clean(self)
    
    @property
    def total(self):
        return (self.cargo - self.abono ) * (1 if self.tipo_partida == "debe" else -1 )

class MovimientoContableForm(ModelForm):
    cuenta = ModelChoiceField(
                queryset=Cuenta.objects.filter(permite_posteo=True),
                label = 'Cuenta',   
    )
    class Meta:
        model = MovimientoContable
        
    def get_readonly_fields(self, request, obj=None):
        if obj is not None:
            field_names = obj._meta.get_all_field_names()[:]
            readonly_fields = list(super(MovimientoContableForm, self).get_readonly_fields(request, obj))
            for name in field_names:
                if name != 'id'  and not isinstance(obj._meta.get_field_by_name(name)[0], RelatedObject):
                    readonly_fields.append(name)

            return readonly_fields

        return ()

#==========================================================
#	Saldo
#==========================================================
class Saldo( models.Model ):
    anyo = models.IntegerField(db_column="anyo", null=False, blank=False,verbose_name=u"Año")
    mes = models.IntegerField(db_column="mes", choices=MESES, null=False, blank=False)
    saldo_anterior = models.DecimalField(db_column="saldo_anterior", null=False, blank=False, max_digits=12, decimal_places=4, verbose_name="Saldo Anterior")
    cargo = models.DecimalField(db_column="cargo", null=False, blank=False, max_digits=12, decimal_places=4, verbose_name="Cargo")
    abono = models.DecimalField(db_column="abono", null=False, blank=False, max_digits=12, decimal_places=4, verbose_name="Abono")
    cuenta = models.ForeignKey(Cuenta, db_column="cuenta", null=False, blank=False, verbose_name="Cuenta")

    class Meta:
        verbose_name = "Saldo"
        verbose_name_plural = "Saldos"
        unique_together = ("anyo", "mes","cuenta")

    @property
    def saldo(self):
        anterior = self.saldo_anterior if self.saldo_anterior is not None else Decimal(0)
        cargo = self.cargo if self.cargo is not None else Decimal(0)
        abono = self.abono if self.abono is not None else Decimal(0)
        s = ( anterior + cargo - abono ) * (-1 if self.cuenta.tipo_cuenta == 'haber' else 1)
        return s

    @property
    def saldo_cuenta(self):
        return moneyfmt(self.saldo,2,'')

    def __unicode__( self ):
        return self.cuenta.nombre + ': ' +  str(self.mes) +  '-'  + str(self.anyo )


    def save(self, force_insert=False, force_update=False, using=None):
        if self.mes == 1:
            fecha_anterior=[self.anyo-1,12]
        else:
            fecha_anterior=[self.anyo,self.mes -1 ]

        saldos = Saldo.objects.filter(cuenta=self.cuenta).filter(anyo=fecha_anterior[0]).filter(mes=fecha_anterior[1])
        if len(saldos)== 0:
            self.saldo_anterior = Decimal(0)
        else:
            self.saldo_anterior = saldos[0].saldo if saldos[0].saldo is not None else Decimal(0)
        mov = self.cuenta.movimientos(self.anyo,self.mes)
        self.cargo = mov[0]
        self.abono = mov[1]
        models.Model.save(self, force_insert=False, force_update=False, using=None)


