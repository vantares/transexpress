from tastypie.resources import ModelResource
from tastypie import fields
from contabilidad.models import Cuenta

class CuentaResource(ModelResource):
    code = fields.CharField(attribute='codigo_cuenta')
    name = fields.CharField(attribute='nombre')
    class Meta:
        queryset = Cuenta.objects.all()
        filtering = {
            "id": ('exact',),
            "code": ('exact',),
            "name": ('startswith',),
        }