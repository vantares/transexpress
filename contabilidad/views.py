# -*- coding: utf-8 -*-
from contabilidad.forms import  *
from contabilidad.reports import *
from facturacion.forms import NotaForm
from facturacion.reports import ResumenReport
from django.contrib.admin.views.decorators import staff_member_required
from django.views.generic import FormView
from django.http import HttpResponse
from reportes.views import ExportedReport, obtener_periodo
from contabilidad.models import *
from django.db import connection
from moneyfmt import moneyfmt
from decimal import Decimal
from array import array
from geraldo.generators import PDFGenerator
import simplejson
from datetime import date, timedelta
import calendar

MESES = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']

@staff_member_required
def index(request):
    return CuentaView.as_view(template_name='contabilidad/cuentas.html')

def obtener_nombre(request):
    """
    Utilizado en javascrip para optener el nombe de la cuenta contable a partir de su codigo o id
    """
    try:    
        if 'id' in request.GET:
            id = int(request.GET['id'])    
            cuenta = Cuenta.objects.get(pk = id)    
            json = simplejson.dumps([ (cuenta.codigo_cuenta,cuenta.nombre) ])            
        else:
            codigo = request.GET['codigo']    
            cuenta = Cuenta.objects.get(codigo_cuenta = codigo)    
            json = simplejson.dumps([ (cuenta.id,cuenta.nombre) ])
    except:
        json = simplejson.dumps([ ('','No definido') ])        
    return HttpResponse(json)

class ChequeView(ExportedReport, FormView):
    """
    Muestra un listado de cheques de acuerdo al filtro, al generar PDF genera el detalle de cada cheque
    """
    form_class = ChequeForm
    template_name = 'contabilidad/cheque.html'
    report_class = ChequeReport
    csv_titles = ['Numero','Fecha','Cliente','Monto','Concepto']
    title = u'Reporte de Cheques'
    required_permissions = ('reportes.cheque',)
    
    def form_valid(self, form):
        cliente = form.cleaned_data['cliente']
        num = form.cleaned_data['numero']
        fecha_inicio = form.cleaned_data['fecha_inicio']
        fecha_fin =  form.cleaned_data['fecha_fin']
        resumen =  form.cleaned_data['resumen']
        data = Cheque.objects.all()
        
        if num <> "":
            data = data.filter(comprobante = num)
            
        if cliente <> "":
            data = data.filter(a_nombre_de__icontains = cliente)
            
        if fecha_inicio is not None:
            data = data.filter(fecha__gte = fecha_inicio)

        if fecha_fin is not None:
            data = data.filter(fecha__lte = fecha_fin)
                    
        if resumen:
            self.report_class = ResumenReport
        
        context = self.get_context_data()
        context.update({
            'form':form,
            'titulo':self.title,
            'data':data,
        })
        return self.render_to_response(context,data)

class ChequeImprimirView( ExportedReport, FormView ):
    """
    Genera el detalle  un cheque de acuerdo al ID recibido
    """
    form_class = ChequeForm
    template_name = 'contabilidad/cheque.html'
    report_class = ChequeReport
    csv_titles = ['Numero','Fecha','Cliente','Monto','Concepto']
    title = u'Reporte de Cheques'
    required_permissions = ('reportes.cheque',)
    def render_to_response(self, context, **response_kwargs):
        if self.request.GET.get('send'):
            cod = self.request.GET.get('send')
            if cod <> '':
                data = Cheque.objects.filter(pk=cod)
            if data is not None or len(data) != 0:
                report = self.report_class(queryset=data)
                report.title = self.title
                resp = HttpResponse(mimetype='application/pdf')
                report.generate_by(PDFGenerator, filename=resp)
                return resp
            
class ImprimirAsientoView( ExportedReport, FormView ):
    """
    Genera el detalle  un cheque de acuerdo al ID recibido
    """
    form_class = ChequeForm
    template_name = 'contabilidad/cheque.html'
    report_class = AsientoReport
    csv_titles = ['Numero','Fecha','Cliente','Monto','Concepto']
    title = u'Reporte de Asientos'
    required_permissions = ('reportes.asiento',)
    def render_to_response(self, context, **response_kwargs):
        if self.request.GET.get('send'):
            cod = self.request.GET.get('send')
            if cod <> '':
                data = Asiento.objects.filter(pk=cod)
            if data is not None or len(data) != 0:
                report = self.report_class(queryset=data)
                report.title = self.title
                resp = HttpResponse(mimetype='application/pdf')
                report.generate_by(PDFGenerator, filename=resp)
                return resp
            
class CuentaView(ExportedReport, FormView):
    """
    Lista todas las cuentas contables de acuerdo al filtro 
    """
    form_class = CuentaForm
    title = u'Reporte de Cuentas Contables'
    template_name = 'contabilidad/cuentas.html'
    report_class = CuentaReport
    csv_titles = ['Codigo','Descripcion','Permite Posteo','Tipo']
    required_permissions = ('reportes.cuentas',)
    
    def form_valid(self, form):

        codigo = form.cleaned_data['codigo']
        nombre = form.cleaned_data['nombre']
        posteo = int(form.cleaned_data['posteo'])
        tipo = int(form.cleaned_data['tipo'])

        data = Cuenta.objects.all()
        
        if codigo <> "":
            data = data.filter(codigo_cuenta__icontains = codigo)

        if nombre <> "":
            data = data.filter(nombre__icontains = nombre)

        if posteo != 0 :
            posteo = 1 if posteo == 1 else 0
            data = data.filter(permite_posteo = posteo)
        
        if tipo != 0 :
            tipo = "debe" if tipo == 1 else "haber"
            data = data.filter(tipo_cuenta = tipo)        
            
        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })

        return self.render_to_response(context,data)
     
class EstadoResultadoView(ExportedReport, FormView):
    form_class = EstadosForm
    title = u'Estado de Resultado'
    template_name = 'contabilidad/estadoresultado.html'
    report_class = EstadoResultadoReport
    csv_titles = ['Concepto','Mes 1','% Mes 1','Mes 2','% Mes 2', 'Acumulado', '% Acumulado']
    required_permissions = ('reportes.estadoresultado',)
    report_variables = {'mes1':None, 'mes2':None}
    

    def form_valid(self, form):    
        mes_inicio = int(form.cleaned_data['mes_inicio'])
        anyo_inicio = int(form.cleaned_data['anyo_inicio'])
        mes_fin = int(form.cleaned_data['mes_fin'])
        anyo_fin = int(form.cleaned_data['anyo_fin'])
        
        
        if form.cleaned_data['anexos']:
            self.title = u'ANEXO AL ESTADO DE RESULTADO'
            self.template_name = 'contabilidad/gastos.html'
            self.report_class = GastosReport
            fecha_mes1 = date(anyo_inicio,mes_inicio,calendar.monthrange(anyo_inicio,mes_inicio)[1]) 
            fecha_mes2 = date(anyo_fin, mes_fin,calendar.monthrange(anyo_fin, mes_fin)[1])        
            cursor = connection.cursor()
            query = self.query(fecha_mes1,fecha_mes2)            
            cursor.execute(query,None)
            data = super(EstadoResultadoView,self).dictfetchall(cursor)
            
        else:                            
                data = self.dictfetchall(None,anyo_inicio,mes_inicio, anyo_fin, mes_fin)

        context = self.get_context_data()        
        self.report_variables = {
                'mes1':MESES[mes_inicio-1], 
                'anho1':str(anyo_inicio), 
                'mes2':MESES[mes_fin-1],
                'anho2':str(anyo_fin),
                'fin_mes2':str(calendar.monthrange(anyo_fin,mes_fin)[1]),
                 
        }
        context.update({
            'form':form,
            'data':data,
            'mes1': MESES[mes_inicio-1] + '/' + str(anyo_inicio),
            'mes2': MESES[mes_fin-1] + '/' + str(anyo_fin),
        })
#
        return self.render_to_response(context,data)

    def query(self,fecha_mes1,fecha_mes2):
        inicio_mes1 = date(fecha_mes1.year,fecha_mes1.month,1)
        inicio_mes2 = date(fecha_mes2.year,fecha_mes2.month,1)
        periodo = obtener_periodo()
        fin_periodo = (date.today() if periodo is None else periodo.fin)

        query = """
            SELECT 
            codigo,
            nombre,
            mes1,
            mes2,
            mes2 - mes1 as variacion,
            acumulado
            FROM (select 
            cuenta.codigo_cuenta as codigo,
            cuenta.nombre as nombre,
            (
                SELECT 
                    COALESCE(sum(COALESCE(cargo,0) - COALESCE(abono,0)),0) 
                FROM contabilidad_movimientocontable mov 
                JOIN contabilidad_asiento asiento ON mov.asiento = asiento.id 
                JOIN contabilidad_cuenta c ON mov.cuenta = c.id 
                AND c.lft >= cuenta.lft 
                AND c.lft <= cuenta.rght 
                AND c.tree_id = cuenta.tree_id 
                AND asiento.fecha >='%(inicio_mes1)s' and asiento.fecha <='%(fin_mes1)s'
            )AS mes1,
            (
                SELECT 
                    COALESCE(sum(COALESCE(cargo,0) - COALESCE(abono,0)),0) 
                FROM contabilidad_movimientocontable mov 
                JOIN contabilidad_asiento asiento ON mov.asiento = asiento.id 
                JOIN contabilidad_cuenta c ON mov.cuenta = c.id 
                AND c.lft >= cuenta.lft 
                AND c.lft <= cuenta.rght 
                AND c.tree_id = cuenta.tree_id 
                AND asiento.fecha >='%(inicio_mes2)s' and asiento.fecha <='%(fin_mes2)s'
            )AS mes2,
            (
                SELECT 
                    COALESCE(sum(CASE WHEN 
                    asiento.fecha <='%(fin_periodo)s'
                    THEN COALESCE(cargo,0) - COALESCE(abono,0) ELSE 0 END),0) 
                FROM contabilidad_movimientocontable mov 
                JOIN contabilidad_asiento asiento ON mov.asiento = asiento.id 
                JOIN contabilidad_cuenta c ON mov.cuenta = c.id 
                AND c.lft >= cuenta.lft 
                AND c.lft <= cuenta.rght 
                AND c.tree_id = cuenta.tree_id 
            )AS acumulado
            FROM contabilidad_cuenta cuenta
        """ % {'inicio_mes1':inicio_mes1, 'fin_mes1':fecha_mes1,'inicio_mes2':inicio_mes2, 'fin_mes2':fecha_mes2,'fin_periodo':fin_periodo}
        query = query + """
            WHERE (cuenta.codigo_cuenta like '4_%' OR
            cuenta.codigo_cuenta like '5_%') and
             cuenta.level in (1,3)) tabla
            WHERE not(mes1 = 0 and mes2 = 0 and acumulado = 0)
            ORDER BY codigo;        
            """
        return query
        

    def get_row(self,concept,values,percent_base = None):
        cols = [concept]
        i = 0 
        for v in values:
            if v != ' ':
                valor = moneyfmt(v,2,'')
                cols.append(valor)
                if (percent_base is not None):
                    
                    if percent_base[i] <> Decimal(0) :
                        valor = moneyfmt(abs(v/percent_base[i])*100,2,'') + '%'
                    else:
                        valor = ''
                    cols.append(valor)
                    i +=1
                else:
                    cols.append('')
            else:
                cols.append('')
                
        return cols
                
    def dictfetchall(self,cursor,anyo_inicio,mes_inicio, anyo_fin, mes_fin):
        "Returns all rows from a cursor as a dict"
        desc = ['concepto','mes1','porcentaje_mes1','mes2','porcentaje_mes2','acumulado','porcentaje_acumulado']        
        cuenta = Cuenta.objects.get(nombre="VENTAS",level = 1)
        mes1 = cuenta.movimientos(anyo_inicio,mes_inicio)
        mes2 = cuenta.movimientos(anyo_fin,mes_fin)
        
        periodo = obtener_periodo()
        fin_periodo = (date.today() if periodo is None else periodo.fin)
        self.report_variables = {'fin_periodo': '%s-%s-%s'%(fin_periodo.day, MESES[fin_periodo.month][1], fin_periodo.year)}      
        
        if periodo is None:
            mes3 = cuenta.movimientos(fin_periodo.year,fin_periodo.month)
        else:
            cuenta.fecha_inicio = periodo.inicio
            cuenta.fecha_fin = periodo.fin
            mes3 = cuenta.movimientos()
        
        mes1 = mes1[1] - mes1[0]
        mes2 = mes2[1] - mes2[0]
        mes3 = mes3[1] - mes3[0]
        ventas_brutas = [
            mes1 ,
            mes2,
            mes3
        ]
        
        cuenta = Cuenta.objects.get(nombre="REBAJAS SOBRE VENTAS" , level = 1)
        mes1 = cuenta.movimientos(anyo_inicio,mes_inicio)
        mes2 = cuenta.movimientos(anyo_fin,mes_fin)
                
        if periodo is None:
            mes3 = cuenta.movimientos(fin_periodo.year,fin_periodo.month)
        else:
            cuenta.fecha_inicio = periodo.inicio
            cuenta.fecha_fin = periodo.fin
            mes3 = cuenta.movimientos()
        
        mes1 = mes1[1] - mes1[0]
        mes2 = mes2[1] - mes2[0]
        mes3 = mes3[1] - mes3[0]

        rebajas = [
            mes1,
            mes2,
            mes3
        ]

        cuenta = Cuenta.objects.get(nombre="COSTOS DE SERVICIOS",level = 1)
        mes1 = cuenta.movimientos(anyo_inicio,mes_inicio)
        mes2 = cuenta.movimientos(anyo_fin,mes_fin)
        if periodo is None:
            mes3 = cuenta.movimientos(fin_periodo.year,fin_periodo.month)
        else:
            cuenta.fecha_inicio = periodo.inicio
            cuenta.fecha_fin = periodo.fin
            mes3 = cuenta.movimientos()
        
        mes1 = mes1[1] - mes1[0]
        mes2 = mes2[1] - mes2[0]
        mes3 = mes3[1] - mes3[0]     
          
        costos = [
            mes1,
            mes2,
            mes3    
        ]
        cuenta = Cuenta.objects.get(nombre="GASTOS DE VENTA",level = 1)
        mes1 = cuenta.movimientos(anyo_inicio,mes_inicio)
        mes2 = cuenta.movimientos(anyo_fin,mes_fin)
        if periodo is None:
            mes3 = cuenta.movimientos(fin_periodo.year,fin_periodo.month)
        else:
            cuenta.fecha_inicio = periodo.inicio
            cuenta.fecha_fin = periodo.fin
            mes3 = cuenta.movimientos()
        
        mes1 = mes1[1] - mes1[0]
        mes2 = mes2[1] - mes2[0]
        mes3 = mes3[1] - mes3[0]     
          
        gastos_venta = [
            mes1,
            mes2,
            mes3    
        ]            

        cuenta = Cuenta.objects.get(nombre="GASTOS DE ADMINISTRACION",level = 1)
        mes1 = cuenta.movimientos(anyo_inicio,mes_inicio)
        mes2 = cuenta.movimientos(anyo_fin,mes_fin)
        if periodo is None:
            mes3 = cuenta.movimientos(fin_periodo.year,fin_periodo.month)
        else:
            cuenta.fecha_inicio = periodo.inicio
            cuenta.fecha_fin = periodo.fin
            mes3 = cuenta.movimientos()
        
        mes1 = mes1[1] - mes1[0]
        mes2 = mes2[1] - mes2[0]
        mes3 = mes3[1] - mes3[0]     
          
        gastos_administracion = [
            mes1,
            mes2,
            mes3    
        ]

        cuenta = Cuenta.objects.get(nombre="GASTOS Y PRODUCTOS FINANCIEROS",level = 1)
        mes1 = cuenta.movimientos(anyo_inicio,mes_inicio)
        mes2 = cuenta.movimientos(anyo_fin,mes_fin)
        if periodo is None:
            mes3 = cuenta.movimientos(fin_periodo.year,fin_periodo.month)
        else:
            cuenta.fecha_inicio = periodo.inicio
            cuenta.fecha_fin = periodo.fin
            mes3 = cuenta.movimientos()
        
        mes1 = mes1[1] - mes1[0]
        mes2 = mes2[1] - mes2[0]
        mes3 = mes3[1] - mes3[0]     
          
        gastos_financieros = [
            mes1,
            mes2,
            mes3    
        ]

        cuenta = Cuenta.objects.get(nombre="OTROS INGRESOS",level = 1)
        mes1 = cuenta.movimientos(anyo_inicio,mes_inicio)
        mes2 = cuenta.movimientos(anyo_fin,mes_fin)
        if periodo is None:
            mes3 = cuenta.movimientos(fin_periodo.year,fin_periodo.month)
        else:
            cuenta.fecha_inicio = periodo.inicio
            cuenta.fecha_fin = periodo.fin
            mes3 = cuenta.movimientos()
        
        mes1 = mes1[1] - mes1[0]
        mes2 = mes2[1] - mes2[0]
        mes3 = mes3[1] - mes3[0]     
          
        otros_ingresos = [
            mes1,
            mes2,
            mes3    
        ]

        cuenta = Cuenta.objects.get(nombre="OTROS GASTOS",level = 1)
        mes1 = cuenta.movimientos(anyo_inicio,mes_inicio)
        mes2 = cuenta.movimientos(anyo_fin,mes_fin)
        if periodo is None:
            mes3 = cuenta.movimientos(fin_periodo.year,fin_periodo.month)
        else:
            cuenta.fecha_inicio = periodo.inicio
            cuenta.fecha_fin = periodo.fin
            mes3 = cuenta.movimientos()
        
        mes1 = mes1[1] - mes1[0]
        mes2 = mes2[1] - mes2[0]
        mes3 = mes3[1] - mes3[0]     
          
        otros_egresos = [
            mes1,
            mes2,
            mes3    
        ]

        rows = []    
        rows.append(self.get_row('VENTAS',ventas_brutas))
        rows.append(self.get_row('MENOS',[' ',' ',' ',' ',' ',' ']))
        rows.append(self.get_row('REBAJAS SOBRE VENTAS',rebajas))
        rows.append(self.get_row('',[' ',' ',' ',' ',' ',' ']))
        
        venta_neta = [ ventas_brutas[i] + rebajas[i] for i in range(len(ventas_brutas))]
        
        rows.append(self.get_row('VENTA DE SERVICIO NETO',venta_neta,venta_neta))
        rows.append(self.get_row('MENOS',[' ',' ',' ',' ',' ',' ']))
        rows.append(self.get_row('COSTOS DE SERVICIOS',costos,venta_neta))
        
        utilidad_bruta = [ venta_neta[i] + costos[i] for i in range(len(venta_neta))] 
        
        rows.append(self.get_row('UTILIDAD BRUTA',utilidad_bruta ,venta_neta))
        rows.append(self.get_row('',[' ',' ',' ',' ',' ',' ']))
        rows.append(self.get_row('MENOS',[' ',' ',' ',' ',' ',' ']))
        rows.append(self.get_row('GASTOS DE VENTA',gastos_venta ,venta_neta))
        rows.append(self.get_row('GASTOS DE ADMINISTRACION',gastos_administracion ,venta_neta))
        rows.append(self.get_row('GASTOS Y PRODUCTOS FINANCIEROS',gastos_financieros ,venta_neta))
        
        total_gastos = [ gastos_venta[i] + gastos_administracion[i] + gastos_financieros[i] for i in range(len(venta_neta))]
        rows.append(self.get_row('TOTAL GASTOS GENERALES',total_gastos ,venta_neta))
        rows.append(self.get_row('',[' ',' ',' ',' ',' ',' ']))
        
        rows.append(self.get_row('UTILIDAD O PERDIDA DE OPERACION',[ utilidad_bruta[i] + total_gastos[i]for i in range(len(venta_neta))] ,venta_neta))
        rows.append(self.get_row('MAS O MENOS',[' ',' ',' ',' ',' ',' ']))
        rows.append(self.get_row('OTROS INGRESOS',otros_ingresos ,venta_neta))
        rows.append(self.get_row('OTROS GASTOS',otros_egresos ,venta_neta))
        rows.append(self.get_row('',[' ',' ',' ',' ',' ',' ']))
        
        rows.append(self.get_row('UTILIDA (O PERDIDA) ANTES DE I.R.',[ utilidad_bruta[i] + total_gastos[i] + otros_ingresos[i] +otros_egresos[i]  for i in range(len(venta_neta))] ,venta_neta))
    
        return [
            dict(zip([col for col in desc], row))
            for row in rows

        ]

class FilaBalance(object):
    """
    Objeto utilizado para la elaboracion del Reporte de Situacion Financiera
    """
    codigo_cuenta = ''
    contepto = ''
    mes1 = Decimal(0)
    rubro1 = Decimal(0)
    mes2 = Decimal(0)
    rubro2 = Decimal(0)
    es_titulo = False
    gran_total = None
    gran_total2 = None
    
    @property
    def porcentaje_mes1(self):
        return (self.rubro / self.gran_total) if self.gran_total is not None else 1

    @property
    def porcentaje_mes2(self):
        return (self.rubro2 / self.gran_total2) if self.gran_total2 is not None else 1



class ReporteComparativoView(ExportedReport, FormView):
    form_class = ReporteComparativoForm
    template_name = 'contabilidad/gastos.html'
    report_class = GastosReport
    
    def extra_context(self,data):
        return {}
    
    def query(self, fecha_mes1, fecha_mes2):
        return

    def form_valid(self, form):    
        mes_inicio = int(form.cleaned_data['mes_inicio'])
        anyo_inicio = int(form.cleaned_data['anyo_inicio'])
        mes_fin = int(form.cleaned_data['mes_fin'])
        anyo_fin = int(form.cleaned_data['anyo_fin'])
        
        try:
            self.anexo = form.cleaned_data['anexos']
            if self.anexo:
                self.set_anexo()
        except:
            pass
        
        fecha_mes1 = date(anyo_inicio,mes_inicio,calendar.monthrange(anyo_inicio,mes_inicio)[1]) 
        fecha_mes2 = date(anyo_fin, mes_fin,calendar.monthrange(anyo_fin, mes_fin)[1])        
        cursor = connection.cursor()
        query = self.query(fecha_mes1,fecha_mes2)
        
        cursor.execute(query,None)
        data = self.dictfetchall(cursor)
                                  
        context = self.get_context_data()
        
        self.report_variables = {
                'mes1':MESES[mes_inicio-1], 
                'anho1':str(anyo_inicio), 
                'mes2':MESES[mes_fin-1],
                'anho2':str(anyo_fin),
                'fin_mes2':str(calendar.monthrange(anyo_fin,mes_fin)[1])                 
        }        
        context.update({
            'form':form,
            'data':data,            
            'mes1': MESES[mes_inicio-1] + '/' + str(anyo_inicio),
            'mes2': MESES[mes_fin-1] + '/' + str(anyo_fin),
        })
        context.update(self.extra_context(data))
        return self.render_to_response(context,data)        

    
    
class SituacionFinancieraView(ReporteComparativoView):    
    title = u'Estado de Situacion Financiera'
    template_name = 'contabilidad/situacionfinanciera.html'
    report_class = SituacionFinancieraReport
    form_class = EstadosForm
    csv_titles = ['Concepto','Mes 1','% Mes 1','Mes 2','% Mes 2', 'Acumulado', '% Acumulado']
    required_permissions = ('reportes.estadoresultado',)
    anexo = False
    
    def set_anexo(self):
        self.title = u'Anexo al Estado de Situacion Financiera'
        self.template_name = 'contabilidad/anexobalance.html'
        self.report_class = AnexoBalanceReport
        
    def extra_context(self,data):
        total1 = 0 
        total2 = 0

        for r in data :
            valid = r['level'] == '0' and r['codigo'] != '1' if self.anexo else r['clasificacion'] != 'ACTIVO'  
            if valid:
                total1 += r['mes1']
                total2 += r['mes2']        
                
        self.report_variables.update(
                                     {
                                      'total1': -total1,
                                      'total2': -total2,
                                      'total1_': moneyfmt(-total1,2),
                                      'total2_': moneyfmt(-total2,2)
                                      }
                                     )
        return {'gran_total':{'mes1':-total1,'mes2':-total2}}    
    
    def query(self, fecha_mes1, fecha_mes2):    
        if self.anexo:
            query = """        
                SELECT
                CASE WHEN codigo_cuenta LIKE '1%' THEN 'ACTIVO' ELSE CASE WHEN codigo_cuenta LIKE '2%' THEN 'PASIVO' ELSE 'CAPITAL' END END as clasificacion, 
                level,
                codigo_cuenta,
                concepto as nombre,    
                sum(mes1) as mes1,
                sum(mes2) as mes2
                FROM (
                SELECT 
                cuenta.level,
                    CASE WHEN codigo_cuenta LIKE '4%' or codigo_cuenta LIKE '5%' THEN 'UTILIDAD O PERDIDA DEL EJERCICIO' ELSE cuenta.nombre END as concepto,
                    CASE WHEN codigo_cuenta LIKE '4%' or codigo_cuenta LIKE '5%' THEN '3103'ELSE codigo_cuenta END as codigo_cuenta,
                            ( SELECT COALESCE(sum(CASE WHEN asiento.fecha <='"""
            query = query + format(fecha_mes1,'%Y-%m-%d') + """' THEN COALESCE(cargo,0) - COALESCE(abono,0) ELSE 0 END),0) FROM contabilidad_movimientocontable mov JOIN contabilidad_asiento asiento ON mov.asiento = asiento.id JOIN contabilidad_cuenta c ON mov.cuenta = c.id AND c.lft >= cuenta.lft AND c.lft <= cuenta.rght AND c.tree_id = cuenta.tree_id )AS mes1, ( SELECT COALESCE(sum(CASE WHEN asiento.fecha <='"""
            query = query + format(fecha_mes2,'%Y-%m-%d') + """' THEN COALESCE(cargo,0) - COALESCE(abono,0) ELSE 0 END),0) FROM contabilidad_movimientocontable mov JOIN contabilidad_asiento asiento ON mov.asiento = asiento.id JOIN contabilidad_cuenta c ON mov.cuenta = c.id AND c.lft >= cuenta.lft AND c.lft <= cuenta.rght AND c.tree_id = cuenta.tree_id )AS mes2
                    FROM contabilidad_cuenta  cuenta 

            where level !=0 and (codigo_cuenta like '1_%' OR codigo_cuenta like '2_%' OR codigo_cuenta like '3_%' or codigo_cuenta like '4_%' or codigo_cuenta like '5_%')
                    ) balance  WHERE mes1 !=0 or mes2!=0 GROUP BY 
                level,
                codigo_cuenta,
                    concepto    
                    ORDER BY codigo_cuenta;                 
                """            
        else:
            query = """        
             SELECT 
                clasificacion,
                tipo,
                categoria,
                concepto,
                codigo_cuenta,
                sum(mes1) as mes1,
                sum(mes2) as mes2
              FROM (
              SELECT 
                (CASE  WHEN codigo_cuenta LIKE '1%' THEN 
                'ACTIVO' 
                ELSE CASE WHEN codigo_cuenta LIKE '2%'THEN 
                'PASIVO' 
                ELSE 
                'CAPITAL' 
                END END ) as clasificacion,
                (CASE  WHEN codigo_cuenta LIKE '11%' THEN 
                'ACTIVO CIRCULANTE' 
                ELSE CASE WHEN codigo_cuenta LIKE '13%' OR codigo_cuenta LIKE '12%'  THEN 
                'ACTIVO FIJO' 
                ELSE CASE WHEN codigo_cuenta LIKE '1_%' THEN 
                'OTROS ACTIVOS' 
                ELSE CASE WHEN codigo_cuenta LIKE '21%' THEN 
                'PASIVO CIRCULANTE' 
                
                ELSE CASE WHEN codigo_cuenta LIKE '22%' THEN 
                'PASIVO FIJO' 
                ELSE CASE WHEN codigo_cuenta LIKE '3101%' OR codigo_cuenta LIKE '3102%' OR codigo_cuenta LIKE '3103%' OR codigo_cuenta LIKE '5%' OR codigo_cuenta LIKE '4%' THEN 
                'CAPITAL CONTABLE' 
                ELSE CASE WHEN codigo_cuenta LIKE '3_%' THEN 
                'RESERVAS Y APORTES' 
                END
                END
                END
                END
                END
                END END)as tipo,
                (CASE  WHEN codigo_cuenta LIKE '110%' THEN 
                'CAJA Y BANCO' 
                ELSE CASE WHEN codigo_cuenta LIKE '111%' THEN 
                'DOCUMENTOS Y CUENTAS POR COBRAR' 
                ELSE CASE WHEN codigo_cuenta LIKE '113%' THEN 
                'PAGOS POR ANTICIPADO' 
                ELSE CASE WHEN codigo_cuenta LIKE '1320%' THEN 
                'MENOS DEPRECIACION'
                ELSE
                ' ' 
                END
                END
                END END)as categoria,
                CASE WHEN codigo_cuenta LIKE '4%' or codigo_cuenta LIKE '5%' THEN 'UTILIDAD O PERDIDA DEL EJERCICIO' ELSE cuenta.nombre END as concepto,
                CASE WHEN codigo_cuenta LIKE '4%' or codigo_cuenta LIKE '5%' THEN '31021'ELSE codigo_cuenta END as codigo_cuenta,
                        ( SELECT COALESCE(sum(CASE WHEN asiento.fecha <='"""
            query = query + format(fecha_mes1,'%Y-%m-%d') + """' THEN COALESCE(cargo,0) - COALESCE(abono,0) ELSE 0 END),0) FROM contabilidad_movimientocontable mov JOIN contabilidad_asiento asiento ON mov.asiento = asiento.id JOIN contabilidad_cuenta c ON mov.cuenta = c.id AND c.lft >= cuenta.lft AND c.lft <= cuenta.rght AND c.tree_id = cuenta.tree_id )AS mes1, ( SELECT COALESCE(sum(CASE WHEN asiento.fecha <='"""
            query = query + format(fecha_mes2,'%Y-%m-%d') + """' THEN COALESCE(cargo,0) - COALESCE(abono,0) ELSE 0 END),0) FROM contabilidad_movimientocontable mov JOIN contabilidad_asiento asiento ON mov.asiento = asiento.id JOIN contabilidad_cuenta c ON mov.cuenta = c.id AND c.lft >= cuenta.lft AND c.lft <= cuenta.rght AND c.tree_id = cuenta.tree_id )AS mes2
                FROM contabilidad_cuenta  cuenta 

            where level = 1  and (codigo_cuenta like '1_%' OR codigo_cuenta like '2_%' OR codigo_cuenta like '3_%' or codigo_cuenta like '4_%' or codigo_cuenta like '5_%')
                ) balance  WHERE mes1 !=0 or mes2!=0 GROUP BY 
                clasificacion,
                tipo,
                categoria,
                concepto,
                codigo_cuenta
                ORDER BY codigo_cuenta
                        ;         
            """        
        return query


                    
class AnexoView(ExportedReport, FormView):
    form_class = ReporteComparativoForm
    title = u'Balance de Comprobación'
    template_name = 'contabilidad/balancecomprobacion.html'
    report_class = BalanceComprobacionReport
    required_permissions = ('reportes.balancecomprobacion',)


    def form_valid(self, form):
        mes_inicio = int(form.cleaned_data['mes_inicio'])
        anyo_inicio = int(form.cleaned_data['anyo_inicio'])
        self.parameters.append(anyo_inicio)
        self.parameters.append(mes_inicio)


        cursor = connection.cursor()
        cursor.execute("""
SELECT 
c.codigo_cuenta as codigo,
nombre,
COALESCE(s1.saldo_anterior,0) as saldo_anterior,
COALESCE(s1.cargo,0) as debe,
COALESCE(s1.abono,0) as haber,
(COALESCE(s1.saldo_anterior + s1.cargo - s1.abono,0)) as saldo_mes,
ROUND(SUM(COALESCE(s3.saldo_anterior + s3.cargo - s3.abono,0)),2)  as acumulado
 FROM contabilidad_cuenta c
LEFT JOIN contabilidad_saldo s1 ON s1.cuenta = c.id AND s1.anyo = %s AND s1.mes = %s
LEFT JOIN contabilidad_saldo s3 ON s3.cuenta = c.id
GROUP BY  c.codigo_cuenta, c.nombre,s1.saldo_anterior,s1.cargo,s1.abono
ORDER BY c.codigo_cuenta;
        """ , self.parameters)

        data = self.dictfetchall(cursor)
        context = self.get_context_data()
        
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context,data)


class BalanceComparativoView(ReporteComparativoView):
    title = u'Reporte de Balance Comparativo'
    template_name = 'contabilidad/balancecomparativo.html'
    report_class = BalanceComparativoReport
    csv_titles = ['Diferencia','Descripcion','Mes1','Codigo','Mes2']
    parameters = ['','','','']
    required_permissions = ('reportes.balancecomparativo',)

    def query(self, fecha_mes1, fecha_mes2):    
        query = """        
            SELECT
            codigo_cuenta,
            nombre,    
            mes1,
            mes2,
            mes2 - mes1 as diferencia
            FROM (
            SELECT
                cuenta.codigo_cuenta,    
                cuenta.nombre,
            ( SELECT COALESCE(sum(CASE WHEN asiento.fecha <='"""
        query = query + format(fecha_mes1,'%Y-%m-%d') + """' THEN COALESCE(cargo,0) - COALESCE(abono,0) ELSE 0 END),0) FROM contabilidad_movimientocontable mov JOIN contabilidad_asiento asiento ON mov.asiento = asiento.id JOIN contabilidad_cuenta c ON mov.cuenta = c.id AND c.lft >= cuenta.lft AND c.lft <= cuenta.rght AND c.tree_id = cuenta.tree_id )AS mes1, 
        ( SELECT COALESCE(sum(CASE WHEN asiento.fecha <='"""
        query = query + format(fecha_mes2,'%Y-%m-%d') + """' THEN COALESCE(cargo,0) - COALESCE(abono,0) ELSE 0 END),0) FROM contabilidad_movimientocontable mov JOIN contabilidad_asiento asiento ON mov.asiento = asiento.id JOIN contabilidad_cuenta c ON mov.cuenta = c.id AND c.lft >= cuenta.lft AND c.lft <= cuenta.rght AND c.tree_id = cuenta.tree_id )AS mes2
                FROM contabilidad_cuenta  cuenta  
                ) balance  WHERE mes1 !=0 or mes2!=0                 
                ORDER BY codigo_cuenta;                 
            """   
        return query
    
#==========================================================
#    Vista Reporte Mensual
#==========================================================
    
class ReporteMesView(ExportedReport, FormView):
    form_class = ReporteMensualForm
    template_name = 'contabilidad/balancecomprobacion.html'
    report_class = BalanceComprobacionReport

    required_permissions = ('reportes.',)
    total_data = None

    def get_data(self,form=None):
        cursor = connection.cursor()
        cursor.execute(self.query , self.parameters)
        return self.dictfetchall(cursor)

    def form_valid(self, form):
        mes_inicio = int(form.cleaned_data['mes_inicio'])
        anyo_inicio = int(form.cleaned_data['anyo_inicio'])
        self.parameters[0] = anyo_inicio
        self.parameters[1] = mes_inicio

        data = self.get_data(form)
        context = self.get_context_data()
        
        self.title += ' ' + MESES[mes_inicio-1] + '/' + str(anyo_inicio)
        
        context.update({
            'form':form,
            'data':data,
        })
        if self.total_data is not None:
            context.update({'totales':self.total_data})    
        return self.render_to_response(context,data)
    

    
#==========================================================
#    Auxiliar de Cuentas Corrientes
#==========================================================
class AuxiliarCuentaView(ReporteMesView):
    title = u'Auxiliar de Cuentas Corrientes'
    required_permissions = ('reportes.auxiliarcuentas',)
    form_class = AuxiliarCuentaForm
    report_class = AuxiliarCuentaReport
    parameters = ['','']
    query = """
SELECT 
* 
FROM (
SELECT 
cliente.codigo || CASE WHEN nicabox.nicabox is not null THEN ('/' || nicabox.nicabox) ELSE '' END as codigo_cuenta,
(CASE organization WHEN '' THEN contacto.nombre || ' ' || contacto.apellido  ELSE cliente.organization END) as nombre,
SUM(CASE WHEN  fecha < %(fecha_inicio)s THEN (CASE tipo_documento WHEN 5 THEN -doc.monto ELSE doc.monto END) ELSE 0 END) as saldo_anterior,
SUM(CASE WHEN tipo_documento != 5 AND fecha >= %(fecha_inicio)s AND fecha < %(fecha_fin)s THEN doc.monto ELSE 0 END)  as debe,
SUM(CASE WHEN tipo_documento = 5 AND fecha >= %(fecha_inicio)s AND fecha < %(fecha_fin)s THEN doc.monto ELSE 0 END) as haber,
SUM(CASE WHEN fecha >= %(fecha_inicio)s AND fecha < %(fecha_fin)s THEN (CASE tipo_documento WHEN 5 THEN -doc.monto ELSE doc.monto END) ELSE 0 END) as saldo,
SUM(CASE WHEN fecha < %(fin_periodo)s THEN (CASE tipo_documento WHEN 5 THEN -doc.monto ELSE doc.monto END) ELSE 0 END) as acumulado
FROM 
crm_cliente cliente 
JOIN  crm_contacto contacto ON cliente.contacto_ptr_id = contacto.id 
LEFT JOIN postal_casillero nicabox ON nicabox.cliente_id = contacto.id
LEFT JOIN facturacion_documento doc ON doc.cliente_id = cliente.contacto_ptr_id AND anulado = False
group by cliente.codigo,nicabox.nicabox, (CASE organization WHEN '' THEN contacto.nombre || ' ' || contacto.apellido  ELSE cliente.organization END)
ORDER BY cliente.codigo ) tabla
    """
    
    def get_data(self,form):
        cursor = connection.cursor()
        fecha_inicio = date(self.parameters[0],self.parameters[1],1)
        fecha_fin = fecha_inicio + timedelta(days=calendar.monthrange(fecha_inicio.year,fecha_inicio.month)[1])
        periodo = obtener_periodo()
        fin_periodo = (date.today() if periodo is None else periodo.fin)         
        self.report_variables = {'fin_periodo': '%s-%s-%s'%(fin_periodo.day, MESES[fin_periodo.month -1], fin_periodo.year)}
        fin_periodo += timedelta(days=1)
        
        self.parameters = {'fecha_inicio':fecha_inicio , 'fecha_fin':fecha_fin, 'fin_periodo':fin_periodo }
        
        if form.cleaned_data['mostrar'] == "M":
            self.query = self.query + "WHERE saldo_anterior != 0 OR saldo != 0 OR acumulado != 0;"   
        
        
        cursor.execute(self.query , self.parameters)
        data = self.dictfetchall(cursor)
        self.total_data = {}
        self.total_data['saldo_anterior'] = 0
        self.total_data['debe'] = 0
        self.total_data['haber'] = 0
        self.total_data['saldo'] = 0
        self.total_data['acumulado'] = 0        

        for row in data:
            self.total_data['saldo_anterior'] += row['saldo_anterior']
            self.total_data['debe'] += row['debe']
            self.total_data['haber'] += row['haber']
            self.total_data['saldo'] += row['saldo']
            self.total_data['acumulado'] += row['acumulado']        

        return data 
#==========================================================
#    Balance de Comprobacion
#==========================================================
class BalanceComprobacionView(ReporteMesView):
    title = u'Balance de Comprobación'
    required_permissions = ('reportes.balancecomprobacion',)
    
    def get_data(self,form=None):
#        cursor = connection.cursor()
#        cursor.execute(self.query , self.parameters)
        anyo_inicio= self.parameters[0]
        mes_inicio = self.parameters[1] 
        cuentas = Cuenta.objects.filter(level__in=[0,1]).exclude(codigo_cuenta__startswith='7')
        fecha_inicio = date(anyo_inicio,mes_inicio,1)
        fecha_fin = fecha_inicio + timedelta(days=calendar.monthrange(fecha_inicio.year,fecha_inicio.month)[1] -1)
        self.report_variables = {'fin_periodo':fecha_fin}    

        ids = []
        
        self.total_data = {}
        self.total_data['saldo_anterior'] = 0
        self.total_data['debe'] = 0
        self.total_data['haber'] = 0
        self.total_data['saldo'] = 0
        self.total_data['acumulado'] = 0        
        for cuenta in cuentas:
            cuenta.fecha_inicio=fecha_inicio
            cuenta.fecha_fin=fecha_fin
            if cuenta.saldo_anterior == 0 and cuenta.debe == 0 and cuenta.haber == 0:
                ids.append(cuenta.id)
            elif cuenta.level == 0: 
                
                self.total_data['saldo_anterior'] += cuenta.saldo_anterior
                self.total_data['debe'] += cuenta.debe
                self.total_data['haber'] += cuenta.haber
                self.total_data['saldo'] += cuenta.saldo
                self.total_data['acumulado'] += cuenta.acumulado
                                    
        cuentas = cuentas.exclude(id__in=ids).order_by('codigo_cuenta')
        for cuenta in cuentas:
            cuenta.fecha_inicio=fecha_inicio
            cuenta.fecha_fin=fecha_fin
            
        return cuentas
        
#==========================================================
#    Mayor Auxiliar
#==========================================================
class MayorAuxiliarView(BalanceComprobacionView):
    template_name = 'contabilidad/mayorauxiliar.html'
    required_permissions = ('reportes.mayorauxiliar',)
    title = u'Mayor Auxiliar'
    report_class= MayorAuxiliarReport
    form_class = MayorAuxiliarForm
    
    def form_valid(self, form):
#        mes_inicio = int(form.cleaned_data['mes_inicio'])
#        anyo_inicio = int(form.cleaned_data['anyo_inicio'])
        cuenta_inicio = form.cleaned_data['cuenta_inicio']
        cuenta_fin =  form.cleaned_data['cuenta_fin']        

        data = Cuenta.objects.filter(id__gte=cuenta_inicio,id__lte=cuenta_fin)
        
        
        fecha_inicio = form.cleaned_data['fecha_inicio']#date(anyo_inicio,mes_inicio,1)
        fecha_fin = form.cleaned_data['fecha_fin']#fecha_inicio + timedelta(days=calendar.monthrange(fecha_inicio.year,fecha_inicio.month)[1] -1)
        
        exclude_ids = []
        for cuenta in data:        
            if fecha_inicio:
                cuenta.fecha_inicio=fecha_inicio
                
            if fecha_fin:
                cuenta.fecha_fin=fecha_fin
                
            if cuenta.saldo_anterior == 0 and cuenta.debe == 0 and  cuenta.haber == 0 and cuenta.acumulado == 0:
                exclude_ids.append(cuenta.id)  
                      
        data = data.exclude(id__in=exclude_ids)
        for cuenta in data:        
            if fecha_inicio:
                cuenta.fecha_inicio=fecha_inicio
                
            if fecha_fin:
                cuenta.fecha_fin=fecha_fin       
        
        context = self.get_context_data()
        
        self.title += ' Desde %s Hasta %s' %(format(fecha_inicio,"%d/%m/%y"),format(fecha_fin,"%d/%m/%y"))
        
        context.update({
            'form':form,
            'data':data,
            #'totales':total_data
        })
        return self.render_to_response(context,data)    

#==========================================================
#    Mayor Auxiliar
#==========================================================
class MayorGeneralView(ReporteMesView):
    title = u'Mayor General'
    required_permissions = ('reportes.mayorgeneral',)
    report_class= MayorGeneralReport
    template_name = 'contabilidad/mayorgeneral.html'

    query = """
    SELECT
        cuenta.parent,
        cuenta.codigo_cuenta,
        cuenta.nombre,
        -- a.fecha,
        CONCAT('Dia ' , EXTRACT (DAY FROM a3.fecha )) as dia,
        SUM(CASE COALESCE(a2.fecha, '1900-01-01' ) WHEN ( '1900-01-01' ) THEN 0 else ( COALESCE(mov.cargo,0) - COALESCE(mov.abono,0)) END ) as saldo_anterior,
        SUM(CASE COALESCE(a3.fecha, '1900-01-01' ) WHEN ( '1900-01-01' ) THEN 0 else COALESCE(mov.cargo,0) END) as debe,
        SUM(CASE COALESCE(a3.fecha, '1900-01-01' ) WHEN ( '1900-01-01' ) THEN 0 else COALESCE(mov.abono,0) END) as haber,
        SUM(CASE COALESCE(a2.fecha, '1900-01-01' ) WHEN ( '1900-01-01' ) THEN 0 else ( COALESCE(mov.cargo,0) - COALESCE(mov.abono,0)) END ) +
        SUM(CASE COALESCE(a3.fecha, '1900-01-01' ) WHEN ( '1900-01-01' ) THEN 0 else COALESCE(mov.cargo,0) END) -
        SUM(CASE COALESCE(a3.fecha, '1900-01-01' ) WHEN ( '1900-01-01' ) THEN 0 else COALESCE(mov.abono,0) END) as saldo_mes,
        SUM(CASE COALESCE(a.fecha, '1900-01-01' ) WHEN ( '1900-01-01' ) THEN 0 else ( COALESCE(mov.cargo,0) - COALESCE(mov.abono,0)) END ) as acumulado
    FROM
    contabilidad_cuenta cuenta
    LEFT JOIN contabilidad_movimientocontable mov ON mov.cuenta= cuenta.id
    LEFT JOIN contabilidad_asiento a ON a.id = mov.asiento
    LEFT JOIN contabilidad_asiento a3 ON a3.id = mov.asiento AND EXTRACT (YEAR FROM a3.fecha)=%s AND EXTRACT (MONTH FROM a3.fecha) = %s
    LEFT JOIN contabilidad_asiento a2 ON a2.id = mov.asiento AND a2.fecha <= %s
    GROUP BY codigo_cuenta,nombre,cuenta.parent,dia
    ORDER BY cuenta.codigo_cuenta, dia ASC
    """
    def get_data(self):
        cursor = connection.cursor()
        anyo = self.parameters[0]
        mes = self.parameters[1]

        if len(self.parameters) == 2:
            self.parameters.append("'" + str(anyo) + '-' + str(mes)+"-01'")

        cursor.execute(self.query , self.parameters)
        self.total_data =  self.dictfetchall(cursor)

        cursor = connection.cursor()
        cursor.execute(self.query , self.parameters)
        return self.dictfetchall(cursor)


class LibroDiarioView(ExportedReport, FormView):
    form_class = LibroDiarioForm
    title = u'DIARIO GENERAL'
    template_name = 'contabilidad/librodiario.html'
    report_class = LibroDiarioReport
    csv_titles = ['Comprobante','Tipo','Concepto','Cargo','Abono']
    required_permissions = ('reportes.librodiario',)

    
    def form_valid(self, form):
        fecha_inicio = form.cleaned_data['fecha_inicio']
        fecha_fin = form.cleaned_data['fecha_fin']
        tipo = form.cleaned_data['tipo']
        
        
        data = Asiento.objects.all()
        
        if fecha_inicio is not None:
            data = data.filter(fecha__gte = fecha_inicio)
            self.description = "Desde " + format(fecha_inicio,'%d/%m/%y' )

        if fecha_fin is not None:
            data = data.filter(fecha__lte = fecha_fin)
            self.description = self.description + " Hasta " + format(fecha_fin,'%d/%m/%y' )
        
        if tipo != '0' :
            data = data.filter(tipo_partida = tipo)


        
        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })

        return self.render_to_response(context,data)
