from django.conf.urls.defaults import patterns, url
from facturacion.views import *

urlpatterns = patterns('facturacion.views',
    url(r'^$', 'index'),
    url('^(?P<pk>\d+)/$', 'index'),
    url('^documento/$', NotaView.as_view(template_name='facturacion/notas.html'), name='documento'),
    url(r'^imprimirrequerimiento/$', ImprimirRequerimientoView.as_view(), name='imprimirrequerimiento'),
    url(r'^imprimirabono/$', ImprimirAbonoView.as_view(), name='imprimirabono'),
    url(r'^imprimirfactura/$', ImprimirFacturaView.as_view(), name='imprimirfactura'),
    url(r'^imprimirnotacredito/$', ImprimirNotaCreditoView.as_view(), name='imprimirnotacredito'),
    url(r'^imprimirnotadebito/$', ImprimirNotaDebitoView.as_view(), name='imprimirnotadebito'),
    url(r'^requerimientosautomaticos/$',GenerarRequerimientosView.as_view(), name='reqautomaticos'),
    url('^servicios/$', ServicioView.as_view(), name='servicios'),
    
)

