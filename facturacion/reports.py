# -*- coding: utf-8 -*-
import os
from geraldo import Report, ReportBand, DetailBand, SystemField, Label, ObjectValue,  ReportGroup, SubReport
from geraldo.utils import cm, BAND_WIDTH, TA_CENTER, TA_RIGHT
from reportlab.lib.pagesizes import LETTER
from reportes.report import ReporteMembretado
from crm.models import Parametro
from reportlab.lib.units import inch
from settings import PROJECT_DIR

t = Parametro.objects.get(pk=2)
#=================================================
# Resumen Report
#=================================================
class ResumenReport(ReporteMembretado):
    default_style = {'fontName':'Courier'}
    class band_detail(DetailBand):
        height = 0.7*cm
        elements = [
            ObjectValue(expression='numero', left=1*cm, width = 3*cm),
            ObjectValue(expression='cliente.id', left=3*cm),
            ObjectValue(expression='cliente', left=5*cm, width = 7*cm),
            ObjectValue(expression='subtotal__', left=12*cm),
            ObjectValue(expression='iva__', left=15*cm),
            ObjectValue(expression='total__', left=18*cm),
        ]

    class band_page_header(ReportBand):
        height = 1.3*cm
        elements = [
            SystemField(expression='%(report_title)s', top=0.1*cm, left=0, width=BAND_WIDTH,
                style={'fontName': 'Courier', 'fontSize': 14, 'alignment': TA_CENTER}),
            SystemField(expression=u'Página %(page_number)d de %(page_count)d', top=0.1*cm,
                width=BAND_WIDTH, style={'alignment': TA_RIGHT}),
            Label(text="Numero Doc.", top=0.8*cm, left=0, style={'fontName': 'Courier'}),
            Label(text="Codigo", top=0.8*cm, left=3*cm, style={'fontName': 'Courier'}),
            Label(text="Cliente", top=0.8*cm, left=5*cm, width= 7*cm, style={'fontName': 'Courier'}),
            Label(text="Sub-Total", top=0.8*cm, left=12*cm, style={'fontName': 'Courier'}),
            Label(text="Iva", top=0.8*cm, left=15*cm, style={'fontName': 'Courier'}),
            Label(text="Total", top=0.8*cm, left=18*cm, style={'fontName': 'Courier'}),
        ]

    class band_summary(ReportBand):
        height = 0.5*cm
        elements = [
            Label(text='Totales:'),
            ObjectValue(expression='count(numero)', left=7*cm, style={'fontName': 'Courier'}),
            ObjectValue(expression='sum(subtotal__)', left=12*cm, style={'fontName': 'Courier'}),
            ObjectValue(expression='sum(iva__)', left=15*cm, style={'fontName': 'Courier'}),
            ObjectValue(expression='sum(total__)', left=18*cm, style={'fontName': 'Courier'}),
            ]
        borders = {'top': True}



#=================================================
# Abono Report
#=================================================
class AbonoReport(ReporteMembretado):
    title = ''
    default_style = {'fontName':'Courier'}

    class band_detail(DetailBand):
        height = 14*cm
        elements = [
            Label(text="TRANS-EXPRESS,S.A ", top=0.5*cm, left=8*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="RUC No. J0310000012008", top=1*cm, left=7.5*cm),
            Label(text="RECIBO OFICIAL DE CAJA", top=2*cm, left=8.2*cm),
            Label(text="No.", top=2*cm, left=15*cm),
            ObjectValue(expression='numero', top=2*cm, left=16.5*cm),
            Label(text="(CORDOBAS)", top=2.5*cm, left=9*cm),
            Label(text="Managua,", top=3.5*cm, left=1*cm),
            ObjectValue(expression='fecha_formato', top=3.5*cm, left=3.2*cm),
            Label(text="Cliente No.", top=3.5*cm, left=7*cm),
            ObjectValue(expression='cliente.codigo', top=3.5*cm, left=10.3*cm),

            Label(text="Recibidos de: ", top=4.5*cm, left=1*cm),
            ObjectValue(expression='cliente', top=4.5*cm, left=4*cm,width=15*cm ),
            Label(text="Para ser aplicado de la siguiente forma:", top=6.5*cm, left=1*cm, width=15*cm ),
            Label(text="Fact. No.", top=7*cm, left=5*cm),
            Label(text="Valor C$", top=7*cm, left=9*cm),
            Label(text="Retencion/I.R", top=7*cm, left=13*cm),
            ObjectValue(expression='documentos', top=7.5*cm, left=1*cm,width=10*cm ),
            Label(text="Efectivo C$", top=8*cm, left=3*cm),
            Label(text="CK $", top=8*cm, left=6*cm),
            Label(text="Banco", top=8*cm, left=9*cm),
            Label(text="Efectivo $", top=8*cm, left=13*cm),
            Label(text="Tarjeta de Credito C$", top=8.5*cm, left=3*cm),
            Label(text="Valor C$", top=9*cm, left=8.5*cm),
            ObjectValue(expression='monto', top=9*cm, left=10.5*cm),
            Label(text="La suma de ", top=10*cm, left=1*cm, width=15*cm ),
            ObjectValue(expression='monto_to_words', top=10*cm, left=4*cm,width=15*cm ),


            Label(text="IMPORTANTE: Este recibo no es valido sin la firma responsable o si contiene, borrones o enmiendas.", top=11*cm, left=1*cm, width= 12*cm),
            Label(text="FIRMA RESPONSABLE:", top=11*cm, left=14*cm,width=15*cm ),
        ]
#        borders = {'all':True}



class NotaCreditoReport(ReporteMembretado):
    title = ''
    default_style = {'fontName':'Courier'}
    page_size=  (8.5*inch, 5.5*inch)
    class band_page_header(ReportBand):
        height = 1.3*cm
        elements = [
            SystemField(expression='%(report_title)s', top=0.1*cm, left=0, width=BAND_WIDTH,
                style={'fontName': 'Courier', 'fontSize': 14, 'alignment': TA_CENTER}),
            SystemField(expression=u'Página %(page_number)d de %(page_count)d', top=0.1*cm,
                width=BAND_WIDTH, style={'alignment': TA_RIGHT}),
        ]

    class band_detail(DetailBand):
        height = 9.5*cm
        elements = [
            Label(text="TRANS-EXPRESS,S.A ", top=0.5*cm, left=8*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="RUC No. J0310000012008", top=1*cm, left=7.5*cm),
            Label(text="NOTA DE CREDITO", top=2*cm, left=8.2*cm),
            Label(text="No.", top=2*cm, left=12*cm),
            ObjectValue(expression='numero', top=2*cm, left=13*cm),
            Label(text="(DOLARES)", top=2.5*cm, left=9*cm),
            Label(text="Managua,", top=3.5*cm, left=1*cm),
            ObjectValue(expression='fecha_formato', top=3.5*cm, left=3.2*cm),
            Label(text="Cliente No.", top=3.5*cm, left=7*cm),
            ObjectValue(expression='cliente.codigo', top=3.5*cm, left=9.3*cm),
            Label(text="Valor $", top=3.5*cm, left=12*cm),
            ObjectValue(expression='monto', top=3.5*cm, left=15.5*cm),
            Label(text="Nombre: ", top=4.5*cm, left=1*cm),
            ObjectValue(expression='cliente', top=4.5*cm, left=3.2*cm,width=15*cm ),
            Label(text="Direccion: ", top=5.5*cm, left=1*cm),
            ObjectValue(expression='direccion',  top=5.5*cm, left=3.2*cm, width=15*cm ),
            Label(text="Estimado cliente, rogamos tomar nota, que en esta fecha hemos cargado a su apreciable cuenta, la cantidad de:", top=6.5*cm, left=1*cm, width=15*cm ),
            ObjectValue(expression='monto_to_words', top=7.5*cm, left=1*cm,width=15*cm ),
            Label(text="por el siguiente concepto:", top=8.5*cm, left=1*cm, width=15*cm ),
            ObjectValue(expression='concepto', top=8.5*cm, left=6.5*cm,width=15*cm ),
            Label(text="Apliado a:", top=9*cm, left=1*cm),
            ObjectValue(expression='documentos', top=9.5*cm, left=1*cm,width=10*cm ),
            Label(text="Subtotal $:", top=9*cm, left=12*cm),
            Label(text="IVA 15%:", top=9.5*cm, left=12*cm),
            Label(text="Total $", top=10*cm, left=12*cm),
            ObjectValue(expression='subtotal', top=9*cm, left=16*cm,width=10*cm ),
            ObjectValue(expression='iva', top=9.5*cm, left=16*cm,width=10*cm ),
            ObjectValue(expression='monto', top=10*cm, left=16*cm,width=10*cm ),
            Label(text="Elaborado:", top=13.5*cm, left=1*cm,width=15*cm ),
            Label(text="Revisado:", top=13.5*cm, left=9*cm,width=15*cm ),
            Label(text="Aprobado:", top=13.5*cm, left=14*cm,width=15*cm ),
        ]


class NotaDebitoReport(Report):
    title = ''
    additional_fonts = { 'epson': os.path.join(PROJECT_DIR,'static/fonts/epson.ttf'),}
    default_style = {'fontName':'epson'}
    margin_top = 0
    margin_bottom = 0
    page_size= (8.5*inch, 5.5*inch)
    class band_detail(DetailBand):
        height = 9.5*cm
        elements = [
            Label(text="TRANS-EXPRESS,S.A ", top=0.5*cm, left=8*cm),
            Label(text="RUC No. J0310000012008", top=1*cm, left=7.5*cm),
            Label(text="NOTA DE DEBITO",top=2*cm, left=8.2*cm),
            Label(text="No.", top=2*cm, left=12*cm),
            ObjectValue(expression='numero', top=2*cm, left=13*cm),
            Label(text="(DOLARES)", top=2.5*cm, left=9*cm),
            Label(text="Managua,", top=3.5*cm, left=1*cm),
            ObjectValue(expression='fecha_formato', top=3.5*cm, left=3.2*cm),
            Label(text="Cliente No.", top=3.5*cm, left=7*cm),
            ObjectValue(expression='cliente.codigo', top=3.5*cm, left=9.5*cm),
            Label(text="Valor $", top=3.5*cm, left=12*cm),
            ObjectValue(expression='monto', top=3.5*cm, left=15.5*cm),
            Label(text="Nombre: ", top=4.5*cm, left=1*cm),
            ObjectValue(expression='cliente', top=4.5*cm, left=3.2*cm,width=15*cm ),
            Label(text="Direccion: ", top=5.5*cm, left=1*cm),
            Label(text="Estimado cliente, rogamos tomar nota, que en esta fecha hemos cargado a su apreciable cuenta, la cantidad de:", top=6.5*cm, left=1*cm, width=15*cm ),
            ObjectValue(expression='monto_to_words', top=7.5*cm, left=1*cm,width=15*cm ),
            Label(text="por el siguiente concepto :", top=8.5*cm, left=1*cm),
            ObjectValue(expression='concepto', top=8.5*cm, left=5.5*cm,width=15*cm ),
            ObjectValue(expression='descripcion', top=9*cm, left=5.5*cm,width=15*cm ),
            Label(text="Elaborado:", top=10.5*cm, left=1*cm,width=15*cm ),
            Label(text="Revisado:", top=10.5*cm, left=9*cm,width=15*cm ),
            Label(text="Aprobado:", top=10.5*cm, left=14*cm,width=15*cm ),
        ]


class RequerimientoReport(ReporteMembretado):
    title = ''
    page_size=  (8.5*inch, 5.5*inch)


    class band_detail(DetailBand):
        height = 5*cm
        elements = [
            ObjectValue(expression='cliente', top=0.5*cm, left=0.1*cm,width=14*cm),
            Label(text="REQUERIMIENTO", top=0.5*cm, left=15*cm),

            ObjectValue(expression='direccion',top=1*cm, left=0.1*cm, width=14*cm ),
            Label(text="No.", top=1*cm, left=15*cm),
            ObjectValue(expression='numero', top=1*cm, left=16*cm),

            ObjectValue(expression='ciudad',top=2*cm, left=0.1*cm, width=14*cm ),
            Label(text="Cliente Cod.", top=1.5*cm, left=15*cm),
            ObjectValue(expression='cliente.codigo', top=1.5*cm, left=18*cm),


            Label(text="FECHA", top=2.5*cm, left=0.1*cm),
            ObjectValue(expression='fecha_formato', top=2.5*cm, left=1.5*cm),
            Label(text=u"Condición de pago: ", top=2.5*cm, left=3.5*cm, width=9*cm),
            ObjectValue(expression='condicion_pago', top=2.5*cm, left=8*cm),

            Label(text=u"El valor de esta factura se cancelará en moneda nacional al tipo de cambio paralelo", top=3.5*cm, left=0, width = 20*cm),


            Label(text="Servicio", top=4.5*cm, left=0),
            Label(text="Fecha",top=4.5*cm, left=4*cm),
            Label(text="Descripcion",top=4.5*cm, left=6.5*cm, width= 10*cm),
            Label(text="Total", top=4.5*cm, left=18*cm)



        ]
        borders = {'top':True}

    class band_page_header(ReportBand):
        elements = [
            SystemField(expression='%(report_title)s', top=0, left=0, width=BAND_WIDTH,
                style={'fontName': 'Courier', 'fontSize': 14}),
            SystemField(expression=u'Página %(page_number)d de %(page_count)d', top=0.1*cm,
                width=BAND_WIDTH, style={'alignment': TA_RIGHT}),
            Label(text="TRANS-EXPRESS,S.A ", top=0, left=8*cm),
            Label(text="RUC No. J0310000012008", top=0.5*cm, left=7.5*cm),
        ]


    subreports = [
        SubReport(
                  queryset_string = '%(object)s.detalle',
                  band_detail = ReportBand(
                        height=0.5*cm,
                        elements=[
                            ObjectValue(attribute_name='requerimiento.concepto', top=0, left=0),
                            ObjectValue(attribute_name='concepto', top=0, left=6.5*cm,width=10*cm),
                            ObjectValue(attribute_name='total', top=0, left=17*cm, width=3*cm, style={'alignment': TA_RIGHT}),
                        ],
                    ),
                    band_footer = ReportBand(
                        height=2*cm,
                        elements=[
                            Label(text="SUB-TOTAL", top=0.5*cm, left=13*cm),
                            Label(text="IVA", top=1*cm, left=13*cm),
                            Label(text="TOTAL US$", top=1.5*cm, left=13*cm ),
                            ObjectValue(expression='subtotal',top=0.5*cm, left=17*cm , width=3*cm, style={'alignment': TA_RIGHT}),
                            ObjectValue(expression='iva',top=1*cm, left=17*cm, width=3*cm, style={'alignment': TA_RIGHT}),
                            ObjectValue(expression='total',top=1.5*cm, left=17*cm, width=3*cm, style={'alignment': TA_RIGHT}),
                            ],
                    ),

        ),

    ]




class FacturaReport(ReporteMembretado):
    title = ''
    default_style = {'fontName':'Courier'}
    page_size=  (8.5*inch, 5.5*inch)


    class band_detail(DetailBand):
        height = 6*cm
        elements = [
            Label(text="TRANS-EXPRESS,S.A ", top=0, left=8*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="RUC No. J0310000012008", top=0.5*cm, left=7.5*cm),

            ObjectValue(expression='cliente', top=1.5*cm, left=0.1*cm,width=14*cm),
            Label(text="FACTURA", top=1.5*cm, left=15*cm),

            ObjectValue(expression='direccion',top=2*cm, left=0.1*cm, width=14*cm ),
            Label(text="No.", top=2*cm, left=15*cm),
            ObjectValue(expression='numero', top=2*cm, left=16*cm),

            ObjectValue(expression='ciudad',top=3*cm, left=0.1*cm, width=14*cm ),
            Label(text="Cliente Cod.", top=2.5*cm, left=15*cm),
            ObjectValue(expression='cliente.codigo', top=2.5*cm, left=18*cm),


            Label(text="FECHA", top=3.5*cm, left=0.1*cm),
            ObjectValue(expression='fecha_formato', top=3.5*cm, left=1.5*cm),
            ObjectValue(expression='es_credito', top=3.5*cm, left=3.5*cm),
            Label(text="Condición de pago: ", top=3.5*cm, left=5.5*cm, width=9*cm),
            ObjectValue(expression='condicion_pago', top=3.5*cm, left=9.5*cm),

            Label(text="El valor de esta factura se cancelará en moneda nacional al tipo de cambio paralelo", top=4.5*cm, left=0.1*cm, width = 20*cm),


            Label(text="Referencia", top=5.5*cm, left=0.1*cm),
            Label(text="Fecha",top=5.5*cm, left=3*cm, width= 1*cm),
            Label(text="Concepto",top=5.5*cm, left=5.5*cm, width=3*cm),
            Label(text="Total", top=5.5*cm, left=17*cm)

        ]
#        borders = {'top':True,'left':True,'right':True}

    class band_page_header(ReportBand):
        elements = [
            SystemField(expression='%(report_title)s', top=0, left=0, width=BAND_WIDTH,
                style={'fontName': 'Courier', 'fontSize': 14}),
            SystemField(expression=u'Página %(page_number)d de %(page_count)d', top=0.1*cm,
                width=BAND_WIDTH, style={'alignment': TA_RIGHT}),
        ]
#        borders = {'all': False}


    subreports = [
        SubReport(
                  queryset_string = '%(object)s.items.all()',
                  band_detail = ReportBand(
                        height=0.5*cm,
                        elements=[
                            Label(text="Req ", top=0*cm, left=0.1*cm),
                            ObjectValue(attribute_name='numero', top=0, left=1*cm),
                            ObjectValue(attribute_name='fecha_formato', top=0, left=3*cm, width= 1*cm),
                            ObjectValue(attribute_name='concepto', top=0, left=5.5*cm, width= 3*cm),
                            ObjectValue(attribute_name='monto', top=0, left=17*cm),
                        ],
#                        borders = {'left':True,'right':True}
                    ),
                    band_footer = ReportBand(
                        height=2*cm,
                        elements=[
                            Label(text="SUB-TOTAL", top=0.5*cm, left=13*cm),
                            Label(text="IVA", top=1*cm, left=13*cm),
                            Label(text="TOTAL US$", top=1.5*cm, left=13*cm ),
                            ObjectValue(expression='subtotal',top=0.5*cm, left=17*cm),
                            ObjectValue(expression='iva',top=1*cm, left=17*cm),
                            ObjectValue(expression='total',top=1.5*cm, left=17*cm),
                            ],
#                        borders = {'bottom':True,'left':True,'right':True}
                    ),

        ),

    ]

class ServicioReport( Report ):
    margin_top = 0.5*cm
    margin_bottom = 0.3*cm
    margin_left = 0.3*cm
    margin_right = 0.3*cm
    page_size = LETTER
    default_style = {'fontName':'Courier','fontSize': 8 }
    class band_page_header(ReportBand):
        height = 3.2*cm
        elements = [
            Label(text=t.valor, top=0.5*cm, left=0.5*cm),
            SystemField(expression=u'%(report_title)s', top=1*cm, left=0.5*cm, width=10*cm),
            SystemField(expression=u'Pagina: %(page_number)d', top=0.5*cm, left=14*cm),
            Label(text="Emitido:", top=1*cm, left=14*cm),
            SystemField(expression='%(now:%d/%m/%Y)s', top=1*cm, left=15.5*cm),
            SystemField(expression='%(now:%H:%M:%S)s', top=1.5*cm, left=15.5*cm),
            SystemField(expression=u'%(var:fecha)s', top=1.5*cm, left=0.5*cm),
            Label(text="Codigo", top=2.7*cm, left=0.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Cliente", top=2.7*cm, left=3*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Fecha", top=2.7*cm, left=6.3*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="# Documento", top=2.7*cm, left=9.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Descripcion", top=2.7*cm, left=12.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Total", top=2.7*cm, left=19*cm, style={'fontName': 'Courier-Bold'}),
        ]
        borders = {'top': True, 'bottom':True}

    class band_detail(ReportBand):
        auto_expand_height = True
        elements=(
            ObjectValue(attribute_name='cliente.codigo', left=0.5*cm),
            ObjectValue(attribute_name='cliente', left=2.7*cm, width=7*cm),
            ObjectValue(attribute_name='fecha', left=6.3*cm),
            ObjectValue(attribute_name='numero', left=9.5*cm),
            ObjectValue(attribute_name='descripcion', left=12.5*cm, width =7*cm),
            ObjectValue(attribute_name='monto', left=19*cm),
        )

    class band_summary(ReportBand):
        height = 0.5*cm
        default_style = {'fontName': 'Courier-Bold'}
        elements = [
            Label(text='Totales:', left=14*cm),
            ObjectValue(expression='count(numero)', left=15.5*cm ),
            ObjectValue(expression='sum(monto)', left=19*cm ),
            ]
        borders = {'top': True}
