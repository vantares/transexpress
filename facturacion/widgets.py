from django.contrib.admin.widgets import ForeignKeyRawIdWidget
import string
from crm.models  import Cliente

class ClienteRawIdWidget(ForeignKeyRawIdWidget):

    def __init__(self,attrs=None, using=None):
        self.rel = Cliente._meta.get_field("cliente").rel
        self.db = using
        super(ClienteRawIdWidget, self).__init__(self.rel, attrs=None, using=None)
    
    def render(self, *args, **kwargs):
        from django.utils.safestring import mark_safe
        original_render = super(ClienteRawIdWidget, 
            self).render(*args, **kwargs)
        ADMIN_ROOT_URL = "/admin/"
        name = args[0]
        value = args[1]
        codigo = ''
        nombre = ''
        
        if value:
            try:
                objeto = Cliente.objects.get(pk = int(value))
                codigo = objeto.codigo_cuenta
                nombre = objeto.nombre            
            except (ValueError, self.rel.to.DoesNotExist):
                pass

        original_render = original_render.replace('<a',"<input id = 'codigo_%s' onchange=obtener_nombre($(this).attr('id')) type='text' value = '%s'> <a " %(name,codigo))
        original_render =  original_render + "<input id = 'nombre_%s' type='text' style='font-weight:bold; width:500px;' readonly='readonly' value = '%s'>"%(name,nombre)
        original_render = original_render.replace('name="%s"' %name,'name="%s" style="display:none;"' %name)

        return mark_safe(string.replace(original_render,"../../../", ADMIN_ROOT_URL))

    def label_for_value(self, value):
        return ''
