# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'ConceptoND.tipo_concepto'
        db.add_column('facturacion_conceptond', 'tipo_concepto',
                      self.gf('django.db.models.fields.IntegerField')(default=2),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'ConceptoND.tipo_concepto'
        db.delete_column('facturacion_conceptond', 'tipo_concepto')


    models = {
        'caja.casacambio': {
            'Meta': {'object_name': 'CasaCambio'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'descripcion'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'nombre'"})
        },
        'caja.comprobantecaja': {
            'Meta': {'object_name': 'ComprobanteCaja'},
            'monto': ('django.db.models.fields.DecimalField', [], {'db_column': "'monto'", 'decimal_places': '4', 'max_digits': '10'}),
            'numero': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'numero'"}),
            'signo': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'es_negativo'"})
        },
        'caja.formapago': {
            'Meta': {'object_name': 'FormaPago'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'codigo'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'nombre'"})
        },
        'caja.servicio': {
            'Meta': {'object_name': 'Servicio'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'descripcion'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'nombre'"})
        },
        'caja.tarifaservicio': {
            'Meta': {'object_name': 'TarifaServicio'},
            'adicional': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'null': 'True', 'db_column': "'tarifa_adicional'", 'decimal_places': '2', 'max_digits': '10'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'limite_inferior': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'null': 'True', 'db_column': "'limite_inferior'", 'decimal_places': '4', 'max_digits': '10'}),
            'limite_superior': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'null': 'True', 'db_column': "'limite_superior'", 'decimal_places': '4', 'max_digits': '10'}),
            'monto': ('django.db.models.fields.DecimalField', [], {'db_column': "'monto'", 'decimal_places': '2', 'max_digits': '10'}),
            'por_paquete': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'por_paquete'"}),
            'recargo': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'null': 'True', 'db_column': "'recargo'", 'decimal_places': '2', 'max_digits': '10'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.Region']", 'null': 'True', 'db_column': "'region'", 'blank': 'True'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['caja.Servicio']", 'db_column': "'tipo'"}),
            'tipo_cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.TipoCliente']", 'db_column': "'tipo_cliente'"})
        },
        'caja.tipocambio': {
            'Meta': {'object_name': 'TipoCambio'},
            'base': ('django.db.models.fields.DecimalField', [], {'db_column': "'base'", 'decimal_places': '4', 'max_digits': '6'}),
            'casa_cambio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['caja.CasaCambio']"}),
            'compra': ('django.db.models.fields.DecimalField', [], {'db_column': "'compra'", 'decimal_places': '4', 'max_digits': '6'}),
            'fecha': ('django.db.models.fields.DateField', [], {'unique': 'True', 'db_column': "'fecha'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'moneda': ('django.db.models.fields.CharField', [], {'max_length': '4', 'db_column': "'moneda'"}),
            'venta': ('django.db.models.fields.DecimalField', [], {'db_column': "'venta'", 'decimal_places': '4', 'max_digits': '6'})
        },
        'crm.cliente': {
            'Meta': {'object_name': 'Cliente', '_ormbases': ['crm.Contacto']},
            'activo': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'activo'"}),
            'bloqueado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'clasificacion': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'clasificacion'"}),
            'codigo': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'contacto_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['crm.Contacto']", 'unique': 'True', 'primary_key': 'True'}),
            'exonerado': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'exonerado'"}),
            'organization': ('django.db.models.fields.CharField', [], {'default': "'N/A'", 'max_length': '35', 'null': 'True', 'db_column': "'organization'", 'blank': 'True'}),
            'ruc': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'db_column': "'ruc'", 'blank': 'True'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.TipoCliente']", 'null': 'True', 'blank': 'True'}),
            'tipocobro': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.TipoCobro']", 'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '100', 'null': 'True', 'db_column': "'url'", 'blank': 'True'}),
            'zona': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.Ruta']", 'null': 'True', 'blank': 'True'})
        },
        'crm.contacto': {
            'Meta': {'object_name': 'Contacto'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'apellido'"}),
            'categoria': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'categoria'", 'blank': 'True'}),
            'cedula': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'db_column': "'cedula'", 'blank': 'True'}),
            'estadocivil': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'estadocivil'", 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'db_column': "'foto'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'licencia': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'licencia'", 'blank': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'db_column': "'logo'", 'blank': 'True'}),
            'nacimiento': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'nacimiento'", 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'nombre'"}),
            'nombrecorto': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'nombrecorto'", 'blank': 'True'}),
            'notas': ('django.db.models.fields.TextField', [], {'null': 'True', 'db_column': "'notas'", 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'title'", 'blank': 'True'})
        },
        'crm.ruta': {
            'Meta': {'object_name': 'Ruta'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ruta': ('django.db.models.fields.TextField', [], {'max_length': '300', 'db_column': "'ruta'"}),
            'zona': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'zona'"})
        },
        'crm.tipocliente': {
            'Meta': {'object_name': 'TipoCliente'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'nombre'"})
        },
        'crm.tipocobro': {
            'Meta': {'object_name': 'TipoCobro'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meses': ('django.db.models.fields.IntegerField', [], {}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'facturacion.abono': {
            'Meta': {'object_name': 'Abono', '_ormbases': ['facturacion.Documento']},
            'alcaldia': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'null': 'True', 'db_column': "'alcaldia'", 'decimal_places': '2', 'max_digits': '8'}),
            'banco': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'banco'", 'blank': 'True'}),
            'cheque': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'cheque'", 'blank': 'True'}),
            'concepto': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['facturacion.ConceptoAbono']"}),
            'documento_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['facturacion.Documento']", 'unique': 'True'}),
            'facturas': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'facturas_abonadas'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['facturacion.Factura']"}),
            'forma_pago': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['caja.FormaPago']", 'db_column': "'forma_pago'"}),
            'ir': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'null': 'True', 'db_column': "'ir'", 'decimal_places': '2', 'max_digits': '8'}),
            'moneda': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'moneda'"}),
            'notas': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'notas_abonadas'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['facturacion.NotaDebito']"}),
            'numero': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'numero'"}),
            'tipo_cambio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['caja.TipoCambio']", 'null': 'True', 'blank': 'True'})
        },
        'facturacion.cobroadicional': {
            'Meta': {'object_name': 'CobroAdicional'},
            'fecha': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'monto': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '9', 'decimal_places': '2'}),
            'paquete': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.Paquete']", 'null': 'True', 'blank': 'True'}),
            'tipocobro': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['facturacion.ConceptoND']"})
        },
        'facturacion.conceptoabono': {
            'Meta': {'object_name': 'ConceptoAbono'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '80', 'db_column': "'nombre'"})
        },
        'facturacion.conceptonc': {
            'Meta': {'object_name': 'ConceptoNC'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '80', 'db_column': "'nombre'"})
        },
        'facturacion.conceptond': {
            'Meta': {'object_name': 'ConceptoND'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'tipo_concepto': ('django.db.models.fields.IntegerField', [], {'default': '2'})
        },
        'facturacion.documento': {
            'Meta': {'object_name': 'Documento'},
            'anulado': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'anulado'"}),
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.Cliente']"}),
            'fecha': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2013, 2, 2, 0, 0)', 'db_column': "'fecha'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'monto': ('django.db.models.fields.DecimalField', [], {'db_column': "'monto'", 'decimal_places': '2', 'max_digits': '12'}),
            'tipo_documento': ('django.db.models.fields.IntegerField', [], {'db_column': "'tipo_documento'"}),
            'vencimiento': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'vencimiento'", 'blank': 'True'})
        },
        'facturacion.factura': {
            'Meta': {'object_name': 'Factura', '_ormbases': ['facturacion.Documento']},
            'credito': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'credito'"}),
            'documento_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['facturacion.Documento']", 'unique': 'True'}),
            'numero': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'numero'"}),
            'origen': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'origen'"}),
            'sucursal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['facturacion.Sucursal']", 'null': 'True', 'blank': 'True'})
        },
        'facturacion.itemfactura': {
            'Meta': {'object_name': 'ItemFactura'},
            'comprobante_caja': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['caja.ComprobanteCaja']", 'null': 'True', 'blank': 'True'}),
            'concepto': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'concepto'"}),
            'monto': ('django.db.models.fields.DecimalField', [], {'db_column': "'monto'", 'decimal_places': '2', 'max_digits': '8'}),
            'numero': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'numero'"}),
            'requerimiento': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['facturacion.Requerimiento']", 'null': 'True', 'blank': 'True'}),
            'tarifa': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['caja.TarifaServicio']"}),
            'unidades': ('django.db.models.fields.IntegerField', [], {'db_column': "'unidades'"})
        },
        'facturacion.notacredito': {
            'Meta': {'object_name': 'NotaCredito', '_ormbases': ['facturacion.Documento']},
            'concepto': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['facturacion.ConceptoNC']", 'db_column': "'concepto'"}),
            'documento_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['facturacion.Documento']", 'unique': 'True'}),
            'facturas': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'facturas'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['facturacion.Factura']"}),
            'notas': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'notas'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['facturacion.NotaDebito']"}),
            'numero': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'numero'"})
        },
        'facturacion.notadebito': {
            'Meta': {'object_name': 'NotaDebito', '_ormbases': ['facturacion.Documento']},
            'concepto': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['facturacion.ConceptoND']", 'db_column': "'concepto'"}),
            'descripcion': ('django.db.models.fields.TextField', [], {'null': 'True', 'db_column': "'descripcion'", 'blank': 'True'}),
            'documento_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['facturacion.Documento']", 'unique': 'True'}),
            'numero': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'numero'"})
        },
        'facturacion.requerimiento': {
            'Meta': {'object_name': 'Requerimiento', '_ormbases': ['facturacion.Documento']},
            'casillero': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.Casillero']", 'null': 'True', 'blank': 'True'}),
            'documento_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['facturacion.Documento']", 'unique': 'True'}),
            'factura': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['facturacion.Factura']", 'null': 'True', 'blank': 'True'}),
            'generado': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'generado'"}),
            'numero': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'numero'"}),
            'servicio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['caja.Servicio']"}),
            'tarjeta': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'null': 'True', 'db_column': "'tarjeta'", 'decimal_places': '2', 'max_digits': '12'})
        },
        'facturacion.sucursal': {
            'Meta': {'object_name': 'Sucursal'},
            'codigo': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'codigo'"}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'nombre'"})
        },
        'nomina.cargo': {
            'Meta': {'object_name': 'Cargo'},
            'cargo': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'cargo'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'nomina.empleado': {
            'Meta': {'object_name': 'Empleado', '_ormbases': ['crm.Contacto']},
            'activo': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'activo'"}),
            'cargo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomina.Cargo']"}),
            'codigoempleado': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'codigo'"}),
            'comisionista': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'comisionista'"}),
            'contacto_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['crm.Contacto']", 'unique': 'True', 'primary_key': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'fecha'", 'blank': 'True'}),
            'idseguro': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'seguro'"}),
            'madre': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'db_column': "'madre'", 'blank': 'True'}),
            'numerohijos': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'hijos'", 'blank': 'True'}),
            'padre': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'db_column': "'padre'", 'blank': 'True'}),
            'sueldo': ('django.db.models.fields.DecimalField', [], {'db_column': "'sueldo'", 'decimal_places': '2', 'max_digits': '10'})
        },
        'nomina.vendedor': {
            'Meta': {'object_name': 'Vendedor', '_ormbases': ['nomina.Empleado']},
            'codvendedor': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'codvendedor'"}),
            'empleado_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['nomina.Empleado']", 'unique': 'True', 'primary_key': 'True'})
        },
        'postal.casillero': {
            'Meta': {'object_name': 'Casillero'},
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.Cliente']"}),
            'codigonicabox': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'nicabox'"}),
            'dirdomicilio': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'domicilio': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'fechafin': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'fin'", 'blank': 'True'}),
            'fincontrato': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'fincontrato'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iniciocontrato': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'inicio'", 'blank': 'True'}),
            'justificacion': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'default': "'HBQ7KU'", 'max_length': '100', 'null': 'True', 'db_column': "'password'", 'blank': 'True'}),
            'pesomax': ('django.db.models.fields.DecimalField', [], {'db_column': "'pesomax'", 'decimal_places': '2', 'max_digits': '8'}),
            'proximocobro': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'proximocobro'", 'blank': 'True'}),
            'vendedor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomina.Vendedor']", 'null': 'True', 'blank': 'True'})
        },
        'postal.estadopaquete': {
            'Meta': {'object_name': 'EstadoPaquete'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '5', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'comentarios': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'estado': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'estado'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'statusid': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'postal.gc': {
            'Meta': {'object_name': 'GC'},
            'alto': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'ancho': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'declarado': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'fechaingreso': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'horaingreso': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'largo': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'numref': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'observaciones': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'piezas': ('django.db.models.fields.IntegerField', [], {'default': '1', 'null': 'True', 'blank': 'True'}),
            'volumetrico': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '12', 'decimal_places': '2', 'blank': 'True'})
        },
        'postal.paquete': {
            'Meta': {'object_name': 'Paquete', '_ormbases': ['postal.GC']},
            'bolsa': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'casillero': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.Casillero']", 'null': 'True', 'blank': 'True'}),
            'ciudad': ('django.db.models.fields.CharField', [], {'default': "'Managua'", 'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'cliente': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'consolidado': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'contenido': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'estado': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.EstadoPaquete']", 'null': 'True', 'blank': 'True'}),
            'fechadespacho': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'flete': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'gc_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['postal.GC']", 'unique': 'True', 'primary_key': 'True'}),
            'guiaaerea': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'guiaingreso': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'otros': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'pesokg': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'pesolb': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'remitente': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'servicio': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        'postal.region': {
            'Meta': {'object_name': 'Region'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'region': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'region'"})
        }
    }

    complete_apps = ['facturacion']