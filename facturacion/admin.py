# -*- coding: UTF-8 -*-
from django.contrib import admin
from django.db import models, transaction
from django.conf import settings
from facturacion.models import *
from facturacion.forms import *
from caja.models import Servicio
from postal.models import Courier,Carta
from postal.admin import RawIdCustomAdmin, RawIdCustomAdminWithFilter

class ItemFacturaInline( admin.TabularInline ):
    model = ItemFactura
    readonly_fields = ['total']
    fields = ['concepto','unidades','monto','tarifa','total']
    exclude = ['comprobante_caja']
    extra = 1

class NotaCreditoInline( admin.TabularInline ):
    model = NotaCredito
    readonly_fields = ['numero']
    fields = ['numero', 'fecha','cliente','concepto','monto']
    extra = 1

class ConceptoAbonoAdmin( admin.ModelAdmin ):
    model = ConceptoAbono
    extra = 1

class ConceptoNCAdmin( admin.ModelAdmin ):
    model = ConceptoNC
    extra = 1

class ConceptoNDAdmin( admin.ModelAdmin ):
    search_fields = ['tipo']
    list_display = ['tipo', 'tipo_concepto']
    list_filter = ['tipo_concepto']
    model = ConceptoND
    extra = 1

class AbonoAdmin( RawIdCustomAdminWithFilter, admin.ModelAdmin ):
    form = AbonoForm
    readonly_fields = ['numero']
    fieldsets = (
        ( None, {
            'fields':('numero', 'fecha', 'cliente','concepto', 'moneda', 'tipo_cambio','forma_pago', 'ir', 'alcaldia','monto',)
        }),
        ('Datos del Cheque',{
            'fields': ('banco','cheque')
        }),
        ('Facturas a Pagar',{
            'fields': ('facturas','notas')
        }),
    )
    list_display = ('numero','fecha','cliente','concepto','total')
    raw_id_fields = ('cliente',)
    filter_horizontal = ('facturas','notas')

    def get_form(self, request, obj=None, **kwargs):
        form = super(AbonoAdmin, self).get_form(request, obj, **kwargs)

        choices_f = []
        initial_f = []
        choices_n = []
        initial_n = []
        if obj is not None:
            choices_f =(
                        (c.pk,c.descripcion) for c in Factura.objects.filter(cliente=obj.cliente)
                        )
            initial_f = [c.pk for c in Factura.objects.all()]
            choices_n =(
                        (c.pk,c.description) for c in NotaDebito.objects.filter(cliente=obj.cliente)
                        )
            initial_n = [c.pk for c in NotaDebito.objects.all()]
        elif 'cliente' in request.POST and request.POST['cliente'] !='':
            choices_f =(
                     (c.pk,c.descripcion)
                     for c in Factura.objects.filter(cliente=request.POST['cliente'])
                     )
            choices_n =(
                     (c.pk,c.description)
                     for c in NotaDebito.objects.filter(cliente=request.POST['cliente'])
                     )

        form.base_fields['facturas'].choices = choices_f
        form.base_fields['notas'].choices = choices_n
        if initial_f:
            form.base_fields['facturas'].initial = initial_f
        if initial_n:
            form.base_fields['notas'].initial = initial_n
        return form

class RequerimientoAdmin( RawIdCustomAdmin, admin.ModelAdmin ):
    readonly_fields = ('numero','subtotal','iva','total')
    fields = [ 'numero','fecha','cliente','vencimiento','servicio','anulado','tarjeta','subtotal','iva','total']
    inlines = [ItemFacturaInline]
    list_display = ('numero', 'cliente','subtotal','iva','total','factura','generado')
    raw_id_fields = ('cliente',)
    form = RequerimientoForm

    class Media:
        js = (  "//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js",
                 settings.MEDIA_URL + "js/filtrar_campo.js",)

    def get_form(self, request, obj=None, **kwargs):
        form = super(RequerimientoAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['servicio'].queryset=Servicio.objects.exclude(nombre__icontains='Courier').exclude(nombre__icontains='libre').exclude(nombre__icontains='nicabox').exclude(nombre__icontains='memb')
        return form

class RequerimientoServicioAdmin( RawIdCustomAdmin, admin.ModelAdmin):
    readonly_fields = ('numero','subtotal','iva','total')
    fields = [ 'numero','fecha','vencimiento','anulado','servicio','cliente','items','tarjeta','subtotal','iva','total']
    list_display = ('numero', 'cliente','subtotal','iva','total','servicio','generado')
    raw_id_fields = ('cliente',)
    form = RequerimientoForm


class RequerimientoNicaboxAdmin( RawIdCustomAdminWithFilter, RequerimientoServicioAdmin ):
    model = RequerimientoNicabox
    form = RequerimientoNicaboxForm
    fields = [ 'numero','fecha','vencimiento','anulado','servicio','casillero','items','tarjeta','subtotal','iva','total']
    raw_id_fields = ('casillero',)

    def queryset(self, request):
        qs = super(RequerimientoNicaboxAdmin, self).queryset(request)
        ids = ControlPeso.objects.exclude(requerimiento=None).values_list('requerimiento_id', flat=True)
        qs = Requerimiento.objects.filter(numero__in=ids)
        return qs

    def get_form(self, request, obj=None, **kwargs):
        form = super(RequerimientoNicaboxAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['servicio'].queryset=Servicio.objects.filter(models.Q(nombre__icontains='nicabox') | models.Q(nombre__icontains='libre') | models.Q(nombre__icontains='membresia'))

        choices = []
        initial = []
        if obj is not None:
            choices =(
                        (c.pk,c.descripcion) for c in ControlPeso.objects.filter(
                                                                 casillero = obj.casillero, requerimiento=obj
                                                                 )
                        )
            initial = [c.pk for c in ControlPeso.objects.filter(requerimiento=obj)]          
        elif 'casillero' in request.POST and request.POST['casillero'] !='':
            choices =(
                     (c.pk,c.descripcion)                     
                     for c in ControlPeso.objects.filter(
                                                         casillero=request.POST['casillero']
                                                         )
                     )                
            
        form.base_fields['items'].choices = choices        
        if initial:
            form.base_fields['items'].initial = initial

        return form

    def save_model(self, request, obj, form, change):
        if obj is not None:
            ControlPeso.objects.filter(requerimiento=obj).update(requerimiento=None)
        super(RequerimientoNicaboxAdmin, self).save_model(request, obj, form, change)
        items = ControlPeso.objects.filter(pk__in=form.cleaned_data['items'])
        #Asigna el requerimiento y llama al metodo guardar que actualiza el total de la carta
        for item in items:
            item.requerimiento = obj
            item.save()        


class RequerimientoCourierAdmin( RawIdCustomAdminWithFilter, RequerimientoServicioAdmin ):
    model = RequerimientoCourier
    form = RequerimientoCourierForm

    def queryset(self, request):
        qs = super(RequerimientoCourierAdmin, self).queryset(request)
        ids = Courier.objects.exclude(requerimiento=None).values_list('requerimiento_id', flat=True)
        qs = Requerimiento.objects.filter(numero__in=ids)
        return qs

    def get_form(self, request, obj=None, **kwargs):
        form = super(RequerimientoCourierAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['servicio'].queryset=Servicio.objects.filter(nombre__icontains='Courier')

        choices = []
        initial = []
        if obj is not None:
            choices =(
                        (c.pk,c.descripcion) for c in Courier.objects.filter(cliente=obj.cliente, requerimiento=obj)
                        )
            initial = [c.pk for c in Courier.objects.filter(requerimiento=obj)]
        elif 'cliente' in request.POST and request.POST['cliente'] !='':
            choices =(
                     (c.pk,c.descripcion)
                     for c in Courier.objects.filter(cliente=request.POST['cliente'])
                     )

        form.base_fields['items'].choices = choices
        if initial:
            form.base_fields['items'].initial = initial
        return form


    def save_model(self, request, obj, form, change):
        if obj is not None:
            Courier.objects.filter(requerimiento=obj).update(requerimiento=None)

        super(RequerimientoCourierAdmin, self).save_model(request, obj, form, change)
        couriers = Courier.objects.filter(pk__in=form.cleaned_data['items'])
        #Asigna el requerimiento y llama al metodo guardar que actualiza el total del courier
        for courier in couriers:
            courier.requerimiento = obj
            courier.save()

class RequerimientoCartaAdmin( RawIdCustomAdminWithFilter, RequerimientoServicioAdmin ):
    model = RequerimientoCarta
    form = RequerimientoCartaForm
    raw_id_fields = ('casillero',)
    fields = ['numero', 'fecha', 'vencimiento', 'anulado', 'servicio', 'casillero', 'items', 'tarjeta', 'subtotal',
              'iva', 'total']

    def queryset(self, request):
        qs = super(RequerimientoCartaAdmin, self).queryset(request)
        ids = Carta.objects.exclude(requerimiento=None).values_list('requerimiento_id', flat=True)
        qs = Requerimiento.objects.filter(numero__in=ids)
        return qs

    def get_form(self, request, obj=None, **kwargs):
        form = super(RequerimientoCartaAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['servicio'].queryset = Servicio.objects.filter(nombre__icontains='Carta')

        choices = []
        initial = []

        if obj is None:
            pass
        else:
            self.readonly_fields = self.readonly_fields + ('fecha','vencimiento','anulado','casillero','tarjeta')
            self.change_form_template = 'admin/facturacion/requerimientocarta/readonly.html'

        if obj is not None:
            choices = (
                (c.pk, c.descripcion) for c in Carta.objects.filter(casillero=obj.casillero, requerimiento=obj)
            )
            initial = [c.pk for c in Carta.objects.filter(requerimiento=obj)]

        elif 'casillero' in request.POST and request.POST['casillero'] !='':
            choices = (
                (c.pk, c.descripcion)
                for c in Carta.objects.filter(casillero=request.POST['casillero'], requerimiento=None)
            )
            
        form.base_fields['items'].choices = choices
        form.base_fields['servicio'].initial = Servicio.objects.filter(nombre__icontains='Carta')[0]
        if initial:
            form.base_fields['items'].initial = initial
        return form

    def save_model(self, request, obj, form, change):
        if obj is not None:
            Carta.objects.filter(requerimiento=obj).update(requerimiento=None)

        super(RequerimientoCartaAdmin, self).save_model(request, obj, form, change)
        cartas = Carta.objects.filter(pk__in=form.cleaned_data['items'])
        #Asigna el requerimiento y llama al metodo guardar que actualiza el total de la carta
        for carta in cartas:
            carta.requerimiento = obj
            carta.save()

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({'object':obj})
        return super(RequerimientoCartaAdmin,self).render_change_form(request, context, add=add, change=change, form_url=form_url, obj=obj)

class FacturaFormAdmin( RawIdCustomAdminWithFilter, admin.ModelAdmin):
    model = Factura
    form = FacturaForm
    readonly_fields = ('numero','subtotal','iva','total')
    fields = ['numero','fecha','vencimiento','credito','anulado',
              'cliente',
              'items',
               'subtotal','iva','total']
    
    list_display = ('numero', 'cliente','total','saldo_pendiente','concepto')
    raw_id_fields = ('cliente',)
    search_fields = ['cliente__nombre', 'cliente__apellido', 'cliente__organization']

    def save_model(self, request, obj, form, change):
        super(FacturaFormAdmin, self).save_model(request, obj, form, change)        
        Requerimiento.objects.filter(factura=obj).update(factura=None)
        Requerimiento.objects.filter(numero__in=form.cleaned_data['items']).update(factura=obj)

class NotaCreditoAdmin( RawIdCustomAdminWithFilter, admin.ModelAdmin ):
    form = AbonoForm
    readonly_fields = ('numero',)
    fields = ['numero','fecha','cliente','monto','concepto','facturas','notas']
    list_display = ['numero', 'fecha','cliente','concepto','total']
    raw_id_fields = ('cliente',)

    def get_form(self, request, obj=None, **kwargs):
        form = super(NotaCreditoAdmin, self).get_form(request, obj, **kwargs)

        choices_f = []
        initial_f = []
        choices_n = []
        initial_n = []
        if obj is not None:
            choices_f =(
                        (c.pk,c.descripcion) for c in Factura.objects.filter(cliente=obj.cliente)
                        )
            initial_f = [c.pk for c in Factura.objects.all()]
            choices_n =(
                        (c.pk,c.description) for c in NotaDebito.objects.filter(cliente=obj.cliente)
                        )
            initial_n = [c.pk for c in NotaDebito.objects.all()]
        elif 'cliente' in request.POST and request.POST['cliente'] !='':
            choices_f =(
                     (c.pk,c.descripcion)
                     for c in Factura.objects.filter(cliente=request.POST['cliente'])
                     )
            choices_n =(
                     (c.pk,c.description)
                     for c in NotaDebito.objects.filter(cliente=request.POST['cliente'])
                     )

        form.base_fields['facturas'].choices = choices_f
        form.base_fields['notas'].choices = choices_n
        if initial_f:
            form.base_fields['facturas'].initial = initial_f
        if initial_n:
            form.base_fields['notas'].initial = initial_n
        return form

class NotaDebitoAdmin( RawIdCustomAdmin, admin.ModelAdmin ):
    model = NotaDebito
    fields = ['numero', 'fecha','cliente','concepto','monto', 'descripcion']
    readonly_fields = ['numero',]
    list_display = ['__unicode__','cliente','total','saldo_pendiente','concepto']
    raw_id_fields = ('cliente',)
    search_fields = ['cliente__nombre', 'cliente__apellido', 'cliente__organization']

class CobroAdicionalAdmin(admin.ModelAdmin, RawIdCustomAdmin):
    raw_id_fields = ('paquete',)
    list_display = ['paquete','fecha','tipocobro','monto']
    list_filter = ['tipocobro']

admin.site.register(CobroAdicional, CobroAdicionalAdmin)
admin.site.register(Factura, FacturaFormAdmin)
admin.site.register(NotaCredito, NotaCreditoAdmin)
admin.site.register(NotaDebito, NotaDebitoAdmin)
admin.site.register(ConceptoAbono, ConceptoAbonoAdmin)
admin.site.register(ConceptoNC, ConceptoNCAdmin)
admin.site.register(ConceptoND,ConceptoNDAdmin)
admin.site.register(Requerimiento, RequerimientoAdmin)
admin.site.register(RequerimientoCourier, RequerimientoCourierAdmin)
admin.site.register(RequerimientoCarta, RequerimientoCartaAdmin)
admin.site.register(RequerimientoNicabox, RequerimientoNicaboxAdmin)

admin.site.register(Abono, AbonoAdmin)

