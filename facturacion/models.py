# -*- coding: UTF-8 -*-
from django.db import models
from crm.models import Cliente, Parametro
from postal.models import Paquete , Courier , Carta, Casillero,ControlPeso
from django.db.models import Sum
from decimal import Decimal
from django.forms import ModelForm, ModelMultipleChoiceField
from django.core.exceptions import ValidationError
from contabilidad.moneyfmt import moneyfmt, number_to_words
from caja.models import FormaPago, Servicio,TarifaServicio,ComprobanteCaja, TipoCambio
import math
from datetime import date

TIPO_CONCEPTO_ND = [
    (1 , 'Local'),
    (2 , 'Controlbox'),
    ]

TIPO_DOC = [
    (1 , 'Requerimiento'),
    (2 , 'Factura'),
    (3 , 'Nota de Debito'),
    (4 , 'Nota de Credito'),
    (5 , 'Abono'),

    ]
#=================================================
# Sucursal
#=================================================
class Sucursal( models.Model ):
    codigo = models.AutoField(primary_key=True, db_column="codigo", verbose_name="Codigo Sucursal")
    nombre = models.CharField(db_column="nombre", null=False, blank=False, max_length=20, verbose_name="Nombre Sucursal")

    class Meta:
        verbose_name = "Sucursal"
        verbose_name_plural = "Sucursales"

    def __unicode__( self ):
        return self.nombre

#=================================================
# Concepto de Abono
#=================================================
class ConceptoAbono( models.Model ):
    nombre = models.CharField(db_column="nombre", null=False, blank=False, max_length=80, verbose_name="Concepto del Abono")

    class Meta:
        verbose_name = "Concepto de Abonos"
        verbose_name_plural = "Conceptos de Abonos"

    def __unicode__( self ):
        return self.nombre

class ConceptoNC( models.Model ):
    nombre = models.CharField(db_column="nombre", null=False, blank=False, max_length=80, verbose_name=u"Concepto Nota de Crédito")

    class Meta:
        verbose_name = u"Concepto de Notas de Crédito"
        verbose_name_plural = u"Conceptos de Notas de Crédito"

    def __unicode__( self ):
        return self.nombre

#=================================================
# Concepto de Nota de Debito
#=================================================
class ConceptoND( models.Model ):
    tipo = models.CharField(null=True, blank=True, max_length=500, verbose_name="Detalle")
    tipo_concepto = models.IntegerField(choices=TIPO_CONCEPTO_ND, null=False, blank=False, default=2)

    class Meta:
        verbose_name = u"Concepto de Notas de Débito"
        verbose_name_plural = u"Conceptos de Notas de Débito"

    def __unicode__(self):
        return unicode(self.tipo)

#=================================================
# Cobro Adicional
#=================================================
class CobroAdicional( models.Model ):
    fecha = models.DateTimeField()
    monto = models.DecimalField(default=0, max_digits=9, decimal_places=2)
    tipocobro = models.ForeignKey(ConceptoND,verbose_name="Detalle")
    paquete = models.ForeignKey('postal.Paquete', null=True, blank=True, verbose_name="paquete")

    class Meta:
        verbose_name_plural = "Cobros Adicionales"

    def __unicode__(self):
        return unicode(self.monto)

#=================================================
# Documento
#=================================================
class Documento(models.Model):
    fecha = models.DateField(db_column="fecha", default=date.today(), null=False, blank=False, verbose_name="fecha")
    cliente = models.ForeignKey(Cliente, blank=False, null=False, verbose_name="Cliente")
    vencimiento = models.DateField(db_column="vencimiento", null=True, blank=True, verbose_name="Vencimiento")
    monto = models.DecimalField(db_column="monto", null=True, blank=True, max_digits=12, decimal_places=2, verbose_name="Monto")
    anulado = models.BooleanField(db_column="anulado", null=False, blank=False, default=False, verbose_name="Anulado")
    tipo_documento = models.IntegerField(db_column="tipo_documento",null=True, blank=True,)
    moneda = "DOLARES"

    def fecha_formato(self):
        return format(self.fecha,"%d/%m/%y")

    @property
    def values(self):
        return [self.numero, self.fecha,("\"" + self.cliente.nombre + "\"" ),self.monto,("\"" + self.servicio.nombre+ "\"" )]

    @property
    def direccion(self):
        return self.cliente.direccion


    def monto_to_words(self):
        return number_to_words(self.monto,self.moneda)

    def clean(self):
        validar =  False

        try:
            validar = self.cliente is not None
        except:
            pass

        if validar and not self.cliente.activo:
                raise ValidationError('El cliente ' + self.cliente.nombre + ' se encuentra temporalmente bloqueado, por favor seleccione un cliente activo')

        models.Model.clean(self)


#=================================================
# Documento Cobro
#=================================================
class DocumentoCobro(object):

    def obtener_monto(self):
        if self.pk is not None and "Nicabox" in self.servicio.nombre:
        
            casillero = self.casillero
            tarifas = TarifaServicio.objects.filter(tipo=self.servicio)

            pesototal = ControlPeso.objects.filter(requerimiento=self).aggregate(Sum('peso'))['peso__sum']
            pesototal = pesototal if pesototal is not None else Decimal('0')

            pesomax = casillero.pesomax if casillero.pesomax is not None and casillero.pesomax > pesototal else pesototal

            if self.casillero.cliente.tipo is not None:
                # if (self.casillero.cliente.tipo.nombre.lower().find('libre') > -1)
                # and (self.servicio.nombre.lower().find('libre') > -1)  else Decimal('1')
                factor_peso = pesototal
                if self.casillero.cliente.tipocobro is None:
                    cuotas = Decimal('1')
                else:
                    cuotas = self.casillero.cliente.tipocobro.meses if (self.casillero.cliente.tipocobro.meses is not None) and self.casillero.cliente.tipo.nombre.lower().find('membresia')>-1 and self.servicio.nombre.lower().find('membresia')>-1   else Decimal('1')
            else:
                factor_peso = Decimal('1')
                cuotas = Decimal('1')

            domicilio = Decimal(Parametro.objects.get(parametro="Servicio Domicilio").valor) if casillero.domicilio else Decimal(0)

            t = None
            for t in tarifas:
                if pesomax >= t.limite_inferior:
                    if pesomax <= t.limite_superior :
                        return (t.monto * factor_peso + domicilio ) * cuotas

            if t is not None:
                if t.limite_inferior is None:
                    return (t.monto * factor_peso + domicilio ) * cuotas
                else:
                    limite = t.limite_superior if t.limite_superior is not None else t.limite_inferior
                    adicional = (Decimal(str(math.ceil(pesomax - limite))) * t.adicional) if t.adicional is not None else Decimal(0)

                    return (t.monto * factor_peso + domicilio ) * cuotas + adicional
            return Decimal(0)
        else:
            t = sum([item.total for item in self.items])
            return Decimal(t if t is not None else 0)


    @property
    def subtotal__(self):
        if self.items is None:
            monto = self.monto
        else:
            monto = self.obtener_monto()

        cargo_tarjeta = Decimal(0)
        try:
            if self.tarjeta is not None:
                cargo_tarjeta =  self.tarjeta
        except:
            pass
        return Decimal(monto + cargo_tarjeta).quantize(Decimal('0.01'))

    @property
    def iva__(self):
        if self.monto is None:
            self.monto = 0
        monto = self.subtotal__ * Decimal('0.15')
        return  monto.quantize(Decimal('0.01'))

    @property
    def total__(self):
        if self.monto is None:
            self.monto = 0
        monto = self.subtotal__ * Decimal('1.15')
        return  monto.quantize(Decimal('0.01'))


    @property
    def subtotal(self):
        return  moneyfmt(self.subtotal__,2,'US$ ')

    @property
    def iva(self):
        return  moneyfmt(self.subtotal__*Decimal(0.15),2,'US$ ')

    @property
    def total(self):
        return  moneyfmt(self.subtotal__ * Decimal(1.15),2,'US$ ')

    def condicion_pago(self):
        if self.vencimiento is not None:
            return "Vence el  " + format(self.vencimiento,"%d/%m/%y")
        else:
            return "No Vence"

    def ciudad(self):
        return self.cliente.ciudad



#=================================================
# Requerimiento
#=================================================
class Requerimiento( Documento,DocumentoCobro ):
    numero = models.AutoField(db_column="numero",primary_key=True)
    servicio = models.ForeignKey(Servicio, blank=True, null=True , verbose_name="Servicio requerido")
    factura = models.ForeignKey(to='Factura', blank=True, null=True, verbose_name=u"Factura")
    tarjeta = models.DecimalField(db_column="tarjeta", null=True, blank=True, max_digits=12, decimal_places=2, verbose_name="Cargo por Tarjeta")
    casillero = models.ForeignKey(Casillero, blank=True, null=True, verbose_name="Casillero")
    generado = models.BooleanField(db_column="generado",default=False, null=False, blank=False, verbose_name=u"Generado Automáticamente")

    @property
    def js_info(self):
        return self.numero,self.descripcion

    class Meta:
        verbose_name = "Requerimiento"
        verbose_name_plural = "Requerimientos"

    def __unicode__(self):
        return "Requerimiento %s"%unicode(self.numero)

    @property
    def items(self):
        if self.servicio.nombre.lower().find('carta') > -1:
            return Carta.objects.filter(requerimiento=self)
        elif self.servicio.nombre.lower().find('courier') > -1:
            return Courier.objects.filter(requerimiento=self)
        elif (self.servicio.nombre.lower().find('nicabox') > -1) or (self.servicio.nombre.lower().find('membresia') > -1) or (self.servicio.nombre.lower().find('libre') > -1):
            return ControlPeso.objects.filter(requerimiento=self)
        else:
            return ItemFactura.objects.filter(requerimiento=self)

    @property
    def detalle(self):
        if (self.servicio.nombre.lower().find('nicabox') > -1) or (self.servicio.nombre.lower().find('membresia') > -1) or (self.servicio.nombre.lower().find('libre') > -1):
            desc = ['requerimiento','fecha','concepto','total']
            concepto = "Peso Total " + str(ControlPeso.objects.filter(requerimiento=self).aggregate(Sum('peso'))['peso__sum'])

            rows = [(self,self.fecha,concepto,self.subtotal__)]
            if self.servicio.nombre.lower().find('membres') > -1:
                rows.append((self,'','Membresia ' + self.cliente.tipocobro.nombre,''))

            filas = [
            dict(zip([col for col in desc], row))
            for row in rows

        ]
        else:
            filas = self.items
        return filas

    def save(self, force_insert=False, force_update=False, using=None):
        self.tipo_documento = 1
        self.monto = self.obtener_monto()
        if self.factura is not None:
            self.cliente = self.factura.cliente
        if self.casillero is not None:
            self.cliente = self.casillero.cliente

        models.Model.save(self, force_insert=False, force_update=False, using=None)

    def concepto(self):
        return self.servicio

    @property
    def descripcion(self):
        descr = u'Requerimiento %s (%s) | subtotal %s | total %s'%(self.numero,self.fecha_formato(),self.subtotal,self.total)
        return descr

#=================================================
# Requerimiento Courier
#=================================================
class RequerimientoCourier( Requerimiento ):
    class Meta:
        proxy = True
        verbose_name = "Requerimiento Courier"
        verbose_name_plural = "Requerimientos Courier"

#=================================================
# Requerimiento Courier
#=================================================
class RequerimientoCarta(Requerimiento):
    tipo = 'carta'
    class Meta:
        proxy = True
        verbose_name = "Requerimiento Carta"
        verbose_name_plural = "Requerimientos Carta"

#=================================================
# Requerimiento Nicabox
#=================================================
class RequerimientoNicabox( Requerimiento ):
    class Meta:
        proxy = True
        verbose_name = "Requerimiento Nicabox"
        verbose_name_plural = "Requerimientos Nicabox"


    def clean(self):
        validar =  False
        try:
            validar = self.casillero is not None
        except:
            pass

        if validar:
            self.cliente = self.casillero.cliente
            if self.cliente.tipo is None:
                raise ValidationError('Al cliente ' + self.cliente.nombre + ' no se le ha establecido un tipo de cliente')

            if self.servicio.nombre.lower().find('membres') > -1:
                if self.cliente.tipo.nombre.lower().find('membres')== -1:
                    raise ValidationError('El cliente ' + self.cliente.nombre + ' no cuenta con membresia')
                elif self.cliente.tipocobro is None:
                    raise ValidationError('No se ha definido el tipo de cobro para el cliente ' + self.cliente.nombre)
                elif self.casillero.proximocobro is None:
                    raise ValidationError('No se ha definido el proximo cobro para el casillero ' + unicode(self.casillero) + ' del cliente ' + self.cliente.nombre)
                elif self.casillero.proximocobro > date.today():
                    raise ValidationError('El cliente ' + self.cliente.nombre + ' tiene vigente su cuota hasta ' + format(self.casillero.proximocobro,"%d/%m/%y") + '.  Hoy es ' +  format(date.today(),"%d/%m/%y"))

            elif self.servicio.nombre.lower().find('libre') > -1 and self.cliente.tipo.nombre.lower().find('libre')== -1:
                raise ValidationError('El cliente ' + self.cliente.nombre + ' no es tarifa libre')

        else:
            raise ValidationError('Seleccione un casillero')
        Requerimiento.clean(self)

    def save(self):
        if self.servicio.nombre.lower().find('membres') > -1:
            self.casillero.proximocobro = date.today()
            self.casillero.save()
        Requerimiento.save(self)


#=================================================
# Factura
#=================================================
class Factura( Documento,DocumentoCobro ):
    ORIGEN_FACTURA=(
        ('nic','Managua, Nicaragua'),
        ('usa','Miami, Estados Unidos'),
    )
    numero = models.AutoField(db_column="numero",primary_key=True)
    credito = models.BooleanField(db_column="credito",default=False, null=False, blank=False, verbose_name=u"Crédito")
    sucursal = models.ForeignKey(Sucursal, blank=True, null=True, verbose_name="Sucursal")
    origen = models.CharField(db_column="origen",choices=ORIGEN_FACTURA, null=False, blank=False, max_length=20, verbose_name="Origen Factura")

    class Meta:
        verbose_name = "Factura"
        verbose_name_plural = "Facturas"

    def __unicode__(self):
        return "Factura %s"%unicode(self.numero)

    @property
    def descripcion(self):
        return "Factura " + str(self.numero) + " | saldo " + str(self.saldo_pendiente)

    @property
    def items(self):
        return Requerimiento.objects.filter(factura=self)

    @property
    def subtotal__(self):
        t = sum([item.subtotal__ for item in self.items])
        return  Decimal(t if t is not None else 0)

    @property
    def saldo__(self):
        if self.credito:
            s = Abono.objects.filter(facturas__in=[self]).aggregate(Sum('monto'))['monto__sum']
            s = Decimal(s if s is not None else 0)
            return Decimal(self.total__ - s if s is not None else 0)
        else:
            return  Decimal(0)

    @property
    def saldo_pendiente(self):
        return  moneyfmt(Decimal(self.saldo__),2,'US$ ')

    @property
    def concepto(self):
        return  'Requerimiento #: ' + ', '.join([str(r.numero) for r in Requerimiento.objects.filter(factura=self)])

    def es_credito(self):
        return u"Crédito" if self.credito else "Contado"

    def save(self, force_insert=False, force_update=False, using=None):
        self.tipo_documento = 2
        self.monto = 0
        models.Model.save(self, force_insert=False, force_update=False, using=None)



class NotaDebito(Documento):
    numero = models.AutoField(db_column="numero",primary_key=True)
    concepto = models.ForeignKey(ConceptoND, db_column="concepto", null=False, blank=False)
    descripcion = models.TextField(db_column="descripcion", null=True, blank=True, verbose_name="Descripcion")

    class Meta:
        verbose_name = u"Nota de Débito"
        verbose_name_plural = u"Notas de Débito"

    def __unicode__(self):
        return unicode(self.numero)

    @property
    def description(self):
        return "Nota " + str(self.numero) + " | " + self.concepto.tipo + " | saldo " + str(self.saldo_pendiente)

    @property
    def total__(self):
        if self.monto is None:
            self.monto = 0
        return  Decimal(self.monto)


    @property
    def total(self):
        if self.monto is None:
            self.monto = 0
        return  moneyfmt(Decimal(self.monto),2,'US$ ')

    @property
    def saldo__(self):
        s = Abono.objects.filter(notas__in=[self]).aggregate(Sum('monto'))['monto__sum']
        s = Decimal(s if s is not None else 0)
        return Decimal(self.total__ - s if s is not None else 0)

    @property
    def saldo_pendiente(self):
        return  moneyfmt(Decimal(self.saldo__),2,'US$ ')

    def clean(self):
        validar =  False
        models.Model.clean(self)

    def save(self):
        self.tipo_documento = 3
        models.Model.save(self)


class NotaCredito(Documento):
    numero = models.AutoField(db_column="numero",primary_key=True)
    concepto = models.ForeignKey(ConceptoNC, db_column="concepto", null=False, blank=False)
    facturas = models.ManyToManyField(Factura, null= True, blank= True, related_name="facturas", verbose_name="Facturas Relacionadas")
    notas = models.ManyToManyField(NotaDebito,null= True, blank= True,  related_name="notas", verbose_name=u"Notas de Débito Relacionadas")


    class Meta:
        verbose_name = u"Nota de Crédito"
        verbose_name_plural = u"Notas de Crédito"

    def __unicode__(self):
        return unicode(self.numero)

    def subtotal(self):
        if self.monto is None:
            self.monto = 0
        return round(Decimal(self.monto/Decimal(1.15)),2)

    def iva(self):
        return round(self.subtotal()*(0.15),2)

    @property
    def total__(self):
        if self.monto is None:
            self.monto = 0
        return  Decimal(self.monto)

    def save(self):
        self.tipo_documento = 4
        Documento.save(self)

    @property
    def documentos(self):
        return ("Facturas:" + ",".join([str(f.numero) + "(" + format(f.fecha,"%d/%m/%y") +")" for f in self.facturas.all()]) + " y " if len(self.facturas.all()) !=0 else "") + ("Notas: " + ",".join([str(f.numero) + "(" + format(f.fecha,"%d/%m/%y") +")" for f in self.notas.all()])) if len(self.notas.all()) !=0 else ""

    @property
    def total(self):
        if self.monto is None:
            self.monto = 0
        return  moneyfmt(Decimal(self.monto),2,'US$ ')

class Abono( Documento ):
    MONEDA = (
              ('C$', u'Córdobas'),
              ('US$', u'Dólares')
    )
    numero = models.AutoField(db_column="numero",primary_key=True, verbose_name=u"Numero de Comprobante")
    concepto = models.ForeignKey(ConceptoAbono, verbose_name=u"Concepto del Abono")
    ir = models.DecimalField(db_column="ir",null=True, blank=True,max_digits=8, decimal_places=2, verbose_name=u"Retencion IR")
    alcaldia = models.DecimalField(db_column="alcaldia",null=True, blank=True,max_digits=8, decimal_places=2, verbose_name=u"Retencion Alcaldia")
    tipo_cambio = models.ForeignKey(TipoCambio, blank=True, null=True, help_text=u"Vacio cuando el abono sea en córdobas", verbose_name="Tipo cambio")
    forma_pago = models.ForeignKey(FormaPago, db_column="forma_pago", null=False, blank=False, verbose_name="Forma Pago")
    cheque = models.CharField(db_column="cheque", null=True, blank=True, max_length=20, verbose_name="Numero de Cheque")
    banco = models.CharField(db_column="banco", null=True, blank=True, max_length=20, verbose_name="Banco")
    facturas = models.ManyToManyField(Factura, null= True, blank= True, related_name="facturas_abonadas", verbose_name="Facturas Relacionadas")
    notas = models.ManyToManyField(NotaDebito,null= True, blank= True,  related_name="notas_abonadas", verbose_name=u"Notas de Débito Relacionadas")
    moneda = models.CharField(db_column="moneda",choices=MONEDA, null=False, blank=False, max_length=20, verbose_name="Moneda")

    class Meta:
        verbose_name = "Abono"
        verbose_name_plural = "Abonos"

    def __unicode__(self):
        return unicode(self.numero)

    def save(self):
        self.tipo_documento = 5
        Documento.save(self)

    def documentos(self):
        return ("Facturas:" + ",".join([str(f.numero) + "(" + format(f.fecha,"%d/%m/%y") +")" for f in self.facturas.all()]) + " y " if len(self.facturas.all()) !=0 else "") + ("Notas: " + ",".join([str(f.numero) + "(" + format(f.fecha,"%d/%m/%y") +")" for f in self.notas.all()])) if len(self.notas.all()) !=0 else ""


    @property
    def total__(self):
        if self.monto is None:
            self.monto = 0
        return  Decimal(self.monto)

    @property
    def total(self):
        if self.monto is None:
            self.monto = 0
        return  moneyfmt(Decimal(self.monto),2,self.moneda)

    @property
    def saldo_pendiente(self):
        return  Decimal(0)


class ItemFactura( models.Model ):
    numero = models.AutoField(db_column="numero",primary_key=True)
    requerimiento = models.ForeignKey(Requerimiento, blank=True, null=True)
    comprobante_caja = models.ForeignKey(ComprobanteCaja, blank=True, null=True, verbose_name=u"Comprobante Caja")
    concepto = models.CharField(db_column="concepto", null=False, blank=False, max_length=50, verbose_name=u"Concepto Item")
    unidades = models.IntegerField(db_column="unidades", null=False, blank=False, verbose_name="Unidades de Item")
    tarifa = models.ForeignKey(TarifaServicio, blank=False, null=False, verbose_name=u"Tarifa Servicio")
    monto = models.DecimalField(db_column="monto",null=False, blank=False,max_digits=8, decimal_places=2, verbose_name=u"Monto Item")

    class Meta:
        verbose_name = "Item Factura"
        verbose_name_plural = "Items Factura"

    def __unicode__( self ):
        return unicode(self.numero)

    @property
    def total(self):
        return Decimal(self.unidades * self.tarifa.monto * self.monto)
