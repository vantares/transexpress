from tastypie.resources import ModelResource
from tastypie import fields, utils
from tastypie.constants import *
from postal.models import Carta, Courier, Casillero
from facturacion.models import Requerimiento, Factura, NotaDebito

class RequerimientoResource(ModelResource):
    descripcion = fields.CharField(attribute='descripcion')
    numero = fields.IntegerField(attribute='numero')
        
    class Meta:
        queryset = Requerimiento.objects.filter(factura__isnull=True).order_by('numero')
        resource_name = 'requerimiento'
        allowed_methods = ['get']
        filtering = {
            "id": ('exact',),
            "cliente": ('exact',),
        }

class FacturaResource(ModelResource):
    descripcion = fields.CharField(attribute='descripcion')
    numero = fields.IntegerField(attribute='numero')

    class Meta:
        queryset = Factura.objects.all()

class NotaDebitoResource(ModelResource):
    descripcion = fields.CharField(attribute='description')
    numero = fields.IntegerField(attribute='numero')

    class Meta:
        queryset = NotaDebito.objects.all()
        filtering = {
            "id": ('exact',),
            "cliente": ('exact',),
        }