# -*- coding: utf-8 -*-
from contabilidad.models import Cuenta
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext, loader
from django.views.generic import FormView
from reportes.views import ExportedReport
from facturacion.forms import *
from facturacion.reports import *
from facturacion.models import *
from django.db import models
from postal.models import Paquete , Courier , Carta, Casillero,ControlPeso
from crm.models import Cliente
import simplejson
from django.http import HttpResponse
from datetime import date
from geraldo.generators import PDFGenerator, TextGenerator

def index(request,pk=""):
    if request.is_ajax():
        if pk == "":
            tarifas = TarifaServicio.objects.all()
        else:
            servicio = get_object_or_404(Servicio, pk=pk)
            tarifas = TarifaServicio.objects.filter(tipo=servicio).all() 
        json = simplejson.dumps([ (i.pk, str(i)) for i in tarifas])
        return HttpResponse(json)
    else:
        return render_to_response('base.html', RequestContext(request))


def tarifas(request, pk):
    servicio = get_object_or_404(Requerimiento, pk=pk)
    tarifas = TarifaServicio.objects.filter(tipo=servicio).all()
    json = simplejson.dumps([ (i.pk,i.monto) for i in tarifas])
    return HttpResponse(json)

def requerimientos(request, pk):
    if pk == "":
        req = Requerimiento.objects.all()
    else:
        cliente = get_object_or_404(Cliente, pk=pk)
        req = Requerimiento.objects.filter(cliente=cliente).all() 
    json = simplejson.dumps([ (i.pk, str(i)) for i in req])
    return HttpResponse(json)

class NotaView(ExportedReport, FormView):
    form_class = NotaForm
    template_name = 'facturacion/notas.html'
    csv_titles = ['Numero','Fecha','Cliente','Monto','Concepto']
    title = u'Documentos'
    required_permissions = ('reportes.documento',)

    
    def form_valid(self, form):
        
        tipo = form.cleaned_data['tipo']
        if tipo<>"":
            tipo = int(tipo)
        cliente = form.cleaned_data['cliente']
        num = form.cleaned_data['numero']
        fecha_inicio = form.cleaned_data['fecha_inicio']
        fecha_fin =  form.cleaned_data['fecha_fin']
        resumen =  form.cleaned_data['resumen']

        
        if tipo == 5:
            self.title = 'Abonos'
            data = Abono.objects.all()
            self.report_class = AbonoReport
        
        elif tipo == 1:
            self.title = 'Requerimientos'
            data = Requerimiento.objects.all()
            self.report_class = RequerimientoReport
            
        
        elif tipo == 2:
            self.title = 'Facturas'
            data = Factura.objects.all()
            self.report_class = FacturaReport
            
        
                
        elif tipo == 3:
            self.title = u'Notas de Crédito'
            data = NotaCredito.objects.all()
            self.report_class = NotaCreditoReport
            
            
    
        elif tipo == 4:
            self.title = u'Notas de Débito'
            data = NotaDebito.objects.all()
            self.report_class = NotaDebitoReport     
                   
         
        else:
            self.title = u'Documentos'
            data = None
        
        if num <> "":
            data = data.filter(numero = num)
            
        if cliente <> "":
            data = data.filter(cliente__nombre__icontains = cliente)
            

        if fecha_inicio is not None:
            data = data.filter(fecha__gte = fecha_inicio)

        if fecha_fin is not None:
            data = data.filter(fecha__lte = fecha_fin)
            
        
        if resumen:
            self.report_class = ResumenReport
        
        context = self.get_context_data()
        context.update({
            'form':form,
            'titulo':self.title,
            'data':data,
        })

        return self.render_to_response(context,data)

      
class GenerarRequerimientosView(ExportedReport, FormView):
    form_class = GenerarRequerimientosForm
    report_class = RequerimientoReport
    template_name = 'facturacion/notas.html'
    title = 'Generar Requerimientos'
    csv_titles = ['Numero','Fecha','Cliente','Monto','Concepto']
    required_permissions = ('reportes.reqautomaticos',)
    
    def form_valid(self, form):
        cliente_inicial = form.cleaned_data['cliente_inicial']
        cliente_final = form.cleaned_data['cliente_final']
        fecha_inicio = form.cleaned_data['fecha_inicio']
        fecha_fin = form.cleaned_data['fecha_fin']
        tipo = form.cleaned_data['tipo']

        

        clientes = Cliente.objects.filter(activo=True,bloqueado=False)
        
        
        if cliente_inicial is not None:
            clientes = clientes.filter(id__gte =  cliente_inicial.id)
#
        if cliente_final is not None:
            clientes = clientes.filter(id__lte =  cliente_final.id)            

        
        if (tipo.nombre.lower().find('nicabox') > -1) :
            clientes = clientes.filter(tipo = None)
            
        elif (tipo.nombre.lower().find('membresia') > -1):
            clientes = clientes.filter(tipo__nombre__icontains='membresia')
        elif (tipo.nombre.lower().find('libre') > -1):
            clientes = clientes.filter(tipo__nombre__icontains='libre')
            
        
        for cliente in clientes:            
            if (tipo.nombre.lower().find('nicabox') > -1) or (tipo.nombre.lower().find('membresia') > -1) or (tipo.nombre.lower().find('libre') > -1):

                casilleros = Casillero.objects.filter(cliente=cliente)           
                for casillero in casilleros:
                    req = Requerimiento(
                        casillero = casillero,
                        servicio = tipo,
                        fecha = date.today(), 
                        generado=True,
                    )
                    items = ControlPeso.objects.filter(casillero=casillero,requerimiento=None)
                    if fecha_inicio is not None:
                        items = items.filter(fecha__gte = fecha_inicio)
                    if fecha_fin is not None:
                        items = items.filter(fecha__lte = fecha_fin)                   

                    
                    if len(items)>0:
                        req.save()
                        for item in items:
                            item.requerimiento=req
                            item.save()

            else:
                
                req = Requerimiento(
                    cliente = cliente,
                    servicio = tipo,
                    fecha = date.today(), 
                    generado=True,
                    )
                if tipo.nombre.lower().find('carta') > -1:
                    items = Carta.objects.filter(cliente=cliente,requerimiento=None)
                    if fecha_inicio is not None:
                        items = items.filter(fecha__gte = fecha_inicio)
                    if fecha_fin is not None:
                        items = items.filter(fecha__lte = fecha_fin)
                        
                elif tipo.nombre.lower().find('courier') > -1:
                    items = Courier.objects.filter(requerimiento=None)
                    if fecha_inicio is not None:
                        items = items.filter(fechaingreso__gte = fecha_inicio)
                    if fecha_fin is not None:
                        items = items.filter(fechaingreso__lte = fecha_fin)               
                
                if len(items)>0:
                    req.save()
                    for item in items:
                        item.requerimiento=req
                        item.save()


#        data = Requerimiento.objects.all()
        data = Requerimiento.objects.filter(generado = True, fecha = date.today(),servicio=tipo)
        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context,data)


class ImprimirDocumentoView(ExportedReport, FormView ):
    form_class = NotaForm
    template_name = 'facturacion/notas.html'
    required_permissions = ('reportes.documento',)

    title = 'Documentos'
    report_class = RequerimientoReport
    model = None

    def render_to_response(self, context, **response_kwargs):
        if self.request.GET.get('send'):
            cod = self.request.GET.get('send')
            if cod <> '':
                data = self.model.objects.filter(pk=cod)

            if data is not None or len(data) != 0:
                report = self.report_class(queryset=data)
                report.title = self.title
                resp = HttpResponse(report.generate_by(TextGenerator, variables={'description':self.description, 'fecha':self.fecha, 'nombre':self.nombre}, encode_to='utf-8'), mimetype='text/plain')
                resp['Cache-Control'] = 'no-cache'
                return resp

class ImprimirRequerimientoView(ImprimirDocumentoView):
    model = Requerimiento
    title = 'Requerimiento'
    report_class = RequerimientoReport

class ImprimirAbonoView(ImprimirDocumentoView):
    model = Abono
    title = 'Abono'
    report_class = AbonoReport

class ImprimirFacturaView(ImprimirDocumentoView):
    model = Factura
    title = 'Factura'
    report_class = FacturaReport

class ImprimirNotaCreditoView(ImprimirDocumentoView):
    model = NotaCredito
    title = 'Nota Credito'
    report_class = NotaCreditoReport

class ImprimirNotaDebitoView(ImprimirDocumentoView):
    model = NotaDebito
    title = 'Nota Debito'
    report_class = NotaDebitoReport

class ServicioView(ExportedReport, FormView):
    form_class = ServicioForm
    title = u'Reporte por tipo de servicio'
    template_name = 'facturacion/servicios.html'
    report_class = ServicioReport
    csv_titles = ['Fecha','Compra','Venta','Base']
    required_permissions = ('reportes.servicio',)
    
    def form_valid(self, form):
        servicio = form.cleaned_data['servicio']
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']
        self.fecha = str(start_date)+' ... '+ str(end_date)
        data = Requerimiento.objects.filter(servicio=servicio, fecha__range=[start_date, end_date] )

        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context,data)
