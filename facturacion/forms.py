# -*- coding: utf-8 -*-

from django import forms
from contabilidad.models import TipoDocumento
from crm.models import Cliente
from caja.models import Servicio
from postal.models import *
from facturacion.models import *
from django.contrib.admin import widgets
from django.forms import ModelForm, ModelMultipleChoiceField, ModelChoiceField
from django.contrib import admin as admin_module
from django.db.models.query_utils import Q
from django.conf import settings

class FacturaForm(ModelForm):
    items = forms.MultipleChoiceField(
                label='Requerimientos a Facturar',
                required=True,
    )  

    class Meta:
        model = Factura
        
    def __init__(self, *args, **kwargs):
        
        instance = kwargs['instance'] if 'instance' in kwargs else None
        if instance is not None:
            requerimientos = Requerimiento.objects.filter(cliente=instance.cliente, factura=instance).order_by('-numero')
            self.base_fields['items'].choices = [ (f.pk,f.descripcion) for f in                                                    
                                     requerimientos
                                     ]
            requerimientos_id = [
                           r.pk 
                           for r in instance.requerimiento_set.all()]            
            self.base_fields['items'].initial = requerimientos_id
        else:
            try:
                cliente = args[0]['cliente']
            except (KeyError, IndexError):
                cliente = "" 
            choices = []
            if cliente != '':
                choices =[
                           (c.pk,c.descripcion) for c in
                            Requerimiento.objects.filter(cliente=cliente,factura__isnull=True).order_by('numero')
                            ]
            self.base_fields['items'].choices = choices
                    
        super(FacturaForm,self).__init__(*args, **kwargs)        

class RequerimientoForm(ModelForm):
    items = forms.MultipleChoiceField(
                label='Items a Facturar',
                required=False,
    )    
    class Meta:
        model = Requerimiento   

class RequerimientoCourierForm(RequerimientoForm):    
    class Meta:
        model = RequerimientoCourier
        
        

class RequerimientoCartaForm(RequerimientoForm):
    class Meta:
        model = RequerimientoCarta

    def clean(self):
        cleaned_data = super(RequerimientoForm,self).clean()
        
        if self.is_valid():
            if cleaned_data['items']:
                for i in cleaned_data['items']:
                    item = Carta.objects.get(pk=i)
                    count = TarifaServicio.objects.filter(
                        Q(tipo=item.formaenvio.tarifa_asociada) & (Q(region=item.region) |
                                                                   Q(region__region=settings.DEFAULT_REGION))).count()
                    if count == 0:
                        raise forms.ValidationError(u"No existe una tarifa para la región %s de la carta #%s"%(item.region,i))
            else:
                raise forms.ValidationError(u"No hay cartas seleccionadas")
        
        return cleaned_data

class RequerimientoNicaboxForm(RequerimientoForm):
    class Meta:
        model = RequerimientoCarta

class NotaForm(forms.Form):
    TIPO_DOC= [
        ('','-------'),       
        (1,'Requerimientos'),
        (2,'Facturas'),
        (3,u'Notas de Crédito'),
        (4,u'Notas de Débito'),
        (5,'Abono'),

    ]
    tipo = forms.ChoiceField(choices=TIPO_DOC, label='Tipo Documento', required=True)
    numero = forms.CharField(label=u'Número', required=False)
    cliente = forms.CharField(label=u'Cliente', required=False)
    fecha_inicio = forms.DateField(widget=widgets.AdminDateWidget(),label='Desde', required=False)
    fecha_fin = forms.DateField(widget=widgets.AdminDateWidget(),label='Hasta', required=False)
    resumen = forms.BooleanField(label='Resumen', required=False)
    
    
class GenerarRequerimientosForm(forms.Form):    
    cliente_inicial = forms.ModelChoiceField(queryset=Cliente.objects.all(),required=False)
    cliente_final = forms.ModelChoiceField(queryset=Cliente.objects.all(),required=False)
    tipo = forms.ModelChoiceField(queryset=Servicio.objects.all(), label='Tipo de Servicio')
    fecha_inicio = forms.DateField(widget=widgets.AdminDateWidget(),label='Desde', required=False)
    fecha_fin = forms.DateField(widget=widgets.AdminDateWidget(),label='Hasta', required=False)

class ServicioForm( forms.Form ):
    servicio = forms.ModelChoiceField(queryset=Servicio.objects.all())
    start_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Desde')
    end_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Hasta')

class AbonoForm(ModelForm):
    facturas = forms.MultipleChoiceField(
                label='Facturas',
                required=False,
    )
    notas = forms.MultipleChoiceField(
                label='Notas de Debito',
                required=False,
    )
    class Meta:
        model = Abono

class NotaDebitoForm(AbonoForm):
    class Meta:
        model = NotaDebito
