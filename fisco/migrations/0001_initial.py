# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'EsquemaImpuestos'
        db.create_table('fisco_esquemaimpuestos', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombreImpuesto', self.gf('django.db.models.fields.CharField')(max_length=50, db_column='nombreImpuesto')),
        ))
        db.send_create_signal('fisco', ['EsquemaImpuestos'])

        # Adding model 'TarifaImpuesto'
        db.create_table('fisco_tarifaimpuesto', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('esquema', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['fisco.EsquemaImpuestos'], db_column='esquema')),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=50, db_column='tipo')),
            ('monto', self.gf('django.db.models.fields.DecimalField')(db_column='monto', decimal_places=4, max_digits=10)),
            ('porcentaje', self.gf('django.db.models.fields.DecimalField')(db_column='porcentaje', decimal_places=2, max_digits=6)),
            ('limiteInferior', self.gf('django.db.models.fields.DecimalField')(blank=True, null=True, db_column='limiteInferior', decimal_places=4, max_digits=10)),
            ('limiteSuperior', self.gf('django.db.models.fields.DecimalField')(blank=True, null=True, db_column='limiteSuperior', decimal_places=4, max_digits=10)),
        ))
        db.send_create_signal('fisco', ['TarifaImpuesto'])

        # Adding model 'CampoDeclaracion'
        db.create_table('fisco_campodeclaracion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tipoCampo', self.gf('django.db.models.fields.CharField')(max_length=50, db_column='tipoCampo')),
            ('label', self.gf('django.db.models.fields.CharField')(max_length=50, db_column='label')),
            ('valor', self.gf('django.db.models.fields.DecimalField')(db_column='valor', decimal_places=4, max_digits=10)),
            ('signo', self.gf('django.db.models.fields.CharField')(max_length=2, db_column='signo')),
            ('valorDeclarable', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contabilidad.Cuenta'], db_column='valorDeclarable')),
        ))
        db.send_create_signal('fisco', ['CampoDeclaracion'])

        # Adding model 'EsquemaDeclaracion'
        db.create_table('fisco_esquemadeclaracion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombreDeclaracion', self.gf('django.db.models.fields.CharField')(max_length=50, db_column='nombreDeclaracion')),
            ('esquemaImpuestos', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['fisco.EsquemaImpuestos'], db_column='esquemaImpuestos')),
            ('campo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['fisco.CampoDeclaracion'], db_column='campoDeclaracion')),
        ))
        db.send_create_signal('fisco', ['EsquemaDeclaracion'])

        # Adding model 'Quincena'
        db.create_table('fisco_quincena', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('declaracion', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['fisco.Declaracion'])),
            ('inicio', self.gf('django.db.models.fields.DateField')()),
            ('fin', self.gf('django.db.models.fields.DateField')()),
            ('quincena_transc', self.gf('django.db.models.fields.IntegerField')()),
            ('quincena_falttr', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('fisco', ['Quincena'])

        # Adding model 'Declaracion'
        db.create_table('fisco_declaracion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('inicioPeriodo', self.gf('django.db.models.fields.DateField')(db_column='inicioPeriodo')),
            ('finPeriodo', self.gf('django.db.models.fields.DateField')(db_column='finPeriodo')),
        ))
        db.send_create_signal('fisco', ['Declaracion'])

    def backwards(self, orm):
        # Deleting model 'EsquemaImpuestos'
        db.delete_table('fisco_esquemaimpuestos')

        # Deleting model 'TarifaImpuesto'
        db.delete_table('fisco_tarifaimpuesto')

        # Deleting model 'CampoDeclaracion'
        db.delete_table('fisco_campodeclaracion')

        # Deleting model 'EsquemaDeclaracion'
        db.delete_table('fisco_esquemadeclaracion')

        # Deleting model 'Quincena'
        db.delete_table('fisco_quincena')

        # Deleting model 'Declaracion'
        db.delete_table('fisco_declaracion')

    models = {
        'contabilidad.cuenta': {
            'Meta': {'object_name': 'Cuenta'},
            'codigo_cuenta': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20', 'db_column': "'codigo_cuenta'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '70', 'db_column': "'nombre'"}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'db_column': "'parent'", 'to': "orm['contabilidad.Cuenta']"}),
            'permite_posteo': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'permite_posteo'"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tipo_cuenta': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'tipo_cuenta'"}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'fisco.campodeclaracion': {
            'Meta': {'object_name': 'CampoDeclaracion'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'label'"}),
            'signo': ('django.db.models.fields.CharField', [], {'max_length': '2', 'db_column': "'signo'"}),
            'tipoCampo': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'tipoCampo'"}),
            'valor': ('django.db.models.fields.DecimalField', [], {'db_column': "'valor'", 'decimal_places': '4', 'max_digits': '10'}),
            'valorDeclarable': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contabilidad.Cuenta']", 'db_column': "'valorDeclarable'"})
        },
        'fisco.declaracion': {
            'Meta': {'object_name': 'Declaracion'},
            'finPeriodo': ('django.db.models.fields.DateField', [], {'db_column': "'finPeriodo'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inicioPeriodo': ('django.db.models.fields.DateField', [], {'db_column': "'inicioPeriodo'"})
        },
        'fisco.esquemadeclaracion': {
            'Meta': {'object_name': 'EsquemaDeclaracion'},
            'campo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['fisco.CampoDeclaracion']", 'db_column': "'campoDeclaracion'"}),
            'esquemaImpuestos': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['fisco.EsquemaImpuestos']", 'db_column': "'esquemaImpuestos'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombreDeclaracion': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'nombreDeclaracion'"})
        },
        'fisco.esquemaimpuestos': {
            'Meta': {'object_name': 'EsquemaImpuestos'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombreImpuesto': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'nombreImpuesto'"})
        },
        'fisco.quincena': {
            'Meta': {'object_name': 'Quincena'},
            'declaracion': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['fisco.Declaracion']"}),
            'fin': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inicio': ('django.db.models.fields.DateField', [], {}),
            'quincena_falttr': ('django.db.models.fields.IntegerField', [], {}),
            'quincena_transc': ('django.db.models.fields.IntegerField', [], {})
        },
        'fisco.tarifaimpuesto': {
            'Meta': {'object_name': 'TarifaImpuesto'},
            'esquema': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['fisco.EsquemaImpuestos']", 'db_column': "'esquema'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'limiteInferior': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'null': 'True', 'db_column': "'limiteInferior'", 'decimal_places': '4', 'max_digits': '10'}),
            'limiteSuperior': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'null': 'True', 'db_column': "'limiteSuperior'", 'decimal_places': '4', 'max_digits': '10'}),
            'monto': ('django.db.models.fields.DecimalField', [], {'db_column': "'monto'", 'decimal_places': '4', 'max_digits': '10'}),
            'porcentaje': ('django.db.models.fields.DecimalField', [], {'db_column': "'porcentaje'", 'decimal_places': '2', 'max_digits': '6'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'tipo'"})
        }
    }

    complete_apps = ['fisco']