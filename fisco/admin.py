# -*- coding: UTF-8 -*- 
from django.contrib import admin
from django.db import models
from django.conf import settings

from fisco.models import *

class TarifaImpuestoInLine( admin.TabularInline ):
    model = TarifaImpuesto

#================================================
# Admin
#================================================
class EsquemaImpuestosAdmin( admin.ModelAdmin ):
    model = EsquemaImpuestos
    inlines = [TarifaImpuestoInLine]

class EsquemaDeclaracionAdmin( admin.ModelAdmin ):
    model = EsquemaDeclaracion
    extra = 1

class DeclaracionAdmin( admin.ModelAdmin ):
    model = Declaracion

class CampoDeclaracionAdmin( admin.ModelAdmin ):
    model = CampoDeclaracion

class QuincenaAdmin( admin.ModelAdmin ):
    list_display = ('inicio','fin','quincena_transc','quincena_falttr')

#admin.site.register(CampoDeclaracion, CampoDeclaracionAdmin)
#admin.site.register(EsquemaImpuestos, EsquemaImpuestosAdmin)
#admin.site.register(EsquemaDeclaracion, EsquemaDeclaracionAdmin)
admin.site.register(Declaracion)
admin.site.register(Quincena, QuincenaAdmin)
