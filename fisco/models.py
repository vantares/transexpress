from django.db import models
from contabilidad.models import Cuenta
from crm.models import Parametro

MES30 = [ 4, 6, 9, 11]
MES31 = [ 1, 3, 5, 7, 8, 10, 12]

#==========================================================
#    Esquema Impuestos
#==========================================================
class EsquemaImpuestos( models.Model ):
    nombreImpuesto = models.CharField(db_column="nombreImpuesto", null=False, blank=False, max_length=50, verbose_name='Nombre Impuesto')

    class Meta:
        verbose_name = "Esquema Impuestos"
        verbose_name_plural = "Esquemas Impuestos"

    def __unicode__( self ):
        return self.nombreImpuesto

#==========================================================        
#    Tarifa Impuesto    
#==========================================================        
class TarifaImpuesto( models.Model ):
    esquema = models.ForeignKey(EsquemaImpuestos, db_column="esquema", null=False, blank=False, verbose_name='Esquema Impuesto')
    tipo = models.CharField(db_column="tipo", null=False, blank=False, max_length=50, verbose_name='Tipo Impuesto')
    monto = models.DecimalField(db_column="monto", null=False, blank=False, max_digits=10, decimal_places=4, verbose_name='Monto')
    porcentaje = models.DecimalField(db_column="porcentaje", null=False, blank=False, max_digits=6, decimal_places=2, verbose_name='porcentaje')
    limiteInferior = models.DecimalField(db_column="limiteInferior", null=True, blank=True, max_digits=10, decimal_places=4, verbose_name='Limite Inferior')
    limiteSuperior = models.DecimalField(db_column="limiteSuperior", null=True, blank=True, max_digits=10, decimal_places=4, verbose_name='Limite Superior')
        
    class Meta:    
        verbose_name = "Tarifa Impuesto"
        verbose_name_plural = "Tarifas Impuestos"
        
    def __unicode__( self ):    
        return self.tipo


#==========================================================        
#    Campo Declaracion    
#==========================================================        
class CampoDeclaracion( models.Model ):
    tipoCampo = models.CharField(db_column="tipoCampo", null=False, blank=False, max_length=50, verbose_name='Tipo Campo')
    label = models.CharField(db_column="label", null=False, blank=False, max_length=50, verbose_name='Label')
    valor = models.DecimalField(db_column="valor", null=False, blank=False, max_digits=10, decimal_places=4, verbose_name='Valor')
    signo = models.CharField(db_column="signo", null=False, blank=False, max_length=2, verbose_name='Signo')
    valorDeclarable = models.ForeignKey(Cuenta, db_column="valorDeclarable", null=False, blank=False, verbose_name='Valor Declarable')
        
    class Meta:    
        verbose_name = "Campo Declaracion"
        verbose_name_plural = "Campos Declaraciones"
        
    def __unicode__( self ):    
        return self.tipoCampo
        

#==========================================================
#    Esquema Declaracion
#==========================================================
class EsquemaDeclaracion( models.Model ):
    nombreDeclaracion = models.CharField(db_column="nombreDeclaracion", null=False, blank=False, max_length=50, verbose_name='Nombre Declaracion')
    esquemaImpuestos = models.ForeignKey(EsquemaImpuestos, db_column="esquemaImpuestos", null=False, blank=False, verbose_name='Esquema Impuestos')
    campo = models.ForeignKey(CampoDeclaracion, db_column="campoDeclaracion", null=False, blank=False, verbose_name='Campo Declaracion')

    class Meta:
        verbose_name = "Esquema Declaracion"
        verbose_name_plural = "Esquemas Declaraciones"

    def __unicode__( self ):
        return self.nombreDeclaracion

class Quincena( models.Model ):
    declaracion = models.ForeignKey(to='Declaracion')
    inicio = models.DateField()    
    fin = models.DateField()    
    quincena_transc =   models.IntegerField( )
    quincena_falttr =   models.IntegerField( )
    
    def __unicode__( self ):
        return 'Del ' + str(self.inicio) +' al '+ str(self.fin)
        
        
#==========================================================        
#    Declaracion    
#==========================================================        
class Declaracion( models.Model ):
    #esquema = models.ForeignKey(EsquemaDeclaracion, db_column="esquema", null=False, blank=False, verbose_name='Esquema Declaracion')
    inicioPeriodo = models.DateField(db_column="inicioPeriodo", null=False, blank=False, verbose_name='Inicio Periodo')    
    finPeriodo = models.DateField(db_column="finPeriodo", null=False, blank=False, verbose_name='Fin Periodo')    

    class Meta:
        verbose_name = "Declaracion"
        verbose_name_plural = "Declaraciones"
        
    def __unicode__( self ):    
        return 'Del ' + str(self.inicioPeriodo) +' al '+ str(self.finPeriodo)

