# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Servicio'
        db.create_table('caja_servicio', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=20, db_column='nombre')),
            ('descripcion', self.gf('django.db.models.fields.CharField')(max_length=50, db_column='descripcion')),
        ))
        db.send_create_signal('caja', ['Servicio'])

        # Adding model 'TarifaServicio'
        db.create_table('caja_tarifaservicio', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tipo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['caja.Servicio'], db_column='tipo')),
            ('tipo_cliente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crm.TipoCliente'], db_column='tipo_cliente')),
            ('monto', self.gf('django.db.models.fields.DecimalField')(db_column='monto', decimal_places=2, max_digits=10)),
            ('adicional', self.gf('django.db.models.fields.DecimalField')(blank=True, null=True, db_column='tarifa_adicional', decimal_places=2, max_digits=10)),
            ('recargo', self.gf('django.db.models.fields.DecimalField')(blank=True, null=True, db_column='recargo', decimal_places=2, max_digits=10)),
            ('limite_inferior', self.gf('django.db.models.fields.DecimalField')(blank=True, null=True, db_column='limite_inferior', decimal_places=4, max_digits=10)),
            ('limite_superior', self.gf('django.db.models.fields.DecimalField')(blank=True, null=True, db_column='limite_superior', decimal_places=4, max_digits=10)),
            ('por_paquete', self.gf('django.db.models.fields.BooleanField')(default=False, db_column='por_paquete')),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.Region'], null=True, db_column='region', blank=True)),
        ))
        db.send_create_signal('caja', ['TarifaServicio'])

        # Adding model 'ComprobanteCaja'
        db.create_table('caja_comprobantecaja', (
            ('numero', self.gf('django.db.models.fields.AutoField')(primary_key=True, db_column='numero')),
            ('monto', self.gf('django.db.models.fields.DecimalField')(db_column='monto', decimal_places=4, max_digits=10)),
            ('signo', self.gf('django.db.models.fields.BooleanField')(default=False, db_column='es_negativo')),
        ))
        db.send_create_signal('caja', ['ComprobanteCaja'])

        # Adding model 'CasaCambio'
        db.create_table('caja_casacambio', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=20, db_column='nombre')),
            ('descripcion', self.gf('django.db.models.fields.CharField')(max_length=50, db_column='descripcion')),
        ))
        db.send_create_signal('caja', ['CasaCambio'])

        # Adding model 'TipoCambio'
        db.create_table('caja_tipocambio', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('casa_cambio', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['caja.CasaCambio'])),
            ('fecha', self.gf('django.db.models.fields.DateField')(unique=True, db_column='fecha')),
            ('moneda', self.gf('django.db.models.fields.CharField')(max_length=4, db_column='moneda')),
            ('compra', self.gf('django.db.models.fields.DecimalField')(db_column='compra', decimal_places=4, max_digits=6)),
            ('venta', self.gf('django.db.models.fields.DecimalField')(db_column='venta', decimal_places=4, max_digits=6)),
            ('base', self.gf('django.db.models.fields.DecimalField')(db_column='base', decimal_places=4, max_digits=6)),
        ))
        db.send_create_signal('caja', ['TipoCambio'])

        # Adding model 'FormaPago'
        db.create_table('caja_formapago', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=20, db_column='nombre')),
            ('codigo', self.gf('django.db.models.fields.CharField')(max_length=20, db_column='codigo')),
        ))
        db.send_create_signal('caja', ['FormaPago'])

    def backwards(self, orm):
        # Deleting model 'Servicio'
        db.delete_table('caja_servicio')

        # Deleting model 'TarifaServicio'
        db.delete_table('caja_tarifaservicio')

        # Deleting model 'ComprobanteCaja'
        db.delete_table('caja_comprobantecaja')

        # Deleting model 'CasaCambio'
        db.delete_table('caja_casacambio')

        # Deleting model 'TipoCambio'
        db.delete_table('caja_tipocambio')

        # Deleting model 'FormaPago'
        db.delete_table('caja_formapago')

    models = {
        'caja.casacambio': {
            'Meta': {'object_name': 'CasaCambio'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'descripcion'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'nombre'"})
        },
        'caja.comprobantecaja': {
            'Meta': {'object_name': 'ComprobanteCaja'},
            'monto': ('django.db.models.fields.DecimalField', [], {'db_column': "'monto'", 'decimal_places': '4', 'max_digits': '10'}),
            'numero': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'numero'"}),
            'signo': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'es_negativo'"})
        },
        'caja.formapago': {
            'Meta': {'object_name': 'FormaPago'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'codigo'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'nombre'"})
        },
        'caja.servicio': {
            'Meta': {'object_name': 'Servicio'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'descripcion'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'nombre'"})
        },
        'caja.tarifaservicio': {
            'Meta': {'object_name': 'TarifaServicio'},
            'adicional': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'null': 'True', 'db_column': "'tarifa_adicional'", 'decimal_places': '2', 'max_digits': '10'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'limite_inferior': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'null': 'True', 'db_column': "'limite_inferior'", 'decimal_places': '4', 'max_digits': '10'}),
            'limite_superior': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'null': 'True', 'db_column': "'limite_superior'", 'decimal_places': '4', 'max_digits': '10'}),
            'monto': ('django.db.models.fields.DecimalField', [], {'db_column': "'monto'", 'decimal_places': '2', 'max_digits': '10'}),
            'por_paquete': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'por_paquete'"}),
            'recargo': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'null': 'True', 'db_column': "'recargo'", 'decimal_places': '2', 'max_digits': '10'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.Region']", 'null': 'True', 'db_column': "'region'", 'blank': 'True'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['caja.Servicio']", 'db_column': "'tipo'"}),
            'tipo_cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.TipoCliente']", 'db_column': "'tipo_cliente'"})
        },
        'caja.tipocambio': {
            'Meta': {'object_name': 'TipoCambio'},
            'base': ('django.db.models.fields.DecimalField', [], {'db_column': "'base'", 'decimal_places': '4', 'max_digits': '6'}),
            'casa_cambio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['caja.CasaCambio']"}),
            'compra': ('django.db.models.fields.DecimalField', [], {'db_column': "'compra'", 'decimal_places': '4', 'max_digits': '6'}),
            'fecha': ('django.db.models.fields.DateField', [], {'unique': 'True', 'db_column': "'fecha'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'moneda': ('django.db.models.fields.CharField', [], {'max_length': '4', 'db_column': "'moneda'"}),
            'venta': ('django.db.models.fields.DecimalField', [], {'db_column': "'venta'", 'decimal_places': '4', 'max_digits': '6'})
        },
        'crm.tipocliente': {
            'Meta': {'object_name': 'TipoCliente'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'nombre'"})
        },
        'postal.region': {
            'Meta': {'object_name': 'Region'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'region': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'region'"})
        }
    }

    complete_apps = ['caja']