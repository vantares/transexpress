from django.contrib import admin
from facturacion.admin import ItemFactura

from caja.models import *

class TipoCambioInline( admin.TabularInline ):
    model = TipoCambio
    extra = 1

class CasaCambioAdmin( admin.ModelAdmin):
    model = CasaCambio
    inlines = [TipoCambioInline]
    extra = 1

class TipoCambioAdmin( admin.ModelAdmin ):
    model = TipoCambio
    list_display = ['casa_cambio','fecha','moneda','compra','venta']

class FormaPagoAdmin( admin.ModelAdmin ):
    model = FormaPago

class TarifaServicioInline( admin.TabularInline ):
    model = TarifaServicio
    extra = 1

class TarifaServicioAdmin( admin.ModelAdmin ):
    model = TarifaServicio

class ServicioAdmin( admin.ModelAdmin):
    model = Servicio
    inlines = [TarifaServicioInline]



class ItemComprobanteInline( admin.TabularInline ):
    model = ItemFactura
    exclude = ['requerimiento']
    extra = 1

class ComprobanteCajaAdmin( admin.ModelAdmin):
    model = ComprobanteCaja
    readonly_fields = ['numero',]
    inlines = [ItemComprobanteInline]
    extra = 1

admin.site.register(CasaCambio, CasaCambioAdmin)
admin.site.register(Servicio, ServicioAdmin)
#admin.site.register(ComprobanteCaja,ComprobanteCajaAdmin)
admin.site.register(TipoCambio, TipoCambioAdmin)
admin.site.register(FormaPago, FormaPagoAdmin)
admin.site.register(TarifaServicio, TarifaServicioAdmin)
