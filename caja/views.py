from caja.forms import  *
from caja.reports import *
from django.contrib.admin.views.decorators import staff_member_required
from models import TipoCambio
from django.views.generic import FormView
from reportes.views import ExportedReport

@staff_member_required
def index(request):
    return TipoCambioView.as_view(template_name='caja/tipocambio.html')

class TipoCambioView(ExportedReport, FormView):
    form_class = TipoCambioForm
    title = u'Reporte de Tipos de Cambio'
    template_name = 'caja/tipocambio.html'
    report_class = TipoCambioReport
    csv_titles = ['Fecha','Compra','Venta','Base']
    required_permissions = ('reportes.tipocambio',)
    
    def form_valid(self, form):
        casa = form.cleaned_data['casa']
        anyo = form.cleaned_data['anyo']
        mes = int(form.cleaned_data['mes'])
        self.title = "Tipo de Cambio en %s" %casa

        data = TipoCambio.objects.filter(casa_cambio = casa ).filter(fecha__year = anyo).filter(fecha__month = mes).all()
        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })

        return self.render_to_response(context,data)
