# -*- coding: utf-8 -*-
from reportes.report import ReporteMembretado
from geraldo import Report, ReportBand, DetailBand, SystemField, Label, ObjectValue, ReportGroup
from geraldo.utils import cm, BAND_WIDTH, TA_CENTER, TA_RIGHT

class TipoCambioReport(ReporteMembretado):
    title = 'Tipos de Cambio'
    default_style = {'fontName':'Courier'}

    class band_detail(DetailBand):
        height = 0.7*cm
        elements = [
            ObjectValue(expression='fecha', left=1.5*cm),
            ObjectValue(expression='compra', left=5*cm),
            ObjectValue(expression='venta', left=8*cm),
            ObjectValue(expression='base', left=11*cm),
            
        ]
        borders = {'bottom': True, 'left': True, 'right': True}

    class band_page_header(ReportBand):
        height = 1.3*cm
        elements = [
            SystemField(expression='%(report_title)s', top=0.1*cm, left=0, width=BAND_WIDTH,
                style={'fontName': 'Courier', 'fontSize': 14, 'alignment': TA_CENTER}),
            SystemField(expression=u'Página %(page_number)d de %(page_count)d', top=0.1*cm,
                width=BAND_WIDTH, style={'alignment': TA_RIGHT}),
            Label(text="Fecha", top=0.8*cm, left=1.5*cm, style={'fontName': 'Courier'}),
            Label(text="Compra", top=0.8*cm, left=5*cm, style={'fontName': 'Courier'}),
            Label(text="Venta", top=0.8*cm, left=8*cm, style={'fontName': 'Courier'}),
            Label(text="Base", top=0.8*cm, left=11*cm, style={'fontName': 'Courier'}),
        ]
        borders = {'all': True}

    class band_summary(ReportBand):
        height = 0.5*cm
        elements = [
            Label(text='Promedios:'),
            ObjectValue(expression='avg(compra)', left=5*cm, style={'fontName': 'Courier'}),
            ObjectValue(expression='avg(venta)', left=8*cm, style={'fontName': 'Courier'}),
            ObjectValue(expression='avg(base)', left=11*cm, style={'fontName': 'Courier'}),

            ]
        borders = {'top': True}
