from django.conf.urls.defaults import patterns, url
from caja.views import *


urlpatterns = patterns('caja.views',
    url(r'^$', TipoCambioView.as_view(template_name='caja/tipocambio.html')),
    url('^tipocambio/$', TipoCambioView.as_view(template_name='caja/tipocambio.html'), name='tipocambio'),
)
