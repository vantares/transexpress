# -*- coding: utf-8 -*-

from django import forms
from caja.models import CasaCambio

class TipoCambioForm(forms.Form):
    MESES = [
        ('01','Enero'),
        ('02','Febrero'),
        ('03','Marzo'),
        ('04','Abril'),
        ('05','Mayo'),
        ('06','Junio'),
        ('07','Julio'),
        ('08','Agosto'),
        ('09','Septiembre'),
        ('10','Octubre'),
        ('11','Noviembre'),
        ('12','Diciembre'),
    ]
    mes = forms.ChoiceField(choices=MESES, label='Mes')
    anyo = forms.IntegerField(label=u'Año')
    casa = forms.ModelChoiceField(queryset=CasaCambio.objects.all())
