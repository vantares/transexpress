from django.db import models
from crm.models import TipoCliente
from contabilidad.moneyfmt import moneyfmt

#==========================================================
#	Servicio
#==========================================================
class Servicio( models.Model ):
    nombre = models.CharField(db_column="nombre", null=False, blank=False, max_length=20, verbose_name="Nombre del Servicio")
    descripcion = models.CharField(db_column="descripcion", null=False, blank=False, max_length=50, verbose_name="Descripcion")


    class Meta:
        verbose_name = "Servicio"
        verbose_name_plural = "Servicios"

    def __unicode__( self ):
        return self.nombre



#==========================================================
#	Tarifa Servicio
#==========================================================
class TarifaServicio( models.Model ):
    tipo = models.ForeignKey(Servicio, db_column="tipo", null=False, blank=False, verbose_name="Tipo de Servicio")
    tipo_cliente = models.ForeignKey(TipoCliente, db_column="tipo_cliente", null=False, blank=False, verbose_name="Tipo de Cliente")
    monto = models.DecimalField(db_column="monto", null=False, blank=False, max_digits=10, decimal_places=2, verbose_name="Monto")
    adicional = models.DecimalField(db_column="tarifa_adicional", null=True, blank=True, max_digits=10, decimal_places=2, verbose_name="Tarifa Adicional")
    recargo = models.DecimalField(db_column="recargo", null=True, blank=True, max_digits=10, decimal_places=2, verbose_name="Recargo")
    limite_inferior = models.DecimalField(db_column="limite_inferior", null=True, blank=True, max_digits=10, decimal_places=4, verbose_name="Limite Inferior")
    limite_superior = models.DecimalField(db_column="limite_superior", null=True, blank=True, max_digits=10, decimal_places=4, verbose_name="Limite Superior")
    por_paquete = models.BooleanField(db_column="por_paquete",default=False,verbose_name="Por paquete")
    region = models.ForeignKey('postal.Region',db_column="region", null=True, blank=True, verbose_name="Region")

    class Meta:
        verbose_name = "Tarifa Servicio"
        verbose_name_plural = "Tarifas de Servicios"

    def __unicode__( self ):
        return self.tipo.nombre + ' : ' + self.tipo_cliente.nombre + ' : ' + moneyfmt(self.monto,2,'US$ ')


#==========================================================
#	Comprobante Caja
#==========================================================
class ComprobanteCaja( models.Model ):
    numero = models.AutoField(db_column='numero', primary_key=True, verbose_name="Numero Comprabante")
    monto = models.DecimalField(db_column="monto", null=False, blank=False, max_digits=10, decimal_places=4, verbose_name="Monto")
    signo = models.BooleanField(db_column="es_negativo", null=False, blank=False, verbose_name="Negativo")

    class Meta:
        verbose_name = "Comprobante de Caja"
        verbose_name_plural = "Comprobantes de Caja"

    def __unicode__( self ):
        return unicode(self.numero)

#==========================
# Informacion de Tipo de Cambio
#==========================
class CasaCambio( models.Model ):
    nombre = models.CharField(db_column="nombre", null=False, blank=False, max_length=20, verbose_name="Nombre")
    descripcion = models.CharField(db_column="descripcion", null=False, blank=False, max_length=50, verbose_name="Descripcion")

    class Meta:
        verbose_name = "Casa Cambio"
        verbose_name_plural = "Casas Cambio"

    def __unicode__( self ):
        return self.nombre

class TipoCambio( models.Model ):
    MONEDAS=(
        ('US$','Dolares'),
    )
    casa_cambio = models.ForeignKey(CasaCambio, blank=False, null=False, verbose_name="Casa Cambio")
    fecha = models.DateField(db_column="fecha", unique=True, null=False, blank=False, verbose_name="fecha")
    moneda = models.CharField(db_column="moneda",choices=MONEDAS, null=False, blank=False, max_length=4, verbose_name="Moneda")
    compra = models.DecimalField(db_column="compra",null=False, blank=False,max_digits=6, decimal_places=4, verbose_name=u"Tasa Compra")
    venta = models.DecimalField(db_column="venta",null=False, blank=False,max_digits=6, decimal_places=4, verbose_name=u"Tasa Venta")
    base = models.DecimalField(db_column="base",null=False, blank=False,max_digits=6, decimal_places=4, verbose_name=u"Tasa Base")
    
    class Meta:
        verbose_name = "Tipo Cambio"
        verbose_name_plural = "Tipos de Cambio"

    def __unicode__( self ):
        return self.base.to_eng_string()

    def values(self):
        return [self.fecha,self.compra,self.venta,self.base]

#==========================================================
#	Forma Pago
#==========================================================
class FormaPago( models.Model ):
    nombre = models.CharField(db_column="nombre", null=False, blank=False, max_length=20, verbose_name="Nombre")
    codigo = models.CharField(db_column="codigo", null=False, blank=False, max_length=20, verbose_name="Codigo")

    class Meta:
        verbose_name = "Forma de Pago"
        verbose_name_plural = "Formas de Pagos"

    def __unicode__( self ):
        return self.nombre
