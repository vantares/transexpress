from tastypie.resources import ModelResource
from tastypie import fields, utils
from tastypie.constants import ALL
from caja.models import TarifaServicio, Servicio

class TarifaServicioResource(ModelResource):    
    class Meta:
        queryset = TarifaServicio.objects.all()


class ServicioResource(ModelResource):    
    code = fields.CharField(attribute='codigo')
    name = fields.CharField(attribute='clientecompleto')
    tarifas = fields.ToManyField(TarifaServicioResource, 'tarifas', full = True, null = True, blank = True)
    
    class Meta:
        queryset = Servicio.objects.all()
        resource_name = 'cliente'
#        fields = ['id','nombre']
        filtering = {
            "code": ('exact',),
            "name": ('startswith',),
            "id": ('exact',),
        }      