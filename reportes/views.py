# -*- coding: UTF-8 -*-
from django.template import RequestContext, Variable, defaultfilters
from django.shortcuts import render_to_response
from geraldo.generators import PDFGenerator
from django.http import HttpResponse
from django.http import  Http404
from reportes.models import *
import unicodecsv as csv
import xlwt
from django.template.defaultfilters import slugify
from contabilidad.models import Periodo

def obtener_periodo():
    periodos = Periodo.objects.filter(activo = True).order_by('-fin')
    if periodos is not None:
        periodo = periodos[0] if len(periodos) >0 else None
    else:
        periodo = None

    return periodo


class ExportedReport(object):
    title = ''
    description = ''
    fecha = ''
    nombre = ''
    airbill = ''
    carrier = ''
    query = ''
    parameters = ['','']
    required_permissions = 'reportes.reportes'
    template_name = 'reportes/base_report.html'
    report_class = None
    csv_titles = []
    csv_ordering = []
    report_variables = None

    def get_context_data(self, **kwargs):
        context = super(ExportedReport, self).get_context_data(**kwargs)
        context['title'] =  self.title
        return context

    def render_to_csv_response(self, rows,titles=None,order=None):
        response = HttpResponse(mimetype='text/csv')
        response['Content-Type'] = 'application/vnd.ms-excel'
        response['Content-Disposition'] = 'attachment; filename=%s.txt' % slugify(self.title)
        writer = csv.writer(response, encoding='utf-8', delimiter='|')
        if titles is not None:
            writer.writerow(titles)
        if order is None:
            for row in rows:
                try:

                    if isinstance(row, dict) and self.csv_ordering is not None:
                        writer.writerow([row[i] for i in self.csv_ordering])
                    elif isinstance(row, dict) and self.csv_ordering is None:
                        raise RuntimeError('csv_ordering can\'t be None')
                    else:
                        writer.writerow(row.values())
                except :
                    writer.writerow(['*****************ERROR EN VALOR********************'])
        else:
            for row in rows:
                writer.writerow([row[i] for i in order])
        return response

    def render_to_xls_response(self, data, head=None, filename='report.xls' ):
        response = HttpResponse(mimetype='application/vnd-ms-excel')
        response['Content-Disposition'] = 'attachment; filename=%s' % filename
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet('untitled')

        heading_xf = xlwt.easyxf('font: bold on; align: wrap on, vert centre, horiz center')
        rowx = 0
        sheet.set_panes_frozen(True)
        sheet.set_horz_split_pos(rowx+1)
        sheet.set_remove_splits(True)
        for colx, value in enumerate(head):
            sheet.write(rowx, colx, value, heading_xf)
        sheet.col(0).width = 256*max([len(row[0]) for row in data])

        for i, row in enumerate(data):
            for j, col in enumerate(row):
                sheet.write(i+1, j, col)
        sheet.col(0).width = 256*max([len(row[0]) for row in data])
        book.save(response)
        return response

#=============================================================================================
    def render_to_xls(self,queryset, fields, filename='report'):
        """
        Output an XLS file of the queryset

        Usage :
        -------
        queryset = Book.objects.all()
        fields = (
            ('Author', 'author.get_full_name'),
            ('Release date', 'release_date|date:"Y-m-d"'),
            ('Title', 'title|title')
        )
        filename = "BooksDB"
        return render_to_xls(
            queryset=queryset,
            filename=filename,
            fields=fields
        )
        """
        ezxf = xlwt.easyxf
        book = xlwt.Workbook(encoding="utf-8")
        sheet = book.add_sheet("Untitled")

        # Set title styles
        heading_xf = ezxf('font:name Verdana, bold on; align: wrap on, vert centre, horiz center;pattern: fore_color green ;')
        i, j = 0, 0

        # Write the titles on the first line
        for f in fields:
            sheet.write(i, j, f[0], heading_xf)
            j+=1

        for object in queryset:
            i += 1
            j = 0
            context = {'object': object}
            for f in fields:

                # I did not find yet a django stuff to
                # deal with templatefilter
                chains = str("object.%s" % f[1]).split("|")
                var = chains[0]

                # We resolve the var to get raw data
                data = Variable(var).resolve(context)

                # If we have filters, we apply them
                # TODO : If django has a method to deal
                # with it, jsut do it
                for chain in chains[1:]:
                    filter_nameargs = chain.split(':')
                    filter_name = filter_nameargs[0]
                    template_filter = getattr(defaultfilters, filter_name)
                    if len(filter_nameargs)>1:
                        args = filter_nameargs[1].replace('"', '')
                        data = template_filter(data, args)
                    else:
                        data = template_filter(data)

                # Finally we write the data on the sheet
                sheet.write(i, j, u"%s" % data)
                j+=1
        response = HttpResponse(mimetype='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename=%s.xls' % filename
        book.save(response)
        return response
#=============================================================================================
    def render_to_response(self,  context, data=None,**response_kwargs):
        if data is None or len(data) == 0:
            return super(ExportedReport, self).render_to_response(context, **response_kwargs)
        elif 'pdf' in self.request.POST:
            periodo = obtener_periodo()
            if periodo is not None:
                texto_periodo = "Del " + periodo.fecha_inicio + " al " + periodo.fecha_fin
                contador = periodo.contador
                gerente = periodo.gerente
            else:
                texto_periodo = ''
                contador = ''
                gerente = ''

            report = self.report_class(queryset=data)
            report.title = self.title
            resp = HttpResponse(mimetype='application/pdf')

            report_variables = {'airbill':self.airbill, 'carrier':self.carrier, 'description':self.description, 'fecha':self.fecha, 'nombre':self.nombre, 'periodo':texto_periodo, 'contador':contador, 'gerente': gerente}
            if self.report_variables is not None:

                report_variables.update(self.report_variables)
            report.generate_by(PDFGenerator,variables=report_variables, filename=resp)

            return resp
        elif 'csv' in self.request.POST:
            return self.render_to_csv_response(data,self.csv_titles)
        elif 'xls' in self.request.POST:
            return self.render_to_xls_response(data,self.xls_titles)
        elif 'xls2' in self.request.POST:
            return self.render_to_xls(data,self.xls_titles)
        else:
            return super(ExportedReport, self).render_to_response(context, **response_kwargs)

    def dispatch(self, request, *args, **kwargs):
        def check_permissions(user):
            for permission in self.required_permissions:
                if not user.has_perm(permission):
                    return False
            return True
        if not check_permissions(request.user):
            raise Http404
        return super(ExportedReport, self).dispatch(request, *args, **kwargs)

    def dictfetchall(self,cursor):
        "Returns all rows from a cursor as a dict"
        desc = cursor.description
        return [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
        ]
