# -*- coding: utf-8 -*-
from geraldo import Report, ReportBand, DetailBand, SystemField, Label, ObjectValue, ReportGroup
from geraldo.utils import cm, BAND_WIDTH, TA_CENTER, TA_RIGHT
from reportlab.lib.pagesizes import LETTER
from crm.models import Parametro
t = Parametro.objects.get(pk=2)
class ReporteMembretado(Report):      
    author = t.valor
    parameters = []
    margin_top = 0.3*cm
    margin_bottom = 0.3*cm
    margin_left = 0.3*cm
    margin_right = 0.3*cm
    page_size = (LETTER)
    class band_page_footer(ReportBand):
        height = 0.5*cm
        elements = [
            Label(text=t.valor, top=0.1*cm),
            SystemField(expression='Impreso en %(now:%b %d, %Y)s  a las %(now:%H:%M)s', top=0.1*cm,
                width=BAND_WIDTH, style={'alignment': TA_RIGHT}),
            ]
        borders = {'top': True}    
