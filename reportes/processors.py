from reportes.models import Noticia

def noticias(request):
    noti = Noticia.objects.all().order_by('fecha')
    return{
        'noticias':noti
    }
