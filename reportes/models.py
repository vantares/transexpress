from django.db import models
from postal.models import Casillero
from crm.models import Cliente
from django.core.management import call_command

class Noticia( models.Model ):
    texto = models.CharField(max_length=100, verbose_name='texto')    
    detalle = models.TextField(blank=True)
    fecha = models.DateTimeField(auto_now=True)    

    class Meta:
        verbose_name = "noticia Browse"
        verbose_name_plural = "noticias Browse" 

    def __unicode__( self ):
        return self.texto

class BloqueoCuenta( models.Model ):
    casillero = models.ForeignKey(Casillero, blank=True, null=True, verbose_name="Nicabox")
    cliente = models.ForeignKey(Cliente, blank=True, null=True, verbose_name="cliente")
    bloquear = models.BooleanField(null=False, blank=False, default=False, verbose_name="Bloquear / Desbloquear")
    comentario = models.CharField(max_length=500)

    def __unicode__( self ):
        return self.comentario
    
    def save(self):
        if self.bloquear == True:
            if self.cliente <> None:
                c = Cliente.objects.get(pk=self.cliente)
                c.activo = False
                c.bloqueado = True
                c.save()
            elif self.casillero <> None:
                c = Cliente.objects.get(pk=self.casillero.cliente)
                c.activo = False
                c.bloqueado = True
                c.save()
        if self.bloquear == False:
            if self.cliente <> None:
                c = Cliente.objects.get(pk=self.cliente)
                c.activo = True
                c.bloqueado = False
                c.save()
            elif self.casillero <> None:
                c = Cliente.objects.get(pk=self.casillero.cliente)
                c.activo = True
                c.bloqueado = False
                c.save()
        models.Model.save(self)

class NoticiaRead( Noticia ):
    class Meta:
        verbose_name = "noticia"
        verbose_name_plural = "noticias" 
        proxy = True

class Reportes( models.Model ):
    class Meta:
        permissions = (
            ('reportes', 'Puede ver seccion reportes'),
            ('clientes', 'Clientes-Reporte'), #Reportes Crm
            ('estadocuenta', 'Estado Cuenta-Reporte'),
            ('cajapostal', 'Caja Postal-Reporte'), #Reportes Postal
            ('cajaanulada', 'Caja Anulada-Reporte'),
            ('paquete', 'Paquete-Reporte'),
            ('courier', 'Courier-Reporte'),
            ('cartaxcliente', 'Cartas Cliente-Reporte'),
            ('status', 'Status-Paquete-Reporte'),
            ('cobros', 'Cobros-Reporte'),
            ('exoneraciones', 'Exoneraciones-Reporte'),
            ('hojasc', 'Hojas C-Reporte'),
            ('envios', 'Control Envio-Reporte'),
            ('peso', 'Control Peso-Reporte'),
            ('examen', 'Examen Previo-Reporte'),
            ('tramite', 'Tramite Articulo-Reporte'),
            ('aduanapeso', 'Modificar Peso-Aduana'),
            ('manifiestopaquete', 'Manifiesto Paquete-Report'),
            ('cobroadicional', 'Cobro Adicional Paquete-Report'),
            ('pesosclientes', 'Pesos Clientes-Report'),
            ('remisiones', 'Remisiones-Report'),
            ('importar', 'Importar Paquetes'),
            ('manifiestocarta', 'Manifiesto Carta-Report'),
            ('retencion', 'Retencion Aduana-Reporte'),
            ('cambioestado', 'Cambio estado-Reporte'),
            ('empleados', 'Empleados-Reporte'), #Reportes Nominal
            ('horasextras', 'Horas Extras-Reporte'),
            ('planilla', 'Planilla-Reporte'),
            ('ir', 'IR-Reporte'),
            ('inss', 'INSS-Reporte'),
            ('recibo', 'Recibo Empleado-Reporte'),
            ('documento', 'Imprimir Documento'),
            ('reqautomaticos', 'Generar Requerimientos'),
            ('tipocambio', 'Tipos de Cambio'),
            ('cheque', 'Cheque-Reporte'),
            ('cuentas', 'Cuentas-Reporte'),
            ('gastos', 'Gastos-Reporte'),
            ('balancecomprobacion', 'Balance Comprobacion-Reporte'),
            ('auxiliarcuentas', 'Auxiliar Cuentas-Reporte'),
            ('anexoestado', 'Anexo Estado-Reporte'),
            ('balancecomparativo', 'Balance Comparativo-Reporte'),
            ('librodiario', 'Libro Diario-Reporte'),
            ('situacionfinanciera', 'Situacion Financiera-Reporte'),
            ('estadoresultado', 'Estado Resultado-Reporte'),
            ('anexobalance', 'Anexo Balance-Reporte'),
            ('estadocomparativo', 'Estado Comparativo-Reporte'),
            ('mayorauxiliar', 'Mayor Auxiliar-Reporte'),
            ('mayorgeneral', 'Mayor General-Reporte'),
            ('hojasc', 'Hojas C'),
            ('comisiones', 'Comisiones-Reporte'),
            ('pod', 'Exportar POD'),
            ('casilleros', 'Exportar Casilleros'),
            ('servicio', 'Ventas por Servicio'),
        )

class Importar( models.Model ):
    TIPO=(
        #('1','Clientes Nicabox'),
        ('2','Carga Recibida'),
        ('3','Carga Despachada'),
        ('4','Contenido Despachado'),
        ('5','Contenido Recibido'),
        ('6','Cobro Despachado'),
        ('7','Cobro Recibido'),
        ('8','Estado'),
    )
    archivo = models.FileField(upload_to = 'txt', null=False, blank=False)
    tipo = models.CharField(choices=TIPO, null=True,blank=True, max_length=20)

    def __unicode__(self):
        return unicode(self.archivo)
class ImportarManifiesto( models.Model ):
    fecha_despacho = models.DateField()
    cargarec = models.FileField(upload_to = 'txt')
    cargadsp = models.FileField(upload_to = 'txt')
    conterec = models.FileField(upload_to = 'txt', null=True, blank=True)
    contedsp = models.FileField(upload_to = 'txt', null=True, blank=True)
    cobrorec = models.FileField(upload_to = 'txt', null=True, blank=True)
    cobrodsp = models.FileField(upload_to = 'txt', null=True, blank=True)
    status = models.FileField(upload_to = 'txt', null=False, blank=True)

    def __unicode__(self):
        return unicode(self.fecha_despacho)
from reportes.tasks import importar
def comando2(sender, **kwargs):
    obj = kwargs['instance']
    importar.delay(obj.id)

def comando(sender, **kwargs):
    obj = kwargs['instance']
    if obj.tipo == '2':
        call_command('paquetes', obj.id)
    if obj.tipo == '3':
        call_command('paquetes_dsp', obj.id)
    if obj.tipo == '4' or obj.tipo == '5':
        call_command('importar_contenidos', obj.id)
    if obj.tipo == '6' or obj.tipo == '7':
        call_command('importar_cobros', obj.id)
    if obj.tipo == '8':
        call_command('importar_estados', obj.id)

from django.db.models.signals import post_save
#post_save.connect(comando,sender=Importar)
post_save.connect(comando2,sender=ImportarManifiesto)
