# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Noticia'
        db.create_table('reportes_noticia', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('texto', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('detalle', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('fecha', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal('reportes', ['Noticia'])

        # Adding model 'BloqueoCuenta'
        db.create_table('reportes_bloqueocuenta', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('casillero', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.Casillero'], null=True, blank=True)),
            ('cliente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crm.Cliente'], null=True, blank=True)),
            ('bloquear', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('comentario', self.gf('django.db.models.fields.CharField')(max_length=500)),
        ))
        db.send_create_signal('reportes', ['BloqueoCuenta'])

        # Adding model 'Reportes'
        db.create_table('reportes_reportes', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('reportes', ['Reportes'])

        # Adding model 'Importar'
        db.create_table('reportes_importar', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('archivo', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
        ))
        db.send_create_signal('reportes', ['Importar'])

    def backwards(self, orm):
        # Deleting model 'Noticia'
        db.delete_table('reportes_noticia')

        # Deleting model 'BloqueoCuenta'
        db.delete_table('reportes_bloqueocuenta')

        # Deleting model 'Reportes'
        db.delete_table('reportes_reportes')

        # Deleting model 'Importar'
        db.delete_table('reportes_importar')

    models = {
        'crm.cliente': {
            'Meta': {'object_name': 'Cliente', '_ormbases': ['crm.Contacto']},
            'activo': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'activo'"}),
            'bloqueado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'clasificacion': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'clasificacion'"}),
            'codigo': ('django.db.models.fields.IntegerField', [], {}),
            'contacto_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['crm.Contacto']", 'unique': 'True', 'primary_key': 'True'}),
            'exonerado': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'exonerado'"}),
            'organization': ('django.db.models.fields.CharField', [], {'default': "'N/A'", 'max_length': '35', 'null': 'True', 'db_column': "'organization'", 'blank': 'True'}),
            'ruc': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'db_column': "'ruc'", 'blank': 'True'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.TipoCliente']", 'null': 'True', 'blank': 'True'}),
            'tipocobro': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.TipoCobro']", 'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '100', 'null': 'True', 'db_column': "'url'", 'blank': 'True'}),
            'zona': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.Ruta']", 'null': 'True', 'blank': 'True'})
        },
        'crm.contacto': {
            'Meta': {'object_name': 'Contacto'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'apellido'"}),
            'categoria': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'categoria'", 'blank': 'True'}),
            'cedula': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'db_column': "'cedula'", 'blank': 'True'}),
            'estadocivil': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'estadocivil'", 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'db_column': "'foto'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'licencia': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'licencia'", 'blank': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'db_column': "'logo'", 'blank': 'True'}),
            'nacimiento': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'nacimiento'", 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'nombre'"}),
            'nombrecorto': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'nombrecorto'", 'blank': 'True'}),
            'notas': ('django.db.models.fields.TextField', [], {'null': 'True', 'db_column': "'notas'", 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'title'", 'blank': 'True'})
        },
        'crm.ruta': {
            'Meta': {'object_name': 'Ruta'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ruta': ('django.db.models.fields.TextField', [], {'max_length': '300', 'db_column': "'ruta'"}),
            'zona': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'zona'"})
        },
        'crm.tipocliente': {
            'Meta': {'object_name': 'TipoCliente'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'nombre'"})
        },
        'crm.tipocobro': {
            'Meta': {'object_name': 'TipoCobro'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meses': ('django.db.models.fields.IntegerField', [], {}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'nomina.cargo': {
            'Meta': {'object_name': 'Cargo'},
            'cargo': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'cargo'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'nomina.empleado': {
            'Meta': {'object_name': 'Empleado', '_ormbases': ['crm.Contacto']},
            'activo': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'activo'"}),
            'cargo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomina.Cargo']"}),
            'codigoempleado': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'codigo'"}),
            'comisionista': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'comisionista'"}),
            'contacto_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['crm.Contacto']", 'unique': 'True', 'primary_key': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'fecha'", 'blank': 'True'}),
            'idseguro': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'seguro'"}),
            'madre': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'db_column': "'madre'", 'blank': 'True'}),
            'numerohijos': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'hijos'", 'blank': 'True'}),
            'padre': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'db_column': "'padre'", 'blank': 'True'}),
            'sueldo': ('django.db.models.fields.DecimalField', [], {'db_column': "'sueldo'", 'decimal_places': '2', 'max_digits': '10'})
        },
        'nomina.vendedor': {
            'Meta': {'object_name': 'Vendedor', '_ormbases': ['nomina.Empleado']},
            'codvendedor': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'codvendedor'"}),
            'empleado_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['nomina.Empleado']", 'unique': 'True', 'primary_key': 'True'})
        },
        'postal.casillero': {
            'Meta': {'object_name': 'Casillero'},
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.Cliente']"}),
            'codigonicabox': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'nicabox'"}),
            'dirdomicilio': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'domicilio': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'fechafin': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'fin'", 'blank': 'True'}),
            'fincontrato': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'fincontrato'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iniciocontrato': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'inicio'", 'blank': 'True'}),
            'justificacion': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'default': "'2NMX86'", 'max_length': '100', 'null': 'True', 'db_column': "'password'", 'blank': 'True'}),
            'pesomax': ('django.db.models.fields.DecimalField', [], {'db_column': "'pesomax'", 'decimal_places': '2', 'max_digits': '8'}),
            'proximocobro': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'proximocobro'", 'blank': 'True'}),
            'vendedor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomina.Vendedor']", 'null': 'True', 'blank': 'True'})
        },
        'reportes.bloqueocuenta': {
            'Meta': {'object_name': 'BloqueoCuenta'},
            'bloquear': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'casillero': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.Casillero']", 'null': 'True', 'blank': 'True'}),
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.Cliente']", 'null': 'True', 'blank': 'True'}),
            'comentario': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'reportes.importar': {
            'Meta': {'object_name': 'Importar'},
            'archivo': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'})
        },
        'reportes.noticia': {
            'Meta': {'object_name': 'Noticia'},
            'detalle': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'texto': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'reportes.reportes': {
            'Meta': {'object_name': 'Reportes'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['reportes']