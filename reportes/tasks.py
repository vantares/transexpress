from celery import task
from django.core.management import call_command

@task()
def importar(x):
    call_command('paquetes', x)                 # cargarec.txt
    call_command('paquetes_dsp', x)             # cargadsp.txt
    call_command('importar_contenidos', x)      # conterec.txt
    call_command('importar_contenidos_dsp', x)  # contedsp.txt
    call_command('importar_cobros', x)          # cobrorec.txt
    call_command('importar_cobros_dsp', x)      # cobrodsp.txt
    call_command('importar_estados', x)         # status.txt
