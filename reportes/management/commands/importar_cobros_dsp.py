from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ValidationError
from django.conf import settings
from reportes.models import ImportarManifiesto
from postal.models import Movimiento, EstadoPaquete, Paquete, GC
from facturacion.models import CobroAdicional, ConceptoND
import time
import os
import logging
from datetime import datetime

logger = logging.getLogger('cobrosaves')

class Command(BaseCommand):
    # MGA0199133624|2|1/13/2012 9:50:36 AM|Informacion editada|.COMMENTS / comentario
    args = '<archivo_id ...>'
    help = 'Comando para la importacion de Cobros Adicionales de paquetes'

    def handle(self, *args, **options):
        for archivo_id in args:
            try:
                archivo = ImportarManifiesto.objects.get(pk=int(archivo_id))
                archivo.cobrodsp.open()
                os.system("iconv -f ISO_8859-1 -t UTF-8 "+ settings.PROJECT_DIR+archivo.cobrodsp.url+" > "+settings.PROJECT_DIR+"/media/tmp/out.txt")
                os.system("cp "+settings.PROJECT_DIR+"/media/tmp/out.txt " +settings.PROJECT_DIR+archivo.cobrodsp.url)
                os.system("cat /dev/null > "+settings.PROJECT_DIR+"/media/tmp/out.txt ")
                f = archivo.cobrodsp.readlines()
                data = []
                for line in f:
                    data.append(line.split('|'))
                data.pop(len(data)-2)
                guias = []
                for a in GC.objects.values_list('numref'):
                    guias.append(a[0])

                for paquete in data:
                    if paquete[0] in guias:
                        try:
                            #El formato de la fecha no es valido
                            datestring = paquete[1]
                            c = time.strptime(datestring, '%m/%d/%Y')
                            fecha = time.strftime('%Y-%m-%d',c)

                            #Los Movimientos o cambios de estado
                            try:
                                concepto = ConceptoND.objects.get(tipo=paquete[2])
                            except:
                                a = ConceptoND(tipo=paquete[2])
                                a.save()
                                concepto = a
                            m =  CobroAdicional(
                                fecha=fecha,
                                monto=paquete[3],
                                tipocobro=concepto,
                                paquete=Paquete.objects.get(numref=paquete[0]),
                                )
                            m.save()
                            logger.debug(paquete[0]+' | CA | '+str(datetime.now()))

                        except ValidationError:
                            raise CommandError('Alguno de los datos tiene formato invalido o no compatible')
                    else:
                        logger.error(paquete[0] + ' | NE | '+str(datetime.now()))
                archivo.cobrodsp.close()
            except ImportarManifiesto.DoesNotExist:
                raise CommandError('Error, Archivo de Texto no existe')

