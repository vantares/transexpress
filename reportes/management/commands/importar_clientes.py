# -*- coding: utf8 -*-
from django.utils.encoding import smart_str
from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ValidationError
from reportes.models import Importar
from postal.models import Casillero
from crm.models import Cliente, DireccionContacto, TelefonoContacto, EmailContacto, Ruta, TipoDireccion, TipoTelefono, TipoEmail
import time
import os
from django.conf import settings

class Command(BaseCommand):
    # El archivo esta compuesto de la siguiente manera
    #Codigo|Casillero|Fecha Ingreso|Nombre|Apellido|Compania|Direccion|Direccion|Ciudad|Departamento|Telefono|Fax|Registro Iva|Giro|Exento|Tasa Cero|Clasificacion|Vendedor|Cobrador|Ruta Zona|Activo|Tipo Domicilio|Categoria|1-800|Inicio Contrato|Fin Contrato|Acumulacion|Peso Permitido|Tipo Casillero|Tipo Contrato|Email|Tipo Casillero
    
    args = '<archivo_id ...>'
    help = 'Importar Cliente y Casillero'

    def handle(self, *args, **options):
        for archivo_id in args:
            try:
                archivo = Importar.objects.get(pk=int(archivo_id))
                archivo.archivo.open()
                os.system("iconv -f ISO_8859-1 -t UTF-8 "+ settings.PROJECT_DIR+archivo.archivo.url+" > "+settings.PROJECT_DIR+"/media/tmp/out.txt")
                os.system("cp "+settings.PROJECT_DIR+"/media/tmp/out.txt " +settings.PROJECT_DIR+archivo.archivo.url)
                data = []
                f = archivo.archivo.readlines()
                for line in f:
                    data.append(line.split('|'))
                for cliente in data:
                    exs = []
                    for a in Cliente.objects.values_list('codigo'):
                        exs.append(a[0])

                    #try:
                    #Buscamos la zona a la que pertenece
                    try:
                        z = Ruta.objects.get(zona=cliente[19])
                    except:
                        z = None

                    if cliente[0] not in exs:
                    #Aqui guardamos los datos del cliente
                        c = Cliente(
                            codigo=smart_str(cliente[0], encoding='utf-8'), 
                            nombre=smart_str(cliente[3], encoding='utf-8') , 
                            apellido=smart_str(cliente[4], encoding='utf-8'), 
                            organization=smart_str(cliente[5], encoding='utf-8'), 
                            clasificacion= 1 if smart_str(cliente[5], encoding='utf-8') == '' else 2,
                            zona = z,
                            activo= True if smart_str(cliente[20], encoding='utf-8') == 'A' else False,
                            )
                        print c
                        c.save()
                        print c
                    else:
                        print cliente[0]+' ya existe'
                    print cliente[0]
                    #Guardamos su informacion personal como direcciones, telefonos, email
                    if cliente[6] <> '':
                        d1 = DireccionContacto(
                           tipo=TipoDireccion.objects.get(pk=1),
                           direccion=smart_str(cliente[6], encoding='utf-8')+' '+smart_str(cliente[7], encoding='utf-8'),
                           ciudad=smart_str(cliente[8], encoding='utf-8'),
                           departamento=smart_str(cliente[9], encoding='utf-8'),
                           contacto=c
                        )
                        d1.save()
                    
                    if cliente[10] <> '':
                        t1 = TelefonoContacto(
                            tipo=TipoTelefono.objects.get(pk=1),
                            telefono=smart_str(cliente[10], encoding='utf-8'),
                            contacto=c
                        )
                        t1.save()

                    if cliente[11] <> '':
                        t2 = TelefonoContacto(
                            tipo=TipoTelefono.objects.get(pk=2),
                            telefono=smart_str(cliente[11], encoding='utf-8'),
                            contacto=c
                        )
                        t2.save()

                    if cliente[30] <> '':
                        e1 = EmailContacto(
                            tipo=TipoEmail.objects.get(pk=2),
                            email=smart_str(cliente[30], encoding='utf-8'),
                            contacto=c
                        )
                        e1.save()
                    
                    #Guardamos los datos de su casillero
                    if cliente[1] <> '':
                        d1 = cliente[24]
                        d2 = cliente[25]
                        c1 = time.strptime(d1, '%d/%m/%Y')
                        c2 = time.strptime(d2, '%d/%m/%Y')
                        fechai = time.strftime('%Y-%m-%d',c1)
                        fechaf = time.strftime('%Y-%m-%d',c2)
                        ca = Casillero(
                            id=smart_str(cliente[0], encoding='utf-8'),
                            cliente= c,
                            codigonicabox=smart_str(cliente[1], encoding='utf-8'),
                            iniciocontrato=fechai,
                            fechafin=fechaf,
                            pesomax=smart_str(cliente[27], encoding='utf-8')
                        )
                        ca.save()
                    #except:
                    #    print 'ocurrio un error con el cliente no '+cliente[0]
                archivo.archivo.close()
            except Importar.DoesNotExist:
                raise CommandError('Error, Archivo de Texto no existe')

