from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ValidationError
from django.conf import settings
from django.db import IntegrityError, connection
from reportes.models import ImportarManifiesto
from postal.models import Movimiento, EstadoPaquete, Paquete
import time
import logging
from datetime import datetime
logger = logging.getLogger('statussaves')

class Command(BaseCommand):
    # MGA0199133624|2|1/13/2012 9:50:36 AM|Informacion editada|.COMMENTS / comentario
    args = '<archivo_id ...>'
    help = 'Comando para la importacion de estados de paquetes'

    def handle(self, *args, **options):
        for archivo_id in args:
            archivo = ImportarManifiesto.objects.get(pk=int(archivo_id))
            try:
                with open(settings.PROJECT_DIR + archivo.status.url):
                    f = archivo.status.readlines()
                    data = []
                    for line in f:
                        data.append(line.split('|'))
                    data.pop(len(data)-2)
                    guias = []
                    for a in Paquete.objects.values_list('numref'):
                        guias.append(a[0])
                    i = 0
                    for paquete in data:
                        try:
                            exs = []
                            for a in EstadoPaquete.objects.values_list('statusid'):
                                exs.append(a[0])
                            if int(paquete[1]) in exs:
                                try:
                                    stado = EstadoPaquete.objects.get(statusid=paquete[1])
                                except EstadoPaquete.MultipleObjectsReturned:
                                    stado = EstadoPaquete.objects.filter(statusid=paquete[1])[0]
                            else:
                                stado = EstadoPaquete(statusid=paquete[1], estado=paquete[4])
                                stado.save()
                            if paquete[0] in guias:
                                try:
                                    #El formato de la fecha no es valido
                                    datestring = paquete[2]
                                    timestring = datestring.split(' ')
                                    hora = timestring[1]+' '+timestring[2]
                                    c = time.strptime(datestring, '%m/%d/%Y %H:%M:%S %p')
                                    fecha = time.strftime('%Y-%m-%d', c)
                                    #Los Movimientos o cambios de estado
                                    m = Movimiento(
                                        fecha=fecha,
                                        hora=hora,
                                        estado=stado,
                                        paquete=Paquete.objects.get(numref=paquete[0]),
                                        comentarios=paquete[3]
                                    )
                                    try:
                                        m.save()
                                    except IntegrityError:
                                        logger.error(paquete[0] + ' | E | ' + fecha + ' | ' + hora)
                                        connection.close()
                                except ValidationError:
                                    raise CommandError('Alguno de los datos tiene formato invalido o no compatible')
                                    logger.error('ERROR: ' + paquete[0] + ' | ' + str(datetime.now()))
                            else:
                                i += 1
                                logger.error(paquete[0] + ' | NE | ' + str(datetime.now()))
                        except IndexError:
                            logger.error('ENDFILE: '+paquete[0]+' | '+str(datetime.now()))
                    #cantidad de status no importados
                    logger.error('SNI: '+str(i)+' | '+str(datetime.now()))
            except ImportarManifiesto.DoesNotExist:
                raise CommandError('Error, Archivo de Texto no existe')
