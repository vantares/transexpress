from django.core.exceptions import ValidationError
from django.core.management.base import BaseCommand
from reportes.models import ImportarManifiesto
from postal.models import Paquete, Casillero
from django.conf import settings
from datetime import datetime
import logging
import time


logger = logging.getLogger('packagesdspsaves')


class Command(BaseCommand):
    # Solamente para carga despachada ( se actualiza la fecha de dpp de paquetes : los q se envian de miami a nicaragua)
    # MGA0199136794|1 /13/2012|11:9|CASILLERO|MGA0199136794|ca global   partness|29348 roadside dr     ||agoura hills|ca
    # |913011|NIC5166|SISTEMAS INTEGRADOS DE SEGURIDAD S.A|SISTEMAS INTEGRADOS DE SEGURIDAD S.A|LOS ROBLES PORTON PRINCIPAL UNIV. EVANGELICA 1C.SUR 1/2C. ESTE C#75|22788645||Managua||1ZF8A4790396647791|0|0|0|13.35|1|0| DVR|||||13.35||4|541870|79604|8.54|PACKED / Empacado|55064123|False|
    args = '<archivo_id ...>'
    help = 'Importar Paquetes'

    def handle(self, *args, **options):
        """

        :param args:
        :param options:
        """
        for archivo_id in args:
            archivo = ImportarManifiesto.objects.get(pk=int(archivo_id))
            try:
                with open(settings.PROJECT_DIR + archivo.cargarec.url):
                    f = archivo.cargadsp.readlines()

                    data = []
                    for line in f:
                        data.append(line.split('|'))
                    data.pop(len(data) - 2)

                    guias = []
                    for a in Paquete.objects.values_list('numref'):
                        guias.append(a[0])
                    frec, fdsp = None, None
                    for paquete in data:
                        if len(paquete) > 1:
                            if paquete[0] in guias:
                                try:
                                    if paquete[30] != '':
                                        sdsp = paquete[30]
                                        cdsp = time.strptime(sdsp, '%m/%d/%Y %I:%M:%S %p')
                                        fdsp = time.strftime('%Y-%m-%d', cdsp)
                                    else:
                                        fdsp = None
                                    destinatario = unicode(paquete[12], errors='ignore')
                                    direccion_destinatario = unicode(paquete[14], errors='ignore')
                                    telefono_destinatario = paquete[15]
                                    ciudad_destinatario = paquete[17]
                                    Paquete.objects.filter(numref=paquete[0]).update(
                                        fechadespacho=fdsp,
                                        consolidado=paquete[35],
                                        guiaaerea=paquete[33],
                                        servicio=paquete[3],
                                        cliente=destinatario,
                                        direccion=direccion_destinatario,
                                        telefono=telefono_destinatario,
                                        ciudad=ciudad_destinatario,
                                        otros=paquete[22],
                                        bolsa=paquete[34]
                                    )
                                except Exception as e:
                                    logger.error(e)
                                    logger.error('ENDFILE: ' + str(paquete[0]) + ' - ' + str(datetime.now()))
                            else:
                                try:
                                    if paquete[11] != '':
                                        cj = Casillero.objects.get(codigonicabox=paquete[11][3:])
                                    else:
                                        cj = None
                                except Casillero.DoesNotExist:
                                    cj = None
                                try:
                                    if paquete[1] != '':
                                        srec = paquete[1].replace(' ', '')
                                        crec = time.strptime(srec, '%m/%d/%Y')
                                        frec = time.strftime('%Y-%m-%d', crec)
                                    else:
                                        frec = None
                                    if paquete[30] != '':
                                        sdsp = paquete[30]
                                        cdsp = time.strptime(sdsp, '%m/%d/%Y %I:%M:%S %p')
                                        fdsp = time.strftime('%Y-%m-%d', cdsp)
                                    else:
                                        fdsp = None
                                except Exception as e:
                                    logger.error(e)
                                    logger.error('ENDFILE: ' + str(paquete[0]) + ' - ' + str(datetime.now()))

                                if frec and fdsp:
                                    a = (frec.split('-'))
                                    b = (fdsp.split('-'))
                                    print '=================='
                                    print paquete[0]
                                    print 'recibido:' + str(frec)
                                    print 'despachado:' + str(fdsp)
                                    print '=================='

                                    # Caso especial 1 y 2
                                    # Caso 1 : Carga del mismo dia viene en cargadsp pero no en cargarec
                                    # Caso 2 : Carga del mismo mes viene solo en cargadsp y no entro en cargarec
                                    # Caso 3 : Carga del mes anterior viene solo en cargadsp y no entro en cargarec
                                    # Caso 4 : Carga recibida en anio anterior solo viene en cargadsp * aun no esta probado
                                    if (frec == fdsp and frec is not None and fdsp is not None) or (
                                            int(b[1]) == int(a[1])) or (int(b[1]) - 1 == int(a[1])) or (
                                            int(b[0]) > int(a[0])):
                                        try:
                                            re = paquete[5] + '  ' + paquete[6] + '  ' + paquete[8] + '  ' + paquete[
                                                9] + '  ' + paquete[10]
                                            p = Paquete(
                                                casillero=cj,
                                                numref=paquete[0],
                                                fechaingreso=frec,
                                                horaingreso=paquete[2],
                                                servicio=paquete[3],
                                                remitente=unicode(re, errors='ignore'),
                                                cliente=unicode(paquete[12], errors='ignore'),
                                                direccion=unicode(paquete[14], errors='ignore'),
                                                telefono=paquete[15],
                                                ciudad=paquete[17],
                                                otros=paquete[22],
                                                pesolb=paquete[23],
                                                piezas=paquete[24],
                                                declarado=paquete[25],
                                                contenido=paquete[26],
                                                guiaingreso=paquete[19],
                                                fechadespacho=fdsp,
                                                guiaaerea=paquete[33],
                                                bolsa=paquete[34],
                                                consolidado=paquete[35],
                                                flete=paquete[36]
                                            )
                                            p.save()
                                        except Exception as e:
                                            logger.error(e)
                                    else:
                                        #esto significa que fue recibido en meses antes, paquete35 es el num de manifiesto
                                        logger.error(paquete[0] + ' | ' + str(frec) + ' | ' + str(fdsp))

            except ValidationError as e:
                logger.debug(e.messages)