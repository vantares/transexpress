# -*- coding: utf8 -*-
from django.utils.encoding import smart_str
from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ValidationError
from reportes.models import Importar
from postal.models import Casillero
from crm.models import Cliente, DireccionContacto, TelefonoContacto, EmailContacto, Ruta, TipoDireccion, TipoTelefono, TipoEmail, TipoCobro, TipoCliente
import time

class Command(BaseCommand):
    args = '<archivo_id ...>'
    help = 'Importar Cliente y Casillero'

    def handle(self, *args, **options):
        for archivo_id in args:
            try:
                archivo = Importar.objects.get(pk=int(archivo_id))
                archivo.archivo.open()
                data = []
                f = archivo.archivo.readlines()
                for line in f:
                    data.append(line.split('|'))
                for cliente in data:
                    if cliente[29]:
                        tipo, cobro = None, None
                        if cliente[29] == 'Mensual' or cliente[29] == 'Trismestral' or cliente[29] == 'Semestral' or cliente[29] == 'Anual':
                            cobro = TipoCobro.objects.get(nombre=cliente[29])
                        elif cliente[29] == 'Libre' or cliente[29] == 'Contado':
                            tipo = TipoCliente.objects.get(nombre=cliente[29])
                        c = Cliente.objects.filter(codigo=cliente[0]).update(tipo=tipo,tipocobro=cobro)        
                archivo.archivo.close()
            except Importar.DoesNotExist:
                raise CommandError('Error, Archivo de Texto no existe')

