from django.core.management.base import BaseCommand
from django.forms import ValidationError
from reportes.models import ImportarManifiesto
from postal.models import Paquete, Casillero
from django.conf import settings
from datetime import datetime
import logging
import time

logger = logging.getLogger('packagessaves')


class Command(BaseCommand):
    # MGA0199136794|1 /13/2012|11:9|CASILLERO|MGA0199136794|ca global   partness|29348 roadside dr     ||agoura hills|ca|913011|NIC5166|SISTEMAS INTEGRADOS DE SEGURIDAD S.A|SISTEMAS INTEGRADOS DE SEGURIDAD S.A|LOS ROBLES PORTON PRINCIPAL UNIV. EVANGELICA 1C.SUR 1/2C. ESTE C#75|22788645||Managua||1ZF8A4790396647791|0|0|0|13.35|1|0| DVR|||||13.35||4|541870|79604|8.54|PACKED / Empacado|55064123|False|
    args = '<archivo_id ...>'
    help = 'Importar Paquetes'

    def handle(self, *args, **options):
        for archivo_id in args:
            archivo = ImportarManifiesto.objects.get(pk=int(archivo_id))
            try:
                with open(settings.PROJECT_DIR+archivo.cargarec.url) as manifiesto_file:

                    f = archivo.cargarec.readlines()

                    data = []
                    for line in f:
                        data.append(line.split('|'))
                    data.pop(len(data)-2)

                    guias = []
                    for a in Paquete.objects.values_list('numref'):
                        guias.append(a[0])

                    for paquete in data:
                        if len(paquete) > 1:
                            guia = unicode(paquete[0], errors='ignore')
                            nicabox = unicode(paquete[11], errors='ignore')
                            fecha_rec = paquete[1]
                            fecha_dsp = paquete[30]
                            hora_ingreso = paquete[2]
                            servicio = paquete[3]
                            destinatario = unicode(paquete[12], errors='ignore')
                            direccion_destinatario = unicode(paquete[14], errors='ignore')
                            telefono_destinatario = paquete[15]
                            ciudad_destinatario = paquete[17]

                            if guia not in guias:
                                try:
                                    if nicabox != '':
                                        cj = Casillero.objects.get(codigonicabox=nicabox[3:])
                                    else:
                                        cj = None
                                except Casillero.DoesNotExist:
                                    cj = None
                                    logger.debug('CASILLERO NE {0}'.format(nicabox))
                                try:
                                    if fecha_rec != '':
                                        srec = fecha_rec.replace(' ', '')
                                        crec = time.strptime(srec, '%m/%d/%Y')
                                        frec = time.strftime('%Y-%m-%d', crec)
                                    else:
                                        frec = None
                                    if fecha_dsp != '':
                                        cdsp = time.strptime(fecha_dsp, '%m/%d/%Y %I:%M:%S %p')
                                        fdsp = time.strftime('%Y-%m-%d', cdsp)
                                    else:
                                        fdsp = None

                                except Exception as e:
                                    logger.exception(e)
                                try:
                                    re = paquete[5]+'  '+paquete[6]+'  '+paquete[8]+'  '+paquete[9]+'  '+paquete[10]
                                    print paquete
                                    p = Paquete(
                                        casillero=cj,
                                        numref=guia,
                                        fechaingreso=frec,
                                        horaingreso=hora_ingreso,
                                        servicio=servicio,
                                        remitente=re,
                                        cliente=destinatario,
                                        direccion=direccion_destinatario,
                                        telefono=telefono_destinatario,
                                        ciudad=ciudad_destinatario,
                                        otros=paquete[22],
                                        pesolb=paquete[23],
                                        piezas=paquete[24],
                                        declarado=paquete[25],
                                        contenido=paquete[26],
                                        guiaingreso=paquete[19],
                                        fechadespacho=fdsp,
                                        guiaaerea=paquete[33],
                                        bolsa=paquete[34],
                                        consolidado=paquete[35],
                                        flete=paquete[36]
                                    )
                                    p.save()
                                except Exception as e:
                                    logger.error(e)
                                    logger.error('ENDFILE: '+str(guia)+ ' - '+str(datetime.now()))
                            else:
                                logger.debug(guia +' | E | '+str(datetime.now()))
            except ValidationError as e:
                logger.debug(e.messages)

