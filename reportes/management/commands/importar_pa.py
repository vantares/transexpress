# -*- coding: utf8 -*-
from django.utils.encoding import smart_str
from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ValidationError
from reportes.models import Importar
import time, os
from django.conf import settings
from postal.models import Casillero, PersonaAutorizada

class Command(BaseCommand):
    args = '<archivo_id ...>'
    help = 'Importar Cliente y Casillero'

    def handle(self, *args, **options):
        for archivo_id in args:
            try:
                archivo = Importar.objects.get(pk=int(archivo_id))
                archivo.archivo.open()
                os.system("iconv -f ISO_8859-1 -t UTF-8 "+ settings.PROJECT_DIR+archivo.archivo.url+" > "+settings.PROJECT_DIR+"/media/tmp/out.txt")
                os.system("cp "+settings.PROJECT_DIR+"/media/tmp/out.txt " +settings.PROJECT_DIR+archivo.archivo.url)
                data = []
                f = archivo.archivo.readlines()
                for line in f:
                    data.append(line.split(','))
                for cliente in data:
                    cod =  cliente[0].replace(' ','').replace('"','')
                    try:
                        c = Casillero.objects.get(codigonicabox=str(cod))
                    except Casillero.DoesNotExist:
                        c = None
                    nombre = cliente[2].replace('"','') +' '+ cliente[1].replace('"','')
                    direccion = cliente[3].replace('"','') +' '+ cliente[4].replace('"','') +' '+ cliente[5].replace('"','') +' '+ cliente[6].replace('"','')
                    if c:
                        p = PersonaAutorizada(casillero=c, nombre=nombre, direccion=direccion)
                        p.save()
                archivo.archivo.close()
            except Importar.DoesNotExist:
                raise CommandError('Error, Archivo de Texto no existe')

