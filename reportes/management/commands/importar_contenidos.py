from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ValidationError
from django.conf import settings
from reportes.models import ImportarManifiesto
from postal.models import Movimiento, EstadoPaquete, Paquete, GC
import time
import logging
import os
from datetime import datetime

logger = logging.getLogger('contentsaves')

class Command(BaseCommand):
    # MGA0199136772|REPUESTOS DE TRACTOR|1|325.96 
    args = '<archivo_id ...>'
    help = 'Comando para la importacion de estados de paquetes'

    def handle(self, *args, **options):
        for archivo_id in args:
            try:
                archivo = ImportarManifiesto.objects.get(pk=int(archivo_id))
                archivo.conterec.open()
                os.system("iconv -f ISO_8859-1 -t UTF-8 "+ settings.PROJECT_DIR+archivo.conterec.url+" > "+settings.PROJECT_DIR+"/media/tmp/out.txt")
                os.system("cp "+settings.PROJECT_DIR+"/media/tmp/out.txt " +settings.PROJECT_DIR+archivo.conterec.url)
                os.system("cat /dev/null > "+settings.PROJECT_DIR+"/media/tmp/out.txt ")
                f = archivo.conterec.readlines()
                data = []
                for line in f:
                    data.append(line.split('|'))
                data.pop(len(data)-2)
                guias = []
                for a in GC.objects.values_list('numref'):
                    guias.append(a[0])

                for paquete in data:
                    if paquete[0] in guias:
                        try:
                            m =  Paquete.objects.filter(numref=paquete[0]).update(contenido=paquete[1],piezas=paquete[2],declarado=paquete[3])
                            logger.debug(paquete[0] +' | A | '+str(datetime.now()))

                        except ValidationError:
                            raise CommandError('Alguno de los datos tiene formato invalido o no compatible')
                            logger.error('Alguno de los datos tiene formato invalido o no compatible | '+str(datetime.now()))
                    else:
                        logger.error(paquete[0] + ' | NE | '+str(datetime.now()))
                archivo.conterec.close()
            except ImportarManifiesto.DoesNotExist:
                raise CommandError('Error, Archivo de Texto no existe')

