# -*- coding: UTF-8 -*- 
from django.contrib import admin
from django.db import models
from django.conf import settings
from read_only import ReadOnlyAdmin
from reportes.models import *
from postal.admin import RawIdCustomAdmin

class NoticiaRO( ReadOnlyAdmin, admin.ModelAdmin):
    model = NoticiaRead

class BloqueoAdmin( RawIdCustomAdmin, admin.ModelAdmin ):
    list_display = ['comentario', 'casillero', 'cliente']
    raw_id_fields = ('cliente','casillero')

class ImportarAdmin( admin.ModelAdmin ):
    search_fields = ['archivo']
    list_display = ['id','archivo']

admin.site.register(Noticia)
admin.site.register(ImportarManifiesto)
admin.site.register(Importar, ImportarAdmin)
admin.site.register(NoticiaRead, NoticiaRO)
admin.site.register(BloqueoCuenta, BloqueoAdmin)
