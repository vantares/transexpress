from django.db.models.related  import RelatedObject

class ReadOnlyAdmin( object ):
    actions_on_top = False
    change_form_template = 'admin/trans/read_only.html'

    def get_readonly_fields(self, request, obj=None):
        if getattr(self, 'model', None):
            obj = self.model

        if obj is not None:
            field_names = obj._meta.get_all_field_names()[:]
            readonly_fields = list(super(ReadOnlyAdmin, self).get_readonly_fields(request, obj))
            for name in field_names:
                if name != 'id'  and not isinstance(obj._meta.get_field_by_name(name)[0], RelatedObject):
                    readonly_fields.append(name)

            return readonly_fields

        return ()
    
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
