# -*- coding: utf-8 -*-
from geraldo import *
from reportlab.lib.pagesizes import LETTER, A5
from reportlab.lib.units import cm
from reportlab.lib.enums import TA_RIGHT, TA_CENTER
from reportlab.lib.colors import navy, red, yellow
from reportes.report import ReporteMembretado
from crm.models import Parametro
from reportlab.lib.units import inch


t = Parametro.objects.get(pk=2)
telefono = Parametro.objects.get(pk=3)
direccion = Parametro.objects.get(pk=4)
puerto = Parametro.objects.get(pk=10)
leyenda = Parametro.objects.get(pk=11)
###################################
# Reporte de Cajas Postales
###################################
class CajasReport(ReporteMembretado):
    page_size = landscape(LETTER)
    default_style = {'fontName':'Courier','fontSize': 11 }

    class band_detail(ReportBand):
        height = 0.5*cm
        auto_expand_height = True
        elements=(
            ObjectValue(attribute_name='cliente', left=0.5*cm, width=15*cm),
            ObjectValue(attribute_name='codigonicabox', left=16*cm),
            ObjectValue(attribute_name='pesomax', left=18*cm),
            ObjectValue(attribute_name='password', left=20*cm),
        )

    class band_page_header(ReportBand):
        height = 1.5*cm
        elements = [
            SystemField(expression='%(report_title)s', top=0.1*cm, left=0, width=BAND_WIDTH,
                style={'fontName': 'Courier', 'fontSize': 14, 'alignment': TA_CENTER, 'textColor':navy}),
            Label(text="Cliente", top=0.8*cm, left=0.5*cm),
            Label(text="Codigo Nicabox", top=0.8*cm, left=16*cm),
            Label(text="Peso Maximo", top=0.8*cm, left=18*cm),
            Label(text="Password", top=0.8*cm, left=20*cm),
            SystemField(expression=u'Pag. %(page_number)d of %(page_count)d', top=0.1*cm, width=BAND_WIDTH, style={'alignment': TA_RIGHT}),
            ]
        borders = {'bottom': True}

###################################
# Reporte de Paquetes
###################################
class PaquetesReport(ReporteMembretado):
    page_size = landscape(LETTER)
    default_style = {'fontName':'Courier','fontSize': 11 }

    class band_detail(ReportBand):
        height = 0.5*cm
        auto_expand_height = True
        elements=(
            ObjectValue(attribute_name='numref', left=0.5*cm),
            ObjectValue(attribute_name='cliente', left=5*cm),
            ObjectValue(attribute_name='contenido', left=12*cm),
            ObjectValue(attribute_name='ciudad', left=18*cm),
            ObjectValue(attribute_name='pesolb', left=24*cm),
        )

    class band_page_header(ReportBand):
        height = 1.5*cm
        elements = [
            SystemField(expression=u'%(report_title)s', top=0.1*cm, left=0, width=BAND_WIDTH,
                style={'fontName': 'Courier', 'fontSize': 16, 'alignment': TA_CENTER, 'textColor': navy}),
            Label(text="Referencia", top=0.8*cm, left=0.5*cm),
            Label(text="Cliente", top=0.8*cm, left=5*cm),
            Label(text="Contenido", top=0.8*cm, left=12*cm),
            Label(text="Destino", top=0.8*cm, left=18*cm),
            Label(text="Peso Lb.", top=0.8*cm, left=24*cm),
            SystemField(expression=u'Pag. %(page_number)d de %(page_count)d', top=0.1*cm,
                width=BAND_WIDTH, style={'alignment': TA_RIGHT}),
            ]
        borders = {'bottom':True}


###################################
# Reporte de Cartas
###################################
class CartasReport(ReporteMembretado):
    page_size = landscape(LETTER)
    default_style = {'fontName':'Courier','fontSize': 10 }

    class band_detail(ReportBand):
        height = 0.5*cm
        auto_expand_height = True
        elements=(
            ObjectValue(attribute_name='fecha', left=0.5*cm),
            ObjectValue(attribute_name='formaenvio', left=5*cm),
            ObjectValue(attribute_name='region', left=12*cm),
            ObjectValue(attribute_name='peso', left=18*cm),
            ObjectValue(attribute_name='valor', left=23*cm),
        )

    class band_page_header(ReportBand):
        height = 1.5*cm
        elements = [
            SystemField(expression=u'%(report_title)s', top=0.1*cm, left=0, width=BAND_WIDTH,
                style={'fontName': 'Courier', 'fontSize': 16, 'alignment': TA_CENTER, 'textColor': navy}),
            Label(text="Fecha", top=0.8*cm, left=0.5*cm),
            Label(text="Tipo envio/ servicio", top=0.8*cm, left=5*cm),
            Label(text="Destino", top=0.8*cm, left=12*cm),
            Label(text="Peso", top=0.8*cm, left=18*cm),
            Label(text="Valor", top=0.8*cm, left=23*cm),
            SystemField(expression=u'Pag. %(page_number)d de %(page_count)d', top=0.1*cm,
                width=BAND_WIDTH, style={'alignment': TA_RIGHT}),
            ]
        borders = {'bottom':True}

class ControlEnviosReport(Report):
    title = ''
    default_style = {'fontName':'Courier','fontSize': 10 }
    page_size=  (8.5*inch, 5.5*inch)
    class band_detail(DetailBand):
        height = 5*cm
        elements = [
            Label(text=t.valor, top=0.5*cm, left=1*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Fecha :", top=0.5*cm, left=14.5*cm, style={'fontName': 'Courier-Bold'}),
            SystemField(expression='%(now:%d %b %Y)s', top=0.5*cm, left=16.2*cm),
            Label(text="Control NICABOX", top=1*cm, left=1*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Hora :", top=1*cm, left=14.5*cm, style={'fontName': 'Courier-Bold'}),
            SystemField(expression='%(now:%H:%M)s', top=1*cm, left=16.2*cm),
            Label(text="Responsable :", top=1.5*cm, left=14.5*cm, style={'fontName': 'Courier-Bold'}),
            ObjectValue(expression='responsable', top=1.5*cm, left=17.5*cm),
            Label(text="NICABOX:", top=2*cm, left=1*cm, style={'fontName': 'Courier-Bold'}),
            ObjectValue(expression='casillero', top=2*cm, left=2.8*cm),
            Label(text="CONTROL DE ENVIOS", top=2*cm, left=8*cm),
            ObjectValue(expression="es_credito", top=2*cm, left=14.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="No.", top=2.5*cm, left=7*cm),
            ObjectValue(expression='id', top=2.5*cm, left=7.7*cm),
            Label(text="Suc. 01", top=2.5*cm, left=11*cm),
            Label(text="El ", top=3.5*cm, left=1.5*cm),
            ObjectValue(expression='fecha', top=3.5*cm, left=2.3*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="recibimos de ", top=3.5*cm, left=5*cm),
            ObjectValue(expression='pautorizados', top=3.5*cm, left=8*cm, width=15*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="la correspondencia abajo detallada: ", top=4*cm, left=1.5*cm, width=18*cm),
            Label(text="Destino", top=4.6*cm, left=3*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Tipo Envio", top=4.6*cm, left=9*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Cantidad", top=4.6*cm, left=15*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Depositado por: ", top=10*cm, left=1.5*cm),
            Line(left=4.8*cm, top=10.3*cm, right=13*cm, bottom=10.3*cm),
            Label(text="Firma: ", top=10*cm, left=13.36*cm),
            Line(left=14.8*cm, top=10.3*cm, right=19*cm, bottom=10.3*cm),
            Label(text=leyenda.valor, top=10.7*cm, left=2.2*cm, width=17*cm),
        ]

    subreports = [
        SubReport(
                  queryset_string = '%(object)s.detalles.all()',
                  band_detail = ReportBand(
                        height=0.5*cm,
                        elements=[
                            ObjectValue(attribute_name='lugar', top=0, left=2.4*cm),
                            ObjectValue(attribute_name='tipenvio', top=0, left=8.3*cm),
                            ObjectValue(attribute_name='cantidadcartas', top=0, left=16*cm),
                        ],
                    ),
                    band_footer=ReportBand(
                    height = 1*cm,
                    elements=[
                        Label(text='Total:', left=14*cm, style={'fontName': 'Courier-Bold'}),
                        ObjectValue(attribute_name='cantidad', left=16*cm, action=FIELD_ACTION_SUM, style={'fontName': 'Courier-Bold'}),
                        ],
                    ),
        ),
    ]

#================================================================================================================
class ControlPesoReport(ReporteMembretado):
    title = ''

    page_size=  (8.5*inch, 5.5*inch)
    default_style = {'fontName':'Courier','fontSize': 10 }
    class band_detail(DetailBand):
        height = 9.5*cm
        elements = [
            Label(text=t.valor, top=0.5*cm, left=1*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Fecha :", top=0.5*cm, left=14.5*cm, style={'fontName': 'Courier-Bold'}),
            SystemField(expression='%(now:%d/%m/%Y)s', top=0.5*cm, left=16.2*cm),
            Label(text="Control NICABOX", top=1*cm, left=1*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Hora :", top=1*cm, left=14.5*cm, style={'fontName': 'Courier-Bold'}),
            SystemField(expression='%(now:%H:%M)s', top=1*cm, left=16.2*cm),
            Label(text="Responsable :", top=1.5*cm, left=14.5*cm, style={'fontName': 'Courier-Bold'}),
            ObjectValue(expression='responsable', top=1.5*cm, left=17.3*cm),
            Label(text="Colector :", top=2*cm, left=14.5*cm, style={'fontName': 'Courier-Bold'}),
            SystemField(expression=u'%(var:description)s', top=2*cm, left=17*cm),
            Label(text="CONTROL DE PESO", top=2*cm, left=8*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="No.", top=2.5*cm, left=7*cm),
            ObjectValue(expression='id', top=2.5*cm, left=7.6*cm),
            Label(text="Suc. 01", top=2.5*cm, left=11*cm),
            Label(text="El ", top=3.5*cm, left=1.5*cm),
            ObjectValue(expression='fecha', top=3.5*cm, left=2.6*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="recibimos de USA la correspondencia abajo detallada para el", top=3.5*cm, left=5*cm, width=18*cm),
            Label(text="NICABOX ", top=4*cm, left=1.5*cm, width=16*cm),
            ObjectValue(expression='casillero', top=4*cm, left=3.3*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="de", top=4*cm, left=5*cm),
            ObjectValue(expression='casillero.cliente', top=4*cm, left=5.8*cm, width=12*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="con Domicilio en:", top=4*cm, left=14*cm, width=15*cm),
            ObjectValue(expression='casillero.cliente.direccion', top=4.5*cm, left=1.5*cm, width=12*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Piezas: ", top=5.5*cm, left=1.5*cm),
            ObjectValue(expression='piezas', top=5.5*cm, left=6.5*cm, width=12*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Peso: ", top=6*cm, left=1.5*cm),
            ObjectValue(expression='peso', top=6*cm, left=6.5*cm, width=12*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Recibido por: ", top=7*cm, left=1.5*cm),
            Line(left=4.3*cm, top=7.3*cm, right=12*cm, bottom=7.3*cm),
            Label(text="Firma: ", top=7*cm, left=13*cm),
            Line(left=14.5*cm, top=7.3*cm, right=18.2*cm, bottom=7.3*cm),
            Label(text="FAVOR ESCRIBA SU NOMBRE Y SU FIRMA.", top=7.8*cm, left=5.5*cm, width=15*cm ,style={'fontName': 'Courier-Bold'}),
            Label(text="DESCRIPCION DE DOCUMENTOS", top=8.4*cm, left=6.5*cm, width=15*cm),
            Label(text="Descripcion", top=9*cm, left=2*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Pza", top=9*cm, left=7*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Descripcion", top=9*cm, left=11*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Pza", top=9*cm, left=16*cm, style={'fontName': 'Courier-Bold'}),
        ]

    subreports = [
        SubReport(
                  queryset_string = '%(object)s.detallesd.all()',
                  band_detail = ReportBand(
                        height=0.5*cm,
                        elements=[
                            ObjectValue(attribute_name='tipodoc', top=0.5*cm, left=2.4*cm),
                            ObjectValue(attribute_name='piezas', top=0.5*cm, left=7*cm),
                        ],
                    ),
        ),
        SubReport(
                  queryset_string = '%(object)s.paquetes.all()',
                  band_detail = ReportBand(
                        height=0.5*cm,
                        elements=[
                            ObjectValue(attribute_name='paquete.numref', top=0*cm, left=10.8*cm),
                            ObjectValue(attribute_name='paquete.piezas', top=0*cm, left=16.1*cm),
                        ],
                    ),
        ),
    ]
#======================================================================
class CartaReport(Report):
    title = ''
    default_style = {'fontName':'Courier'}
    margin_top = 1*cm
    margin_left = 0.3*cm
    margin_right = 0.3*cm
    margin_bottom = 0.3*cm
    page_size = LETTER
    default_style = {'fontName':'Courier','fontSize': 10 }
    class band_detail(DetailBand):
        height = 13.3*cm
        elements = [
            Label(text=t.valor, top=0*cm, left=1*cm),
            ObjectValue(expression='paquete.numref', top=0*cm, left=16*cm, width=10*cm, style={'fontName': 'Courier-Bold'}),
            Label(text=direccion.valor, top=0.5*cm, left=1*cm, width=12*cm),
            Label(text="Telefono: "+telefono.valor, top=1*cm, left=1*cm),
            Line(left=1*cm, top=1.7*cm, right=7*cm, bottom=1.7*cm),
            Line(left=7.8*cm, top=1.7*cm, right=19*cm, bottom=1.7*cm),
            Label(text="REMITENTE", top=2*cm, left=1*cm, style={'fontName': 'Courier-Bold'}),
            ObjectValue(expression='paquete.remitente', top=2.5*cm, left=1*cm, width=7*cm),
            Label(text="DESTINATARIO", top=2*cm, left=8*cm, style={'fontName': 'Courier-Bold'}),
            ObjectValue(expression='paquete.casillero.cliente', top=2.5*cm, left=8*cm, width=10*cm),
            ObjectValue(expression='paquete.casillero.cliente.direccion', top=3*cm, left=8*cm, width=13*cm),
            ObjectValue(expression='paquete.casillero.cliente.ciudad', top=3.9*cm, left=8*cm, width=10*cm),
            ObjectValue(expression='paquete.casillero.cliente.telefono', top=4.4*cm, left=8*cm, width=14*cm),
            Line(left=1*cm, top=4.8*cm, right=7*cm, bottom=4.8*cm),
            Line(left=7.8*cm, top=4.8*cm, right=19*cm, bottom=4.8*cm),
            Label(text="NUMERO DE PAQUETES :", top=5*cm, left=1*cm, style={'fontName': 'Courier-Bold'}),
            ObjectValue(expression='paquete.piezas', top=5*cm, left=6*cm, width=10*cm),
            Label(text="FLETE US $:", top=5.5*cm, left=1*cm, style={'fontName': 'Courier-Bold'}),
            ObjectValue(expression='paquete.flet', top=5.5*cm, left=6*cm, width=10*cm),
            Label(text="PESO :", top=6*cm, left=1*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="kg", top=6*cm, left=7*cm, style={'fontName': 'Courier-Bold'}),
            ObjectValue(attribute_name='paquete.kilos', top=6*cm, left=6*cm, width=10*cm),
            Label(text="lb", top=6*cm, left=10.3*cm, style={'fontName': 'Courier-Bold'}),
            ObjectValue(expression='paquete.pesolb', top=6*cm, left=9*cm, width=10*cm),
            Label(text="VALOR DECLARADO US $:", top=6.5*cm, left=1*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="NICABOX #:", top=7*cm, left=1*cm, style={'fontName': 'Courier-Bold'}),
            ObjectValue(expression='paquete.casillero', top=7*cm, left=6*cm, width=10*cm),
            Label(text="DESCRIPCION :", top=8*cm, left=1*cm, style={'fontName': 'Courier-Bold'}),
            SystemField(expression=u'%(var:description)s', top=8.5*cm, left=1*cm, width=19*cm),
            Label(text="Recibido por: ", top=11.5*cm, left=1*cm),
            Line(left=4*cm, top=11.8*cm, right=10*cm, bottom=11.8*cm),
            Label(text="Fecha Entrega: ", top=11.5*cm, left=11*cm),
            Line(left=14*cm, top=11.8*cm, right=19*cm, bottom=11.8*cm),
        ]

class ExamenPrevioReport(Report):
    page_size = LETTER
    default_style = {'fontName':'Courier','fontSize': 10 }
    margin_top = 0.5*cm

    class band_detail(ReportBand):
        height = 0.5*cm
        auto_expand_height = True
        elements=(
            ObjectValue(attribute_name='paquete.numref', left=2*cm),
            ObjectValue(attribute_name='paquete.cliente', left=7*cm),
            ObjectValue(attribute_name='paquete.casillero', left=16*cm),
        )
    class band_page_header(ReportBand):
        height = 10*cm
        elements = [
            SystemField(expression=u'%(report_title)s', top=2*cm, left=1*cm),
            SystemField(expression=u'Managua %(var:fecha)s', top=1*cm, left=1*cm),
            Label(text="Administrador", top=3*cm, left=1*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Aduana Central de Carga Aerea", top=3.5*cm, left=1*cm, width=10*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Su Despacho:", top=4*cm, left=1*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Estimado ", top=5*cm, left=1*cm),
            SystemField(expression=u'%(report_title)s :', top=5*cm, left=3.1*cm),
            SystemField(expression=u'%(var:description)s', top=6*cm, left=1*cm, width=18*cm),
        ]
    class band_summary(ReportBand):
        height = 0.8*cm
        child_bands = [
            ReportBand(
                height = 0.6*cm,
                elements = [
                    Line(left=1*cm, top=10.3*cm, right=18*cm, bottom=10.3*cm),
                    Label(text="Total: ", top=10.5*cm, left=5*cm),
                    ObjectValue(attribute_name='paquete', top=10.5*cm, left=7*cm, action=FIELD_ACTION_COUNT),
                ]),
        ]
    class band_page_footer(ReportBand):
        height = 1*cm
        elements = [
            SystemField(expression=u'%(var:nombre)s', top=0.1*cm),
            Label(text='Gestor Aduanero', top=0.5*cm),
            Label(text=t.valor, top=0.9*cm),
            ]

class TramiteReport( ExamenPrevioReport ):
    page_size = LETTER
    default_style = {'fontName':'Courier','fontSize': 10 }
    class band_page_header(ReportBand):
        height = 10*cm
        elements = [
            SystemField(expression=u'%(report_title)s', top=2*cm, left=1*cm),
            SystemField(expression=u'Managua %(var:fecha)s', top=1*cm, left=1*cm),
            Label(text="Jefe Regimen Viajero", top=3*cm, left=1*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Aduana Central de Carga Aerea", top=3.5*cm, left=1*cm, width=10*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Su Despacho:", top=4*cm, left=1*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Estimado ", top=5*cm, left=1*cm),
            SystemField(expression=u'%(report_title)s :', top=5*cm, left=3.1*cm),
            SystemField(expression=u'%(var:description)s', top=6*cm, left=1*cm, width=18*cm),
        ]

class ManifiestoPaqueteReport( Report ):
    margin_top = 0.5*cm
    margin_bottom = 0.3*cm
    margin_left = 0.3*cm
    margin_right = 0.3*cm
    page_size = LETTER
    default_style = {'fontName':'Courier','fontSize': 8 }
    class band_page_header(ReportBand):
        height = 4.2*cm
        elements = [
            Label(text=t.valor, top=0.5*cm, left=0.5*cm),
            SystemField(expression=u'%(report_title)s:', top=1*cm, left=0.5*cm, width=10*cm),
            SystemField(expression=u'%(var:airbill)s', top=1*cm, left=6.3*cm),
            SystemField(expression=u'Pagina: %(page_number)d', top=0.5*cm, left=14*cm),
            Label(text="Emitido:", top=1*cm, left=14*cm),
            SystemField(expression='%(now:%d/%m/%Y)s', top=1*cm, left=15.5*cm),
            SystemField(expression='%(now:%H:%M:%S)s', top=1*cm, left=17.5*cm),
            Label(text="Fecha de Envio:", top=1.5*cm, left=0.5*cm),
            SystemField(expression=u'%(var:fecha)s', top=1.5*cm, left=3.3*cm),
            Label(text="Destino: Nicaragua", top=2*cm, left=0.5*cm),
            Label(text="Puerto: "+puerto.valor, top=2.5*cm, left=0.5*cm),
            Label(text="Guia Aerea: ", top=3*cm, left=0.5*cm),
            SystemField(expression=u'%(var:description)s', top=3*cm, left=2.5*cm),
            Label(text="Bolsa", top=3.5*cm, left=0.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Guia Ingreso", top=3.5*cm, left=3*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Destinatario", top=3.5*cm, left=8.3*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Pzas", top=3.5*cm, left=13.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Peso Kg", top=3.5*cm, left=14.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Casillero", top=3.5*cm, left=16.2*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Guia", top=3.5*cm, left=18.5*cm, style={'fontName': 'Courier-Bold'}),
        ]
        borders = {'top': True, 'bottom':True}

    class band_detail(ReportBand):
        auto_expand_height = True
        elements=(
            ObjectValue(attribute_name='bolsa', left=0.5*cm),
            ObjectValue(attribute_name='guiaingreso', left=2.7*cm, width=7*cm),
            ObjectValue(attribute_name='destinatario', left=8.3*cm),
            ObjectValue(attribute_name='piezas', left=13.7*cm),
            ObjectValue(attribute_name='kilos', left=14.5*cm),
            ObjectValue(attribute_name='casillero', left=16.3*cm),
            ObjectValue(attribute_name='numref', left=18.3*cm),
        )

    groups = [
        ReportGroup(attribute_name='bolsa',
            band_footer=ReportBand(
                height = 1*cm,
                elements=[
                    Label(text="Total Bolsa :", left=9*cm, style={'fontName': 'Courier-Bold'}),
                    ObjectValue(expression='count(numref)', left=13.7*cm, style={'fontName': 'Courier-Bold'}),
                    ObjectValue(expression='sum(kilos)', left=14.5*cm, style={'fontName': 'Courier-Bold'}),
                ],
            ),
        )
    ]

    class band_summary(ReportBand):
        child_bands = [
            ReportBand(
                elements = [
                    Label(text="Total Manifiesto: ", left=5*cm, style={'fontName':'Courier-Bold'}),
                    ObjectValue(attribute_name='bolsa', left=9*cm, style={'fontName':'Courier-Bold'}, action=FIELD_ACTION_DISTINCT_COUNT),
                    Label(text="Bolsas", left=9.9*cm),
                    ObjectValue(attribute_name='numref', left=13*cm, style={'fontName':'Courier-Bold'}, action=FIELD_ACTION_DISTINCT_COUNT),
                    Label(text="Articulos", left=13.9*cm),
                    ObjectValue(attribute_name='kilos',  left=17*cm, style={'fontName':'Courier-Bold'}, action=FIELD_ACTION_SUM),
                    Label(text="Kilos", left=18.3*cm),
                ]),
        ]
        borders = {'top': True}


class PesosClientesReport( Report ):
    margin_top = 0.5*cm
    margin_bottom = 0.3*cm
    margin_left = 0.3*cm
    margin_right = 0.3*cm
    page_size = LETTER
    default_style = {'fontName':'Courier','fontSize': 8 }
    class band_page_header(ReportBand):
        height = 3*cm
        elements = [
            Label(text=t.valor, top=0.5*cm, left=0.5*cm),
            SystemField(expression=u'%(report_title)s', top=1*cm, left=0.5*cm, width=10*cm),
            SystemField(expression=u'Pagina: %(page_number)d', top=0.5*cm, left=14*cm),
            Label(text="Emitido:", top=1*cm, left=14*cm),
            SystemField(expression='%(now:%d/%m/%Y)s', top=1*cm, left=15.5*cm),
            SystemField(expression='%(now:%H:%M:%S)s', top=1*cm, left=17.5*cm),
            Label(text="Cliente", top=2.5*cm, left=0.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Peso", top=2.5*cm, left=8*cm, style={'fontName': 'Courier-Bold'}),
            ]

    class band_detail(ReportBand):
        auto_expand_height = True

    groups = [
        ReportGroup(attribute_name='casillero',
            band_footer=ReportBand(
                height = 0.5*cm,
                elements=[
                    ObjectValue(attribute_name='casillero.cliente', left=0.5*cm),
                    ObjectValue(expression='sum(peso)', left=8*cm, style={'fontName': 'Courier-Bold'}),
                ],
            ),
        )
    ]

class RemisionReport( Report ):
    margin_top = 0.5*cm
    margin_bottom = 0.3*cm
    margin_left = 0.3*cm
    margin_right = 0.3*cm
    page_size = landscape(LETTER)
    default_style = {'fontName':'Courier','fontSize': 8 }
    class band_page_header(ReportBand):
        height = 3*cm
        elements = [
            Label(text=t.valor, top=0.5*cm, left=0.5*cm),
            SystemField(expression=u'Remision de Mercancias', top=1*cm, left=0.5*cm, width=10*cm),
            SystemField(expression=u'Pagina: %(page_number)d', top=0.5*cm, left=14*cm),
            Label(text="Remision No.", top=2*cm, left=2*cm, style={'fontName': 'Courier-Bold'}),
            SystemField(expression=u'%(var:description)s', top=2*cm, left=4.5*cm),
            Label(text="Fecha", top=2*cm, left=10*cm, style={'fontName': 'Courier-Bold'}),
            SystemField(expression='%(var:fecha)s', top=2*cm, left=11.5*cm),
            Label(text="Registro No.", top=2*cm, left=15*cm, style={'fontName': 'Courier-Bold'}),
            SystemField(expression='%(report_title)s', top=2*cm, left=17.5*cm),
            Label(text="No.guia", top=2.5*cm, left=0.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Consignatario", top=2.5*cm, left=5.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Kilos", top=2.5*cm, left=15*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Bultos", top=2.5*cm, left=16.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Descripcion", top=2.5*cm, left=19*cm, style={'fontName': 'Courier-Bold'}),
            ]

    class band_detail(ReportBand):
        auto_expand_height = True
        elements=(
             ObjectValue(attribute_name='paquete', left=0.5*cm),
            #ObjectValue(attribute_name='paquete.destinatario', left=5.5*cm),
        #    ObjectValue(attribute_name='paquete.kilos', left=15*cm),
        #    ObjectValue(attribute_name='paquete.piezas', left=16.5*cm),
        #    ObjectValue(attribute_name='paquete.contenido', left=19*cm),
           )

    groups = [
        ReportGroup(attribute_name='paquete.cliente',
            band_footer=ReportBand(
                height = 1*cm,
                elements=[
                    ObjectValue(attribute_name='paquete.destinatario', left=5.5*cm),
                    ObjectValue(attribute_name='paquete.kilos', left=15*cm, action=FIELD_ACTION_SUM),
                    ObjectValue(attribute_name='paquete.piezas', left=16.5*cm, action=FIELD_ACTION_SUM),
                ],
            ),
        )
    ]

###################################
# Reporte de Courier
###################################
class CourierReport(ReporteMembretado):
    page_size = landscape(LETTER)
    default_style = {'fontName':'Courier','fontSize': 10 }

    class band_page_header(ReportBand):
        height = 1.5*cm
        elements = [
            SystemField(expression=u'%(report_title)s', top=0.1*cm, left=0, width=BAND_WIDTH,
                style={'fontName': 'Courier', 'fontSize': 16, 'alignment': TA_CENTER, 'textColor': navy}),
            Label(text="Guia", top=0.8*cm, left=0.5*cm),
            Label(text="Remitente", top=0.8*cm, left=3*cm),
            Label(text="Destinatario", top=0.8*cm, left=11*cm),
            Label(text="Piezas", top=0.8*cm, left=18*cm),
            Label(text="Peso", top=0.8*cm, left=21*cm),
            Label(text="Valor Declarado", top=0.8*cm, left=23*cm),
            SystemField(expression=u'Pag. %(page_number)d de %(page_count)d', top=0.1*cm,
                width=BAND_WIDTH, style={'alignment': TA_RIGHT}),
            ]
        borders = {'bottom':True}

    class band_detail(ReportBand):
        height = 0.5*cm
        auto_expand_height = True
        elements=(
            ObjectValue(attribute_name='numref', left=0.5*cm),
            ObjectValue(attribute_name='nombrer', left=3*cm),
            ObjectValue(attribute_name='direccionr', left=3*cm, top=0.8*cm),
            ObjectValue(attribute_name='info_ciudadr', left=3*cm, top=1.6*cm),
            ObjectValue(attribute_name='nombred', left=11*cm),
            ObjectValue(attribute_name='direcciond', left=11*cm, top=0.8*cm),
            ObjectValue(attribute_name='info_ciudadd', left=11*cm, top=1.6*cm),
            ObjectValue(attribute_name='piezas', left=18.5*cm),
            ObjectValue(attribute_name='peso', left=21*cm),
            ObjectValue(attribute_name='declarado', left=24*cm),
        )

class SPXReport( CourierReport ):
    class band_page_header(ReportBand):
        height = 3.5*cm
        elements = [
            Label(text="Emited :", top=0.3*cm, left=23.3*cm),
            SystemField(expression='%(now:%d %b %Y)s', top=0.3*cm, left=25*cm),
            SystemField(expression='%(now:%H:%M)s', top=0.8*cm, left=25*cm),
            Label(text="Owner/Operator: "+t.valor, top=0.3*cm, left=0.5*cm, width=9*cm),
            Label(text="Port of Lading: Managua Airport INT", top=0.8*cm, left=0.5*cm, width=9*cm),
            Label(text="Consolidator: TNT", top=1.3*cm, left=0.5*cm),
            Label(text="Airbill:", top=1.8*cm, left=0.5*cm),
            Label(text="Marks of Nationality and Registration: "+t.valor, top=0.3*cm, left=10*cm, width=15*cm),
            Label(text="Port of Unlading: Miami Airport INT ", top=0.8*cm, left=10*cm, width=10*cm),
            Label(text="De Consolidator: TNT", top=1.3*cm, left=10*cm, width=10*cm),
            SystemField(expression=u'%(var:description)s', top=1.8*cm, left=10*cm, width=10*cm),
            Label(text="No.", top=2.3*cm, left=10*cm),
            SystemField(expression=u'%(var:nombre)s', top=2.3*cm, left=11*cm),
            Label(text="Date:", top=2.3*cm, left=13*cm),
            SystemField(expression=u'%(var:fecha)s', top=2.3*cm, left=14.3*cm),
            Label(text="Guia Aerea", top=3*cm, left=0.5*cm),
            Label(text="Shipper", top=3*cm, left=5*cm),
            Label(text="Consignee", top=3*cm, left=12*cm),
            Label(text="Piezas", top=3*cm, left=20*cm),
            Label(text="Peso", top=3*cm, left=23*cm),
            Label(text="Valor", top=3*cm, left=25*cm),
            ]
        borders = {'bottom': True}

class DOCReport( CourierReport ):
    class band_page_header(ReportBand):
        height = 3.5*cm
        elements = [
            Label(text="Emited :", top=0.3*cm, left=21.5*cm),
            SystemField(expression='%(now:%d %b %Y)s', top=0.3*cm, left=23.2*cm),
            SystemField(expression='%(now:%H:%M)s', top=0.8*cm, left=23.2*cm),
            Label(text="Courier: "+t.valor, top=0.3*cm, left=0.5*cm, width=9*cm),
            Label(text="Origin: Nicaragua", top=0.8*cm, left=0.5*cm),
            Label(text="Carrier:", top=1.3*cm, left=0.5*cm),
            SystemField(expression=u'%(var:carrier)s', top=1.3*cm, left=2.5*cm, width=10*cm),
            Label(text="Airbill:", top=1.8*cm, left=0.5*cm),
            SystemField(expression=u'%(var:airbill)s', top=1.8*cm, left=2.5*cm, width=10*cm),
            SystemField(expression=u'%(var:description)s', top=1.8*cm, left=10*cm, width=10*cm),
            Label(text="No.", top=2.3*cm, left=10*cm),
            SystemField(expression=u'%(var:nombre)s', top=2.3*cm, left=11*cm),
            Label(text="Date:", top=2.3*cm, left=13*cm),
            SystemField(expression=u'%(var:fecha)s', top=2.3*cm, left=14.3*cm),
            Label(text="Guia Aerea", top=3*cm, left=0.5*cm),
            Label(text="Shipper", top=3*cm, left=5*cm),
            Label(text="Consignee", top=3*cm, left=12*cm),
            Label(text="Piezas", top=3*cm, left=20*cm),
            Label(text="Peso", top=3*cm, left=23*cm),
            Label(text="Valor", top=3*cm, left=25*cm),
            ]
        borders = {'bottom': True}
###################################
# Reporte de Manifiesto Carta
###################################
class ManifiestoCartaReport(ReporteMembretado):
    page_size = landscape(LETTER)
    default_style = {'fontName':'Courier','fontSize': 10 }

    class band_detail(ReportBand):
        height = 0.5*cm
        auto_expand_height = True
        elements=(
            ObjectValue(attribute_name='cliente', left=0.5*cm),
            ObjectValue(attribute_name='destinatario', left=10*cm),
            ObjectValue(attribute_name='piezas', left=20*cm),
            ObjectValue(attribute_name='peso', left=23*cm),
            ObjectValue(attribute_name='valor', left=25*cm),
        )

    class band_page_header(ReportBand):
        height = 3.5*cm
        elements = [
            Label(text="Emited :", top=0.3*cm, left=21.5*cm),
            SystemField(expression='%(now:%d %b %Y)s', top=0.3*cm, left=23.2*cm),
            SystemField(expression='%(now:%H:%M)s', top=0.8*cm, left=23.2*cm),
            Label(text="Courier: "+t.valor, top=0.3*cm, left=0.5*cm, width=9*cm),
            Label(text="Origin: Nicaragua", top=0.8*cm, left=0.5*cm),
            Label(text="Carrier:", top=1.3*cm, left=0.5*cm),
            Label(text="Airbill:", top=1.8*cm, left=0.5*cm),
            SystemField(expression=u'%(var:description)s', top=1.8*cm, left=10*cm, width=10*cm),
            Label(text="No.", top=2.3*cm, left=10*cm),
            SystemField(expression=u'%(var:nombre)s', top=2.3*cm, left=11*cm),
            Label(text="Date:", top=2.3*cm, left=13*cm),
            SystemField(expression=u'%(var:fecha)s', top=2.3*cm, left=14.3*cm),
            Label(text="Remitente", top=3*cm, left=0.5*cm),
            Label(text="Destinatario", top=3*cm, left=10*cm),
            Label(text="Piezas", top=3*cm, left=20*cm),
            Label(text="Peso", top=3*cm, left=23*cm),
            Label(text="Valor", top=3*cm, left=25*cm),
            ]
        borders = {'bottom':True}


###################################
# Cobro Adicional Reporte
###################################
class CobroAdicionalReport(ReporteMembretado):
    page_size = landscape(LETTER)
    default_style = {'fontName':'Courier','fontSize': 10 }

    class band_detail(ReportBand):
        height = 0.5*cm
        auto_expand_height = True
        elements=(
            ObjectValue(attribute_name='fecha', left=0.5*cm),
            ObjectValue(attribute_name='paquete.casillero', left=5.5*cm),
            ObjectValue(attribute_name='paquete', left=9*cm),
            ObjectValue(attribute_name='tipocobro', left=14*cm, width=7*cm),
            ObjectValue(attribute_name='monto', left=23*cm),
        )

    class band_page_header(ReportBand):
        height = 1.5*cm
        elements = [
            SystemField(expression=u'%(report_title)s', top=0.1*cm, left=0, width=BAND_WIDTH,
                style={'fontName': 'Courier', 'fontSize': 16, 'alignment': TA_CENTER, 'textColor': navy}),
            Label(text="Fecha", top=0.8*cm, left=0.5*cm),
            Label(text="Casillero", top=0.8*cm, left=5.5*cm),
            Label(text="Guia", top=0.8*cm, left=9*cm),
            Label(text="Concepto", top=0.8*cm, left=14*cm),
            Label(text="valor", top=0.8*cm, left=23*cm),
            SystemField(expression=u'Pag. %(page_number)d de %(page_count)d', top=0.1*cm,
                width=BAND_WIDTH, style={'alignment': TA_RIGHT}),
            ]
        borders = {'bottom':True}

class GuiaCourierReport( Report ):
    margin_top = 1*cm
    margin_bottom = 0.3*cm
    margin_left = 0.3*cm
    margin_right = 0.3*cm
    page_size = landscape(A5)
    default_style = {'fontName':'Courier-Bold','fontSize': 8, 'width':'15*cm' }

    class band_detail(DetailBand):
        elements = [
            ObjectValue(attribute_name='codr', top=0.5*cm, left=0.5*cm),
            ObjectValue(attribute_name='telefonor', top=0.5*cm, left=5.2*cm),
            ObjectValue(attribute_name='ciar', top=1.3*cm, left=0.5*cm, width=9*cm),
            ObjectValue(attribute_name='nombrer', top=2*cm, left=0.5*cm, width=9*cm),
            ObjectValue(attribute_name='direccionr', top=2.7*cm, left=0.5*cm, width=9*cm),
            ObjectValue(attribute_name='info_ciudadr', top=4.3*cm, left=0.5*cm, width=9*cm),
            ObjectValue(attribute_name='refr', top=6.1*cm, left=0.5*cm),

            ObjectValue(attribute_name='ciad', top=2*cm, left=10.5*cm, width=9*cm),
            ObjectValue(attribute_name='nombred', top=2.7*cm, left=10.5*cm, width=9*cm),
            ObjectValue(attribute_name='telefonod', top=2.7*cm, left=15.7*cm),
            ObjectValue(attribute_name='direcciond', top=3.5*cm, left=10.5*cm, width=9*cm),
            ObjectValue(attribute_name='info_ciudadd', top=5.1*cm, left=10.5*cm, width=9*cm),

            ObjectValue(attribute_name='piezas', top=6.7*cm, left=10*cm),
            ObjectValue(attribute_name='peso', top=6.7*cm, left=12*cm),
            ObjectValue(attribute_name='declarado', top=6.7*cm, left=14*cm),
            ObjectValue(attribute_name='largo', top=7.7*cm, left=10*cm),
            ObjectValue(attribute_name='ancho', top=7.7*cm, left=12*cm),
            ObjectValue(attribute_name='alto', top=7.7*cm, left=13*cm),
        ]



###################################
# Reporte de Status
###################################
class StatusReport(Report):
    margin_top = 0.5*cm
    margin_bottom = 0.3*cm
    margin_left = 0.3*cm
    margin_right = 0.3*cm
    page_size = LETTER
    default_style = {'fontName':'Courier','fontSize': 9 }

    class band_detail(ReportBand):
        height = 0.5*cm
        auto_expand_height = True
        elements=(
            ObjectValue(attribute_name='paquete.numref', left=0.5*cm),
            ObjectValue(attribute_name='paquete.destinatario', left=4*cm),
            ObjectValue(attribute_name='paquete.casillero', left=9.6*cm),
            ObjectValue(attribute_name='paquete.declarado', left=11.5*cm),
            Label(text="________________________", left=16*cm, style={'fontName': 'Courier-Bold'}),
        )

    class band_page_header(ReportBand):
        height = 3*cm
        elements = [
            Label(text=t.valor, top=0.5*cm, left=0.5*cm),
            SystemField(expression=u'Reporte de paquetes x tipo de recepcion', top=1*cm, left=0.5*cm, width=10*cm),
            SystemField(expression=u'Pagina: %(page_number)d', top=0.5*cm, left=14*cm),
            Label(text="Emitido:", top=1*cm, left=14*cm),
            Label(text="Recibido por:", top=1.5*cm, left=14*cm),
            SystemField(expression=u'%(var:nombre)s', top=1.5*cm, left=16.5*cm),

            Label(text="Fecha de recepcion:", top=1.5*cm, left=0.5*cm),
            SystemField(expression=u'%(var:fecha)s', top=1.5*cm, left=5*cm),
            ObjectValue(attribute_name='fecha', left=5.4*cm, style={'fontName':'Courier-Bold'}, action=FIELD_ACTION_DISTINCT_COUNT),

            Label(text="Tipo de recepcion:", top=2*cm, left=0.5*cm),
            SystemField(expression=u'%(var:description)s', top=2*cm, left=5*cm),

            SystemField(expression='%(now:%d/%m/%Y)s', top=1*cm, left=15.7*cm),
            SystemField(expression='%(now:%H:%M:%S)s', top=1*cm, left=17.9*cm),

            Label(text="Guia", top=2.5*cm, left=0.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Destinatario", top=2.5*cm, left=4*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Casillero", top=2.5*cm, left=9.4*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="V. Aduana", top=2.5*cm, left=11.5*cm, style={'fontName': 'Courier-Bold'}),
             Label(text="Tot. cobrar", top=2.5*cm, left=13.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Observacion", top=2.5*cm, left=16*cm, style={'fontName': 'Courier-Bold'}),
            ]

    class band_summary(ReportBand):
        child_bands = [
            ReportBand(
                elements = [
                    Label(text="Totales paquetes: ", left=2*cm, style={'fontName':'Courier-Bold'}),
                    ObjectValue(attribute_name='paquete', left=5.4*cm, style={'fontName':'Courier-Bold'}, action=FIELD_ACTION_DISTINCT_COUNT),
                    Label(text="Totales", left=9.3*cm, style={'fontName':'Courier-Bold'},),
                    Label(text="$", left=11.3*cm, style={'fontName':'Courier-Bold'},),
                    ObjectValue(attribute_name='paquete.declarado',  left=11.5*cm, style={'fontName':'Courier-Bold'}, action=FIELD_ACTION_SUM),
                ]),
        ]
        borders = {'top': True}

###################################
# Reporte de Inconsistencias
###################################
class InconsistenciaReport(Report):

    class band_detail(ReportBand):
        height = 0.5*cm
        auto_expand_height = True
        elements=(
            ObjectValue(attribute_name='paquete.numref', left=0.5*cm),
            ObjectValue(attribute_name='paquete.destinatario', left=4*cm),
            ObjectValue(attribute_name='paquete.casillero', left=9.6*cm),
            ObjectValue(attribute_name='paquete.piezas', left=16*cm),
        )

    class band_page_header(ReportBand):
        height = 3*cm
        elements = [
            Label(text=t.valor, top=0.5*cm, left=0.5*cm),
            SystemField(expression=u'Reporte de Inconsistencias Manifiesto:', top=1*cm, left=0.5*cm, width=10*cm),
            SystemField(expression=u'Pagina: %(page_number)d', top=0.5*cm, left=14*cm),
            Label(text="Emitido:", top=1*cm, left=14*cm),

            Label(text="Fecha", top=1.5*cm, left=0.5*cm),
            SystemField(expression=u'%(var:fecha)s', top=1.5*cm, left=5*cm),

            SystemField(expression=u'%(var:airbill)s', top=1*cm, left=6.8*cm),


            SystemField(expression='%(now:%d/%m/%Y)s', top=1*cm, left=15.7*cm),
            SystemField(expression='%(now:%H:%M:%S)s', top=1*cm, left=17.9*cm),

            Label(text="Guia", top=2.5*cm, left=0.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Destinatario", top=2.5*cm, left=4*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Casillero", top=2.5*cm, left=9.4*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Ciudad", top=2.5*cm, left=11.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Doc/Spx", top=2.5*cm, left=13.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Piezas", top=2.5*cm, left=16*cm, style={'fontName': 'Courier-Bold'}),
            ]

    class band_summary(ReportBand):
        child_bands = [
            ReportBand(
                elements = [
                    Label(text="Totales paquetes: ", left=2*cm, style={'fontName':'Courier-Bold'}),
                    ObjectValue(attribute_name='paquete', left=5.7*cm, style={'fontName':'Courier-Bold'}, action=FIELD_ACTION_DISTINCT_COUNT),
                ]),
        ]
        borders = {'top': True}

###################################
# Reporte de Control de Airbill
###################################
class ControlAirbillReport(Report):

    class band_detail(ReportBand):
        height = 0.5*cm
        auto_expand_height = True
        elements=(
            ObjectValue(attribute_name='fecha_y_hora.date', left=0.5*cm),
            ObjectValue(attribute_name='guia', left=4*cm),
            ObjectValue(attribute_name='get_typedoc_display', left=9.6*cm),
            ObjectValue(attribute_name='piezas', left=13.5*cm),
            ObjectValue(attribute_name='peso', left=16*cm),
        )

    class band_page_header(ReportBand):
        height = 3*cm
        elements = [
            Label(text=t.valor, top=0.5*cm, left=0.5*cm),
            SystemField(expression='%(report_title)s', top=1*cm, left=0.5*cm, width=10*cm),
            SystemField(expression=u'Pagina: %(page_number)d', top=0.5*cm, left=14*cm),
            Label(text="Emitido:", top=1*cm, left=14*cm),

            Label(text="Fecha:", top=1.5*cm, left=0.5*cm),
            SystemField(expression=u'%(var:fecha)s', top=1.5*cm, left=4*cm),

            Label(text="Linea:", top=2*cm, left=0.5*cm),
            SystemField(expression=u'%(var:description)s', top=2*cm, left=4*cm),

            SystemField(expression='%(now:%d/%m/%Y)s', top=1*cm, left=15.7*cm),
            SystemField(expression='%(now:%H:%M:%S)s', top=1*cm, left=17.9*cm),

            Label(text="Fecha", top=2.5*cm, left=0.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Guia", top=2.5*cm, left=4*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Doc/Spx", top=2.5*cm, left=9.4*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Piezas", top=2.5*cm, left=13.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Peso", top=2.5*cm, left=16*cm, style={'fontName': 'Courier-Bold'}),
            ]

    class band_summary(ReportBand):
        child_bands = [
            ReportBand(
                elements = [
                    ObjectValue(attribute_name='piezas', left=13.5*cm, style={'fontName':'Courier-Bold'}, action=FIELD_ACTION_SUM),
                    ObjectValue(attribute_name='peso', left=16*cm, style={'fontName':'Courier-Bold'}, action=FIELD_ACTION_SUM),
                ]),
        ]
        borders = {'top': True}
