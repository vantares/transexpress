# -*- coding: utf-8 -*-
from django import forms
from postal.models import Casillero, EstadoPaquete, Region, Paquete, Carta, ControlEnvio, Movimiento, LineaAerea
from django.contrib.admin import widgets
from django.contrib.admin.widgets import ForeignKeyRawIdWidget
from django.contrib import admin
from django.forms import ModelForm
from postal.widgets import RawIdWidget
TYPEDOC=(
    ('1','DOC'),
    ('2','SPX'),
)

class PostalFormBase( forms.Form ):
    codigo = forms.CharField(max_length=300, label='Codigo Nicabox', required=False)

class CodClienteForm( forms.Form ):
    codigo = forms.CharField(max_length=300, label='Codigo cliente')

class CartasForm( PostalFormBase ):
    codigocliente = forms.CharField(max_length=300, label='Codigo Cliente', required=False)

class CajasForm( PostalFormBase ):
    cliente = forms.CharField(max_length=300, label='Nombre Cliente', required=False)
    anulada = forms.BooleanField(label='Contratos Anulados/Cancelados', required=False)
    domicilio = forms.BooleanField(label='Servicio a domicilio', required=False)
    start_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Desde', required=False)
    end_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Hasta', required=False)

class AnuladasForm( forms.Form ):
    start_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Desde', required=False)
    end_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Hasta', required=False)

class PaquetesForm( AnuladasForm, PostalFormBase ):
    referencia = forms.CharField(max_length=100, label='Guia', required=False)
    peso_1 = forms.DecimalField(max_digits=9, decimal_places=2, required=False)
    peso_2 = forms.DecimalField(max_digits=9, decimal_places=2, required=False)

class StatusForm( forms.Form ):
    referencia = forms.CharField(max_length=100, label='Guia', required=False)
    manifiesto = forms.CharField(max_length=100, required=False)
    destino = forms.CharField(max_length=300, label='Destino', required=False)
    cliente = forms.CharField(max_length=300, label='Cliente', required=False)
    start_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Recibido Desde', required=False, help_text="fecha de ingreso")
    end_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Recibido Hasta', required=False)
    fecha = forms.DateField(widget=widgets.AdminDateWidget(), required=False)
    estado = forms.ModelChoiceField(queryset=EstadoPaquete.objects.all(),required=False)
    sin_estado = forms.ModelChoiceField(queryset=EstadoPaquete.objects.all(),required=False)
    recibido_por = forms.CharField(max_length=300, required=False)

class CourierForm( CartasForm ):
    tipo = forms.ChoiceField(choices=TYPEDOC, label='Tipo')
    destino = forms.ModelChoiceField(queryset=Region.objects.all(), required=False)
    start_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Ingreso Desde', required=False)
    end_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Ingreso Hasta', required=False)
    no = forms.CharField(max_length=10, label='Numero', required=False)
    airbill = forms.CharField(max_length=20)
    carrier = forms.CharField(max_length=20)

class CartaForm( CourierForm ):
    nicabox = forms.CharField(max_length=300, label='Nicabox', required=False)
    start_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Desde', required=False)
    end_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Hasta', required=False)
    destino = forms.ModelChoiceField(queryset=Region.objects.all(), required=False)

class ExoneracionForm( forms.Form ):
    nicabox = forms.CharField(max_length=300, label='Nicabox', required=False)
    start_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Desde', required=False)
    end_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Hasta', required=False)
    declarado = forms.BooleanField(label='Incluir valor declarado', required=False)

class ControlForm( forms.Form ):
    nicabox = forms.CharField(max_length=300, label='id')

class ControlPesoForm( forms.Form ):
    fecha = forms.DateField(widget=widgets.AdminDateWidget())
    nicabox = forms.CharField(max_length=300, label='Nicabox')
    piezas = forms.CharField(max_length=300, label='Piezas')
    peso = forms.CharField(max_length=300, label='Peso')

class DesaduanajeForm( forms.Form ):
    descripcion = forms.CharField( widget=forms.Textarea, initial=u'Su paquete fue retenido por Aduana Central de Carga Aerea. con esta "Guia" de TRANS-EXPRESS debe presentarse a la aduana para tramitar el desaduanaje de su paquete, o puede solicitar nuestros sevicios que tiene un costo muy economico. Si despues de 20 dias quisiera desaduanar su paquete la aduana le cobrara US $100 por gastos administrativos y US $2 de almacenaje por dia.')
    start_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Desde')
    end_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Hasta')
    estado = forms.ModelChoiceField(queryset=EstadoPaquete.objects.all(), initial=7)

class CambioEstadoForm( forms.Form ):
    fecha = forms.DateField(widget=widgets.AdminDateWidget())
    hora = forms.TimeField(widget=widgets.AdminTimeWidget())
    estado = forms.ModelChoiceField(queryset=EstadoPaquete.objects.all())
    paquetes = forms.ModelMultipleChoiceField(queryset=Paquete.objects.all())

class ExamenPrevioForm( forms.Form ):
    fecha = forms.CharField( max_length=300, widget=widgets.AdminDateWidget())
    administrador = forms.CharField( max_length=300, initial='Lic. Ernesto Torrez')
    nombre = forms.CharField( max_length=300, initial='Osman Bravo')
    encabezado = forms.CharField( widget=forms.Textarea, label='Encabezado', initial=u'Por medio de la presente me dirijo a usted, para solicitarle su autorizacion para elaborar EXAMEN PREVIO segun lo establecido en el Articulo 13 de la ley 265 (Ley que establece el autodespacho para la importacion, exportacion y otros regimenes aduaneros) al igual que lo define el CAUCA III  en el Art. 49 y el Art. 48 del Reglamento. En BODEGA 3 TRANS-EXPRESS, los cuales detallo:')
    fechap = forms.DateField(widget=widgets.AdminDateWidget(), label='Fecha estado del paquete')
    estado = forms.ModelChoiceField(queryset=EstadoPaquete.objects.all(), initial=36)

class Tramite116Form( forms.Form ):
    fecha = forms.CharField( max_length=300, widget=widgets.AdminDateWidget())
    administrador = forms.CharField( max_length=300, initial='Lic. Francisco Ocampo')
    nombre = forms.CharField( max_length=300, initial='Osman Bravo')
    encabezado = forms.CharField( widget=forms.Textarea, label='Encabezado', initial=u'Por medio de la presente le solicitamos a usted que el personal de aduana nos atienda en el proceso de tramitacion y presentacion de los paquetes a desaduanar bajo el regimen del articulo 116 y/o 114 para el dia de hoy, a continuacion le detallo la lista de paquetes a desaduanar:')
    fechap = forms.DateField(widget=widgets.AdminDateWidget(), label='Fecha estado del paquete')
    estado = forms.ModelChoiceField(queryset=EstadoPaquete.objects.all(), initial=37)

class AduanaPesoForm( forms.Form ):
    referencia = forms.CharField(max_length=100, label='Guia')
    pesokg = forms.DecimalField(max_digits=9, decimal_places=2)

class ManifiestoPaqueteForm( forms.Form ):
    fecha = forms.DateField(widget=widgets.AdminDateWidget(), help_text='fecha de despacho de paquete', required=False)
    guiaaerea = forms.CharField(max_length=100, label='Guia Aerea', required=False)
    manifiesto = forms.CharField(max_length=100, required=False)
    casillero = forms.CharField(max_length=100, required=False)

class ImportarForm( forms.Form ):
    archivo = forms.FileField()

class CobroAdicionalForm( forms.Form ):
    start_date = forms.DateTimeField(widget=widgets.AdminSplitDateTime(), label='Recibido Desde', required=False)
    end_date = forms.DateTimeField(widget=widgets.AdminSplitDateTime(), label='Recibido Hasta', required=False)

class PesosClientesForm( forms.Form ):
    start_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Recibido Desde')
    end_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Recibido Hasta')

class RemisionForm( forms.Form ):
    remision = forms.CharField(max_length=100, label='Remision')
    estado = forms.ModelChoiceField(queryset=EstadoPaquete.objects.all(), initial=7)
    fecha = forms.DateField(widget=widgets.AdminDateWidget())
    registro = forms.CharField(max_length=100, label='Registro No.')

class ManifiestoCartaForm( forms.Form ):
    tipo = forms.ChoiceField(choices=TYPEDOC, label='Tipo', required=False)
    fecha = forms.DateField(widget=widgets.AdminDateWidget())
    guiaaerea = forms.CharField(max_length=100, label='Guia Aerea')
    no = forms.CharField(max_length=10, label='Numero', required=False)

class PodForm( AnuladasForm ):
    estado = forms.ModelChoiceField(queryset=EstadoPaquete.objects.all())

class ControlAirbillForm( AnuladasForm ):
    linea_aerea = forms.ModelChoiceField(queryset=LineaAerea.objects.all(), required=False)

class InconsistenciaForm( forms.Form ):
    manifiesto = forms.CharField(max_length=300, label='Numero de manifiesto')