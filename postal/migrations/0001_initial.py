# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Region'
        db.create_table('postal_region', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('region', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='region')),
        ))
        db.send_create_signal('postal', ['Region'])

        # Adding model 'Lugar'
        db.create_table('postal_lugar', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('lugar', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='lugar')),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.Region'], null=True, blank=True)),
        ))
        db.send_create_signal('postal', ['Lugar'])

        # Adding model 'LineaAerea'
        db.create_table('postal_lineaaerea', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('linea', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='linea')),
        ))
        db.send_create_signal('postal', ['LineaAerea'])

        # Adding model 'Casillero'
        db.create_table('postal_casillero', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cliente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crm.Cliente'])),
            ('codigonicabox', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='nicabox')),
            ('password', self.gf('django.db.models.fields.CharField')(default='N0HS7C', max_length=100, null=True, db_column='password', blank=True)),
            ('iniciocontrato', self.gf('django.db.models.fields.DateField')(null=True, db_column='inicio', blank=True)),
            ('proximocobro', self.gf('django.db.models.fields.DateField')(null=True, db_column='proximocobro', blank=True)),
            ('fincontrato', self.gf('django.db.models.fields.BooleanField')(default=False, db_column='fincontrato')),
            ('justificacion', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('fechafin', self.gf('django.db.models.fields.DateField')(null=True, db_column='fin', blank=True)),
            ('domicilio', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('dirdomicilio', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('pesomax', self.gf('django.db.models.fields.DecimalField')(db_column='pesomax', decimal_places=2, max_digits=8)),
            ('vendedor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nomina.Vendedor'], null=True, blank=True)),
        ))
        db.send_create_signal('postal', ['Casillero'])

        # Adding model 'PersonaAutorizada'
        db.create_table('postal_personaautorizada', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('casillero', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.Casillero'], null=True, blank=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=100, db_column='nombre')),
            ('cedula', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, db_column='cedula', blank=True)),
        ))
        db.send_create_signal('postal', ['PersonaAutorizada'])

        # Adding model 'EstadoPaquete'
        db.create_table('postal_estadopaquete', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('statusid', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('estado', self.gf('django.db.models.fields.CharField')(max_length=100, db_column='estado')),
            ('comentarios', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal('postal', ['EstadoPaquete'])

        # Adding model 'TipoEnvioPaquete'
        db.create_table('postal_tipoenviopaquete', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('codigo', self.gf('django.db.models.fields.IntegerField')()),
            ('envio', self.gf('django.db.models.fields.CharField')(max_length=100, db_column='envio')),
        ))
        db.send_create_signal('postal', ['TipoEnvioPaquete'])

        # Adding model 'TipoTransaccion'
        db.create_table('postal_tipotransaccion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('casillero', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.Casillero'], null=True, blank=True)),
            ('transaccion', self.gf('django.db.models.fields.CharField')(max_length=100, db_column='transaccion')),
            ('fecha', self.gf('django.db.models.fields.DateField')(null=True, db_column='fecha', blank=True)),
        ))
        db.send_create_signal('postal', ['TipoTransaccion'])

        # Adding model 'GC'
        db.create_table('postal_gc', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('numref', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('fechaingreso', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('horaingreso', self.gf('django.db.models.fields.TimeField')(null=True, blank=True)),
            ('piezas', self.gf('django.db.models.fields.IntegerField')(default=1, null=True, blank=True)),
            ('observaciones', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('declarado', self.gf('django.db.models.fields.DecimalField')(default=0, null=True, max_digits=9, decimal_places=2, blank=True)),
            ('largo', self.gf('django.db.models.fields.DecimalField')(default=0, null=True, max_digits=9, decimal_places=2, blank=True)),
            ('alto', self.gf('django.db.models.fields.DecimalField')(default=0, null=True, max_digits=9, decimal_places=2, blank=True)),
            ('ancho', self.gf('django.db.models.fields.DecimalField')(default=0, null=True, max_digits=9, decimal_places=2, blank=True)),
            ('volumetrico', self.gf('django.db.models.fields.DecimalField')(default=0, null=True, max_digits=12, decimal_places=2, blank=True)),
        ))
        db.send_create_signal('postal', ['GC'])

        # Adding model 'Paquete'
        db.create_table('postal_paquete', (
            ('gc_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['postal.GC'], unique=True, primary_key=True)),
            ('casillero', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.Casillero'], null=True, blank=True)),
            ('pesolb', self.gf('django.db.models.fields.DecimalField')(default=0, null=True, max_digits=9, decimal_places=2, blank=True)),
            ('pesokg', self.gf('django.db.models.fields.DecimalField')(default=0, null=True, max_digits=9, decimal_places=2, blank=True)),
            ('contenido', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('servicio', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('remitente', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('cliente', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('ciudad', self.gf('django.db.models.fields.CharField')(default='Managua', max_length=50, null=True, blank=True)),
            ('guiaingreso', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('otros', self.gf('django.db.models.fields.DecimalField')(default=0, null=True, max_digits=9, decimal_places=2, blank=True)),
            ('fechadespacho', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('guiaaerea', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('bolsa', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('consolidado', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('flete', self.gf('django.db.models.fields.DecimalField')(default=0, null=True, max_digits=9, decimal_places=2, blank=True)),
            ('estado', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.EstadoPaquete'], null=True, blank=True)),
        ))
        db.send_create_signal('postal', ['Paquete'])

        # Adding model 'Courier'
        db.create_table('postal_courier', (
            ('gc_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['postal.GC'], unique=True, primary_key=True)),
            ('cliente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crm.Cliente'], null=True, blank=True)),
            ('codr', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('ciar', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('nombrer', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('telefonor', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('direccionr', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('info_ciudadr', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('refr', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('ciad', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('nombred', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('telefonod', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('direcciond', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('info_ciudadd', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.Region'], null=True, blank=True)),
            ('typedoc', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('valor', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=9, decimal_places=2, blank=True)),
            ('peso', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=9, decimal_places=2)),
            ('guiaaerea', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('requerimiento', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['facturacion.Requerimiento'], null=True, blank=True)),
            ('total', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=9, decimal_places=2, blank=True)),
        ))
        db.send_create_signal('postal', ['Courier'])

        # Adding model 'Carta'
        db.create_table('postal_carta', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('referenciapeso', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('casillero', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.Casillero'], null=True, blank=True)),
            ('cliente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crm.Cliente'], null=True, blank=True)),
            ('contsinval', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('remitente', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('direccionr', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('destinatario', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('direcciond', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('formaenvio', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.TipoEnvioPaquete'])),
            ('typedoc', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('numreg', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('piezas', self.gf('django.db.models.fields.IntegerField')()),
            ('peso', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=9, decimal_places=2)),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(default=2, to=orm['postal.Region'])),
            ('valor', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=9, decimal_places=2, blank=True)),
            ('requerimiento', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['facturacion.Requerimiento'], null=True, blank=True)),
            ('total', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=9, decimal_places=2, blank=True)),
        ))
        db.send_create_signal('postal', ['Carta'])

        # Adding model 'Movimiento'
        db.create_table('postal_movimiento', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('estado', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.EstadoPaquete'])),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('fechareal', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('comentarios', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('paquete', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.Paquete'])),
        ))
        db.send_create_signal('postal', ['Movimiento'])

        # Adding model 'ExoneracionVariosAbonos'
        db.create_table('postal_exoneracionvariosabonos', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('casillero', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.Casillero'])),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('fechafin', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('cargo', self.gf('django.db.models.fields.DecimalField')(default=500, max_digits=9, decimal_places=2)),
        ))
        db.send_create_signal('postal', ['ExoneracionVariosAbonos'])

        # Adding model 'Abono'
        db.create_table('postal_abono', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('exoneracion', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.ExoneracionVariosAbonos'])),
            ('abono', self.gf('django.db.models.fields.DecimalField')(default=0, null=True, max_digits=9, decimal_places=2, blank=True)),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal('postal', ['Abono'])

        # Adding model 'ControlEnvio'
        db.create_table('postal_controlenvio', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('casillero', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.Casillero'], null=True, blank=True)),
            ('responsable', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], db_column='responsable')),
        ))
        db.send_create_signal('postal', ['ControlEnvio'])

        # Adding model 'DetalleEnvios'
        db.create_table('postal_detalleenvios', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('envio', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.ControlEnvio'], null=True, blank=True)),
            ('lugar', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.Lugar'], null=True, blank=True)),
            ('tipenvio', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.TipoEnvioPaquete'], null=True, blank=True)),
            ('cantidadcartas', self.gf('django.db.models.fields.CharField')(max_length=500)),
        ))
        db.send_create_signal('postal', ['DetalleEnvios'])

        # Adding model 'TipoDocumento'
        db.create_table('postal_tipodocumento', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('codigo', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('descripcion', self.gf('django.db.models.fields.CharField')(max_length=500)),
        ))
        db.send_create_signal('postal', ['TipoDocumento'])

        # Adding model 'ControlPeso'
        db.create_table('postal_controlpeso', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('casillero', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.Casillero'], null=True, blank=True)),
            ('peso', self.gf('django.db.models.fields.DecimalField')(default=0, null=True, max_digits=9, decimal_places=2, blank=True)),
            ('piezas', self.gf('django.db.models.fields.IntegerField')()),
            ('responsable', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], db_column='responsable')),
            ('requerimiento', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['facturacion.Requerimiento'], null=True, blank=True)),
            ('total', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=9, decimal_places=2, blank=True)),
        ))
        db.send_create_signal('postal', ['ControlPeso'])

        # Adding model 'DetalleDocControlPeso'
        db.create_table('postal_detalledoccontrolpeso', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('control', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.ControlPeso'], null=True, blank=True)),
            ('tipodoc', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.TipoDocumento'], null=True, blank=True)),
            ('piezas', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('postal', ['DetalleDocControlPeso'])

        # Adding model 'DetalleGuiaControlPeso'
        db.create_table('postal_detalleguiacontrolpeso', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('control', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.ControlPeso'], null=True, blank=True)),
            ('paquete', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['postal.Paquete'])),
        ))
        db.send_create_signal('postal', ['DetalleGuiaControlPeso'])

    def backwards(self, orm):
        # Deleting model 'Region'
        db.delete_table('postal_region')

        # Deleting model 'Lugar'
        db.delete_table('postal_lugar')

        # Deleting model 'LineaAerea'
        db.delete_table('postal_lineaaerea')

        # Deleting model 'Casillero'
        db.delete_table('postal_casillero')

        # Deleting model 'PersonaAutorizada'
        db.delete_table('postal_personaautorizada')

        # Deleting model 'EstadoPaquete'
        db.delete_table('postal_estadopaquete')

        # Deleting model 'TipoEnvioPaquete'
        db.delete_table('postal_tipoenviopaquete')

        # Deleting model 'TipoTransaccion'
        db.delete_table('postal_tipotransaccion')

        # Deleting model 'GC'
        db.delete_table('postal_gc')

        # Deleting model 'Paquete'
        db.delete_table('postal_paquete')

        # Deleting model 'Courier'
        db.delete_table('postal_courier')

        # Deleting model 'Carta'
        db.delete_table('postal_carta')

        # Deleting model 'Movimiento'
        db.delete_table('postal_movimiento')

        # Deleting model 'ExoneracionVariosAbonos'
        db.delete_table('postal_exoneracionvariosabonos')

        # Deleting model 'Abono'
        db.delete_table('postal_abono')

        # Deleting model 'ControlEnvio'
        db.delete_table('postal_controlenvio')

        # Deleting model 'DetalleEnvios'
        db.delete_table('postal_detalleenvios')

        # Deleting model 'TipoDocumento'
        db.delete_table('postal_tipodocumento')

        # Deleting model 'ControlPeso'
        db.delete_table('postal_controlpeso')

        # Deleting model 'DetalleDocControlPeso'
        db.delete_table('postal_detalledoccontrolpeso')

        # Deleting model 'DetalleGuiaControlPeso'
        db.delete_table('postal_detalleguiacontrolpeso')

    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'caja.servicio': {
            'Meta': {'object_name': 'Servicio'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'descripcion'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'nombre'"})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'crm.cliente': {
            'Meta': {'object_name': 'Cliente', '_ormbases': ['crm.Contacto']},
            'activo': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'activo'"}),
            'bloqueado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'clasificacion': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'clasificacion'"}),
            'codigo': ('django.db.models.fields.IntegerField', [], {}),
            'contacto_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['crm.Contacto']", 'unique': 'True', 'primary_key': 'True'}),
            'exonerado': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'exonerado'"}),
            'organization': ('django.db.models.fields.CharField', [], {'default': "'N/A'", 'max_length': '35', 'null': 'True', 'db_column': "'organization'", 'blank': 'True'}),
            'ruc': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'db_column': "'ruc'", 'blank': 'True'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.TipoCliente']", 'null': 'True', 'blank': 'True'}),
            'tipocobro': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.TipoCobro']", 'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '100', 'null': 'True', 'db_column': "'url'", 'blank': 'True'}),
            'zona': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.Ruta']", 'null': 'True', 'blank': 'True'})
        },
        'crm.contacto': {
            'Meta': {'object_name': 'Contacto'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'apellido'"}),
            'categoria': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'categoria'", 'blank': 'True'}),
            'cedula': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'db_column': "'cedula'", 'blank': 'True'}),
            'estadocivil': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'estadocivil'", 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'db_column': "'foto'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'licencia': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'licencia'", 'blank': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'db_column': "'logo'", 'blank': 'True'}),
            'nacimiento': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'nacimiento'", 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'nombre'"}),
            'nombrecorto': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'nombrecorto'", 'blank': 'True'}),
            'notas': ('django.db.models.fields.TextField', [], {'null': 'True', 'db_column': "'notas'", 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'title'", 'blank': 'True'})
        },
        'crm.ruta': {
            'Meta': {'object_name': 'Ruta'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ruta': ('django.db.models.fields.TextField', [], {'max_length': '300', 'db_column': "'ruta'"}),
            'zona': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'zona'"})
        },
        'crm.tipocliente': {
            'Meta': {'object_name': 'TipoCliente'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'nombre'"})
        },
        'crm.tipocobro': {
            'Meta': {'object_name': 'TipoCobro'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meses': ('django.db.models.fields.IntegerField', [], {}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'facturacion.documento': {
            'Meta': {'object_name': 'Documento'},
            'anulado': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'anulado'"}),
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.Cliente']"}),
            'fecha': ('django.db.models.fields.DateField', [], {'db_column': "'fecha'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'monto': ('django.db.models.fields.DecimalField', [], {'db_column': "'monto'", 'decimal_places': '2', 'max_digits': '12'}),
            'tipo_documento': ('django.db.models.fields.IntegerField', [], {'db_column': "'tipo_documento'"}),
            'vencimiento': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'vencimiento'", 'blank': 'True'})
        },
        'facturacion.factura': {
            'Meta': {'object_name': 'Factura', '_ormbases': ['facturacion.Documento']},
            'credito': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'credito'"}),
            'documento_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['facturacion.Documento']", 'unique': 'True'}),
            'numero': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'numero'"}),
            'origen': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'origen'"}),
            'sucursal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['facturacion.Sucursal']", 'null': 'True', 'blank': 'True'})
        },
        'facturacion.requerimiento': {
            'Meta': {'object_name': 'Requerimiento', '_ormbases': ['facturacion.Documento']},
            'casillero': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.Casillero']", 'null': 'True', 'blank': 'True'}),
            'documento_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['facturacion.Documento']", 'unique': 'True'}),
            'factura': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['facturacion.Factura']", 'null': 'True', 'blank': 'True'}),
            'generado': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'generado'"}),
            'numero': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'numero'"}),
            'servicio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['caja.Servicio']"}),
            'tarjeta': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'null': 'True', 'db_column': "'tarjeta'", 'decimal_places': '2', 'max_digits': '12'})
        },
        'facturacion.sucursal': {
            'Meta': {'object_name': 'Sucursal'},
            'codigo': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'codigo'"}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'nombre'"})
        },
        'nomina.cargo': {
            'Meta': {'object_name': 'Cargo'},
            'cargo': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'cargo'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'nomina.empleado': {
            'Meta': {'object_name': 'Empleado', '_ormbases': ['crm.Contacto']},
            'activo': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'activo'"}),
            'cargo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomina.Cargo']"}),
            'codigoempleado': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'codigo'"}),
            'comisionista': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'comisionista'"}),
            'contacto_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['crm.Contacto']", 'unique': 'True', 'primary_key': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'fecha'", 'blank': 'True'}),
            'idseguro': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'seguro'"}),
            'madre': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'db_column': "'madre'", 'blank': 'True'}),
            'numerohijos': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'hijos'", 'blank': 'True'}),
            'padre': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'db_column': "'padre'", 'blank': 'True'}),
            'sueldo': ('django.db.models.fields.DecimalField', [], {'db_column': "'sueldo'", 'decimal_places': '2', 'max_digits': '10'})
        },
        'nomina.vendedor': {
            'Meta': {'object_name': 'Vendedor', '_ormbases': ['nomina.Empleado']},
            'codvendedor': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'codvendedor'"}),
            'empleado_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['nomina.Empleado']", 'unique': 'True', 'primary_key': 'True'})
        },
        'postal.abono': {
            'Meta': {'object_name': 'Abono'},
            'abono': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'exoneracion': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.ExoneracionVariosAbonos']"}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'postal.carta': {
            'Meta': {'object_name': 'Carta'},
            'casillero': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.Casillero']", 'null': 'True', 'blank': 'True'}),
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.Cliente']", 'null': 'True', 'blank': 'True'}),
            'contsinval': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'destinatario': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'direcciond': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'direccionr': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'formaenvio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.TipoEnvioPaquete']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numreg': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'peso': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '9', 'decimal_places': '2'}),
            'piezas': ('django.db.models.fields.IntegerField', [], {}),
            'referenciapeso': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'default': '2', 'to': "orm['postal.Region']"}),
            'remitente': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'requerimiento': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['facturacion.Requerimiento']", 'null': 'True', 'blank': 'True'}),
            'total': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'typedoc': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'valor': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'})
        },
        'postal.casillero': {
            'Meta': {'object_name': 'Casillero'},
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.Cliente']"}),
            'codigonicabox': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'nicabox'"}),
            'dirdomicilio': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'domicilio': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'fechafin': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'fin'", 'blank': 'True'}),
            'fincontrato': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'fincontrato'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iniciocontrato': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'inicio'", 'blank': 'True'}),
            'justificacion': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'default': "'52SPYD'", 'max_length': '100', 'null': 'True', 'db_column': "'password'", 'blank': 'True'}),
            'pesomax': ('django.db.models.fields.DecimalField', [], {'db_column': "'pesomax'", 'decimal_places': '2', 'max_digits': '8'}),
            'proximocobro': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'proximocobro'", 'blank': 'True'}),
            'vendedor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomina.Vendedor']", 'null': 'True', 'blank': 'True'})
        },
        'postal.controlenvio': {
            'Meta': {'object_name': 'ControlEnvio'},
            'casillero': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.Casillero']", 'null': 'True', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'responsable': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'db_column': "'responsable'"})
        },
        'postal.controlpeso': {
            'Meta': {'object_name': 'ControlPeso'},
            'casillero': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.Casillero']", 'null': 'True', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'peso': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'piezas': ('django.db.models.fields.IntegerField', [], {}),
            'requerimiento': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['facturacion.Requerimiento']", 'null': 'True', 'blank': 'True'}),
            'responsable': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'db_column': "'responsable'"}),
            'total': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'})
        },
        'postal.courier': {
            'Meta': {'object_name': 'Courier', '_ormbases': ['postal.GC']},
            'ciad': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'ciar': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.Cliente']", 'null': 'True', 'blank': 'True'}),
            'codr': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'direcciond': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'direccionr': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'gc_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['postal.GC']", 'unique': 'True', 'primary_key': 'True'}),
            'guiaaerea': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'info_ciudadd': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'info_ciudadr': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'nombred': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'nombrer': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'peso': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '9', 'decimal_places': '2'}),
            'refr': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.Region']", 'null': 'True', 'blank': 'True'}),
            'requerimiento': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['facturacion.Requerimiento']", 'null': 'True', 'blank': 'True'}),
            'telefonod': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'telefonor': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'total': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'typedoc': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'valor': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'})
        },
        'postal.detalledoccontrolpeso': {
            'Meta': {'object_name': 'DetalleDocControlPeso'},
            'control': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.ControlPeso']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'piezas': ('django.db.models.fields.IntegerField', [], {}),
            'tipodoc': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.TipoDocumento']", 'null': 'True', 'blank': 'True'})
        },
        'postal.detalleenvios': {
            'Meta': {'object_name': 'DetalleEnvios'},
            'cantidadcartas': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'envio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.ControlEnvio']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lugar': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.Lugar']", 'null': 'True', 'blank': 'True'}),
            'tipenvio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.TipoEnvioPaquete']", 'null': 'True', 'blank': 'True'})
        },
        'postal.detalleguiacontrolpeso': {
            'Meta': {'object_name': 'DetalleGuiaControlPeso'},
            'control': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.ControlPeso']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'paquete': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.Paquete']"})
        },
        'postal.estadopaquete': {
            'Meta': {'object_name': 'EstadoPaquete'},
            'comentarios': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'estado': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'estado'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'statusid': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'postal.exoneracionvariosabonos': {
            'Meta': {'object_name': 'ExoneracionVariosAbonos'},
            'cargo': ('django.db.models.fields.DecimalField', [], {'default': '500', 'max_digits': '9', 'decimal_places': '2'}),
            'casillero': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.Casillero']"}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'fechafin': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'postal.gc': {
            'Meta': {'object_name': 'GC'},
            'alto': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'ancho': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'declarado': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'fechaingreso': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'horaingreso': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'largo': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'numref': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'observaciones': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'piezas': ('django.db.models.fields.IntegerField', [], {'default': '1', 'null': 'True', 'blank': 'True'}),
            'volumetrico': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '12', 'decimal_places': '2', 'blank': 'True'})
        },
        'postal.lineaaerea': {
            'Meta': {'object_name': 'LineaAerea'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'linea': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'linea'"})
        },
        'postal.lugar': {
            'Meta': {'object_name': 'Lugar'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lugar': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'lugar'"}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.Region']", 'null': 'True', 'blank': 'True'})
        },
        'postal.movimiento': {
            'Meta': {'object_name': 'Movimiento'},
            'comentarios': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'estado': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.EstadoPaquete']"}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'fechareal': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'paquete': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.Paquete']"})
        },
        'postal.paquete': {
            'Meta': {'object_name': 'Paquete', '_ormbases': ['postal.GC']},
            'bolsa': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'casillero': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.Casillero']", 'null': 'True', 'blank': 'True'}),
            'ciudad': ('django.db.models.fields.CharField', [], {'default': "'Managua'", 'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'cliente': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'consolidado': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'contenido': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'estado': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.EstadoPaquete']", 'null': 'True', 'blank': 'True'}),
            'fechadespacho': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'flete': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'gc_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['postal.GC']", 'unique': 'True', 'primary_key': 'True'}),
            'guiaaerea': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'guiaingreso': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'otros': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'pesokg': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'pesolb': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'remitente': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'servicio': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        'postal.personaautorizada': {
            'Meta': {'object_name': 'PersonaAutorizada'},
            'casillero': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.Casillero']", 'null': 'True', 'blank': 'True'}),
            'cedula': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'cedula'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'nombre'"})
        },
        'postal.region': {
            'Meta': {'object_name': 'Region'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'region': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'region'"})
        },
        'postal.tipodocumento': {
            'Meta': {'object_name': 'TipoDocumento'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'postal.tipoenviopaquete': {
            'Meta': {'object_name': 'TipoEnvioPaquete'},
            'codigo': ('django.db.models.fields.IntegerField', [], {}),
            'envio': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'envio'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'postal.tipotransaccion': {
            'Meta': {'object_name': 'TipoTransaccion'},
            'casillero': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['postal.Casillero']", 'null': 'True', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'fecha'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'transaccion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'transaccion'"})
        }
    }

    complete_apps = ['postal']