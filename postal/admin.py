# -*- coding: UTF-8 -*-
from django.contrib import admin
from django.db import models
from django.conf import settings
from postal.models import *
from facturacion.models import CobroAdicional
from read_only import ReadOnlyAdmin
from postal.widgets import RawIdWidget
#================================================
# Inlines
#================================================
class RawIdCustomAdminWithFilter ( ):
    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        """
        Get a form Field for a ForeignKey.
        """
        if db_field.name in self.raw_id_fields:
            if str(db_field.name) == 'casillero':
                kwargs['widget'] = RawIdWidget(self.model, 'casillero',"/api/casillero/", 'filtrar()')
            elif str(db_field.name) == 'cliente':
                kwargs['widget'] = RawIdWidget(self.model, 'cliente',"/api/cliente/", 'filtrar()')
        return db_field.formfield(**kwargs)


class RawIdCustomAdmin ( ):
    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        """
        Get a form Field for a ForeignKey.
        """
        if db_field.name in self.raw_id_fields:
            if str(db_field.name) == 'casillero':
                kwargs['widget'] = RawIdWidget(self.model, 'casillero',"/api/casillero/")
            elif str(db_field.name) == 'cliente':
                kwargs['widget'] = RawIdWidget(self.model, 'cliente',"/api/cliente/")
            elif str(db_field.name) == 'parent':
                kwargs['widget'] = RawIdWidget(self.model, 'parent',"/api/cuenta/")
            elif str(db_field.name) == 'cuenta':
                kwargs['widget'] = RawIdWidget(self.model, 'cuenta',"/api/cuenta/")
        return db_field.formfield(**kwargs)

class MovimientoInline( admin.TabularInline):
    model = Movimiento
    extra = 0

class CobroAdicionalInline( admin.TabularInline ):
    model = CobroAdicional
    raw_id_fields = ('tipocobro',)
    extra = 0

class LugarInline( admin.TabularInline ):
    model = Lugar
    extra = 2

class PersonaInline( admin.TabularInline ):
    model = PersonaAutorizada
    extra = 1
#================================================
# Admin
#===============================================
class EstadoPaqueteAdmin( admin.ModelAdmin ):
    search_fields = ['estado', 'statusid']
    list_display = ['statusid', 'estado']
    list_per_page = 500

class RegionAdmin( admin.ModelAdmin ):
    search_fields = ['region']
    inlines = [LugarInline]

class LugarAdmin( admin.ModelAdmin ):
    list_display = ['lugar', 'region']
    list_per_page = 100
    list_filter = ['region']
    ordering = ['region']
    search_fields = ['lugar']

class LineaAereaAdmin( admin.ModelAdmin ):
    search_fields = ['linea']

class PersonaAdmin( RawIdCustomAdmin, admin.ModelAdmin ):
    list_display = ['nombre', 'casillero']
    search_fields = ['casillero__codigonicabox']
    raw_id_fields = ('casillero',)

class CasilleroAdmin( RawIdCustomAdmin, admin.ModelAdmin ):
    list_per_page = 100
    list_display = ['codigonicabox', 'cliente']
    list_display_links = ('codigonicabox', 'cliente')
    search_fields = ['codigonicabox', 'cliente__nombre', 'cliente__organization']
    list_filter = ['domicilio', 'fincontrato']
    inlines = [PersonaInline]
    raw_id_fields = ('cliente',)

class PaqueteAdmin( RawIdCustomAdmin, admin.ModelAdmin ):
    list_display = ['numref', 'consolidado','casillero', 'remitente', 'cliente']
    search_fields = ['numref','contenido', 'observaciones', 'remitente', 'cliente']
    inlines = [CobroAdicionalInline, MovimientoInline]
    readonly_fields = ('volumetrico',)
    raw_id_fields = ('casillero',)
    fieldsets = (
        ( None, {
            'fields':('numref', 'fechaingreso', 'horaingreso', 'servicio', 'piezas', 'observaciones', 'declarado', 'cliente', 'direccion', 'telefono', 'ciudad', 'casillero', 'remitente','pesolb','pesokg', 'contenido')
        }),
        ('Datos del Peso',{
            'classes': ('collapse',),
            'fields': ('largo','alto','ancho','volumetrico')
        }),
        ('Datos Adicionales',{
            'classes': ('collapse',),
            'fields': ('guiaingreso','otros','fechadespacho','guiaaerea','bolsa','consolidado','flete', 'estado')
        }),
    )

class ControlAirbillAdmin( admin.ModelAdmin ):
    list_display = ['guia', 'fecha_y_hora', 'origen','destino', 'linea_aerea_fk', 'recibido_por', 'peso', 'piezas']
    search_fields = ('guia',)

class CourierAdmin( RawIdCustomAdmin, admin.ModelAdmin ):
    exclude = ('requerimiento',)
    list_display = ['numref', 'nombrer', 'nombred','total']
    search_fields = ('numref',)
    readonly_fields = ('volumetrico',)
    raw_id_fields = ('cliente',)
    fieldsets = (
        ( None, {
            'fields':('numref', 'fechaingreso', 'horaingreso', 'piezas', 'observaciones', 'declarado', 'typedoc', 'mensajero', 'comentarios')
        }),
        ('Datos del Peso',{
            'classes': ('collapse',),
            'fields': ('peso', 'largo','alto','ancho','volumetrico')
        }),
        ('Informacion del Remitente',{
            'classes': ('collapse',),
            'fields': ('cliente', 'codr', 'ciar', 'nombrer', 'telefonor', 'direccionr', 'info_ciudadr', 'refr')
        }),
        ('Informacion del Destinatario',{
            'classes': ('collapse',),
            'fields': ('region', 'ciad', 'nombred', 'telefonod', 'direcciond', 'info_ciudadd')
        }),
    )
class GuiaNoImportadaAdmin( CourierAdmin ):
    exclude = None
    list_display = ['numref', 'nombrer', 'nombred']
    fieldsets = (
        ( None, {
            'fields':('numref', 'fechaingreso', 'horaingreso', 'piezas', 'observaciones', 'declarado', 'guiaaerea', 'typedoc', 'mensajero', 'comentarios', 'collect', 'otro_cobro')
        }),
        ('Datos del Peso',{
            'classes': ('collapse',),
            'fields': ('peso', 'largo','alto','ancho','volumetrico')
        }),
        ('Informacion del Remitente',{
            'classes': ('collapse',),
            'fields': ('cliente', 'codr', 'ciar', 'nombrer', 'telefonor', 'direccionr', 'info_ciudadr', 'refr')
        }),
        ('Informacion del Destinatario',{
            'classes': ('collapse',),
            'fields': ('region', 'ciad', 'nombred', 'telefonod', 'direcciond', 'info_ciudadd')
        }),
    )

class CartaAdmin( RawIdCustomAdmin, admin.ModelAdmin ):
    exclude = ('requerimiento','total')
    list_display = ['id','remitente', 'destinatario', 'fecha','facturado', 'total']
    list_filter = ('typedoc', 'formaenvio' )
    raw_id_fields = ('cliente', 'casillero')
    search_fields = ['remitente', 'destinatario', 'destino']

class AbonoInline( admin.TabularInline ):
    model = Abono
    extra = 0

class ExoneracionAdmin( RawIdCustomAdmin, admin.ModelAdmin ):
    raw_id_fields = ('casillero',)
    list_display = ['casillero', 'cargo', 'fecha']
    inlines = [AbonoInline]

class MovimientoAdmin( admin.ModelAdmin ):
    list_display = ['paquete', 'estado', 'fecha', 'hora', 'comentarios']
    raw_id_fields = ('paquete',)
    search_fields = ('paquete__numref',)
    list_filter = ('estado',)
#============================================================================================================
# Read Only Admin
#============================================================================================================
class CobroAdicionalInlineRO( ReadOnlyAdmin, admin.TabularInline):
    model = CobroAdicional
    extra = 0

class MovimientoInlineRO( ReadOnlyAdmin, admin.TabularInline):
    model = Movimiento

    extra = 0

class PersonaInlineRO( ReadOnlyAdmin, admin.TabularInline ):
    model = PersonaAutorizada
    extra = 0

class PaqueteRO( ReadOnlyAdmin, admin.ModelAdmin ):
    list_display = ['numref', 'consolidado','casillero', 'remitente', 'cliente']
    search_fields = ['numref','contenido', 'observaciones', 'remitente', 'cliente']
    list_per_page = 100
    model = PaqueteRead
    inlines = [CobroAdicionalInlineRO, MovimientoInlineRO]
    fieldsets = (
        ( None, {
            'fields':('numref', 'fechaingreso', 'horaingreso', 'servicio', 'piezas', 'observaciones', 'declarado', 'cliente', 'casillero', 'remitente', 'pesolb','contenido')
        }),
        ('Datos del Peso',{
            'classes': ('collapse',),
            'fields': ('largo','alto','ancho','volumetrico')
        }),
        ('Datos Adicionales',{
            'classes': ('collapse',),
            'fields': ('guiaingreso','otros','fechadespacho','guiaaerea','bolsa','consolidado','flete', 'estado')
        }),
    )

class CourierRO( ReadOnlyAdmin, admin.ModelAdmin ):
    list_display = ['numref', 'nombrer', 'nombred']
    fieldsets = (
        ( None, {
            'fields':('numref', 'fechaingreso', 'horaingreso', 'piezas', 'observaciones', 'declarado', 'typedoc', 'guiaaerea')
        }),
        ('Datos del Peso',{
            'classes': ('collapse',),
            'fields': ('peso', 'largo','alto','ancho','volumetrico')
        }),
        ('Informacion del Remitente',{
            'classes': ('collapse',),
            'fields': ('codr', 'ciar', 'nombrer', 'telefonor', 'direccionr', 'refr')
        }),
        ('Informacion del Destinatario',{
            'classes': ('collapse',),
            'fields': ('region', 'ciad', 'nombred', 'telefonod', 'direcciond')
        }),
    )

class CartaRO( ReadOnlyAdmin, admin.ModelAdmin ):
    list_display = ['id','remitente', 'destinatario', 'fecha', 'total']

class CasilleroRO( ReadOnlyAdmin, admin.ModelAdmin ):
    list_display = ['codigonicabox', 'cliente']
    inlines = [PersonaInlineRO]

class DetalleEnvioInline( admin.TabularInline ):
    extra = 0
    model = DetalleEnvios

class DetalleCPDInline( admin.TabularInline ):
    extra = 0
    model = DetalleDocControlPeso

class DetalleCPPInline( admin.TabularInline):
    extra = 0
    model = DetalleGuiaControlPeso
    raw_id_fields = ('paquete',)

class ControlEnvioAdmin( RawIdCustomAdminWithFilter, admin.ModelAdmin ):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'responsable':
            kwargs['initial'] = request.user.id
            return db_field.formfield(**kwargs)
        return super(ControlEnvioAdmin, self).formfield_for_foreignkey( db_field, request, **kwargs )
    raw_id_fields = ('casillero',)
    list_display = ['id', 'responsable', 'fecha', 'casillero']
    inlines = [DetalleEnvioInline]
    list_filter = ['credito']

    def get_form(self, request, obj=None, **kwargs):
        form = super(ControlEnvioAdmin, self).get_form(request, obj, **kwargs)
        if obj:
            form.base_fields['autorizados'].choices =(
            (c.pk,c.descripcion) for c in PersonaAutorizada.objects.filter(casillero=obj.casillero) )
        else:
            form.base_fields['autorizados'].choices = []
        return form

class ControlPesoAdmin( RawIdCustomAdminWithFilter, admin.ModelAdmin ):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'responsable':
            kwargs['initial'] = request.user.id
            return db_field.formfield(**kwargs)

        return super(ControlPesoAdmin, self).formfield_for_foreignkey( db_field, request, **kwargs )
    raw_id_fields = ('casillero',)
    list_display = ['id', 'responsable', 'casillero', 'fecha','facturado']
    readonly_fields = ('total',)
    fields = ['fecha' , 'casillero' , 'peso' ,'piezas', 'responsable', 'paquetes']
    inlines = [DetalleCPDInline]

    def get_form(self, request, obj=None, **kwargs):
        form = super(ControlPesoAdmin, self).get_form(request, obj, **kwargs)
        if obj:
            paq = obj.paquetes.values_list('numref')
            form.base_fields['paquetes'].choices =(
            (c.pk,c.descripcion) for c in Paquete.objects.filter(casillero=obj.casillero, numref__in=paq) )
        else:
            form.base_fields['paquetes'].choices =(
            (c.pk,c.descripcion) for c in Paquete.objects.none())

        return form

class GCAdmin( admin.ModelAdmin ):
    search_fields = ['numref',]

admin.site.register(PersonaAutorizada, PersonaAdmin)
admin.site.register(Casillero, CasilleroAdmin)
admin.site.register(Paquete, PaqueteAdmin)
admin.site.register(Courier, CourierAdmin)
admin.site.register(Region, RegionAdmin)
admin.site.register(Lugar, LugarAdmin)
admin.site.register(LineaAerea, LineaAereaAdmin)
admin.site.register(EstadoPaquete, EstadoPaqueteAdmin)
admin.site.register(TipoEnvioPaquete)
admin.site.register(Carta, CartaAdmin)
admin.site.register(ExoneracionVariosAbonos, ExoneracionAdmin)
admin.site.register(Movimiento, MovimientoAdmin)
admin.site.register(PaqueteRead, PaqueteRO)
admin.site.register(CourierRead, CourierRO)
admin.site.register(CartaRead, CartaRO)
admin.site.register(CasilleroRead, CasilleroRO)
admin.site.register(ControlEnvio, ControlEnvioAdmin)
admin.site.register(ControlPeso, ControlPesoAdmin)
admin.site.register(TipoDocumento)
admin.site.register(GC, GCAdmin)
admin.site.register(ControlAirbill, ControlAirbillAdmin)
admin.site.register(GuiaNoImportada, GuiaNoImportadaAdmin)
