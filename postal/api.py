from tastypie.resources import ModelResource
from tastypie import fields, utils
from tastypie.constants import *
from postal.models import Carta, Courier, Casillero
from facturacion.models import RequerimientoNicabox

class RequerimientoResource(ModelResource):
    class Meta:
        queryset = RequerimientoNicabox.objects.all()
        resource_name = 'requerimiento'
        allowed_methods = ['get']

class CartaResource(ModelResource):
    descripcion = fields.CharField(attribute='descripcion')
    class Meta:
        queryset = Carta.objects.all()
        filtering = {
            "id": ('exact',),
        }

class CourierResource(ModelResource):
    descripcion = fields.CharField(attribute='descripcion')
    class Meta:
        queryset = Courier.objects.all()
        filtering = {
            "id": ('exact',),
        }
class CasilleroResource(ModelResource):
    code = fields.CharField(attribute='codigonicabox')
    name = fields.CharField(attribute='clientecompleto')
    paquetes = fields.ListField(attribute='controlados')
    cartas = fields.ToManyField(CartaResource, attribute=lambda box: Carta.objects.filter(casillero=box.obj, requerimiento=None), full = True, null = True, blank = True)
    autorizados = fields.ListField(attribute='autorizados')
    controles = fields.ListField(attribute='pesos')
    descripcion = fields.CharField(attribute='descripcion')
    class Meta:
        queryset = Casillero.objects.all()
        filtering = {
            "id": ('exact',),
            "code": ('exact',),
            "name": ('startswith',),
        }
