# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.views.generic import FormView
from django.core.exceptions import *
from django.template import RequestContext
from crm.models import Cliente, Parametro, DireccionContacto
from caja.models import TarifaServicio
from facturacion.models import CobroAdicional
from nomina.models import Cobrador
from postal.models import *
from postal.forms import *
from postal.reports import *
from reportes.views import ExportedReport
from datetime import date, datetime
from decimal import Decimal
import time
from django.template import Context, loader
from django.http import HttpResponse, HttpResponseRedirect
from string import atoi
import xml.etree.ElementTree as ET
from django.contrib.auth.models import User
from geraldo.generators import PDFGenerator
import simplejson
from django.db.models import Q

def obtener_cliente(request):
    try:
        if 'id' in request.GET:
            id = int(request.GET['id'])
            cliente = Cliente.objects.get(pk = id)
            json = simplejson.dumps([ (cliente.codigo,cliente.clientecompleto) ])
        else:
            codigo = request.GET['codigo']
            cliente = Cliente.objects.get(codigo = codigo)
            json = simplejson.dumps([ (cliente.id,cliente.clientecompleto) ])
    except:
        json = simplejson.dumps([ ('','No definido') ])

    return HttpResponse(json)

def obtener_casillero(request):
    try:
        if 'id' in request.GET:
            id = int(request.GET['id'])
            casillero = Casillero.objects.get(pk = id)
            json = simplejson.dumps([ (casillero.codigonicabox, casillero.clientecompleto) ])
        else:
            codigo = request.GET['codigo']
            casillero = Casillero.objects.get(codigonicabox = codigo)
            json = simplejson.dumps([ (casillero.id,casillero.clientecompleto) ])
    except:
        json = simplejson.dumps([ ('','No definido') ])

    return HttpResponse(json)

#######################
# Reportes
#######################
class CajasView( ExportedReport, FormView ):
    form_class = CajasForm
    title = 'Reporte de Cajas Postales'
    template_name = 'postal/cajas.html'
    report_class = CajasReport
    required_permissions = ('reportes.cajapostal',)

    def form_valid( self, form ):
        codigo = form.cleaned_data['codigo']
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']
        anulada = form.cleaned_data['anulada']
        domicilio = form.cleaned_data['domicilio']
        self.title = 'Cajas Postales Encontradas'

        data = Casillero.objects.all()

        if codigo <> '':
            data = data.filter(codigonicabox=codigo)

        if anulada == True:
            data = data.filter(fincontrato=True)

        if domicilio == True:
            data = data.filter(domicilio = True)

        if start_date <> None and end_date <> None:
            data = data.filter(iniciocontrato__range=[start_date, end_date])

        if start_date <> None and end_date == None:
            data = data.filter(iniciocontrato__range=[start_date, date.today()])

        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
            'caja':codigo,
        })
        return self.render_to_response(context, data)

class CobrosView( ExportedReport, FormView ):
    form_class = CodClienteForm
    template_name = 'postal/cobros.html'
    required_permissions = ('reportes.cobros',)

    def form_valid( self, form ):
        codigo = form.cleaned_data['codigo']
        casillero = None
        cliente = None
        pd = Parametro.objects.get(parametro__icontains='domicilio')
        domicilio = Decimal(0.00)
        cuota = Decimal(0.00)
        tarifa = []
        i = []
        MESES = []

        if codigo <> '':
            MESES = [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                  'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ]
            try:
                casillero = Casillero.objects.get(cliente__codigo=codigo)
                peso = casillero.pesomax
                cliente = Cliente.objects.get(codigo=codigo)
                try:
                    tarifa = TarifaServicio.objects.filter(tipo_cliente=cliente.tipo, limite_superior = peso)[:1]
                    cuota = tarifa[0].monto * cliente.tipocobro.meses

                    if casillero.domicilio:
                            domicilio = Decimal(pd.valor) * cliente.tipocobro.meses
                    if cliente.tipocobro.meses == 1:
                        i = range(1,13,cliente.tipocobro.meses)
                    else:
                        i = range(1,12,cliente.tipocobro.meses)

                except IndexError:
                    tarifa = None
            except ObjectDoesNotExist:
                casillero = None
        total = str(cuota + domicilio)

        context = self.get_context_data()
        context.update({
            'form':form,
            'casillero':casillero,
            'cliente':cliente,
            'tarifa':str(tarifa),
            'cuota':str(cuota),
            'domicilio':str(domicilio),
            'total':str(total),
            'range':i,
            'meses':MESES,
        })

        return self.render_to_response(context)

class AnuladasView( ExportedReport, FormView ):
    form_class = AnuladasForm
    title = 'Cajas Anuladas por periodo'
    template_name = 'postal/cajas.html'
    report_class = CajasReport
    required_permissions = ('reportes.cajaanulada',)

    def form_valid( self, form ):
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']

        data = Casillero.objects.filter(fincontrato=True)

        if start_date <> None and end_date <> None:
            data = data.filter(fechafin__range=[start_date, end_date])

        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context, data)

class PaquetesView( ExportedReport, FormView):
    form_class = PaquetesForm
    title = 'Reporte de Paquetes'
    template_name = 'postal/paquetes.html'
    report_class = PaquetesReport
    required_permissions = ('reportes.paquete',)

    def form_valid( self, form ):
        referencia = form.cleaned_data['referencia']
        nicabox = form.cleaned_data['codigo']
        start_date= form.cleaned_data['start_date']
        end_date= form.cleaned_data['end_date']
        peso_1= form.cleaned_data['peso_1']
        peso_2= form.cleaned_data['peso_2']

        data = Paquete.objects.all()

        if start_date <> None and end_date <> None:
            data = data.filter(fechaingreso__range=[start_date, end_date])

        if start_date <> None and end_date == None:
            data = data.filter(fechaingreso__range=[start_date, date.today()])

        if peso_1 <> None and peso_2 <> None:
            data = data.filter(pesolb__gte=peso_1, pesolb__lte=peso_2)

        if referencia <> '':
            data = data.filter(numref__icontains=referencia)

        if nicabox <> '':
            data = data.filter(casillero__codigonicabox__contains=nicabox)

        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context, data)


class CourierListView( ExportedReport, FormView):
    form_class = CourierForm
    report_class = CourierReport
    title = 'Listado Courier'
    template_name = 'postal/courier.html'
    required_permissions = ('reportes.courier',)

    def form_valid( self, form ):
        tipo = form.cleaned_data['tipo']
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']
        nicabox = form.cleaned_data['codigo']
        cliente = form.cleaned_data['codigocliente']
        destino = form.cleaned_data['destino']

        data = Courier.objects.all()

        if tipo <> None:
            data = data.filter (typedoc=tipo)

        if start_date <> None and end_date <> None:
            data = data.filter(fechaingreso__range=[start_date, end_date])

        if start_date <> None and end_date == None:
            data = data.filter(fechaingreso__range=[start_date, date.today()])

        if nicabox <> '':
            try:
                data = data.filter (cliente__casillero__codigonicabox__contains=nicabox)
            except:
                data = []

        if cliente <> '':
            try:
                data = data.filter (cliente__codigo__contains=cliente)
            except:
                data = []

        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context, data)

class CartasView( ExportedReport, FormView ):
    form_class = CartaForm
    report_class = CartasReport
    title = 'Cartas por Cliente'
    template_name = 'postal/cartas.html'
    required_permissions = ('reportes.cartaxcliente',)

    def form_valid( self, form ):
        tipo = form.cleaned_data['tipo']
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']
        nicabox = form.cleaned_data['nicabox']
        destino = form.cleaned_data['destino']
        numero = form.cleaned_data['no']

        data = Carta.objects.all()
        if nicabox <> '':
            try:
                data = data.filter (codigonicabox__contains=nicabox)
            except:
                data = []

        if numero <> '':
            try:
                data = data.filter (cliente__codigo__contains=numero)
            except:
                data = []

        if start_date <> None and end_date <> None:
            data = data.filter(fecha__range=[start_date, end_date])

        if start_date <> None and end_date == None:
            data = data.filter(fecha__range=[start_date, date.today()])

        if destino <> None:
            l = Region.objects.get(region=destino)
            data = data.filter(region=l.id)
        total = data.aggregate(Sum('valor'))
        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
            'total':total,
        })
        return self.render_to_response(context, data)

class CourierView( ExportedReport, FormView ):
    form_class = CourierForm
    report_class = CourierReport
    title = 'Manifiesto Courier'
    template_name = 'postal/courier.html'
    required_permissions = ('reportes.courier',)

    def form_valid( self, form ):
        tipo = form.cleaned_data['tipo']
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']
        destino = form.cleaned_data['destino']
        data = Courier.objects.all()
        self.fecha = str(start_date)+'...'+str(end_date)
        self.nombre = form.cleaned_data['no']
        self.airbill = form.cleaned_data['airbill']
        self.carrier = form.cleaned_data['carrier']
        if tipo <> None:
            data = data.filter (typedoc=tipo)
            if tipo =='1':
                self.description = 'ADVANCED INTANGIBLES MANIFEST'
                self.report_class = DOCReport
            if tipo=='2':
                self.description = 'ADVANCED TANGIBLES MANIFEST'
                self.report_class = SPXReport

        if start_date <> None and end_date <> None:
            data = data.filter(fechaingreso__range=[start_date, end_date])

        if start_date <> None and end_date == None:
            data = data.filter(fechaingreso__range=[start_date, date.today()])

        if destino <> None:
            l = Region.objects.get(region=destino)
            data = data.filter(region=l.id)

        total = data.aggregate(Sum('valor'))
        piezas = data.aggregate(Sum('piezas'))
        pesos = data.aggregate(Sum('peso'))
        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
            'piezas':piezas,
            'pesos':pesos,
        })
        return self.render_to_response(context, data)

class StatusView( ExportedReport, FormView ):
    form_class = StatusForm
    report_class = StatusReport
    title = "Reporte de Status"
    template_name = 'postal/status.html'
    required_permissions = ('reportes.status',)

    def form_valid( self, form ):
        referencia = form.cleaned_data['referencia']
        self.airbill = manifiesto = form.cleaned_data['manifiesto']
        destino = form.cleaned_data['destino']
        cliente = form.cleaned_data['cliente']
        self.description = estado = form.cleaned_data['estado']
        sin_estado = form.cleaned_data['sin_estado']
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']
        self.fecha = fecha_status = form.cleaned_data['fecha']
        self.nombre = form.cleaned_data['recibido_por']
        data = Movimiento.objects.all()

        if referencia <> '':
            data = data.filter(paquete__numref__contains=referencia)

        if manifiesto <> '':
            data = data.filter(paquete__consolidado=manifiesto)

        if destino <> '':
            data = data.filter(paquete__ciudad__contains=destino)

        if cliente <> '':
            data = data.filter(paquete__cliente__nombre__icontains = cliente)

        if start_date <> None and end_date <> None:
            data = data.filter(paquete__fechaingreso__range=[start_date, end_date])

        if estado <> None:
            data = data.filter(estado=estado)
            if fecha_status <> None:
                data = data.filter(fecha=fecha_status)

        if sin_estado <> None:
            data = data.exclude(estado=sin_estado)

        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context, data)

class InconsistenciaView( ExportedReport, FormView ):
    form_class = InconsistenciaForm
    report_class = InconsistenciaReport
    title = "Reporte de Inconsistencias"
    template_name = 'postal/inconsistencia.html'
    required_permissions = ('reportes.status',)

    def form_valid( self, form ):
        self.airbill = manifiesto = form.cleaned_data['manifiesto']
        data = Paquete.objects.filter(consolidado=manifiesto)
        data = data.filter(~Q(movimiento__estado__codigo='RET') , ~Q(movimiento__estado__codigo='RBT'))
        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context, data)

class ExoneracionView( ExportedReport, FormView ):
    form_class = ExoneracionForm
    title = "Exoneraciones"
    template_name = 'postal/exoneraciones.html'
    required_permissions = ('reportes.exoneraciones',)

    def form_valid( self, form ):
        nicabox = form.cleaned_data['nicabox']
        data = ExoneracionVariosAbonos.objects.all()
        p = []
        if nicabox <> '':
            try:
                data = data.filter(casillero__codigonicabox=nicabox)
                for d in data:
                    p += Abono.objects.filter(exoneracion=d.id)
            except:
                data = []

        context = self.get_context_data()
        context.update({
            'form':form,
            'data2':p,
            'data':data,
        })
        return self.render_to_response(context, data)

class ControlEnviosView( ExportedReport, FormView ):
    form_class = ControlForm
    report_class = ControlEnviosReport
    title = "Control de Envios"
    template_name = 'postal/envios.html'
    required_permissions = ('reportes.envios',)

    def render_to_response(self, context, **response_kwargs):
        if self.request.GET.get('send'):
            cod = self.request.GET.get('send')
            if cod <> '':
                data = ControlEnvio.objects.filter(pk=cod)
            if data is not None or len(data) != 0:
                report = self.report_class(queryset=data)
                report.title = self.title
                resp = HttpResponse(mimetype='application/pdf')
                report.generate_by(PDFGenerator,variables={'description':self.description, 'fecha':self.fecha, 'nombre':self.nombre}, filename=resp)
                return resp

class ControlPesoView( ExportedReport, FormView ):
    form_class = ControlForm
    report_class = ControlPesoReport
    title = "Control de Peso"
    template_name = 'postal/peso.html'
    required_permissions = ('reportes.peso',)

    def render_to_response(self, context, **response_kwargs):
        if self.request.GET.get('send'):
            cod = self.request.GET.get('send')
            if cod <> '':
                data = ControlPeso.objects.filter(pk=cod)
                try:
                    m = Cobrador.objects.get(ruta=data[0].casillero.cliente.zona)
                    self.description = m.nombrecorto
                except:
                    self.description = ''
            if data is not None or len(data) != 0:
                report = self.report_class(queryset=data)
                report.title = self.title
                resp = HttpResponse(mimetype='application/pdf')
                report.generate_by(PDFGenerator,variables={'description':self.description, 'fecha':self.fecha, 'nombre':self.nombre}, filename=resp)
                return resp

class DesaduanajeView( ExportedReport, FormView ):
    form_class = DesaduanajeForm
    report_class = CartaReport
    title = "Retencion de Aduana"
    template_name = 'postal/retencionaduana.html'
    required_permissions = ('reportes.retencion',)

    def form_valid( self, form ):
        self.description = form.cleaned_data['descripcion']
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']
        estado = form.cleaned_data['estado']
        data = []
        if start_date <> None and end_date <> None:
            try:
                data = Movimiento.objects.filter(fecha__range=[start_date,end_date], estado=estado).exclude(paquete__casillero=None)
            except:
                data = []

        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context, data)

class CambioEstadoView( ExportedReport, FormView ):
    form_class = CambioEstadoForm
    title = "Cambio de Estado a Multiples Paquetes"
    template_name = 'postal/cambioestado.html'
    required_permissions = ('reportes.cambioestado',)

    def form_valid( self, form ):
        fecha = form.cleaned_data['fecha']
        hora = form.cleaned_data['hora']
        estado = form.cleaned_data['estado']
        paquetes = form.cleaned_data['paquetes']
        data = []
        for paquete in paquetes:
            mov = Movimiento(estado=estado, fecha=fecha, paquete=paquete, hora=hora)
            mov.save()
            data.append(mov)
        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context, data)

class ExamenPrevioView( ExportedReport, FormView ):
    form_class = ExamenPrevioForm
    report_class = ExamenPrevioReport
    title = "Solicitud de Examen Previo"
    template_name = 'postal/examenp.html'
    required_permissions = ('reportes.examen',)

    def form_valid( self, form ):
        fecha = form.cleaned_data['fecha']
        self.title = form.cleaned_data['administrador']
        self.nombre = form.cleaned_data['nombre']
        self.description = form.cleaned_data[u'encabezado']
        fechap = form.cleaned_data['fechap']
        estado = form.cleaned_data['estado']
        self.fecha = fecha
        datae = []

        data = Movimiento.objects.filter(estado=estado.id, fecha=fechap)
        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context, data)

class TramiteArticuloView( ExportedReport, FormView ):
    form_class = Tramite116Form
    report_class = TramiteReport
    template_name = 'postal/examenp.html'
    required_permissions = ('reportes.examen',)

    def form_valid( self, form ):
        fecha = form.cleaned_data['fecha']
        self.title = form.cleaned_data['administrador']
        self.nombre = form.cleaned_data['nombre']
        self.description = form.cleaned_data['encabezado']
        fechap = form.cleaned_data['fechap']
        estado = form.cleaned_data['estado']
        self.fecha = fecha
        datae = []

        data = Movimiento.objects.filter(estado=estado.id, fecha=fechap)
        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context, data)

class AduanaPesoView( ExportedReport, FormView ):
    form_class = AduanaPesoForm
    title = "Peso en Kilos de Guia"
    template_name = 'postal/cambiopesokg.html'
    required_permissions = ('reportes.cambioestado',)

    def form_valid( self, form ):
        referencia  = form.cleaned_data['referencia']
        pesokg = form.cleaned_data['pesokg']
        data = []
        try:
            data = Paquete.objects.filter(numref=referencia)
            data.update(pesokg=pesokg)
        except:
            data = []
        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context, data)

class ManifiestoPaqueteView( ExportedReport, FormView ):
    form_class = ManifiestoPaqueteForm
    report_class = ManifiestoPaqueteReport
    title = "Manifiesto de Contenido de Bolsas"
    template_name = 'postal/manifiestopaquete.html'
    required_permissions = ('reportes.manifiestopaquete',)

    def form_valid( self, form ):
        fecha  = form.cleaned_data['fecha']
        guiaaerea = form.cleaned_data['guiaaerea']
        casillero = form.cleaned_data['casillero']
        manifiesto = form.cleaned_data['manifiesto']

        self.fecha = fecha
        self.description = guiaaerea
        self.airbill = manifiesto
        data = []
        try:
            if fecha <> None:
                data = Paquete.objects.filter(fechadespacho=fecha).order_by('bolsa', 'numref')
                if guiaaerea <> '':
                    data = data.filter(guiaaerea=guiaaerea)
                if manifiesto <> '':
                    data = data.filter(consolidado=manifiesto)
                if casillero <> '':
                    data = data.filter(casillero__codigonicabox=casillero)
                self.description = data[0].guiaaerea
        except:
            data = []
        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context, data)

class CobroAdicionalView( ExportedReport, FormView ):
    form_class = CobroAdicionalForm
    report_class = CobroAdicionalReport
    title = "Cobro Adicional por Paquete"
    template_name = 'postal/cobroadicional.html'
    required_permissions = ('reportes.cobroadicional',)

    def form_valid( self, form ):
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']
        try:
            data = CobroAdicional.objects.all()

            if start_date <> None and end_date <> None:
                data = data.filter(fecha__range=[start_date, end_date]).order_by('paquete')
        except:
            data = []

        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context, data)

class PesosClientesView( ExportedReport, FormView ):
    form_class = PesosClientesForm
    report_class = PesosClientesReport
    title = "Reporte de pesos de clientes"
    template_name = 'postal/pesosclientes.html'
    required_permissions = ('reportes.pesosclientes',)

    def form_valid( self, form ):
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']
        try:
            if start_date <> None and end_date <> None:
                data = ControlPeso.objects.filter(fecha__range=[start_date, end_date]).order_by('casillero')
        except:
            data = []
        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context, data)

class RemisionView( ExportedReport, FormView ):
    form_class = RemisionForm
    report_class = RemisionReport
    template_name = 'postal/remisiones.html'
    required_permissions = ('reportes.remisiones',)

    def form_valid( self, form ):
        fecha = form.cleaned_data['fecha']
        estado = form.cleaned_data['estado']
        self.fecha = fecha
        self.description = form.cleaned_data['remision']
        self.title = form.cleaned_data['registro']
        try:
            data = Movimiento.objects.filter(fecha=fecha, estado=estado).order_by('paquete__cliente')
        except:
            data = []

        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context, data)

class ManifiestoCartaView( ExportedReport, FormView ):
    form_class = ManifiestoCartaForm
    report_class = ManifiestoCartaReport
    title = 'Manifiesto de Cartas'
    template_name = 'postal/manifiestocarta.html'
    required_permissions = ('reportes.manifiestocarta',)

    def form_valid( self, form ):
        fecha = form.cleaned_data['fecha']
        tipo = form.cleaned_data['tipo']
        self.fecha = fecha
        self.nombre = form.cleaned_data['no']
        try:
            data = Carta.objects.filter(fecha=fecha)
            if tipo <> None:
                if tipo =='1':
                    self.description = 'REMAIL INTANGIBLES MANIFEST'
                if tipo=='2':
                    self.description = 'REMAIL TANGIBLES MANIFEST'
                data = data.filter(typedoc=tipo)
        except:
            data = []
        piezas = data.aggregate(Sum('piezas'))
        pesos = data.aggregate(Sum('peso'))
        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
            'piezas':piezas,
            'pesos':pesos,
        })
        return self.render_to_response(context, data)

class GuiaCourierView( ExportedReport, FormView ):
    form_class = CourierForm
    report_class = GuiaCourierReport
    title = "Guia Courier"
    required_permissions = ('reportes.courier',)

    def render_to_response(self, context, **response_kwargs):
        if self.request.GET.get('send'):
            cod = self.request.GET.get('send')
            if cod <> '':
                data = Courier.objects.filter(pk=cod)
            if data is not None or len(data) != 0:
                report = self.report_class(queryset=data)
                report.title = self.title
                resp = HttpResponse(mimetype='application/pdf')
                report.generate_by(PDFGenerator, filename=resp)
                return resp

class CasilleroView( ExportedReport, FormView ):
    form_class = AnuladasForm
    title = 'VIPNIC'
    template_name = 'postal/casilleros.html'
    required_permissions = ('reportes.casilleros',)
    csv_titles = ['alias', 'nombre', 'empresa', 'direccion', 'ciudad', 'estado','postal','email','telefono','fax','clave','dop']
    csv_ordering = ['alias', 'nombre', 'empresa', 'direccion', 'ciudad', 'estado','postal','email','telefono','fax','clave','dop']

    def form_valid( self, form ):
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']
        data = []

        if start_date <> None and end_date <> None:
            data1 = Casillero.objects.filter(iniciocontrato__range=[start_date, end_date])
        else:
            data1 = Casillero.objects.all()
        for d in data1:
            data.append({
                'alias':d.alias,
                'nombre':d.cliente.nombrecompleto,
                'empresa':d.cliente.organization if d.cliente.organization <> '' else ' ',
                'direccion':d.cliente.direccion,
                'ciudad':d.cliente.city,
                'estado':d.cliente.departamento,
                'postal':d.codigonicabox,
                'email':d.cliente.email,
                'telefono':d.cliente.telefono,
                'fax':d.cliente.fax,
                'clave':' ',
                'dop':' '
            })
        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context, data)

class PodView( ExportedReport, FormView ):
    form_class = PodForm
    title = 'POD'
    template_name = 'postal/pods.html'
    required_permissions = ('reportes.pod',)
    csv_ordering = ['paquete', 'codigo', '1141', 'estado','fecha','comentarios']

    def form_valid( self, form ):
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']
        estado = form.cleaned_data['estado']
        data1 = Movimiento.objects.all()
        data = []
        if start_date <> None and end_date <> None:
            data1 = data1.filter(fecha__range=[start_date, end_date])

        if estado <> None:
            data1 = data1.filter(estado=estado)

        for d in data1:
            f = time.strptime(str(d.fechareal), '%Y-%m-%d %H:%M:%S.%f')
            fr = time.strftime('%m/%d/%Y %H:%M:%S', f)
            data.append({'paquete':d.paquete.numref,'codigo':d.estado.statusid,'1141':1141, 'estado':'USUARIO STATUS', 'fecha':fr, 'comentarios':d.comentarios })
        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context, data)

class ControlAirbillView( ExportedReport, FormView ):
    form_class = ControlAirbillForm
    report_class = ControlAirbillReport
    title = 'Reporte Outbound'
    template_name = 'postal/control_airbill_report.html'
    required_permissions = ('reportes.pod',)

    def form_valid( self, form ):
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']
        self.description = linea_aerea = form.cleaned_data['linea_aerea']
        data = ControlAirbill.objects.all()

        if start_date <> None and end_date <> None:
            self.fecha = str(start_date) +' - '+ str(end_date)
            data = data.filter(fecha_y_hora__range=[start_date, end_date])

        if linea_aerea <> None:
            data = data.filter(linea_aerea_fk=linea_aerea)

        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context, data)
