from django.contrib.admin.widgets import ForeignKeyRawIdWidget
import string
from django.utils.safestring import mark_safe

class RawIdWidget(ForeignKeyRawIdWidget):
    url = ''
    related_model = None
    script = None
    
    def __init__(self, model, field , url, extra_script=None):
        
        super(RawIdWidget, self).__init__(
                                          model._meta.get_field(field).rel, 
                                          attrs=None, using=None
                                          )
        self.related_model = model._meta.get_field(field).rel.to
        self.url = url
        self.script = extra_script
    
    def render(self, *args, **kwargs):
        original_render = super(RawIdWidget, self).render(*args, **kwargs)

        field = args[0]
        value = args[1]
        code = ''
        name = ''
        if value:
            try:
                obj = self.related_model.objects.get(pk = int(value))
                code,name = obj.js_info
            except (ValueError, self.rel.to.DoesNotExist):
                pass
            except Exception as e:
                pass
        scripts = [ 
                   "var id=this.parentNode.getElementsByTagName('input')[0]",
                   "var name=this.parentNode.getElementsByTagName('input')[2]",
                   "var code=this.parentNode.getElementsByTagName('input')[1].value",
                   "id.value = ''",
                   "name.value = ''",
                   "var url='%s?format=json&code=' + code "%self.url,                   
                        "$.getJSON(url,function(data){",
                        " console.log(data)",
                        "if ( data['meta']['total_count'] > 0){",
                        "id.value = data['objects'][0]['id']",                              
                        "name.value = data['objects'][0]['name']",
                        "}else{name.value = 'No disponible' }",    
                        "})",                                                                                                      
                   ]
        if self.script:
          scripts += [self.script]
        original_render = original_render.replace('<a',
                                                  """
                                                  <input 
                                                      id = 'code_%s' 
                                                      onchange="%s" 
                                                      type='text' 
                                                      value = '%s'
                                                  > 
                                                  <a """ 
                                                  %(
                                                    field,
                                                    ";".join(scripts),
                                                    code                                                    
                                                    )
                                                  )
        
        original_render =  original_render + """
                                <input 
                                id = 'name_%s' 
                                type='text' 
                                style='font-weight:bold; width:500px;' 
                                readonly='readonly' 
                                value = '%s'>
                                """%(field,name)
        original_render = original_render.replace('name="%s"' %field,'name="%s" style="display:none;"' %field)

        ADMIN_ROOT_URL = "/admin/"
        return mark_safe(string.replace(original_render,"../../../", ADMIN_ROOT_URL))

    def label_for_value(self, value):
        return ''
