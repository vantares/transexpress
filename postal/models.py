# -*- coding: UTF-8 -*-
from django.db import models
from django.db.models import Sum
from crm.models import Cliente
from nomina.models import Vendedor, Cobrador
from caja.models import TarifaServicio, Servicio
from django.contrib.contenttypes.models import ContentType
from django.contrib.admin.models import LogEntry
from django.contrib.auth.models import User
import string
from datetime import date
import random
from decimal import Decimal
import math
from contabilidad.moneyfmt import moneyfmt
from django.core.exceptions import ValidationError
from django.conf import settings
#Generar Password para casillero de cliente
def passwd_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))

TYPEDOC=(
    ('1','DOC'),
    ('2','SPX'),
)

#=================================================
# Regiones, Lugares y Lineas Aereas
#=================================================
class Region( models.Model ):
    region = models.CharField(db_column="region", null=False, blank=False, max_length=300, verbose_name=u"región")

    class Meta:
        verbose_name = u"región"
        verbose_name_plural = "regiones"

    def __unicode__( self ):
        return self.region

class Lugar(models.Model):
    lugar = models.CharField(db_column="lugar", null=False, blank=False, max_length=300, verbose_name="lugar")
    region = models.ForeignKey(Region, null=True, blank=True, verbose_name=u"región")

    class Meta:
        verbose_name = "ciudad"
        verbose_name_plural = "ciudades"

    def __unicode__( self ):
        return self.lugar

class LineaAerea( models.Model ):
    linea = models.CharField(db_column="linea", null=False, blank=False, max_length=300, verbose_name=u"línea aérea")

    class Meta:
        verbose_name = u"línea aérea"
        verbose_name_plural = u"líneas aéreas"

    def __unicode__( self ):
        return self.linea

#=================================================
# Caja Postal (Casillero)
#=================================================
class Casillero( models.Model ):
    cliente = models.ForeignKey(Cliente, blank=False, null=False, verbose_name="cliente")
    codigonicabox = models.CharField(db_column="nicabox", null=False, blank=False, max_length=300, verbose_name=u"Número nicabox")
    password = models.CharField(db_column="password", null=True, blank=True, max_length=100, default=passwd_generator, verbose_name="password", help_text='Este es el password que utilizara el cliente para la consulta de sus paquetes')
    iniciocontrato = models.DateField(db_column="inicio", null=True, blank=True, verbose_name="inicio contrato")
    proximocobro = models.DateField(db_column="proximocobro", null=True, blank=True,help_text='aplica solo para clientes con membresia', verbose_name=u"Próximo cobro")
    fincontrato = models.BooleanField(db_column="fincontrato", null=False, blank=False, default=False, verbose_name="contrato cancelado")
    justificacion = models.CharField(null=True, blank=True, max_length=500)
    fechafin = models.DateField(db_column="fin", null=True, blank=True, verbose_name=u"fecha cancelación")
    domicilio = models.BooleanField(default=False, verbose_name="Servicio a Domicilio")
    dirdomicilio = models.CharField(null=True, blank=True, max_length=500, verbose_name=u"Dirección Domicilio")
    pesomax = models.DecimalField(db_column="pesomax", null=False, blank=False, max_digits=8, decimal_places=2, verbose_name=u"peso máximo")
    vendedor = models.ForeignKey(Vendedor, blank=True, null=True)

    @property
    def js_info(self):
        return self.codigonicabox,self.clientecompleto

    class Meta:
        verbose_name = "casillero Browse"
        verbose_name_plural = "casilleros Browse"

    def __unicode__( self ):
        return self.codigonicabox

    @property
    def controlados(self):
        lista = []
        dicc = []
        for a in self.controlpeso_set.all():
            for p in a.paquetes.all():
                lista.append(p.numref)
        paquetes = Paquete.objects.filter(casillero=self).exclude(numref__in=lista)
        dicc = [{'id': i.id, 'descripcion': i.descripcion } for i in paquetes]
        return dicc

    @property
    def autorizados(self):
        autorizados = PersonaAutorizada.objects.filter(casillero=self)
        dicc = [{'id': i.id, 'descripcion': i.descripcion } for i in autorizados]
        return dicc

    @property
    def pesos(self):
        dicc = [{'id': i.id, 'descripcion': i.descripcion } for i in self.controlpeso_set.filter(requerimiento=None)]
        return dicc

    @property
    def descripcion(self):
        return self.codigonicabox

    @property
    def clientecompleto(self):
        return self.cliente.organization if self.cliente.organization else self.cliente.nombrecompleto

    @property
    def alias(self):
        return 'NIC'+self.codigonicabox

    def clean(self):
        validar =  False
        try:
            validar = self.cliente is not None
        except:
            pass

        if validar:
            if self.cliente.bloqueado or (not self.cliente.activo) :
                raise ValidationError('El cliente ' + self.cliente.nombre + ' se encuentra bloqueado, por favor seleccione un cliente activo')

        models.Model.clean(self)

    def save(self):

        if self.iniciocontrato is None:
            self.iniciocontrato = date.today()

            if self.cliente.tipocobro is not None:
                self.proximocobro = self.iniciocontrato
        models.Model.save(self)

class CasilleroRead( Casillero ):
    class Meta:
        verbose_name = "Casillero"
        verbose_name_plural = "Casillero"
        proxy = True

#=========================================
# Personas Autorizadas por caja postal
#=========================================
class PersonaAutorizada( models.Model ):
    casillero = models.ForeignKey(Casillero, blank=True, null=True, verbose_name="Caja Postal")
    nombre = models.CharField(db_column="nombre", null=False, blank=False, max_length=100, verbose_name="nombre")
    cedula = models.CharField(db_column="cedula", null=True, blank=True, max_length=20, verbose_name=u"cédula")
    direccion = models.CharField(max_length=500, null=True, blank=True)

    class Meta:
       verbose_name = "persona autorizada"
       verbose_name_plural = "personas autorizadas"

    def __unicode__( self ):
        return self.nombre

    @property
    def descripcion(self):
        return "%s | %s " %(self.nombre,self.cedula )

    def clean(self):
        validar =  False
        try:
            validar = self.casillero.cliente is not None
        except:
            pass

        if validar and not self.casillero.cliente.activo:
                raise ValidationError('El cliente ' + self.casillero.cliente.nombre + ' se encuentra temporalmente bloqueado, por favor seleccione un cliente activo')

        models.Model.clean(self)

#=================================================
# Estado Paquete, Tipo Envio, Tipos Transacciones
#=================================================
class EstadoPaquete( models.Model ):
    statusid = models.IntegerField(blank=True, null=True, verbose_name='status id')
    codigo = models.CharField(blank=True, null=True, verbose_name='codigo unico', unique=True, max_length=5)
    estado = models.CharField(db_column="estado", null=False, blank=False, max_length=100, verbose_name="estado del paquete")
    comentarios = models.TextField(null=True, blank=True, verbose_name="Comentarios")

    class Meta:
        verbose_name = "estado del paquete"
        verbose_name_plural = "estados del paquete"

    def __unicode__( self ):
        return self.estado

class TipoEnvioPaquete( models.Model ):
    codigo = models.IntegerField()
    envio = models.CharField(db_column="envio", null=False, blank=False, max_length=100, verbose_name=u"tipo de envío")
    tarifa_asociada = models.ForeignKey(Servicio, verbose_name='Servicio Asociado')

    class Meta:
        verbose_name = "tipo de envio"
        verbose_name_plural = "tipos de envio"

    def __unicode__( self ):
        return self.envio

class TipoTransaccion( models.Model ):
    casillero = models.ForeignKey(Casillero, blank=True, null=True, verbose_name="Caja Postal")
    transaccion = models.CharField(db_column="transaccion", null=False, blank=False, max_length=100, verbose_name=u"tipo de transacción")
    fecha = models.DateField(db_column="fecha", null=True, blank=True, verbose_name="fecha transaccion")

    class Meta:
        verbose_name = u"tipo de transacción"
        verbose_name_plural = "tipos de transacciones"

    def __unicode__( self ):
        return self.transaccion


#================================================
# Informacion General Guia-Courier
#=================================================
class GC ( models.Model ):
    numref = models.CharField(max_length=100, verbose_name="Guia")
    fechaingreso = models.DateField(null=True, blank=True, verbose_name="Fecha de Ingreso")
    horaingreso = models.TimeField(null=True, blank=True, verbose_name='Hora Ingreso')


    #===================  Otra Informacion ====================================================
    piezas = models.IntegerField(default=1, null=True, blank=True, verbose_name='Numero de Piezas')
    observaciones = models.CharField(null=True, blank=True, max_length=300)
    declarado = models.DecimalField(null=True, blank=True, default=0, max_digits=9, decimal_places=2, verbose_name='Valor Declarado')

    #===================  Datos de Volumen ====================================================
    largo = models.DecimalField(null=True, blank=True, default=0, max_digits=9, decimal_places=2)
    alto = models.DecimalField(null=True, blank=True, default=0, max_digits=9, decimal_places=2)
    ancho = models.DecimalField(null=True, blank=True, default=0, max_digits=9, decimal_places=2)
    volumetrico = models.DecimalField(null=True, blank=True, default=0, max_digits=12, decimal_places=2)

    def save(self):
        if self.largo <> None or self.alto <> None or self.ancho == None:
            self.volumetrico = (self.largo * self.alto * self.ancho)/166
        models.Model.save(self)

    def __unicode__( self ):
        return self.numref

#================================================
# Informacion de Paquete
#=================================================
class Paquete( GC ):

    casillero = models.ForeignKey(Casillero, blank=True, null=True, verbose_name="Caja Postal")
    pesolb = models.DecimalField(null=True, blank=True, default=0, max_digits=9, decimal_places=2, verbose_name='Peso en libras')
    pesokg = models.DecimalField(null=True, blank=True, default=0, max_digits=9, decimal_places=2, verbose_name='Peso en kg')
    contenido = models.TextField(null=True, blank=True, verbose_name="contenido")

    servicio = models.CharField(null=True, blank=True, max_length=50, verbose_name="servicio")
    remitente = models.TextField(null=True, blank=True, verbose_name="Remitente")

    #=== Referencia a Cliente en Hoja C (nombre, direccion, telefono, )=======================
    cliente = models.TextField(null=True, blank=True, verbose_name="Destinatario")
    direccion = models.TextField(null=True, blank=True)
    telefono = models.CharField(max_length=50, null=True, blank=True)
    ciudad = models.CharField(null=True, blank=True, max_length=50, default='Managua', verbose_name="Ciudad destino")

    #==== Informacion Adicional ==============================================================
    guiaingreso = models.CharField(null=True, blank=True, max_length=50, verbose_name="guia ingreso")
    otros = models.DecimalField(null=True, blank=True, default=0, max_digits=9, decimal_places=2)
    fechadespacho = models.DateField(null=True, blank=True, verbose_name="Fecha de Despacho", help_text="Esta es la fecha en que Miami envio el Paquete hacia Nicaragua")
    guiaaerea = models.CharField(null=True, blank=True, max_length=100, verbose_name="Guia Aerea")
    bolsa = models.CharField(null=True, blank=True, max_length=100)
    consolidado = models.CharField(null=True, blank=True, max_length=100, verbose_name="Manifiesto")
    flete = models.DecimalField(null=True, blank=True, default=0, max_digits=9, decimal_places=2)
    estado = models.ForeignKey(EstadoPaquete, blank=True, null=True, verbose_name="Ultimo Status")


    class Meta:
        verbose_name = "paquete Browse"
        verbose_name_plural = "paquetes Browse"

    def get_history(self):
        content_type = ContentType.objects.get_for_model(self)
        return LogEntry.objects.filter(object_id=self.pk, content_type=content_type)

    def last_edited_by(self):
        history = list(self.get_history()[:1])
        if not history:
            return User.objects.get(pk=1)
        else:
            return history[0].user

    def save(self):
        self.flete = self.pesokg * Decimal('2.99')
        self.flete = self.flete if self.flete is not None else Decimal(0)
        models.Model.save(self)

    def clean(self):
        validar =  False
        try:
            validar = self.casillero.cliente is not None
        except:
            pass

        if validar and not self.casillero.cliente.activo:
                raise ValidationError('El cliente ' + self.casillero.cliente.nombre + ' se encuentra temporalmente bloqueado, por favor seleccione un cliente activo')

        models.Model.clean(self)

    @property
    def fecha(self):
        return self.fechaingreso

    @property
    def concepto(self):
        return 'Paquete - Guia ' + self.numref

    @property
    def peso(self):
        return (self.pesokg * Decimal('2.2') if self.pesokg is not None else Decimal(0)) + (self.pesolb if self.pesolb is not None else Decimal(0))

    @property
    def kilos(self):
        return Decimal(self.pesolb / Decimal(2.2)).quantize(Decimal('1.00'))

    @property
    def destinatario(self):
        return self.cliente[:28]

    @property
    def flet(self):
        return Decimal(self.kilos * Decimal(2.99)).quantize(Decimal('1.00'))

    @property
    def descripcion(self):
        return "%s | peso: %s | remitente: %s " %(self.numref,self.peso, self.remitente )

class PaqueteRead( Paquete ):
    class Meta:
        verbose_name = "paquete"
        verbose_name_plural = "paquetes"
        proxy = True

#=================================================
# Courier
#=================================================
class ControlAirbill( models.Model ):
    guia = models.CharField(blank=True, max_length=300)
    fecha_y_hora = models.DateTimeField()
    typedoc = models.CharField(choices=TYPEDOC, max_length = 20, verbose_name='Tipo', null=True, blank=True)
    origen = models.CharField(blank=True, max_length=300)
    destino = models.CharField(blank=True, max_length=300)
    linea_aerea_fk = models.ForeignKey(LineaAerea, null=True, blank=True)
    recibido_por = models.CharField(blank=True, max_length=300)
    peso = models.DecimalField(blank=True, default=0, max_digits=9, decimal_places=2)
    piezas = models.IntegerField()

class GuiaNoImportada( GC ):
    #=== Referencia a Cliente en caso que exista (nombre, direccion, telefono, )=============
    cliente = models.ForeignKey(Cliente, blank=True, null=True, verbose_name="Remitente", help_text="En caso que el remitente exista en la base de datos seleccione un nombre de la lista")
    comentarios = models.TextField(null=True, blank=True, verbose_name="Comentarios")
    mensajero = models.ForeignKey(Cobrador, blank=True, null=True )

    #========================== Informacion del Remitente ============================================
    codr = models.CharField(null=True, blank=True, max_length=300, verbose_name='codigo' )
    ciar = models.CharField(null=True, blank=True, max_length=300, verbose_name='Empresa' )
    nombrer = models.CharField(max_length=300, verbose_name='nombre' )
    telefonor = models.CharField(null=True, blank=True, max_length=300, verbose_name='telefono' )
    direccionr = models.CharField(null=True, blank=True, max_length=300, verbose_name='direccion' )
    info_ciudadr = models.CharField(null=True, blank=True, max_length=300, verbose_name='Ciudad', help_text='Ciudad - Estado/Provincia - Pais - Codigo Zip/Postal' )
    refr = models.CharField(null=True, blank=True, max_length=300, verbose_name='ref' )

    #========================== Informacion del Destinatario =========================================
    ciad = models.CharField(null=True, blank=True, max_length=300, verbose_name='cia' )
    nombred = models.CharField(max_length=300, verbose_name='nombre' )
    telefonod = models.CharField(null=True, blank=True, max_length=300, verbose_name='telefono' )
    direcciond = models.CharField(null=True, blank=True, max_length=300, verbose_name='direccion' )
    info_ciudadd = models.CharField(null=True, blank=True, max_length=300, verbose_name='Ciudad', help_text='Ciudad - Estado/Provincia - Pais - Codigo Zip/Postal' )
    region = models.ForeignKey(Region, blank=True, null=True, verbose_name="Region Destino")

    typedoc = models.CharField(choices=TYPEDOC, max_length = 20, verbose_name='Tipo')
    valor = models.DecimalField(blank=True, default=0, max_digits=9, decimal_places=2)
    peso = models.DecimalField(default=0, max_digits=9, decimal_places=2)
    guiaaerea = models.CharField(null=True, blank=True, max_length=100)
    collect = models.CharField(null=True, blank=True, max_length=100)
    otro_cobro = models.CharField(null=True, blank=True, max_length=100)

class Courier( GC ):
    #=== Referencia a Cliente en caso que exista (nombre, direccion, telefono, )=============
    cliente = models.ForeignKey(Cliente, blank=True, null=True, verbose_name="Remitente", help_text="En caso que el remitente exista en la base de datos seleccione un nombre de la lista")
    comentarios = models.TextField(null=True, blank=True, verbose_name="Comentarios")
    mensajero = models.ForeignKey(Cobrador, blank=True, null=True )

    #========================== Informacion del Remitente ============================================
    codr = models.CharField(null=True, blank=True, max_length=300, verbose_name='codigo' )
    ciar = models.CharField(null=True, blank=True, max_length=300, verbose_name='Empresa' )
    nombrer = models.CharField(max_length=300, verbose_name='nombre' )
    telefonor = models.CharField(null=True, blank=True, max_length=300, verbose_name='telefono' )
    direccionr = models.CharField(null=True, blank=True, max_length=300, verbose_name='direccion' )
    info_ciudadr = models.CharField(null=True, blank=True, max_length=300, verbose_name='Ciudad', help_text='Ciudad - Estado/Provincia - Pais - Codigo Zip/Postal' )
    refr = models.CharField(null=True, blank=True, max_length=300, verbose_name='ref' )

    #========================== Informacion del Destinatario =========================================
    ciad = models.CharField(null=True, blank=True, max_length=300, verbose_name='cia' )
    nombred = models.CharField(max_length=300, verbose_name='nombre' )
    telefonod = models.CharField(null=True, blank=True, max_length=300, verbose_name='telefono' )
    direcciond = models.CharField(null=True, blank=True, max_length=300, verbose_name='direccion' )
    info_ciudadd = models.CharField(null=True, blank=True, max_length=300, verbose_name='Ciudad', help_text='Ciudad - Estado/Provincia - Pais - Codigo Zip/Postal' )
    region = models.ForeignKey(Region, blank=True, null=True, verbose_name="Region Destino")

    typedoc = models.CharField(choices=TYPEDOC, max_length = 20, verbose_name='Tipo')
    valor = models.DecimalField(blank=True, default=0, max_digits=9, decimal_places=2)
    peso = models.DecimalField(default=0, max_digits=9, decimal_places=2)
    guiaaerea = models.CharField(null=True, blank=True, max_length=100)

    #=== Referencia a Requerimiento donde fue facturado=============
    requerimiento = models.ForeignKey("facturacion.Requerimiento", blank=True, null=True, verbose_name="Requerimiento")
    total = models.DecimalField(blank=True, default=0, max_digits=9, decimal_places=2,verbose_name="Total Facturado")

    class Meta:
        verbose_name = "courier Browse"
        verbose_name_plural = "couriers Browse"

    @property
    def fecha(self):
        f = self.fechaingreso
        if f is None:
            f = self.requerimiento.fecha
        return  f

    @property
    def concepto(self):
        return "Guia " + self.numref

    @property
    def descripcion(self):
        return "Guia " + self.numref


    @property
    def facturado(self):
        return self.requerimiento is not None


    def obtener_total(self):
        valor = Decimal(0)
        if self.requerimiento is not None:
            tarifas = TarifaServicio.objects.filter(tipo=self.requerimiento.servicio).filter(region=self.region)
            t = None
            for t in tarifas:
                if self.peso >= t.limite_inferior:
                    if self.peso <= t.limite_superior :
                        return t.monto + (t.recargo if self.typedoc == '2' and t.recargo is not None else Decimal(0))

            if t is not None:
                if t.limite_inferior is None:
                    return t.monto + (t.recargo if self.typedoc == '2' and t.recargo is not None else Decimal(0))
                else:
                    limite = t.limite_superior if t.limite_superior is not None else t.limite_inferior
                adicional = (Decimal(str(math.ceil(self.peso - limite))) * t.adicional) if t.adicional is not None else Decimal(0)
                return t.monto + adicional + (t.recargo if self.typedoc == '2' and t.recargo is not None else Decimal(0))

        return valor

    def save(self, *args, **kwargs):
        self.total = self.obtener_total()
        if self.cliente:
            self.codr = self.cliente.id
            self.ciar = self.cliente.organization
            self.nombrer  = self.cliente.clientecompleto
            self.telefonor = self.cliente.telefono
            self.direccionr = self.cliente.direccion
            self.info_ciudad = self.cliente.ciudad
        super(Courier, self).save(*args, **kwargs)

class CourierRead( Courier ):
    class Meta:
        verbose_name = "courier"
        verbose_name_plural = "couriers"
        proxy = True

#=================================================
# Cartas
#=================================================
class Carta( models.Model ):
    fecha = models.DateField()
    referenciapeso = models.CharField(null=True, blank=True, max_length = 20, verbose_name='referencia de peso')
    casillero = models.ForeignKey(Casillero, blank=True, null=True, verbose_name="Numero de Nicabox")
    cliente = models.ForeignKey(Cliente, verbose_name="cliente",blank=True, null=True)
    contsinval = models.CharField(null=True, blank=True, max_length = 20, verbose_name='Contado / Sin Valor')

    remitente = models.CharField(max_length=300,blank=True, null=True)
    direccionr = models.CharField(null=True, blank=True, max_length=300, verbose_name='direccion' )

    destinatario = models.CharField(max_length=300)
    direcciond = models.CharField(null=True, blank=True, max_length=300, verbose_name='direccion' )

    formaenvio = models.ForeignKey(TipoEnvioPaquete, verbose_name=u"tipo de envío")
    typedoc = models.CharField(choices=TYPEDOC, max_length = 20, verbose_name='Tipo')

    numreg = models.CharField(null=True, blank=True, max_length=50, verbose_name='numero de registro')

    piezas = models.IntegerField()
    peso = models.DecimalField(default=0, max_digits=9, decimal_places=2)
    region = models.ForeignKey(Region, default=2, verbose_name="Lugar Destino")
    linea_aerea = models.ForeignKey(LineaAerea, null=True, blank=True)
    valor = models.DecimalField(blank=True, default=0, max_digits=9, decimal_places=2)

    #=== Referencia a Requerimiento donde fue facturado=============
    requerimiento = models.ForeignKey("facturacion.Requerimiento", blank=True, null=True, verbose_name="Requerimiento")
    total = models.DecimalField(blank=True, default=0, max_digits=9, decimal_places=2,verbose_name="Total Facturado")

    def __unicode__( self ):
        return str(self.id)+' - '+self.remitente+' - '+self.destinatario+' - '+str(self.fecha)

    class Meta:
        verbose_name = "carta Browse"
        verbose_name_plural = "cartas Browse"

    @property
    def concepto(self):
        return "Carta Enviada a " + self.remitente

    @property
    def facturado(self):
        return self.requerimiento is not None

    @property
    def descripcion(self):
        return "Carta #%s | Enviada a %s | peso: %s | total: %s" %(self.pk,self.remitente,self.peso, moneyfmt(self.total,2))

    def obtener_total(self):
        valor = Decimal(0)
        if self.requerimiento is not None:
            tarifas = TarifaServicio.objects.filter(
                tipo=self.formaenvio.tarifa_asociada,
                region=self.region
            )

            if len(tarifas) == 0:
                tarifas = TarifaServicio.objects.filter(
                    tipo=self.formaenvio.tarifa_asociada,
                    region__region=settings.DEFAULT_REGION
                )

            t = None
            for t in tarifas:
                if self.peso >= t.limite_inferior:
                    if self.peso <= t.limite_superior:
                        return t.monto + (t.recargo if self.typedoc == '2' and t.recargo is not None else Decimal(0))

            if t is not None:
                if t.limite_inferior is None:
                    return t.monto + (t.recargo if self.typedoc == '2' and t.recargo is not None else Decimal(0))
                else:
                    limite = t.limite_superior if t.limite_superior is not None else t.limite_inferior
                adicional = (Decimal(str(math.ceil(self.peso - limite))) * t.adicional) if t.adicional is not None else Decimal(0)
                return t.monto + adicional + (t.recargo if self.typedoc == '2' and t.recargo is not None else Decimal(0))

        return valor


    def clean(self):
        if self.cliente is None and self.casillero is None:
            raise ValidationError('Por favor seleccione un cliente o un casillero')

        validar =  False
        try:
            validar = self.casillero.cliente is not None
        except:
            pass

        if validar and not self.casillero.cliente.activo:
                raise ValidationError('El cliente ' + self.casillero.cliente.nombre + ' se encuentra temporalmente bloqueado, por favor seleccione un cliente activo')


        models.Model.clean(self)

    def save(self, *args, **kwargs):
        self.total = self.obtener_total()
        if self.cliente is None and self.casillero is not None:
            self.cliente = self.casillero.cliente
        super(Carta, self).save(*args, **kwargs)



class CartaRead( Carta ):
    class Meta:
        verbose_name = "carta"
        verbose_name_plural = "cartas"
        proxy = True

#=================================================
# Cambios de estado de un paquete
#=================================================
class Movimiento( models.Model ):
    estado = models.ForeignKey(EstadoPaquete, verbose_name="nuevo estado")
    fecha = models.DateField(verbose_name="Fecha")
    hora = models.CharField(null=True, blank=True, max_length=12 )
    fechareal = models.DateTimeField(verbose_name="Fecha Real", auto_now_add=True)
    comentarios = models.CharField(null=True, blank=True, max_length=500, verbose_name="Comentarios")
    paquete = models.ForeignKey(Paquete, verbose_name="paquete")

    class Meta:
        verbose_name = "Cambio de Estado de un Paquete"
        verbose_name_plural = "Cambio de Estados de Paquetes"
        unique_together = ('paquete','estado', 'fecha', 'hora')

    def __unicode__( self ):
        return unicode(self.estado)


class ExoneracionVariosAbonos( models.Model ):
    casillero = models.ForeignKey(Casillero,verbose_name="Nicabox")
    fecha = models.DateField()
    fechafin = models.DateField(null=True, blank=True, verbose_name="fecha fin")
    cargo = models.DecimalField(default=500, max_digits=9, decimal_places=2)
    saldo = models.DecimalField(null=True, blank=True, default=0, max_digits=9, decimal_places=2)

    def __unicode__(self):
        return unicode(self.casillero)

    @property
    def persona(self):
        return PersonaAutorizada.objects.filter( casillero = self.casillero ).values('nombre')

    def saldo( self):
        s = Abono.objects.filter(fecha__range=[self.fecha,self.fechafin]).aggregate(Sum('abono'))['abono__sum']
        s = s if s is not None else Decimal(0)
        return self.cargo - s if s is not None else Decimal(0)

    def clean(self):
        validar =  False
        try:
            validar = self.casillero.cliente is not None
        except:
            pass

        if validar and not self.casillero.cliente.activo:
                raise ValidationError('El cliente ' + self.casillero.cliente.nombre + ' se encuentra temporalmente bloqueado, por favor seleccione un cliente activo')

        models.Model.clean(self)

class Abono( models.Model ):
    exoneracion = models.ForeignKey(ExoneracionVariosAbonos)
    abono = models.DecimalField(null=True, blank=True, default=0, max_digits=9, decimal_places=2)
    fecha = models.DateField()

    def __unicode__(self):
        return str(self.abono)

#=================================================
# Control de Envios
#=================================================
class ControlEnvio( models.Model ):
    fecha = models.DateField()
    casillero = models.ForeignKey(Casillero, blank=True, null=True, verbose_name="Casillero")
    autorizados = models.ManyToManyField(PersonaAutorizada, null= True, blank= True, related_name="autorizadas", verbose_name="Personas Autorizadas")
    responsable = models.ForeignKey(User, db_column='responsable',verbose_name="Responsable")
    credito = models.BooleanField(null=False, blank=False, default=False)

    def __unicode__(self):
        return unicode(self.responsable)

    @property
    def pautorizados(self):
        lista = ""
        for a in self.autorizados.all():
            lista += a.nombre + ', '
        return lista

    @property
    def detalles(self):
        return DetalleEnvios.objects.filter(envio=self)

    def clean(self):
        validar =  False
        try:
            validar = self.casillero.cliente is not None
        except:
            pass

        if validar and not self.casillero.cliente.activo:
                raise ValidationError('El cliente ' + self.casillero.cliente.nombre + ' se encuentra temporalmente bloqueado, por favor seleccione un cliente activo')

        models.Model.clean(self)

    def es_credito(self):
        if self.credito:
            return 'CREDITO'
        else:
            return 'CONTADO'

class DetalleEnvios( models.Model ):
    envio = models.ForeignKey(ControlEnvio, blank=True, null=True)
    lugar = models.ForeignKey(Lugar, blank=True, null=True)
    tipenvio = models.ForeignKey(TipoEnvioPaquete, blank=True, null=True, verbose_name="Tipo Envio")
    cantidadcartas = models.CharField(max_length=500)

    def cantidad(self):
        return int(self.cantidadcartas)

#=================================================
# Tipos Documento (Control de Peso)
#=================================================
class TipoDocumento( models.Model ):
    codigo = models.CharField(null=True, blank=True, max_length=500)
    descripcion = models.CharField(max_length=500)

    def __unicode__(self):
        return unicode(self.descripcion)

#=================================================
# Control de Peso
#=================================================
class ControlPeso( models.Model ):
    fecha = models.DateField()
    casillero = models.ForeignKey(Casillero, blank=True, null=True, verbose_name="Casillero")
    peso = models.DecimalField(null=True, blank=True, default=0, max_digits=9, decimal_places=2, verbose_name='Peso')
    piezas = models.IntegerField()
    responsable = models.ForeignKey(User, db_column='responsable',verbose_name="Responsable")
    paquetes = models.ManyToManyField(Paquete, null= True, blank= True, related_name="paquetes", verbose_name="Paquetes Relacionados")

    #=== Referencia a Requerimiento donde fue facturado=============
    requerimiento = models.ForeignKey("facturacion.Requerimiento", blank=True, null=True, verbose_name="Requerimiento")
    total = models.DecimalField(blank=True, default=0, max_digits=9, decimal_places=2,verbose_name="Total Facturado")

    def __unicode__(self):
        return format(self.fecha,"%d/%m/%y") +'-' + unicode(self.responsable) + '-casillero' + self.casillero.codigonicabox + '-peso:' + self.peso.to_eng_string()

    @property
    def detallesd(self):
        return DetalleDocControlPeso.objects.filter(control=self)
    @property
    def detallesp(self):
        return DetalleGuiaControlPeso.objects.filter(control=self)

    def clean(self):
        validar =  False
        try:
            validar = self.casillero.cliente is not None
        except:
            pass

        if validar and not self.casillero.cliente.activo:
                raise ValidationError('El cliente ' + self.casillero.cliente.nombre + ' se encuentra temporalmente bloqueado, por favor seleccione un cliente activo')

        models.Model.clean(self)


    @property
    def concepto(self):
        return "Carta Enviada a " + self.remitente

    @property
    def facturado(self):
        return self.requerimiento is not None

    @property
    def descripcion(self):
        return "Control #%s | casillero %s | peso: %s | fecha: %s " %(self.pk,self.casillero,self.peso, self.fecha )

    def save(self, *args, **kwargs):
        super(ControlPeso, self).save(*args, **kwargs)


class DetalleDocControlPeso( models.Model ):
    control = models.ForeignKey(ControlPeso, blank=True, null=True)
    tipodoc = models.ForeignKey(TipoDocumento, null=True, blank=True)
    piezas = models.IntegerField()


class DetalleGuiaControlPeso( models.Model ):
    control = models.ForeignKey(ControlPeso, blank=True, null=True)
    paquete = models.ForeignKey(Paquete, verbose_name="paquete")

    def __unicode__(self):
        return unicode(self.paquete.destinatario)
