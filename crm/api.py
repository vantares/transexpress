from tastypie.resources import ModelResource
from tastypie import fields, utils
from tastypie.constants import ALL

from models import Cliente
from postal.api import CasilleroResource, CartaResource, CourierResource
from facturacion.api import RequerimientoResource,FacturaResource, NotaDebitoResource
from postal.models import Courier
from facturacion.models import Factura, NotaDebito


class ClienteResource(ModelResource):
    code = fields.CharField(attribute='codigo')
    name = fields.CharField(attribute='clientecompleto')
    casilleros = fields.ToManyField(CasilleroResource, 'casillero_set', full = True, null = True, blank = True)
    cartas = fields.ToManyField(CartaResource, 'carta_set', full = True, null = True, blank = True)
    requerimientos = fields.ToManyField(RequerimientoResource, attribute='requerimientos_pendientes', full = True, null = True, blank = True)
    courier = fields.ToManyField(CourierResource, attribute=lambda cli: Courier.objects.filter(cliente=cli.obj, requerimiento=None), full = True, null = True, blank = True)    
    facturas = fields.ToManyField(FacturaResource, attribute=lambda bundle: Factura.objects.filter(cliente__codigo=bundle.obj.codigo), full = True, null = True, blank = True)
    notas = fields.ToManyField(NotaDebitoResource, attribute=lambda bundle: NotaDebito.objects.filter(cliente__codigo=bundle.obj.codigo), full = True, null = True, blank = True)
    
    class Meta:
        queryset = Cliente.objects.all()
        resource_name = 'cliente'
        filtering = {
            "code": ('exact',),
            "name": ('startswith',),
            "id": ('exact',),
        }        
