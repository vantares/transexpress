# -*- coding: utf-8 -*-
from django.conf import settings

from geraldo import *
from reportlab.lib.pagesizes import LETTER
from reportlab.lib.units import cm
from reportlab.lib.enums import TA_RIGHT, TA_CENTER
from reportlab.lib.colors import navy, red
from reportes.report import ReporteMembretado
from crm.models import Parametro
from settings.settings import PROJECT_DIR
import os
t = Parametro.objects.get(pk=2)
###################################
# Reporte de Clientes
###################################
class ClientesReport(ReporteMembretado):
    author = t.valor
    page_size = landscape(LETTER)
    default_style = {'fontName':'Courier'}

    class band_detail(ReportBand):
        height = 0.5*cm
        auto_expand_height = True
        elements=[
            ObjectValue(attribute_name='nombrecompleto', left=0.5*cm, width=12*cm),
            ObjectValue(attribute_name='tipo', left=8*cm),
            ObjectValue(attribute_name='activo', left=12*cm),
            ObjectValue(attribute_name='exonerado', left=15*cm),
            ObjectValue(attribute_name='zona', left=20*cm),
        ]

    class band_page_header(ReportBand):
        height = 1.3*cm
        elements = [
            SystemField(expression=u'%(report_title)s', top=0.1*cm, left=0, width=BAND_WIDTH,
                style={'fontName': 'Courier', 'fontSize': 14, 'alignment': TA_CENTER, 'textColor':navy}),
            Label(text="Nombre", top=0.8*cm, left=0.5*cm),
            Label(text="Tipo", top=0.8*cm, left=8*cm),
            Label(text="Activo", top=0.8*cm, left=12*cm),
            Label(text="Exonerado", top=0.8*cm, left=15*cm),
            Label(text="Zona", top=0.8*cm, left=20*cm),
            SystemField(expression=u'Pag. %(page_number)d de %(page_count)d', top=0.1*cm,
                width=BAND_WIDTH, style={'alignment': TA_RIGHT}),
            ]
        borders = {'bottom': True}

#
class EstadoCuentaReport(Report):
    title = ''
    default_style = {'fontName':'Courier'}
    page_size = LETTER
    margin_left = 0.5*cm
    margin_right = 0.5*cm

    class band_page_header(ReportBand):
        height = 6.5*cm
        elements = [
            SystemField(expression=u'%(now:%d %b %Y)s', top=0.2*cm, left=9*cm, style={'fontName': 'Courier'}),
            SystemField(expression='%(now:%H:%M)s', top=0.2*cm, left=13.8*cm, style={'fontName': 'Courier'}),
            SystemField(expression=u'%(page_number)d', top=0.2*cm, left=17.7*cm, width=BAND_WIDTH),
            Label(text="ESTIMADO CLIENTE: ADJUNTO DOCUMENTOS QUE SOPORTAN SUS TRAMITES EFECTUADOS DURANTE EL TRANSCURSO DEL MES: FACTURAS, RECIBOS OFICIALES DE CAJA, NOTAS DE DEBITO, Y NOTAS DE CREDITO. CUALQUIER CONSULTA NO DUDE EN LLAMARNOS AL PBX 2222-2270 AL DEPARTAMENTO DE COBROS EXTENSION #118 ", top=1.5*cm, left=8*cm, width=12*cm ,style={'fontName': 'Courier'}),
            Label(text="VISITENOS EN  WWW.TRANSEXPRESS.COM.NI", top=4.4*cm, left=9*cm, width=13*cm ,style={'fontName': 'Courier'}),
            ]
    '''
    class band_detail(ReportBand):
        height = 0.5*cm
        auto_expand_height = True
        elements=[
            ObjectValue(expression='fechaemision', left=0*cm),
            ObjectValue(attribute_name='numeroreferencia', left=3*cm),
            ObjectValue(attribute_name='descripcion', left=4.8*cm),
            ObjectValue(attribute_name='cargo', left=13.2*cm),
            ObjectValue(attribute_name='abono', left=15.7*cm),
            ObjectValue(attribute_name='saldop', left=17.9*cm),
        ]

    class band_summary(ReportBand):
        height = 0.8*cm
        child_bands = [
            ReportBand(
                height = 0.6*cm,
                elements = [
                    ObjectValue(attribute_name='cargo', top=10.5*cm, left=13.2*cm, action=FIELD_ACTION_SUM),
                    ObjectValue(attribute_name='abono', top=10.5*cm, left=15.7*cm, action=FIELD_ACTION_SUM),
                    ObjectValue(attribute_name='saldop', top=10.5*cm, left=17.9*cm, action=FIELD_ACTION_SUM),
                    ObjectValue(expression='nombre', left=0.2*cm, top=-6.5*cm),
                    ObjectValue(expression='apellido', left=0.2*cm, top=-6*cm),
                    ObjectValue(expression='direccion', left=0.2*cm, top=-5.5*cm),
                    ObjectValue(expression='ciudad', left=0.2*cm, top=-5*cm),
                    ObjectValue(expression='departamento', left=2*cm, top=-5*cm),
                    ObjectValue(expression='telefono', left=0.2*cm, top=-4.5*cm),
                    ObjectValue(expression='nicabox', left=2.9*cm, top=-3.6*cm),
                ]),
        ] 
        '''
#Este es el que se envia por mail al cliente
class MailEstadoCuentaReport(Report):
    additional_fonts = { 'Calibri': os.path.join(PROJECT_DIR,'static/fonts/Calibri.ttf'),}
    title = ''
    default_style = {'fontName':'Calibri'}
    page_size = LETTER
    margin_left = 0.8*cm
    margin_right = 0.8*cm

    class band_page_header(ReportBand):
        height = 3.8*cm
        elements = [
            Label(text="ESTADO DE CUENTA"),
            Label(text=t.valor, top=0.5*cm),
            SystemField(expression=u'%(var:cliente)s', top=2*cm, left=3.5*cm),
            Label(text='Codigo Caja Postal :', top=2*cm),
            Label(text='Periodo :', top=2.5*cm),
            SystemField(expression=u'Fecha: %(now:%d/%m/%Y)s', left=17.5*cm),
            SystemField(expression='Hora: %(now:%H:%M)s', top=0.5*cm, left=17.5*cm),
            SystemField(expression=u'Pagina: %(page_number)d', top=1.5*cm, left=17.5*cm),
            Line(left=0*cm, top=3*cm, right=20*cm, bottom=3*cm),
            Label(text='Cobrador:', top=2*cm, left=17.5*cm),
            Label(text='Fecha', top=3*cm),
            Label(text='Emision', top=3.3*cm),
            Label(text='Fecha', top=3*cm, left=2*cm),
            Label(text='Vence', top=3.3*cm, left=2*cm),
            Label(text='No', top=3.2*cm, left=3.7*cm),
            Label(text='Descripcion', top=3.2*cm, left=6*cm),
            Label(text='Cargo / Abono', top=3.2*cm, left=13*cm),
            Label(text='Saldo', top=3.2*cm, left=18*cm),
            ]
        borders = {'bottom': True}
    
    class band_detail(ReportBand):
        height = 0.5*cm
        auto_expand_height = True
        elements=[
            ObjectValue(expression='fecha', left=0*cm),
            ObjectValue(expression='vencimiento', left=1.9*cm),
            ObjectValue(attribute_name='numero', left=3.7*cm),
            ObjectValue(attribute_name='concepto', left=5.8*cm),
            ObjectValue(attribute_name='total', left=13.2*cm),
#            ObjectValue(attribute_name='', left=15.7*cm),
            ObjectValue(attribute_name='saldo_pendiente', left=17.9*cm),
        ]

    class band_summary(ReportBand):
        height = 0.8*cm
        child_bands = [
            ReportBand(
                height = 0.6*cm,
                elements = [
                    ObjectValue(attribute_name='monto', top=1*cm, left=13.2*cm, action=FIELD_ACTION_SUM),
                ]),
        ] 
    class band_page_footer(ReportBand):
        height = 0.5*cm
        elements = [
            Label(text='Favor emitir cheque a nombre de Trans-Express,S.A. Tipo de cambio: Mercado Paralelo', top=0.1*cm, width=20*cm),
            Label(text='Nota: Para evitar que nuestro sistema bloquee la entrega de su correspondencia cancelar su estado de cuenta antes del ultimo dia del presente mes.', top=0.6*cm, width=20*cm),
            ]
        borders = {'top': True}    
