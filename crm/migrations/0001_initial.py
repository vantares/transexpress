# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Parametro'
        db.create_table('crm_parametro', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('parametro', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, db_column='parametro', blank=True)),
            ('valor', self.gf('django.db.models.fields.CharField')(max_length=500, db_column='valor')),
        ))
        db.send_create_signal('crm', ['Parametro'])

        # Adding model 'Ruta'
        db.create_table('crm_ruta', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('zona', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='zona')),
            ('ruta', self.gf('django.db.models.fields.TextField')(max_length=300, db_column='ruta')),
        ))
        db.send_create_signal('crm', ['Ruta'])

        # Adding model 'Contacto'
        db.create_table('crm_contacto', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cedula', self.gf('django.db.models.fields.CharField')(max_length=16, null=True, db_column='cedula', blank=True)),
            ('licencia', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, db_column='licencia', blank=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='nombre')),
            ('apellido', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='apellido')),
            ('nombrecorto', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, db_column='nombrecorto', blank=True)),
            ('foto', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, db_column='foto', blank=True)),
            ('estadocivil', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, db_column='estadocivil', blank=True)),
            ('nacimiento', self.gf('django.db.models.fields.DateField')(null=True, db_column='nacimiento', blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, db_column='title', blank=True)),
            ('logo', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, db_column='logo', blank=True)),
            ('categoria', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, db_column='categoria', blank=True)),
            ('notas', self.gf('django.db.models.fields.TextField')(null=True, db_column='notas', blank=True)),
        ))
        db.send_create_signal('crm', ['Contacto'])

        # Adding model 'TipoCliente'
        db.create_table('crm_tipocliente', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=50, db_column='nombre')),
        ))
        db.send_create_signal('crm', ['TipoCliente'])

        # Adding model 'TipoCobro'
        db.create_table('crm_tipocobro', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('meses', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('crm', ['TipoCobro'])

        # Adding model 'Cliente'
        db.create_table('crm_cliente', (
            ('contacto_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['crm.Contacto'], unique=True, primary_key=True)),
            ('codigo', self.gf('django.db.models.fields.IntegerField')()),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=100, null=True, db_column='url', blank=True)),
            ('ruc', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, db_column='ruc', blank=True)),
            ('organization', self.gf('django.db.models.fields.CharField')(default='N/A', max_length=35, null=True, db_column='organization', blank=True)),
            ('tipo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crm.TipoCliente'], null=True, blank=True)),
            ('tipocobro', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crm.TipoCobro'], null=True, blank=True)),
            ('clasificacion', self.gf('django.db.models.fields.CharField')(max_length=20, db_column='clasificacion')),
            ('activo', self.gf('django.db.models.fields.BooleanField')(default=False, db_column='activo')),
            ('exonerado', self.gf('django.db.models.fields.BooleanField')(default=False, db_column='exonerado')),
            ('bloqueado', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('zona', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crm.Ruta'], null=True, blank=True)),
        ))
        db.send_create_signal('crm', ['Cliente'])

        # Adding model 'ClientePotencial'
        db.create_table('crm_clientepotencial', (
            ('contacto_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['crm.Contacto'], unique=True, primary_key=True)),
            ('ultimaactualizacion', self.gf('django.db.models.fields.DateField')(null=True, db_column='ultimaactualizacion', blank=True)),
            ('oportunidadtipo', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, db_column='oportunidad', blank=True)),
            ('probabilidad', self.gf('django.db.models.fields.BooleanField')(default=False, db_column='probabilidad')),
        ))
        db.send_create_signal('crm', ['ClientePotencial'])

        # Adding model 'TipoTarjetaCliente'
        db.create_table('crm_tipotarjetacliente', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, db_column='tipo', blank=True)),
        ))
        db.send_create_signal('crm', ['TipoTarjetaCliente'])

        # Adding model 'TarjetaCliente'
        db.create_table('crm_tarjetacliente', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cliente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crm.Cliente'], null=True, blank=True)),
            ('tipo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crm.TipoTarjetaCliente'], null=True, blank=True)),
            ('tarjeta', self.gf('django.db.models.fields.CharField')(max_length=16, null=True, db_column='tarjeta', blank=True)),
            ('vence', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('banco', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('debitoa', self.gf('django.db.models.fields.BooleanField')(default=False, db_column='debitoa')),
        ))
        db.send_create_signal('crm', ['TarjetaCliente'])

        # Adding model 'DocumentoCliente'
        db.create_table('crm_documentocliente', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cliente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crm.Cliente'], null=True, blank=True)),
            ('titulo', self.gf('django.db.models.fields.CharField')(max_length=20, db_column='titulo')),
            ('archivo', self.gf('django.db.models.fields.files.FileField')(max_length=100, db_column='documento')),
        ))
        db.send_create_signal('crm', ['DocumentoCliente'])

        # Adding model 'TipoDireccion'
        db.create_table('crm_tipodireccion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=50, db_column='tipo')),
        ))
        db.send_create_signal('crm', ['TipoDireccion'])

        # Adding model 'DireccionContacto'
        db.create_table('crm_direccioncontacto', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tipo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crm.TipoDireccion'], null=True, blank=True)),
            ('direccion', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='direccion')),
            ('ciudad', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('departamento', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('contacto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crm.Cliente'], null=True, blank=True)),
        ))
        db.send_create_signal('crm', ['DireccionContacto'])

        # Adding model 'TipoTelefono'
        db.create_table('crm_tipotelefono', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, db_column='tipo', blank=True)),
        ))
        db.send_create_signal('crm', ['TipoTelefono'])

        # Adding model 'TelefonoContacto'
        db.create_table('crm_telefonocontacto', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tipo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crm.TipoTelefono'], null=True, blank=True)),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=50, db_column='telefono')),
            ('contacto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crm.Cliente'], null=True, blank=True)),
        ))
        db.send_create_signal('crm', ['TelefonoContacto'])

        # Adding model 'TipoEmail'
        db.create_table('crm_tipoemail', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=50, db_column='tipo')),
        ))
        db.send_create_signal('crm', ['TipoEmail'])

        # Adding model 'EmailContacto'
        db.create_table('crm_emailcontacto', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tipo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crm.TipoEmail'], null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=100, db_column='email')),
            ('contacto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crm.Cliente'], null=True, blank=True)),
        ))
        db.send_create_signal('crm', ['EmailContacto'])

    def backwards(self, orm):
        # Deleting model 'Parametro'
        db.delete_table('crm_parametro')

        # Deleting model 'Ruta'
        db.delete_table('crm_ruta')

        # Deleting model 'Contacto'
        db.delete_table('crm_contacto')

        # Deleting model 'TipoCliente'
        db.delete_table('crm_tipocliente')

        # Deleting model 'TipoCobro'
        db.delete_table('crm_tipocobro')

        # Deleting model 'Cliente'
        db.delete_table('crm_cliente')

        # Deleting model 'ClientePotencial'
        db.delete_table('crm_clientepotencial')

        # Deleting model 'TipoTarjetaCliente'
        db.delete_table('crm_tipotarjetacliente')

        # Deleting model 'TarjetaCliente'
        db.delete_table('crm_tarjetacliente')

        # Deleting model 'DocumentoCliente'
        db.delete_table('crm_documentocliente')

        # Deleting model 'TipoDireccion'
        db.delete_table('crm_tipodireccion')

        # Deleting model 'DireccionContacto'
        db.delete_table('crm_direccioncontacto')

        # Deleting model 'TipoTelefono'
        db.delete_table('crm_tipotelefono')

        # Deleting model 'TelefonoContacto'
        db.delete_table('crm_telefonocontacto')

        # Deleting model 'TipoEmail'
        db.delete_table('crm_tipoemail')

        # Deleting model 'EmailContacto'
        db.delete_table('crm_emailcontacto')

    models = {
        'crm.cliente': {
            'Meta': {'object_name': 'Cliente', '_ormbases': ['crm.Contacto']},
            'activo': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'activo'"}),
            'bloqueado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'clasificacion': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'clasificacion'"}),
            'codigo': ('django.db.models.fields.IntegerField', [], {}),
            'contacto_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['crm.Contacto']", 'unique': 'True', 'primary_key': 'True'}),
            'exonerado': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'exonerado'"}),
            'organization': ('django.db.models.fields.CharField', [], {'default': "'N/A'", 'max_length': '35', 'null': 'True', 'db_column': "'organization'", 'blank': 'True'}),
            'ruc': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'db_column': "'ruc'", 'blank': 'True'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.TipoCliente']", 'null': 'True', 'blank': 'True'}),
            'tipocobro': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.TipoCobro']", 'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '100', 'null': 'True', 'db_column': "'url'", 'blank': 'True'}),
            'zona': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.Ruta']", 'null': 'True', 'blank': 'True'})
        },
        'crm.clientepotencial': {
            'Meta': {'object_name': 'ClientePotencial', '_ormbases': ['crm.Contacto']},
            'contacto_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['crm.Contacto']", 'unique': 'True', 'primary_key': 'True'}),
            'oportunidadtipo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'oportunidad'", 'blank': 'True'}),
            'probabilidad': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'probabilidad'"}),
            'ultimaactualizacion': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'ultimaactualizacion'", 'blank': 'True'})
        },
        'crm.contacto': {
            'Meta': {'object_name': 'Contacto'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'apellido'"}),
            'categoria': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'categoria'", 'blank': 'True'}),
            'cedula': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'db_column': "'cedula'", 'blank': 'True'}),
            'estadocivil': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'estadocivil'", 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'db_column': "'foto'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'licencia': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'licencia'", 'blank': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'db_column': "'logo'", 'blank': 'True'}),
            'nacimiento': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'nacimiento'", 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'nombre'"}),
            'nombrecorto': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'nombrecorto'", 'blank': 'True'}),
            'notas': ('django.db.models.fields.TextField', [], {'null': 'True', 'db_column': "'notas'", 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'title'", 'blank': 'True'})
        },
        'crm.direccioncontacto': {
            'Meta': {'object_name': 'DireccionContacto'},
            'ciudad': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'contacto': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.Cliente']", 'null': 'True', 'blank': 'True'}),
            'departamento': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'direccion'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.TipoDireccion']", 'null': 'True', 'blank': 'True'})
        },
        'crm.documentocliente': {
            'Meta': {'object_name': 'DocumentoCliente'},
            'archivo': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'db_column': "'documento'"}),
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.Cliente']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_column': "'titulo'"})
        },
        'crm.emailcontacto': {
            'Meta': {'object_name': 'EmailContacto'},
            'contacto': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.Cliente']", 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '100', 'db_column': "'email'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.TipoEmail']", 'null': 'True', 'blank': 'True'})
        },
        'crm.parametro': {
            'Meta': {'object_name': 'Parametro'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parametro': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'parametro'", 'blank': 'True'}),
            'valor': ('django.db.models.fields.CharField', [], {'max_length': '500', 'db_column': "'valor'"})
        },
        'crm.ruta': {
            'Meta': {'object_name': 'Ruta'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ruta': ('django.db.models.fields.TextField', [], {'max_length': '300', 'db_column': "'ruta'"}),
            'zona': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'zona'"})
        },
        'crm.tarjetacliente': {
            'Meta': {'object_name': 'TarjetaCliente'},
            'banco': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.Cliente']", 'null': 'True', 'blank': 'True'}),
            'debitoa': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'debitoa'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tarjeta': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'db_column': "'tarjeta'", 'blank': 'True'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.TipoTarjetaCliente']", 'null': 'True', 'blank': 'True'}),
            'vence': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'})
        },
        'crm.telefonocontacto': {
            'Meta': {'object_name': 'TelefonoContacto'},
            'contacto': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.Cliente']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'telefono'"}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.TipoTelefono']", 'null': 'True', 'blank': 'True'})
        },
        'crm.tipocliente': {
            'Meta': {'object_name': 'TipoCliente'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'nombre'"})
        },
        'crm.tipocobro': {
            'Meta': {'object_name': 'TipoCobro'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meses': ('django.db.models.fields.IntegerField', [], {}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'crm.tipodireccion': {
            'Meta': {'object_name': 'TipoDireccion'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'tipo'"})
        },
        'crm.tipoemail': {
            'Meta': {'object_name': 'TipoEmail'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'tipo'"})
        },
        'crm.tipotarjetacliente': {
            'Meta': {'object_name': 'TipoTarjetaCliente'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'db_column': "'tipo'", 'blank': 'True'})
        },
        'crm.tipotelefono': {
            'Meta': {'object_name': 'TipoTelefono'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'db_column': "'tipo'", 'blank': 'True'})
        }
    }

    complete_apps = ['crm']