# -*- coding: utf-8 -*-

from django import forms
from crm.models import Cliente, TipoCliente
from postal.models import Casillero
from django.contrib.admin import widgets
from nomina.models import Vendedor
class ClientesForm( forms.Form ):
    CLASIFICACION=[
        ('0','Todas'),
        ('1','Natural'),
        ('2','Organizacion'),
    ]
    nombre = forms.CharField(max_length=300, label='Nombre', required=False)
    apellido = forms.CharField(max_length=300, label='Apellido', required=False)
    cedula = forms.CharField(max_length=300, label='Cedula', required=False)
    ruc = forms.CharField(max_length=300, label='RUC', required=False)
    organization = forms.CharField(max_length=300, label='Organizacion', required=False)
    tipo = forms.ModelChoiceField(queryset=TipoCliente.objects.all(),required=False)
    clasificacion = forms.ChoiceField(choices=CLASIFICACION, label='Clasificacion', required=False)
    vendedor = forms.ModelChoiceField(queryset=Vendedor.objects.all(),required=False)

class EstadoForm( forms.Form ):
    codigo = forms.CharField(label='Codigo Cliente', required=False)
    start_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Desde', required=False)
    end_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Hasta', required=False)
    nicabox = forms.CharField(required=False)

class BloqueoForm( forms.Form ):
    bloqueados = forms.BooleanField(label='Mostrar solo bloqueados', required=False)

