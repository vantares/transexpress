from django.contrib.admin.models import LogEntry
from django.core.mail import EmailMessage
from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
from django.views.generic import FormView
from django.db import connection
from facturacion.models import Factura, NotaDebito, Abono
from crm.models import *
from crm.forms import *
from crm.reports import *
from reportes.views import ExportedReport
from reportes.models import BloqueoCuenta
from django.http import HttpResponse, HttpResponseRedirect, Http404
from geraldo.generators import PDFGenerator
import os
from settings.settings import PROJECT_DIR, STATIC_URL
from itertools import chain
# Reportes
class ClientesView( ExportedReport, FormView ):
    form_class = ClientesForm
    title = 'Reporte de Clientes'
    template_name = 'crm/clientes.html'
    report_class = ClientesReport
    required_permissions = ('reportes.clientes',)

    def form_valid( self, form ):
        nombre = form.cleaned_data['nombre']
        apellido = form.cleaned_data['apellido']
        cedula = form.cleaned_data['cedula']
        ruc = form.cleaned_data['ruc']
        organization = form.cleaned_data['organization']
        tipo = form.cleaned_data['tipo']
        clasificacion = form.cleaned_data['clasificacion']
        vendedor = form.cleaned_data['vendedor']

        self.title = 'Clientes encontrados'
        
        data = Cliente.objects.all()

        if nombre <> '':
            data = data.filter(nombre__icontains=nombre)
        
        if apellido <> '':
            data = data.filter(apellido__startswith=apellido)
    
        if cedula <> '':
            data = data.filter(cedula=cedula)

        if ruc <> '':
            data = data.filter(ruc = ruc) 

        if organization <> '':
            data = data.filter(organization__icontains=organization)
        
        if tipo <> None:
            data = data.filter(tipo = tipo)

        if clasificacion <> '0':
            data = data.filter(clasificacion=clasificacion)
        
        if vendedor <> None:
            data = data.filter(casillero__vendedor = vendedor)

        context = self.get_context_data()
        context.update({
                'form':form,
                'data':data,
        })
        return self.render_to_response(context, data)

# Estado de Cuenta
class EstadoView( ExportedReport, FormView ):
    form_class = EstadoForm
    report_class = MailEstadoCuentaReport
    title = 'Estado de Cuenta'
    template_name = 'crm/cuenta.html'
    required_permissions = ('reportes.estadocuenta',)
    
    def render_to_response(self, context, **response_kwargs):
        if self.request.GET.get('id'):
            cod = self.request.GET.get('id')
            c = Cliente.objects.get(pk=cod)
            facturas = Factura.objects.filter(cliente=c)
            notasdeb = NotaDebito.objects.filter(cliente=c)
            abonos = Abono.objects.filter(cliente=c)

            data = list(chain(facturas, abonos, notasdeb))
        if data or len(data) != 0:
            report = self.report_class(queryset=data)
            report.title = self.title
            resp = HttpResponse(mimetype='application/pdf')
            report.generate_by(PDFGenerator,variables={'cliente':c.casillero}, filename=resp)
            return resp
        else:
            raise Http404
            #return redirect('')

class BloqueoView( ExportedReport, FormView ):
    form_class = BloqueoForm
    title = 'Reporte de Cuentas Bloqueadas'
    template_name = 'crm/bloqueos.html'
    required_permissions = ('reportes.clientes',)


    def form_valid( self, form ):
        bloqueados = form.cleaned_data['bloqueados']
        data = BloqueoCuenta.objects.all()
        if bloqueados == True:
            data = data.filter(bloquear=True)
        context = self.get_context_data()
        context.update({
                'form':form,
                'data':data,
        })
        return self.render_to_response(context, data)



class MailEstadoView( ExportedReport, FormView ):
    form_class = EstadoForm
    report_class = MailEstadoCuentaReport
    title = 'Estado de Cuenta'
    template_name = 'crm/cuenta.html'
    required_permissions = ('reportes.estadocuenta',)
    
    def render_to_response(self, context, **response_kwargs):
        if self.request.GET.get('id'):
            cod = self.request.GET.get('id')
            correo = EmailContacto.objects.get(contacto=Cliente.objects.filter(pk=cod))

            c = Cliente.objects.get(pk=cod)
            facturas = Factura.objects.filter(cliente=c)
            notasdeb = NotaDebito.objects.filter(cliente=c)
            abonos = Abono.objects.filter(cliente=c)

            data = list(chain(facturas, abonos, notasdeb))
        if data is not None or len(data) != 0:
            report = self.report_class(queryset=data)
            report.generate_by(PDFGenerator,variables={'cliente':c.casillero}, filename=os.path.join(PROJECT_DIR,'media/estados/estado-cuenta.pdf'))
            cuerpo = " ESTIMADO CLIENTE: \n ADJUNTO DOCUMENTOS QUE SOPORTAN SUS TRAMITES EFECTUADOS DURANTE EL TRANSCURSO DEL MES: FACTURAS, RECIBOS OFICIALES DE CAJA, NOTAS DE DEBITO, Y NOTAS DE CREDITO. CUALQUIER CONSULTA NO DUDE EN LLAMARNOS AL PBX 2222-2270 AL DEPARTAMENTO DE COBROS EXTENSION #118 \n VISITENOS EN WWW.TRANSEXPRESS.COM.NI"
            message = EmailMessage(
                subject='Estado de Cuenta ',
                body = cuerpo,
                to=[correo.email],
            )
            message.attach_file(os.path.join(PROJECT_DIR,'media/estados/estado-cuenta.pdf'))
            message.send()
            return redirect('/admin/crm/cliente')

