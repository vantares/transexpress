# -*- coding: UTF-8 -*- 
from django.db import models
from thumbs import *
#===========================================
# Parametros de Uso Global
#===========================================
class Parametro( models.Model ):
    parametro = models.CharField(db_column="parametro", null=True, blank=True, max_length=300, verbose_name="parametro")
    valor = models.CharField(db_column="valor", null=False, blank=False, max_length=500, verbose_name="valor")

    def __unicode__( self ):
        return self.parametro

#===========================================
# Rutas / Zonas para los Mensajeros
#===========================================
class Ruta( models.Model ):
    zona = models.CharField(db_column="zona", null=False, blank=False, max_length=300, verbose_name="zona")
    ruta = models.TextField(db_column="ruta", null=False, blank=False, max_length=300, verbose_name="ruta")

    def __unicode__( self ):
        return self.zona

#==========================
# Informacion de Contacto
#==========================
class Contacto( models.Model ):
    ESTADO_CIVIL=(
        ('soltero','Soltero/a'),
        ('casado','Casado/a'),
        ('divorciado','Divorciado/a'),
        ('union',u'Unión Libre'),
        ('viudo','Viudo/a'),
        ('separado','Separado/a'),
    )
    cedula = models.CharField(db_column="cedula", null=True, blank=True, max_length=16, verbose_name=u"cédula")
    licencia = models.CharField(db_column="licencia", null=True, blank=True, max_length=20, verbose_name="licencia")
    nombre = models.CharField(db_column="nombre", null=False, blank=False, max_length=300, verbose_name="nombre")
    apellido = models.CharField(db_column="apellido", max_length=300, verbose_name="apellido")
    nombrecorto = models.CharField(db_column="nombrecorto", null=True, blank=True, max_length=300, verbose_name="nombre corto")
    foto = ImageField(db_column="foto",upload_to='contacto', null=True, blank=True, verbose_name="foto")
    estadocivil = models.CharField(db_column="estadocivil", choices=ESTADO_CIVIL, null=True, blank=True, max_length=20, verbose_name="estado civil")
    nacimiento = models.DateField(db_column="nacimiento", null=True, blank=True, verbose_name="fecha de nacimiento")
    title = models.CharField(db_column="title", null=True, blank=True, max_length=300, verbose_name=u"título")
    logo = ImageField(db_column="logo",upload_to='contacto', null=True, blank=True, verbose_name="logo")
    categoria = models.CharField(db_column="categoria", null=True, blank=True, max_length=300, verbose_name="categoría")
    notas = models.TextField(db_column="notas", null=True, blank=True, verbose_name="notas")
    
    class Meta:
        verbose_name = "contacto"
        verbose_name_plural = "contactos"

    @property
    def nombrecompleto( self ):
        return self.nombre + ' ' + self.apellido

    def __unicode__( self ):
        return self.nombrecompleto
        
#==========================================
# Cliente
#==========================================
class TipoCliente( models.Model ):
    nombre = models.CharField(db_column="nombre", null=False, blank=False, max_length=50, verbose_name="nombre tipo")

    class Meta:
        verbose_name = "tipo de cliente"
        verbose_name_plural = "tipos de cliente"

    def __unicode__( self ):
        return self.nombre

class TipoCobro( models.Model ):
    nombre = models.CharField(max_length=50, verbose_name="nombre tipo")
    meses = models.IntegerField(verbose_name='Meses')
    
    def __unicode__( self ):
        return self.nombre
    
class Cliente( Contacto ):
    CLASIFICACION=(
        ('1','Natural'),
        ('2','Organizacion'),
    )
    codigo = models.IntegerField(blank=True)
    url = models.URLField(db_column="url", null=True, blank=True, max_length=100)
    ruc = models.CharField(db_column="ruc", null=True, blank=True, max_length=50, verbose_name="RUC")
    organization = models.CharField(db_column="organization", null=True, blank=True, max_length=35, default='N/A', verbose_name="organización")
    tipo = models.ForeignKey(TipoCliente, blank=True, null=True, verbose_name="Tipo de Cliente")
    tipocobro = models.ForeignKey(TipoCobro, blank=True, null=True, verbose_name="Tipo de Cobro", help_text='Aplicable solo para clientes con membresia')
    clasificacion = models.CharField(db_column="clasificacion", choices=CLASIFICACION, null=False,blank=False, max_length=20, verbose_name="clasificacion")
    activo = models.BooleanField(db_column="activo", null=False, blank=False, default=False, verbose_name="activo")
    exonerado = models.BooleanField(db_column="exonerado", null=False, blank=False, default=False, verbose_name="exonerado")
    bloqueado = models.BooleanField(null=False, blank=False, default=False, verbose_name="Bloquear Cuenta")
    zona = models.ForeignKey(Ruta, blank=True, null=True, verbose_name="Zona") 

    def save(self, *args, **kwargs):
        if self.id is None:
            self.codigo = Cliente.objects.latest('codigo').codigo + 1
        super(Cliente, self).save(*args, **kwargs)

    @property
    def js_info(self):
        return self.codigo,self.clientecompleto

    class Meta:
        permissions = (
            ('reportes','Puede ver reportes de cliente'),
        )
        verbose_name = "cliente Browse"
        verbose_name_plural = "clientes Browse"

    def __unicode__( self ):
        return self.organization if self.organization else self.nombrecompleto
    
    @property
    def clientecompleto(self):
        return self.organization if self.organization else self.nombrecompleto

    @property
    def casillero(self):
        return self.casillero_set.get()

    @property
    def direccion(self):
        try:
            dir = DireccionContacto.objects.filter(contacto=self)[0]
            return dir.direccion
        except:
            return u" "
    
    @property
    def ciudad(self):
        try:
            dir = DireccionContacto.objects.filter(contacto=self)[0]
            return dir.ciudad + ', ' + dir.departamento
        except:
            return u" "        

    @property
    def city(self):
        try:
            dir = DireccionContacto.objects.filter(contacto=self)[0]
            return dir.ciudad
        except:
            return u" "        

    @property
    def departamento(self):
        try:
            dir = DireccionContacto.objects.filter(contacto=self)[0]
            return dir.departamento
        except:
            return u" "        

    @property
    def telefono(self):
        try:
            tel = TelefonoContacto.objects.filter(contacto=self)[0]
            return tel.telefono
        except:
            return u" "        
    
    @property
    def fax(self):
        try:
            tel = TelefonoContacto.objects.filter(contacto=self)[1]
            return tel.telefono
        except:
            return u" "        
    
    @property
    def email(self):
        try:
            ema = EmailContacto.objects.filter(contacto=self)[0]
            return ema.email
        except:
            return u" "        
        
    @property
    def requerimientos(self):
        from facturacion.models import Requerimiento
        return Requerimiento.objects.filter(cliente=self)
    
    @property
    def requerimientos_pendientes(self):
        return self.requerimientos.filter(factura__isnull=True)

class ClienteRead( Cliente ):
    class Meta:
        verbose_name = "cliente"
        verbose_name_plural = "clientes"
        proxy = True

class ClientePotencial( Contacto ):
    ultimaactualizacion = models.DateField(db_column="ultimaactualizacion", null=True, blank=True, verbose_name="ultima actualizacion")
    oportunidadtipo = models.CharField(db_column="oportunidad", null=True, blank=True, max_length=20, verbose_name="oportunidad") 
    probabilidad = models.BooleanField(db_column="probabilidad", null=False, blank=False, default=False, help_text="Marque si el cliente esta dispuesto a contratar los servicios", verbose_name="probabilidad")

    class Meta:
        verbose_name = "cliente potencial"
        verbose_name_plural = "clientes potenciales"

    def __unicode__( self ):
        return unicode(self.nombrecompleto )

class TipoTarjetaCliente( models.Model ):
    tipo = models.CharField(db_column="tipo", null=True, blank=True, max_length=30, verbose_name="Tipo de Tarjeta")    
    
    def __unicode__( self ):
        return self.tipo

class TarjetaCliente( models.Model ):
    cliente = models.ForeignKey(Cliente, blank=True, null=True, verbose_name="cliente")
    tipo = models.ForeignKey(TipoTarjetaCliente, blank=True, null=True, verbose_name="Tipo")
    tarjeta = models.CharField(db_column="tarjeta", null=True, blank=True, max_length=16, verbose_name="Numero de Tarjeta")
    vence = models.CharField(null=True, blank=True, max_length=10, verbose_name="Fecha de Vencimiento")
    banco = models.CharField(null=True, blank=True, max_length=50, verbose_name="Banco")
    debitoa = models.BooleanField(db_column="debitoa", null=False, blank=False, default=False, verbose_name="debito automatico")
    
    def __unicode__( self ):
        return ( self.tarjeta )

class DocumentoCliente( models.Model ):
    cliente = models.ForeignKey(Cliente, blank=True, null=True, verbose_name="cliente")
    titulo = models.CharField(db_column="titulo", null=False, blank=False, max_length=20, verbose_name=u"título")
    archivo = models.FileField(db_column="documento", upload_to = 'documents', null=False, blank=False, verbose_name="Archivo de documento")

    class Meta:
        verbose_name = "documento"
        verbose_name_plural = "documentos"

    def __unicode__( self ):
        return self.titulo

class TipoDireccion( models.Model ):
    tipo = models.CharField(db_column="tipo", null=False, blank=False, max_length=50, verbose_name="tipo")
    
    class Meta:
        verbose_name_plural = 'Tipos de Direcciones'

    def __unicode__( self ):
        return self.tipo

class DireccionContacto( models.Model ):
    tipo = models.ForeignKey(TipoDireccion, blank=True, null=True, verbose_name="Tipo")
    direccion = models.CharField(db_column="direccion", null=False, blank=False, max_length=300, verbose_name=u"dirección")
    ciudad = models.CharField(null=False, blank=False, max_length=300, verbose_name="ciudad")
    departamento = models.CharField(null=False, blank=False, max_length=300, verbose_name="departamento / estado")
    contacto = models.ForeignKey(Cliente, blank=True, null=True, verbose_name="contacto")
  
    class Meta:
        verbose_name = u"dirección"
        verbose_name_plural = "direcciones"
 
    def __unicode__( self ):
        return self.direccion

class TipoTelefono( models.Model ):
    tipo = models.CharField(db_column="tipo", null=True, blank=True, max_length=30, verbose_name="tipo")
    
    def __unicode__( self ):
        return self.tipo

class TelefonoContacto( models.Model ):
    tipo = models.ForeignKey(TipoTelefono, blank=True, null=True, verbose_name="Tipo")
    telefono = models.CharField(db_column="telefono", null=False, blank=False, max_length=50, verbose_name=u"teléfono")
    contacto = models.ForeignKey(Cliente, blank=True, null=True, verbose_name="contacto")

    class Meta:
        verbose_name = u"teléfono"
        verbose_name_plural = u"teléfonos"

    def __unicode__( self ):
        return self.telefono

class TipoEmail( models.Model ):
    tipo = models.CharField(db_column="tipo", null=False, blank=False, max_length=50, verbose_name="tipo")
    
    def __unicode__( self ):
        return self.tipo

class EmailContacto( models.Model ):
    tipo = models.ForeignKey(TipoEmail, blank=True, null=True, verbose_name="Tipo")
    email = models.EmailField(db_column="email",null=False, blank=False, max_length=100, verbose_name="email" )    
    contacto = models.ForeignKey(Cliente, blank=True, null=True, verbose_name="contacto")

    class Meta:
        verbose_name = "email"
        verbose_name_plural = "email"

    def __unicode__( self ):
        return self.email 
