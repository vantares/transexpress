from django.conf.urls.defaults import patterns, url
from crm.views import *

urlpatterns = patterns('crm.views',
    url(r'^reportes/$', ClientesView.as_view(), name='clientes-report'),
    url(r'^estado/$', EstadoView.as_view(), name='estadocuenta'),
    url(r'^mailestado/$', MailEstadoView.as_view(), name='mailestadocuenta'),
    url(r'^bloqueo/$', BloqueoView.as_view(), name='bloqueo-report'),
)
