# -*- coding: UTF-8 -*-
from django.contrib import admin
from django.db import models
from django.conf import settings
from read_only import ReadOnlyAdmin
from crm.models import *
from postal.models import Casillero, PersonaAutorizada
from mptt.admin import MPTTModelAdmin

#================================================
# Inlines
#================================================
class CasilleroInline( admin.StackedInline ):
    model = Casillero
    extra = 0
    template = 'postal/stacked.html'

class CasilleroRO( ReadOnlyAdmin, admin.StackedInline):
    model = Casillero
    extra = 0
    template = 'postal/stackedread.html'

class DireccionInline( admin.TabularInline ):
    model = DireccionContacto
    extra = 1

class DireccionRO( ReadOnlyAdmin, admin.TabularInline ):
    model = DireccionContacto
    extra = 0

class TelefonoInline( admin.TabularInline ):
    model = TelefonoContacto
    extra = 1

class TelefonoRO( ReadOnlyAdmin, admin.TabularInline ):
    model = TelefonoContacto
    extra = 0

class EmailInline( admin.TabularInline ):
    model = EmailContacto
    extra = 1

class EmailRO( ReadOnlyAdmin, admin.TabularInline ):
    model = EmailContacto
    extra = 0

class DocumentoClienteInline( admin.TabularInline ):
    model = DocumentoCliente
    extra = 1

class DocumentoRO( ReadOnlyAdmin, admin.TabularInline ):
    model = DocumentoCliente
    extra = 0

class TarjetaClienteInline( admin.TabularInline ):
    model = TarjetaCliente
    extra = 1

class TarjetaRO( ReadOnlyAdmin, admin.TabularInline):
    model = TarjetaCliente
    extra = 0

#================================================
# Admin
#================================================
class ClienteAdmin( admin.ModelAdmin ):
    search_fields = ['codigo', 'nombre', 'apellido', 'organization', 'casillero__codigonicabox']
    exclude = ('licencia','nombrecorto','foto','logo','url')
    list_filter = ('activo','tipo','zona','clasificacion', 'tipocobro')
    list_display = ['codigo', 'clientecompleto', 'casillero', 'activo']
    list_display_links = ('codigo', 'clientecompleto')
    list_per_page = 500
    inlines = [TarjetaClienteInline, CasilleroInline, DireccionInline, TelefonoInline, EmailInline, DocumentoClienteInline]

class ClientePotencialAdmin( admin.ModelAdmin ):
    list_filter = ['probabilidad']

class ParametroAdmin( admin.ModelAdmin ):
    list_display = ['parametro', 'valor']

class DireccionAdmin( admin.ModelAdmin):
    list_display = ['direccion', 'contacto']
    search_fields = ['direccion', 'contacto__nombre', 'contacto__casillero__codigonicabox']

class TelefonoAdmin( admin.ModelAdmin):
    list_display = ['telefono', 'contacto']
    search_fields = [ 'contacto__nombre', 'contacto__casillero__codigonicabox']

class EmailAdmin( admin.ModelAdmin):
    list_display = ['email', 'contacto']
    search_fields = ['email', 'contacto__nombre', 'contacto__casillero__codigonicabox']

class ContactoAdmin( admin.ModelAdmin ):
    list_display = ['id', 'nombre', 'apellido', ]
    search_fields = ['nombre','apellido']

#================================================
# Read Only Admin
#================================================
class ClienteRO( ReadOnlyAdmin, admin.ModelAdmin ):
    search_fields = ['cedula',]
    exclude = ('licencia','nombrecorto','foto','logo','url')
    list_display = ['clientecompleto', 'zona', 'activo']
    inlines = [TarjetaRO, CasilleroRO, DireccionRO, TelefonoRO, EmailRO, DocumentoRO]

#    def queryset(self, request):
#        if request.user.is_staff:
#            return Cliente.objects.filter(pk=request.user.username)

class RutaAdmin(admin.ModelAdmin):
    list_display = ['zona','ruta']

admin.site.register(Ruta, RutaAdmin)
admin.site.register(TipoCliente)
admin.site.register(TipoDireccion)
admin.site.register(TipoTarjetaCliente)
admin.site.register(TipoEmail)
admin.site.register(TipoTelefono)
admin.site.register(Cliente, ClienteAdmin)
admin.site.register(ClientePotencial, ClientePotencialAdmin)
admin.site.register(Parametro, ParametroAdmin)
admin.site.register(TipoCobro)
admin.site.register(ClienteRead, ClienteRO)
admin.site.register(DireccionContacto, DireccionAdmin)
admin.site.register(TelefonoContacto, TelefonoAdmin)
admin.site.register(EmailContacto, EmailAdmin)
admin.site.register(Contacto, ContactoAdmin)
