# -*- coding: utf-8 -*-
from django import forms
from nomina.models import Departamento, Nomina, Empleado
from django.contrib.admin import widgets

class EmpleadosForm( forms.Form ):
    departamento = forms.ModelChoiceField(queryset = Departamento.objects.all(), required=False)

class PlanillaForm( EmpleadosForm ):
    start_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Desde', help_text='Primera Quincena: 01/mm/yyyy<br /> Segunda Quincena: 16/mm/yyy')
    end_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Hasta')

class HorasForm( forms.Form ):
    start_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Desde')
    end_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Hasta')

class ReciboForm( EmpleadosForm ):
    empleado = forms.ModelChoiceField(queryset = Empleado.objects.all(), required=False)
    concepto = forms.ModelChoiceField(queryset = Nomina.objects.all() )
    start_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Del')
    end_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Al')
