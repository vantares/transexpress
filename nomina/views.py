# -*- coding: UTF-8 -*-
from django.shortcuts import render_to_response, get_object_or_404
from django.views.generic import FormView
from django.db import connection
from nomina.models import *
from nomina.forms import *
from nomina.reports import *
from crm.models import Parametro
from reportes.views import ExportedReport
import simplejson
from django.http import HttpResponse

def obtener_empleado(request):
    try:    
        if 'id' in request.GET:
            id = int(request.GET['id'])    
            empleado = Empleado.objects.get(pk = id)    
            json = simplejson.dumps([ (empleado.codigoempleado,empleado.nombrecompleto) ])            
        else:
            codigo = request.GET['codigo']    
            empleado = Empleado.objects.get(codigoempleado = codigo)    
            json = simplejson.dumps([ (empleado.id, empleado.nombrecompleto) ])
    except:
        json = simplejson.dumps([ ('','No definido') ])
    return HttpResponse(json)

QUINCENAS = [
    ( 7, 1, 1, 24),
    ( 7, 16, 2, 23),
    ( 8, 1, 3, 22),
    ( 8, 16, 4, 21),
    ( 9, 1, 5, 20),
    ( 9, 16, 6, 19),
    ( 10, 1, 7, 18),
    ( 10, 16, 8, 17),
    ( 11, 1, 9, 16),
    ( 11, 16, 10, 15),
    ( 12, 1, 11, 14),
    ( 12, 16, 12, 13),
    ( 1, 1, 13, 12),
    ( 1, 16, 14, 11),
    ( 2, 1, 15, 10),
    ( 2, 16, 16, 9),
    ( 3, 1, 17, 8),
    ( 3, 16, 18, 7),
    ( 4, 1, 19, 6),
    ( 4, 16, 20, 5),
    ( 5, 1, 21, 4),
    ( 5, 16, 22, 3),
    ( 6, 1, 23, 2),
    ( 6, 16, 24, 1),
]

#======================================================================================================
class EmpleadosView( ExportedReport, FormView ):
    form_class = EmpleadosForm
    title = 'Reporte de Empleados'
    template_name = 'nomina/empleados.html'
    report_class = EmpleadosReport
    required_permissions = ('reportes.empleados',)
    xls_titles = ['Nombre', 'Apellido', 'Codigo', 'Cargo','Sueldo']

    def form_valid( self, form ):
        departamento = form.cleaned_data['departamento']
        data = Empleado.objects.all()
        data2 = data.values_list('nombre', 'apellido', 'codigoempleado', 'cargo','sueldo')

        if departamento <> None:
            nom = Departamento.objects.get(nombre=departamento)
            data = nom.trabajadores.all()
            data2 = data.values_list('nombre', 'apellido', 'codigoempleado', 'cargo','sueldo')
        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context, data2)
#======================================================================================================
class HorasView( ExportedReport, FormView ):
    form_class = HorasForm
    title = 'Reporte de Horas Extras'
    template_name = 'nomina/horas.html'
    required_permissions = ('reportes.horasextras',)

    def form_valid( self, form ):
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']

        cursor = connection.cursor()
        cursor.execute("""
        SELECT
        nomina_empleado.codigo as codigo,
        crm_contacto.nombre as nombre,
        crm_contacto.apellido as apellido,
        SUM(nomina_horasextras.ext) as total_horas
        FROM
        nomina_horasextras,
        nomina_empleado,
        crm_contacto
        WHERE
        nomina_empleado.contacto_ptr_id = nomina_horasextras.empleado_id AND
        crm_contacto.id = nomina_empleado.contacto_ptr_id
        GROUP BY codigo, nombre, apellido
        ORDER BY nombre
        """)
        data = self.dictfetchall(cursor)

        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
            'inicio':start_date,
            'fin':end_date,
        })
        return self.render_to_response(context, data)

#======================================================================================================
def calculo_percepcion( t, start_date, end_date ):
    quincenal = sueldo_actual( t, end_date )[0]
    monto = PercepcionesAplicables.objects.filter(empleado=t.id, fecha__range=[start_date,end_date]).aggregate(Sum('monto'))
    percent = PercepcionesAplicables.objects.filter(empleado=t.id, fecha__range=[start_date,end_date]).aggregate(Sum('percent'))
    horas = PercepcionesAplicables.objects.filter(empleado=t.id, fecha__range=[start_date,end_date], tipo=3).aggregate(Sum('monto'))
    try:
        pmonto = Decimal(monto.get('monto__sum') + (quincenal*percent.get('percent__sum')/100)).quantize(Decimal('1.00') ) 
        pmonto = pmonto if pmonto is not None else Decimal(0)
    except:
        pmonto = Decimal(0)
    return pmonto

def calculo_deduccion( t, start_date, end_date ):
    quincenal = sueldo_actual( t, end_date )[0]
    montod = DeduccionesAplicables.objects.filter(empleado=t.id, fecha__range=[start_date,end_date]).aggregate(Sum('monto'))
    percentd = DeduccionesAplicables.objects.filter(empleado=t.id, fecha__range=[start_date,end_date]).aggregate(Sum('percent'))
    try:
        dmonto = Decimal(montod.get('monto__sum') + (quincenal*percentd.get('percent__sum')/100)).quantize(Decimal('1.00') ) 
        dmonto = dmonto if dmonto is not None else Decimal(0)
    except:
        dmonto = Decimal(0)
    return dmonto    

def calculo_inss( totaldevengo, id_tipo=8 ):
    tasa = Parametro.objects.get(pk=id_tipo)
    inss = Decimal(totaldevengo * Decimal(tasa.valor)).quantize(Decimal('1.00') )
    return inss

def percenta( t, end_date ): 
    today = end_date
    anio  = 0
    try:
        f = t.fecha.replace(year=today.year)
    except ValueError:
        f = t.fecha.replace(year=today.year, day=t.fecha.day-1)
    if f > today:
        anio = (today.year - t.fecha.year - 1)
    else:
        anio = (today.year - t.fecha.year)

    if anio >= 1 and anio <=20:
        p = Antiguedad.objects.get(years=anio)
        porcentaje = Decimal(p.porcentaje)
    if anio > 20:
        p = Antiguedad.objects.get(years=20)
        porcentaje = Decimal (p.porcentaje)
    if anio <= 0:
        porcentaje = 0
    return porcentaje

def valor( t, porcentaje, end_date ):
    quincenal = sueldo_actual( t, end_date )[0]
    return Decimal(quincenal * porcentaje / 100).quantize(Decimal('1.00'), ROUND_HALF_UP)

MES30 = [ 4, 6, 9, 11]
MES31 = [ 1, 3, 5, 7, 8, 10, 12]
def acumulado(t, start_date, end_date, salario, quincena):
    anio = start_date.year
    if quincena == 12:
        anio = start_date.year - 1  
    for q in QUINCENAS:
        if q[2] == quincena: #Encontrar la quincena actual
            nsd = start_date.replace(month=q[0], day=q[1], year=anio) #Nueva fecha de inicio para la quincena anterior
            if quincena % 2 == 0: #En caso que la quincena anterior sea >>Segunda Quincena, encontrar fecha de fin de mes
                if nsd.month in MES31:
                    mes = 31
                elif nsd.month in MES30:
                    mes = 30
                elif anio % 4 == 0:
                    mes = 29
                else:
                    mes = 28
            else:
                mes = 15 
            ned = end_date.replace(month=q[0], day=mes, year=anio) #Nueva fecha de fin para la quincena anterior
    if quincena >= 1: #Se comienza a calcular el acumulado de las correpondientes quincenas segun las fechas calculadas anteriormente
        #Antiguedad
        porcentaje = percenta( t, ned )
        antiguedad = valor( t, porcentaje, ned )
        #Total devengado de la quincena
        devengado = calculo_totaldevengo(t, nsd, ned, antiguedad)
        #Inss de la quincena actual
        ninss = calculo_inss( devengado )
        #Salario
        salario = devengado - ninss
        #Funcion recursiva para calcular el total de ingresos acumulados 
        ingresos = acumulado( t, nsd, ned, salario, quincena-1) + salario
        return ingresos 
    else: #Condicion de parada de recursion
        return 0

def calculo_irq( t, totaldevengo, inss, start_date, end_date , quincena):
    anio = start_date.year
    if quincena == 12:
        anio = start_date.year - 1  
    for q in QUINCENAS:
        if q[2] == quincena: #Encontrar la quincena actual
            quincena_falta = q[3]
            nsd = start_date.replace(month=q[0], day=q[1], year=anio) #Nueva fecha de inicio para la quincena anterior
            if quincena % 2 == 0: #En caso que la quincena anterior sea >>Segunda Quincena, encontrar fecha de fin de mes
                if nsd.month in MES31:
                    mes = 31
                elif nsd.month in MES30:
                    mes = 30
                elif anio % 4 == 0:
                    mes = 29
                else:
                    mes = 28
            else:
                mes = 15 
            ned = end_date.replace(month=q[0], day=mes, year=anio) #Nueva fecha de fin para la quincena anterior
    #Encontrar salario de quincena actual con inss actual
    salario = totaldevengo - inss
    #Ingresos de quincena actual 
    ingresos = acumulado( t, start_date, end_date, salario, quincena )
    expectativa = (ingresos / quincena ) * 24
    ira = calculo_iranual(expectativa)
    if quincena > 1:
        retenido = calculo_irq(t, totaldevengo, inss, nsd, ned, quincena-1)
        ir_faltante = ira - retenido[1]
        ir = Decimal(ir_faltante / quincena_falta).quantize(Decimal('1.00'))
        total = ir + retenido[1]
    else: 
        retenido = Decimal(0)    
        ir_faltante = ira - retenido
        ir = Decimal(ir_faltante / quincena_falta).quantize(Decimal('1.00'))
        total = ir + retenido
    return ir, total

def calculo_iranual( expectativa ):
    excesos =  IR.objects.values_list('desde', 'hasta', 'exceso','porcentaje','base')
    for ex in excesos:
        ira = Decimal(0)
        if expectativa >= ex[0] and expectativa <= ex[1]: 
            menos_exceso = Decimal(expectativa - ex[2])             #Menos el exceso segun tabla
            por_porcentaje = Decimal( menos_exceso * ex[3] ) / 100  #Porcentaje aplicable
            ira = Decimal( por_porcentaje + ex[4] ).quantize(Decimal('1.00'))                 #Mas base
            break
        else:
            if expectativa >= 500001:
                menos_exceso = Decimal(expectativa - ex[2])             #Menos el exceso segun tabla
                por_porcentaje = Decimal( menos_exceso * ex[3] ) / 100  #Porcentaje aplicable
                ira = Decimal( por_porcentaje + ex[4] ).quantize(Decimal('1.00'))                 #Mas base
    ira = ira if ira is not None else Decimal(0)
    return ira
    
def calculo_totaldevengo( t, start_date, end_date, antiguedad ):
    quincenal = sueldo_actual( t, end_date )[0]
    #Percepciones
    pmonto = calculo_percepcion( t, start_date, end_date )
    #Total de quincena mas antiguedad mas percepciones
    totaldevengo = quincenal + antiguedad + pmonto
    return totaldevengo

def sueldo_actual( t, end_date ):
    salarios = Salario.objects.filter(empleado=t)
    sf = salarios.filter(fecha__lte=end_date) if salarios else []
    ultimo = sf.latest('fecha') if sf else None
    sueldo = ultimo.salario if ultimo is not None else Decimal(0)
    quincenal =  Decimal(sueldo / 2).quantize(Decimal('1.00'), ROUND_HALF_UP)
    return quincenal, sueldo

#======================================================================================================
class PlanillaView( ExportedReport, FormView ):
    form_class = PlanillaForm
    report_class = PlanillaReport
    title = 'Reporte de Planilla de sueldo'
    template_name ='nomina/planilla.html'
    required_permissions = ('reportes.planilla',)
    csv_titles = ['CODIGO','NOMBRE COMPLETO','SUELDO MENSUAL','SUELDO QUINCENAL','PORCENTAJE ANTIGUEDAD','VALOR ANTIGUEDAD','HORAS EXTRAS','OTROS','TOTAL DEVENGADO','INSS','IR','PRESTAMO','OTROS DESCUENTOS','TOTAL DESCUENTOS','LIQUIDO','SALDO PRESTAMO']
    csv_ordering = ['codigoempleado','nombrecompleto','sueldo','quincenal','antiguedad','valor','horas','pmonto','devengo','inss','ir','prestamo','dmonto','descuento','liquido','saldo']
    

    def form_valid( self, form ):
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']
        departamento = form.cleaned_data['departamento']
        self.description = str(start_date)+' al '+str(end_date)
        data = Empleado.objects.all()
        if departamento <> None:
            dep = Departamento.objects.get(nombre=departamento)
            tra = dep.trabajadores.all().order_by('codigoempleado')
        else:
            tra = Empleado.objects.all().order_by('codigoempleado')
            departamento = ''
        data = []
        #Encontrar la quincena actual            
        for q in QUINCENAS:
            if start_date.day == q[1] and start_date.month == q[0]:
                quincena = q[2]
        for t in tra:
            quincenal = sueldo_actual( t, end_date )[0]
            sueldo = sueldo_actual(t, end_date )[1]
            #Percepciones
            pmonto = calculo_percepcion( t, start_date, end_date )

            #Antiguedad
            porcentaje = percenta( t, end_date )
            antiguedad = valor( t, porcentaje, end_date )

            #Total de quincena mas antiguedad mas percepciones
            totaldevengo = quincenal + antiguedad + pmonto
            
            # Inss Laboral
            inss = calculo_inss( totaldevengo )

            #Abonos de Prestamo
            try:
                s, monto, montoabono = 0, 0, 0
                prestamo = Prestamo.objects.filter(empleado=t.id, cancelado=False, fecha__lte=end_date)
                monto = Prestamo.objects.filter(empleado=t.id, cancelado=False, fecha__lte=end_date).aggregate(Sum('monto'))['monto__sum']
                monto = monto if monto is not None else Decimal(0)
                for p in prestamo:
                    cuota = Cuota.objects.filter(prestamo=p, fecha__lte=end_date).aggregate(Sum('monto'))['monto__sum']
                    cuota2 = Cuota.objects.filter(prestamo=p, fecha__range=[start_date,end_date]).aggregate(Sum('monto'))['monto__sum']
                    s += cuota if cuota is not None else Decimal(0)
                    montoabono += cuota2 if cuota2 is not None else Decimal(0)
                saldo = monto - Decimal(s)
            except TypeError:
                prestamo, abonos = [], []
                montoabono, saldo = Decimal(0), Decimal(0)

            #Deducciones
            dmonto = calculo_deduccion( t, start_date, end_date )
            
            #Calculo de IR Quincenal
            ir = calculo_irq( t, totaldevengo, inss , start_date, end_date, quincena)[0]

            totaldescuento = inss + ir + dmonto + montoabono
            liquido = totaldevengo-totaldescuento
            data.append({
            'codigoempleado':t.codigoempleado,
            'nombrecompleto':t.nombrecompleto,
            'sueldo':sueldo,
            'quincenal':quincenal,
            'antiguedad':porcentaje,
            'valor':antiguedad,
            'inss':inss,
            'prestamo':montoabono,
            'saldo':saldo,
            'pmonto':Decimal(pmonto), 
            'devengo':totaldevengo,
            'dmonto':dmonto, 
            'descuento':totaldescuento,
            'ir':ir,
            'liquido':liquido,
            #'horas':horas
            })

        tqui, tant, tper, tdev, tins, timp, tpre, tdes, ttds, tliq = 0,0,0,0,0,0,0,0,0,0
        totales = []
        for i in data:
            tqui += i.get('quincenal')
            tant += i.get('valor')
            tper += i.get('pmonto')
            tdev += i.get('devengo')
            tins += i.get('inss')
            timp += i.get('ir')
            tpre += i.get('prestamo')
            tdes += i.get('dmonto')
            ttds += i.get('descuento')
            tliq += i.get('liquido')
        totales.append({'quincenales':tqui, 'antiguedades':tant,'percepciones':tper,'devengos':tdev,'inss':tins,'irs':timp,'prestamos':tpre,'deducciones':tdes,'descuentos':ttds,'liquidos':tliq})
        self.nombre = departamento
        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
            'departamento': departamento,
            'inicio': start_date,
            'fin': end_date,
            'totales':totales,
        })
        return self.render_to_response(context, data)
#======================================================================================================
class IrView( ExportedReport, FormView ):
    form_class = HorasForm
    title = 'Reporte de IR'
    template_name = 'nomina/ir.html'
    report_class = IrReport
    required_permissions = ('reportes.ir',)
    csv_titles = ['CODIGO', 'NOMBRE COMPLETO', 'SUELDO ACUMULADO', 'IR']
    csv_ordering = ['codigoempleado','nombrecompleto','acumulado','ir']
    def form_valid( self, form ):
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']
        tra = Empleado.objects.all()
        data = []
        start_date1 = start_date
        end_date1 = start_date.replace(day=15)
        
        start_date2 = start_date.replace(day=16)
        end_date2 = end_date
        #Encontrar la primera quincena  
        for q in QUINCENAS:
            if start_date1.day == q[1] and start_date1.month == q[0]:
                quincena1 = q[2]
            if start_date2.day == q[1] and start_date2.month == q[0]:
                quincena2 = q[2]

        for t in tra:
            '''
            Calculos para la primera quincena
            '''
            quincenal1 = sueldo_actual( t, end_date1 )[0]
            #Percepciones
            pmonto = calculo_percepcion( t, start_date1, end_date1 )
            #Antiguedad
            porcentaje = percenta(t, end_date1 )
            antiguedad = valor( t, porcentaje, end_date1 )
            #Total del mes mas antiguedad mas percepciones (Se supone que el rango de fechas que se provee deberia corresponder al mes)
            totaldevengo = quincenal1 + antiguedad + pmonto
            #Inss
            ninss = calculo_inss( totaldevengo )
            #Salario
            salario = totaldevengo - ninss
            #Funcion recursiva para calcular el total de ingresos acumulados 
            ingresos = acumulado( t, start_date1, end_date1, salario, quincena1) 
            #Calculo de IR Quincenal
            a = calculo_irq( t, totaldevengo, ninss , start_date1, end_date1, quincena1)
            ir1 = a[0]
            
            '''
            Calculos para la segunda quincena
            '''
            quincenal2 = sueldo_actual( t, end_date2 )[0]
            #Percepciones
            pmonto2 = calculo_percepcion( t, start_date2, end_date2 )
            #Antiguedad
            porcentaje2 = percenta(t, end_date2 )
            antiguedad2 = valor( t, porcentaje2, end_date2 )
            #Total del mes mas antiguedad mas percepciones (Se supone que el rango de fechas que se provee deberia corresponder al mes)
            totaldevengo2 = quincenal2 + antiguedad2 + pmonto2
            #Inss
            ninss2 = calculo_inss( totaldevengo2 )
            #Salario
            salario2 = totaldevengo2 - ninss2
            #Funcion recursiva para calcular el total de ingresos acumulados 
            ingresos2 = acumulado( t, start_date2, end_date2, salario2, quincena2) 
            #Calculo de IR Quincenal
            a2 = calculo_irq( t, totaldevengo2, ninss2 , start_date2, end_date2, quincena2)
            ir2 = a2[0] 
            
            ir = a2[1] - ir1 - ir2  #Este es el ir total - lo retenido en el mes
            irmes = ir1 + ir2       # ir correspondiente al mes
            total = ir + irmes      # ir total retenido
            
            
            data.append({'id':t.id, 'codigoempleado':t.codigoempleado,'nombrecompleto':t.nombrecompleto, 'acumulado':ingresos, 'ir':total, 'irmes':irmes})
        ta, td, tm = 0, 0, 0
        for i in data:
            ta += i.get('acumulado')
            tm += i.get('irmes')
            td += i.get('ir')
        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
            'ta':ta,
            'td':td,
            'tm':tm,
        })
        return self.render_to_response(context, data)
#======================================================================================================
class InssView( ExportedReport, FormView ):
    form_class = PlanillaForm
    report_class = INSSReport
    title = 'Reporte de Planilla Inss'
    template_name ='nomina/inss.html'
    required_permissions = ('reportes.inss',)
    csv_titles = ['CODIGO','NOMBRE COMPLETO', 'SUELDO MENSUAL', '% ANTIGUEDAD', 'VALOR ANTIGUEDAD', 'OTROS', 'TOTAL DEVENGOS','APORTE LABORAL', 'APORTE PATRONAL', 'APORTE TOTAL']
    csv_ordering = ['codigoempleado', 'nombrecompleto', 'sueldo', 'antiguedad', 'valor', 'pmonto', 'devengo', 'laboral', 'patronal', 'aporte']
    def form_valid( self, form ):
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']
        departamento = form.cleaned_data['departamento']
        
        data = Empleado.objects.all()
        dep = ''
        if departamento <> None:
            dep = Departamento.objects.get(nombre=departamento)
            tra = dep.trabajadores.all()
        else:
            tra = Empleado.objects.all()
        data = []
        for t in tra: 
            primera_antiguedad_fecha = end_date.replace(day=15)
            segunda_antiguedad_fecha = start_date.replace(day=16)
            horas = PercepcionesAplicables.objects.filter(empleado=t.id, fecha__range=[start_date,end_date], tipo=3).aggregate(Sum('monto'))
            percepcionespq = calculo_percepcion( t, start_date, primera_antiguedad_fecha )
            percepcionessq = calculo_percepcion( t, segunda_antiguedad_fecha, end_date )

            quincenal1 = sueldo_actual( t, primera_antiguedad_fecha )[0]
            quincenal2 = sueldo_actual( t, end_date )[0]
            sueldo = sueldo_actual( t, end_date )[1]

            #Antiguedad
            porcentajepq = percenta(t, primera_antiguedad_fecha )
            porcentajesq = percenta(t, end_date )
            antiguedadpq = valor( t, porcentajepq, primera_antiguedad_fecha )
            antiguedadsq = valor(t, porcentajesq, end_date )

           #Total devendgado
            totaldevengopq = quincenal1 + antiguedadpq + percepcionespq
            totaldevengosq = quincenal2 + antiguedadsq + percepcionessq
            totaldevengo = totaldevengopq + totaldevengosq
            laboral = Parametro.objects.get(pk=8)
            patronal = Parametro.objects.get(pk=9)
            inss_laboral = Decimal(totaldevengopq * Decimal(laboral.valor)).quantize(Decimal('1.00') ) + Decimal(totaldevengosq * Decimal(laboral.valor)).quantize(Decimal('1.00') )
            inss_patronal = Decimal(totaldevengopq * Decimal(patronal.valor)).quantize(Decimal('1.00') ) + Decimal(totaldevengosq * Decimal(patronal.valor)).quantize(Decimal('1.00') )
            inss_total = inss_laboral + inss_patronal

            
            data.append({'codigoempleado':t.codigoempleado,'nombrecompleto':t.nombrecompleto,'sueldo':sueldo,
                    'antiguedad':porcentajesq,'valor':antiguedadpq+antiguedadsq,'laboral':inss_laboral,'patronal':inss_patronal, 'aporte':inss_total, 'pmonto':percepcionespq+percepcionessq, 'devengo':totaldevengo,'horas':horas})

        tant, tper, tdev, tlab, tpat, tapo = 0,0,0,0,0,0
        totales = []
        for i in data:
            tant += i.get('valor')
            tper += i.get('pmonto')
            tdev += i.get('devengo')
            tlab += i.get('laboral')
            tpat += i.get('patronal')
            tapo += i.get('patronal')
        totales.append({'antiguedades':tant,'percepciones':tper,'devengos':tdev,'laboral':tlab,'patronal':tpat, 'aporte':tapo})

        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
            'departamento': dep,
            'inicio': start_date,
            'fin': end_date,
            'totales':totales,
        })
        return self.render_to_response(context, data)


#======================================================================================================
class ReciboView(ExportedReport, FormView):
    form_class = ReciboForm
    report_class = ReciboReport
    template_name = 'nomina/recibo.html'
    required_permissions = ('reportes.recibo',)

    def form_valid( self, form ):
        concepto = form.cleaned_data['concepto']
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']
        departamento = form.cleaned_data['departamento']
        empleado = form.cleaned_data['empleado']
        self.title = str(concepto)
        self.description = str(start_date)+' al '+str(end_date)
        if departamento:
            data = departamento.trabajadores.order_by('codigoempleado')
        else:
            data = Empleado.objects.order_by('codigoempleado')
        if empleado:
            data = Empleado.objects.filter(pk=empleado.id)
        
        #Encontrar la quincena actual            
        for q in QUINCENAS:
            if start_date.day == q[1] and start_date.month == q[0]:
                quincena = q[2]
        for empleado in data:
            quincenal = sueldo_actual( empleado, end_date )[0]
            mensual = sueldo_actual( empleado, end_date )[1]

            #Percepciones
            pmonto = calculo_percepcion( empleado, start_date, end_date )
            #Antiguedad
            porcentaje = percenta( empleado, end_date )
            antiguedad = valor( empleado, porcentaje, end_date )
            #Total de quincena mas antiguedad mas percepciones
            totaldevengo = quincenal + antiguedad + pmonto
            # Inss Laboral
            inss = calculo_inss( totaldevengo )
            #Abonos de Prestamo
            try:
                s, monto, montoabono = 0, 0, 0
                prestamo = Prestamo.objects.filter(empleado=empleado, cancelado=False, fecha__lte=end_date)
                monto = Prestamo.objects.filter(empleado=empleado, cancelado=False, fecha__lte=end_date).aggregate(Sum('monto'))['monto__sum']
                monto = monto if monto is not None else Decimal(0)
                for p in prestamo:
                    cuota = Cuota.objects.filter(prestamo=p, fecha__lte=end_date).aggregate(Sum('monto'))['monto__sum']
                    cuota2 = Cuota.objects.filter(prestamo=p, fecha__range=[start_date,end_date]).aggregate(Sum('monto'))['monto__sum']
                    s += cuota if cuota is not None else Decimal(0)
                    montoabono += cuota2 if cuota2 is not None else Decimal(0)
                saldo = monto - Decimal(s)
            except TypeError:
                prestamo, abonos = [], []
                montoabono, saldo = Decimal(0), Decimal(0)

            #Deducciones
            dmonto = calculo_deduccion( empleado, start_date, end_date )            
            #Calculo de IR Quincenal
            ir = calculo_irq( empleado, totaldevengo, inss , start_date, end_date, quincena)[0]

            empleado.ir = ir
            empleado.prestamo = montoabono
            empleado.inss = inss
            empleado.antiguedad = antiguedad
            empleado.porcentaje = porcentaje
            empleado.devengos = totaldevengo
            empleado.descuentos = inss + ir + dmonto + montoabono
            empleado.total = totaldevengo-empleado.descuentos
            empleado.fecha_inicio = start_date
            empleado.fecha_fin = end_date
            empleado.quincenal = quincenal
            empleado.mensual = mensual

        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context, data)

class ComisionView( ExportedReport, FormView ):
    form_class = HorasForm
    title = 'Reporte de Comisiones'
    template_name = 'nomina/comisiones.html'
    required_permissions = ('reportes.comisiones',)

    def form_valid( self, form ):
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']

        trab = Empleado.objects.all()
        data = []
        for t in trab:
            quincenal = sueldo_actual( t, primera_antiguedad_fecha )[0]
            #Percepciones
            monto = PercepcionesAplicables.objects.filter(empleado=t.id, fecha__range=[start_date,end_date], tipo=1).aggregate(Sum('monto'))
            percent = PercepcionesAplicables.objects.filter(empleado=t.id, fecha__range=[start_date,end_date], tipo=1).aggregate(Sum('percent'))
            try:
                pmonto = Decimal(monto.get('monto__sum') + (quincenal*percent.get('percent__sum')/100)).quantize(Decimal('1.00') ) 
                pmonto = pmonto if pmonto is not None else Decimal(0)
            except:
                pmonto = Decimal(0.00)
            data.append({
                'empleado':t.nombrecompleto,
                'comision':pmonto,
            })
        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
            'inicio':start_date,
            'fin':end_date,
        })
        return self.render_to_response(context, data)
