from django.conf.urls.defaults import patterns, url
from nomina.views import *
urlpatterns = patterns('nomina.views',
    url(r'^empleados/$', EmpleadosView.as_view(), name='empleados-report'),
    url(r'^horas/$', HorasView.as_view(), name='horas-report'),

    url(r'^ir/$', IrView.as_view(), name='ir-report'),
    url(r'^planilla/$', PlanillaView.as_view(), name='planilla-report'),
    url(r'^recibo/$', ReciboView.as_view(), name='recibo-report'),
    url(r'^inss/$', InssView.as_view(), name='inss-report'),
    url(r'^comisiones/$', ComisionView.as_view(), name='comisiones'),
    url('^obtener_empleado/$', 'obtener_empleado', name='obtener_empleado'),
)
