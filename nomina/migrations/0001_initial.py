# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Cargo'
        db.create_table('nomina_cargo', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cargo', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='cargo')),
        ))
        db.send_create_signal('nomina', ['Cargo'])

        # Adding model 'Empleado'
        db.create_table('nomina_empleado', (
            ('contacto_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['crm.Contacto'], unique=True, primary_key=True)),
            ('codigoempleado', self.gf('django.db.models.fields.CharField')(max_length=100, db_column='codigo')),
            ('fecha', self.gf('django.db.models.fields.DateField')(null=True, db_column='fecha', blank=True)),
            ('cargo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nomina.Cargo'])),
            ('sueldo', self.gf('django.db.models.fields.DecimalField')(db_column='sueldo', decimal_places=2, max_digits=10)),
            ('idseguro', self.gf('django.db.models.fields.CharField')(max_length=100, db_column='seguro')),
            ('activo', self.gf('django.db.models.fields.BooleanField')(default=False, db_column='activo')),
            ('padre', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, db_column='padre', blank=True)),
            ('madre', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, db_column='madre', blank=True)),
            ('numerohijos', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='hijos', blank=True)),
            ('comisionista', self.gf('django.db.models.fields.BooleanField')(default=False, db_column='comisionista')),
        ))
        db.send_create_signal('nomina', ['Empleado'])

        # Adding model 'Cobrador'
        db.create_table('nomina_cobrador', (
            ('empleado_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['nomina.Empleado'], unique=True, primary_key=True)),
            ('codcobrador', self.gf('django.db.models.fields.CharField')(max_length=100, db_column='codcobrador')),
            ('ruta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crm.Ruta'], null=True, blank=True)),
        ))
        db.send_create_signal('nomina', ['Cobrador'])

        # Adding model 'Vendedor'
        db.create_table('nomina_vendedor', (
            ('empleado_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['nomina.Empleado'], unique=True, primary_key=True)),
            ('codvendedor', self.gf('django.db.models.fields.CharField')(max_length=100, db_column='codvendedor')),
        ))
        db.send_create_signal('nomina', ['Vendedor'])

        # Adding model 'Prestamo'
        db.create_table('nomina_prestamo', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('empleado', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nomina.Empleado'])),
            ('fecha', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2012, 5, 4, 0, 0), null=True, db_column='fecha', blank=True)),
            ('fechaprimer', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2012, 5, 4, 0, 0), null=True, blank=True)),
            ('comprobante', self.gf('django.db.models.fields.CharField')(max_length=100, db_column='codigo')),
            ('concepto', self.gf('django.db.models.fields.CharField')(max_length=500, db_column='concepto')),
            ('monto', self.gf('django.db.models.fields.DecimalField')(db_column='monto', decimal_places=2, max_digits=8)),
            ('cuotas', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('cancelado', self.gf('django.db.models.fields.BooleanField')(default=False, db_column='cancelado')),
        ))
        db.send_create_signal('nomina', ['Prestamo'])

        # Adding model 'Cuota'
        db.create_table('nomina_cuota', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('prestamo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nomina.Prestamo'], null=True, blank=True)),
            ('monto', self.gf('django.db.models.fields.DecimalField')(db_column='monto', decimal_places=2, max_digits=8)),
            ('fecha', self.gf('django.db.models.fields.DateField')(null=True, db_column='fecha', blank=True)),
            ('estado', self.gf('django.db.models.fields.CharField')(max_length=100, db_column='estado')),
        ))
        db.send_create_signal('nomina', ['Cuota'])

        # Adding model 'TipoDeduccion'
        db.create_table('nomina_tipodeduccion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=100, db_column='tipo')),
        ))
        db.send_create_signal('nomina', ['TipoDeduccion'])

        # Adding model 'TipoPercepcion'
        db.create_table('nomina_tipopercepcion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=100, db_column='tipo')),
        ))
        db.send_create_signal('nomina', ['TipoPercepcion'])

        # Adding model 'DeduccionesAplicables'
        db.create_table('nomina_deduccionesaplicables', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tipo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nomina.TipoDeduccion'])),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('percent', self.gf('django.db.models.fields.DecimalField')(default=0.0, max_digits=5, decimal_places=2)),
            ('monto', self.gf('django.db.models.fields.DecimalField')(decimal_places=2, db_column='monto', default=0.0, max_digits=8, blank=True, null=True)),
            ('limiteinferior', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, db_column='liminferior', blank=True)),
            ('limitesuperior', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, db_column='limsuperior', blank=True)),
            ('empleado', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nomina.Empleado'])),
        ))
        db.send_create_signal('nomina', ['DeduccionesAplicables'])

        # Adding model 'PercepcionesAplicables'
        db.create_table('nomina_percepcionesaplicables', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tipo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nomina.TipoPercepcion'])),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('percent', self.gf('django.db.models.fields.DecimalField')(default=0.0, max_digits=5, decimal_places=2)),
            ('monto', self.gf('django.db.models.fields.DecimalField')(decimal_places=2, db_column='monto', default=0.0, max_digits=8, blank=True, null=True)),
            ('limiteinferior', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, db_column='liminferior', blank=True)),
            ('limitesuperior', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, db_column='limsuperior', blank=True)),
            ('empleado', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nomina.Empleado'])),
        ))
        db.send_create_signal('nomina', ['PercepcionesAplicables'])

        # Adding model 'Nomina'
        db.create_table('nomina_nomina', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nomina', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='nomina')),
            ('pago', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal('nomina', ['Nomina'])

        # Adding M2M table for field empleados on 'Nomina'
        db.create_table('nomina_nomina_empleados', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('nomina', models.ForeignKey(orm['nomina.nomina'], null=False)),
            ('empleado', models.ForeignKey(orm['nomina.empleado'], null=False))
        ))
        db.create_unique('nomina_nomina_empleados', ['nomina_id', 'empleado_id'])

        # Adding model 'IR'
        db.create_table('nomina_ir', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('desde', self.gf('django.db.models.fields.DecimalField')(default=0.0, max_digits=10, decimal_places=2)),
            ('hasta', self.gf('django.db.models.fields.DecimalField')(default=0.0, max_digits=10, decimal_places=2)),
            ('porcentaje', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('base', self.gf('django.db.models.fields.DecimalField')(default=0.0, max_digits=10, decimal_places=2)),
            ('exceso', self.gf('django.db.models.fields.DecimalField')(default=0.0, max_digits=10, decimal_places=2)),
        ))
        db.send_create_signal('nomina', ['IR'])

        # Adding model 'Departamento'
        db.create_table('nomina_departamento', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100, db_column='departamento')),
            ('parent', self.gf('mptt.fields.TreeForeignKey')(blank=True, related_name='child', null=True, to=orm['nomina.Departamento'])),
            ('lft', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('rght', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('tree_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('level', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
        ))
        db.send_create_signal('nomina', ['Departamento'])

        # Adding M2M table for field trabajadores on 'Departamento'
        db.create_table('nomina_departamento_trabajadores', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('departamento', models.ForeignKey(orm['nomina.departamento'], null=False)),
            ('empleado', models.ForeignKey(orm['nomina.empleado'], null=False))
        ))
        db.create_unique('nomina_departamento_trabajadores', ['departamento_id', 'empleado_id'])

        # Adding model 'Vacaciones'
        db.create_table('nomina_vacaciones', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Empleado', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nomina.Empleado'])),
            ('fechainicio', self.gf('django.db.models.fields.DateField')(db_column='fechainicio')),
            ('fechafin', self.gf('django.db.models.fields.DateField')(db_column='fechafin')),
            ('notas', self.gf('django.db.models.fields.TextField')(null=True, db_column='notas', blank=True)),
        ))
        db.send_create_signal('nomina', ['Vacaciones'])

        # Adding model 'Antiguedad'
        db.create_table('nomina_antiguedad', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('years', self.gf('django.db.models.fields.IntegerField')()),
            ('porcentaje', self.gf('django.db.models.fields.DecimalField')(default=0.0, max_digits=5, decimal_places=2)),
        ))
        db.send_create_signal('nomina', ['Antiguedad'])

        # Adding model 'ParametroHE'
        db.create_table('nomina_parametrohe', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('codigodia', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('descripcion', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('hentrada', self.gf('django.db.models.fields.DecimalField')(max_digits=4, decimal_places=2)),
            ('hsalida', self.gf('django.db.models.fields.DecimalField')(max_digits=4, decimal_places=2)),
        ))
        db.send_create_signal('nomina', ['ParametroHE'])

        # Adding model 'HorasExtras'
        db.create_table('nomina_horasextras', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('empleado', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nomina.Empleado'])),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('hentrada', self.gf('django.db.models.fields.DecimalField')(max_digits=4, decimal_places=2)),
            ('hsalida', self.gf('django.db.models.fields.DecimalField')(max_digits=4, decimal_places=2)),
            ('lab', self.gf('django.db.models.fields.DecimalField')(default=0.0, max_digits=4, decimal_places=2)),
            ('ext', self.gf('django.db.models.fields.DecimalField')(default=0.0, max_digits=4, decimal_places=2)),
        ))
        db.send_create_signal('nomina', ['HorasExtras'])

    def backwards(self, orm):
        # Deleting model 'Cargo'
        db.delete_table('nomina_cargo')

        # Deleting model 'Empleado'
        db.delete_table('nomina_empleado')

        # Deleting model 'Cobrador'
        db.delete_table('nomina_cobrador')

        # Deleting model 'Vendedor'
        db.delete_table('nomina_vendedor')

        # Deleting model 'Prestamo'
        db.delete_table('nomina_prestamo')

        # Deleting model 'Cuota'
        db.delete_table('nomina_cuota')

        # Deleting model 'TipoDeduccion'
        db.delete_table('nomina_tipodeduccion')

        # Deleting model 'TipoPercepcion'
        db.delete_table('nomina_tipopercepcion')

        # Deleting model 'DeduccionesAplicables'
        db.delete_table('nomina_deduccionesaplicables')

        # Deleting model 'PercepcionesAplicables'
        db.delete_table('nomina_percepcionesaplicables')

        # Deleting model 'Nomina'
        db.delete_table('nomina_nomina')

        # Removing M2M table for field empleados on 'Nomina'
        db.delete_table('nomina_nomina_empleados')

        # Deleting model 'IR'
        db.delete_table('nomina_ir')

        # Deleting model 'Departamento'
        db.delete_table('nomina_departamento')

        # Removing M2M table for field trabajadores on 'Departamento'
        db.delete_table('nomina_departamento_trabajadores')

        # Deleting model 'Vacaciones'
        db.delete_table('nomina_vacaciones')

        # Deleting model 'Antiguedad'
        db.delete_table('nomina_antiguedad')

        # Deleting model 'ParametroHE'
        db.delete_table('nomina_parametrohe')

        # Deleting model 'HorasExtras'
        db.delete_table('nomina_horasextras')

    models = {
        'crm.contacto': {
            'Meta': {'object_name': 'Contacto'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'apellido'"}),
            'categoria': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'categoria'", 'blank': 'True'}),
            'cedula': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'db_column': "'cedula'", 'blank': 'True'}),
            'estadocivil': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'estadocivil'", 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'db_column': "'foto'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'licencia': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'licencia'", 'blank': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'db_column': "'logo'", 'blank': 'True'}),
            'nacimiento': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'nacimiento'", 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'nombre'"}),
            'nombrecorto': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'nombrecorto'", 'blank': 'True'}),
            'notas': ('django.db.models.fields.TextField', [], {'null': 'True', 'db_column': "'notas'", 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'title'", 'blank': 'True'})
        },
        'crm.ruta': {
            'Meta': {'object_name': 'Ruta'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ruta': ('django.db.models.fields.TextField', [], {'max_length': '300', 'db_column': "'ruta'"}),
            'zona': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'zona'"})
        },
        'nomina.antiguedad': {
            'Meta': {'object_name': 'Antiguedad'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'porcentaje': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '5', 'decimal_places': '2'}),
            'years': ('django.db.models.fields.IntegerField', [], {})
        },
        'nomina.cargo': {
            'Meta': {'object_name': 'Cargo'},
            'cargo': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'cargo'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'nomina.cobrador': {
            'Meta': {'object_name': 'Cobrador', '_ormbases': ['nomina.Empleado']},
            'codcobrador': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'codcobrador'"}),
            'empleado_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['nomina.Empleado']", 'unique': 'True', 'primary_key': 'True'}),
            'ruta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['crm.Ruta']", 'null': 'True', 'blank': 'True'})
        },
        'nomina.cuota': {
            'Meta': {'object_name': 'Cuota'},
            'estado': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'estado'"}),
            'fecha': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'fecha'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'monto': ('django.db.models.fields.DecimalField', [], {'db_column': "'monto'", 'decimal_places': '2', 'max_digits': '8'}),
            'prestamo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomina.Prestamo']", 'null': 'True', 'blank': 'True'})
        },
        'nomina.deduccionesaplicables': {
            'Meta': {'object_name': 'DeduccionesAplicables'},
            'empleado': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomina.Empleado']"}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'limiteinferior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'db_column': "'liminferior'", 'blank': 'True'}),
            'limitesuperior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'db_column': "'limsuperior'", 'blank': 'True'}),
            'monto': ('django.db.models.fields.DecimalField', [], {'decimal_places': '2', 'db_column': "'monto'", 'default': '0.0', 'max_digits': '8', 'blank': 'True', 'null': 'True'}),
            'percent': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '5', 'decimal_places': '2'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomina.TipoDeduccion']"})
        },
        'nomina.departamento': {
            'Meta': {'object_name': 'Departamento'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100', 'db_column': "'departamento'"}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'child'", 'null': 'True', 'to': "orm['nomina.Departamento']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'trabajadores': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'trabajadores'", 'symmetrical': 'False', 'to': "orm['nomina.Empleado']"}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'nomina.empleado': {
            'Meta': {'object_name': 'Empleado', '_ormbases': ['crm.Contacto']},
            'activo': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'activo'"}),
            'cargo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomina.Cargo']"}),
            'codigoempleado': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'codigo'"}),
            'comisionista': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'comisionista'"}),
            'contacto_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['crm.Contacto']", 'unique': 'True', 'primary_key': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'fecha'", 'blank': 'True'}),
            'idseguro': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'seguro'"}),
            'madre': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'db_column': "'madre'", 'blank': 'True'}),
            'numerohijos': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'hijos'", 'blank': 'True'}),
            'padre': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'db_column': "'padre'", 'blank': 'True'}),
            'sueldo': ('django.db.models.fields.DecimalField', [], {'db_column': "'sueldo'", 'decimal_places': '2', 'max_digits': '10'})
        },
        'nomina.horasextras': {
            'Meta': {'object_name': 'HorasExtras'},
            'empleado': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomina.Empleado']"}),
            'ext': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '4', 'decimal_places': '2'}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'hentrada': ('django.db.models.fields.DecimalField', [], {'max_digits': '4', 'decimal_places': '2'}),
            'hsalida': ('django.db.models.fields.DecimalField', [], {'max_digits': '4', 'decimal_places': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lab': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '4', 'decimal_places': '2'})
        },
        'nomina.ir': {
            'Meta': {'object_name': 'IR'},
            'base': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '10', 'decimal_places': '2'}),
            'desde': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '10', 'decimal_places': '2'}),
            'exceso': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '10', 'decimal_places': '2'}),
            'hasta': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '10', 'decimal_places': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'porcentaje': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'nomina.nomina': {
            'Meta': {'object_name': 'Nomina'},
            'empleados': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'empleados'", 'symmetrical': 'False', 'to': "orm['nomina.Empleado']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nomina': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'nomina'"}),
            'pago': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'nomina.parametrohe': {
            'Meta': {'object_name': 'ParametroHE'},
            'codigodia': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'hentrada': ('django.db.models.fields.DecimalField', [], {'max_digits': '4', 'decimal_places': '2'}),
            'hsalida': ('django.db.models.fields.DecimalField', [], {'max_digits': '4', 'decimal_places': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'nomina.percepcionesaplicables': {
            'Meta': {'object_name': 'PercepcionesAplicables'},
            'empleado': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomina.Empleado']"}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'limiteinferior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'db_column': "'liminferior'", 'blank': 'True'}),
            'limitesuperior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'db_column': "'limsuperior'", 'blank': 'True'}),
            'monto': ('django.db.models.fields.DecimalField', [], {'decimal_places': '2', 'db_column': "'monto'", 'default': '0.0', 'max_digits': '8', 'blank': 'True', 'null': 'True'}),
            'percent': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '5', 'decimal_places': '2'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomina.TipoPercepcion']"})
        },
        'nomina.prestamo': {
            'Meta': {'object_name': 'Prestamo'},
            'cancelado': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'cancelado'"}),
            'comprobante': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'codigo'"}),
            'concepto': ('django.db.models.fields.CharField', [], {'max_length': '500', 'db_column': "'concepto'"}),
            'cuotas': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'empleado': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomina.Empleado']"}),
            'fecha': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2012, 5, 4, 0, 0)', 'null': 'True', 'db_column': "'fecha'", 'blank': 'True'}),
            'fechaprimer': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2012, 5, 4, 0, 0)', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'monto': ('django.db.models.fields.DecimalField', [], {'db_column': "'monto'", 'decimal_places': '2', 'max_digits': '8'})
        },
        'nomina.tipodeduccion': {
            'Meta': {'object_name': 'TipoDeduccion'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'tipo'"})
        },
        'nomina.tipopercepcion': {
            'Meta': {'object_name': 'TipoPercepcion'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'tipo'"})
        },
        'nomina.vacaciones': {
            'Empleado': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomina.Empleado']"}),
            'Meta': {'object_name': 'Vacaciones'},
            'fechafin': ('django.db.models.fields.DateField', [], {'db_column': "'fechafin'"}),
            'fechainicio': ('django.db.models.fields.DateField', [], {'db_column': "'fechainicio'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notas': ('django.db.models.fields.TextField', [], {'null': 'True', 'db_column': "'notas'", 'blank': 'True'})
        },
        'nomina.vendedor': {
            'Meta': {'object_name': 'Vendedor', '_ormbases': ['nomina.Empleado']},
            'codvendedor': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'codvendedor'"}),
            'empleado_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['nomina.Empleado']", 'unique': 'True', 'primary_key': 'True'})
        }
    }

    complete_apps = ['nomina']