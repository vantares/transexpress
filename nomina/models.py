# -*- coding: UTF-8 -*- 
from django.db import models
from crm.models import Cliente, ClientePotencial, Ruta, Contacto
from mptt.models import MPTTModel, TreeForeignKey
from crm.models import Parametro
from decimal import Decimal, ROUND_HALF_UP
from django.db.models import Sum
from datetime import date
import time, math
MESES = [
        ('1','Enero'),
        ('2','Febrero'),
        ('3','Marzo'),
        ('4','Abril'),
        ('5','Mayo'),
        ('6','Junio'),
        ('7','Julio'),
        ('8','Agosto'),
        ('9','Septiembre'),
        ('10','Octubre'),
        ('11','Noviembre'),
        ('12','Diciembre'),
]
#===========================================
# Cargos
#===========================================
class Cargo( models.Model ):
    cargo = models.CharField(db_column="cargo", null=False, blank=False, max_length=300, verbose_name="cargo") 
    
    def __unicode__( self ):
        return self.cargo

class Salario( models.Model ):
    empleado = models.ForeignKey(to='Empleado', verbose_name="empleado")
    fecha = models.DateField( help_text='Fecha a partir de la cual se hara efectivo' )
    salario = models.DecimalField(max_digits=10, decimal_places=2, help_text='neto mensual', verbose_name="sueldo mensual")

#===========================================
# Empleados
#===========================================
class Empleado( Contacto ):
    codigoempleado = models.CharField(db_column="codigo", null=False, blank=False, max_length=100, verbose_name=u"código empleado") 
    fecha = models.DateField(db_column="fecha", null=True, blank=True, help_text='Como referencia para antiguedad', verbose_name="fecha contratacion")
    cargo = models.ForeignKey(Cargo, verbose_name="Cargo")
    sueldo = models.DecimalField(db_column="sueldo",null=False, blank=False, max_digits=10, decimal_places=2, help_text='neto mensual', verbose_name="sueldo mensual")
    idseguro = models.CharField(db_column="seguro", null=False, blank=False, max_length=100, verbose_name="id seguro") 
    activo = models.BooleanField(db_column="activo", null=False, blank=False, default=False, verbose_name="activo")
    padre = models.CharField(db_column="padre", null=True, blank=True, max_length=100, verbose_name="padre") 
    madre = models.CharField(db_column="madre", null=True, blank=True, max_length=100, verbose_name="madre") 
    numerohijos =   models.IntegerField(db_column="hijos", null=True, blank=True, verbose_name="cantidad de hijos")
    comisionista = models.BooleanField(db_column="comisionista", null=False, blank=False, default=False, verbose_name="comisionista")

    fecha_inicio = None
    fecha_fin = None
    devengos = None
    descuentos = None
    total = None
    inss = None
    ir = None
    prestamo = None
    antiguedad = None
    porcentaje = None
    quincenal = None
    mensual = None
    
    class Meta:
        verbose_name = "empleado"
        verbose_name_plural = "empleados"
    
    @property
    def deducciones(self):
        ded = DeduccionesAplicables.objects.filter(empleado=self)
        if self.fecha_inicio is not None and self.fecha_fin is not None:
            ded = ded.filter(fecha__range=[self.fecha_inicio,self.fecha_fin]).order_by('fecha')
        return ded

    @property
    def percepciones(self):
        per = PercepcionesAplicables.objects.filter(empleado=self)
        if self.fecha_inicio is not None and self.fecha_fin is not None:
            per = per.filter(fecha__range=[self.fecha_inicio,self.fecha_fin]).order_by('fecha')
        return per

class Cobrador( Empleado ):
    codcobrador = models.CharField(db_column="codcobrador", null=False, blank=False, max_length=100, verbose_name=u"código cobrador")  
    ruta = models.ForeignKey(Ruta, blank=True, null=True, help_text="seleccione la zona que el mensajero tendra asignada", verbose_name="zona")

    class Meta:
        verbose_name = "mensajero"
        verbose_name_plural = "mensajeros"

    def __unicode__( self ):
        return self.nombre + " " + self.apellido

class Vendedor( Empleado ):
    codvendedor = models.CharField(db_column="codvendedor", null=False, blank=False, max_length=100, verbose_name=u"código vendedor")  

    class Meta:
        verbose_name = "vendedor"
        verbose_name_plural = "vendedores"

    def __unicode__( self ):
        return self.nombre + ' ' + self.apellido

#===========================================
# Prestamo
#===========================================
class Prestamo( models.Model ):
    empleado = models.ForeignKey(Empleado, verbose_name="empleado")
    fecha = models.DateField(db_column="fecha", default=date.today(), null=True, blank=True, verbose_name="fecha")
    fechaprimer = models.DateField(default=date.today(),null=True, blank=True, verbose_name="fecha de primer abono")
    comprobante = models.CharField(db_column="codigo", null=False, blank=False, max_length=100, verbose_name=u"comprobante") 
    concepto = models.CharField(db_column="concepto", null=False, blank=False, max_length=500, verbose_name=u"concepto") 
    monto = models.DecimalField(db_column="monto",null=False, blank=False,max_digits=8, decimal_places=2, verbose_name="monto")
    cuotas =   models.IntegerField(default=1, verbose_name="Cantidad de Coutas")
    cancelado = models.BooleanField(db_column="cancelado", null=False, blank=False, default=False, verbose_name="cancelado")
    
    class Meta:
        verbose_name = "prestamo"
        verbose_name_plural = "prestamo"

    def __unicode__( self ):
        return self.concepto + str(self.monto)
    
    def save(self):
        abono = Decimal(self.monto / self.cuotas).quantize(Decimal('1.00'), ROUND_HALF_UP)
        x = self.fechaprimer
        for i in range(self.cuotas):
            cuota = Cuota(prestamo=self, monto=abono, fecha=x, estado='Pago de Cuota '+str(i+1))
            cuota.save()
            try:
                dia = 16 if x.day <= 15 else 1
                nextmonthdate = x.replace(month=x.month+1, day=dia) if dia == 1 else x.replace(day=dia)
            except ValueError: 
                if x.month == 12:
                    nextmonthdate = x.replace(year=x.year+1, month=1, day=dia) if dia == 1 else x.replace(year=x.year+1, day=dia)
                else:
                    raise
            x = nextmonthdate
        models.Model.save(self)

    @property
    def saldo(self):
        s = Cuota.objects.filter(prestamo=self, fecha__lte=date.today()).aggregate(Sum('monto'))['monto__sum']
        s = s if s is not None else Decimal(0)
        return self.monto - s if s is not None else Decimal(0)

class Cuota( models.Model ):
    prestamo = models.ForeignKey(Prestamo, blank=True, null=True, verbose_name="prestamo")
    monto = models.DecimalField(db_column="monto",null=False, blank=False,max_digits=8, decimal_places=2, verbose_name="monto")
    fecha = models.DateField(db_column="fecha", null=True, blank=True, verbose_name="fecha")
    estado = models.CharField(db_column="estado", null=False, blank=False, max_length=100, verbose_name="estado") 

    class Meta:
        verbose_name = "Pago de cuota por prestamo"
        verbose_name_plural = "Pagos de cuotas por prestamo"

    def __unicode__(self):
        return unicode(self.monto)

#===========================================
# Deducciones y Percepciones
#===========================================
class TipoDeduccion( models.Model ):
    tipo = models.CharField(db_column="tipo", null=False, blank=False, max_length=100, verbose_name="tipo deduccion") 

    class Meta:
        verbose_name = "tipo deduccion"
        verbose_name_plural = "tipos deduccion"

    def __unicode__( self ):
        return self.tipo

class TipoPercepcion( models.Model ):
    tipo = models.CharField(db_column="tipo", null=False, blank=False, max_length=100, verbose_name="tipo percepcion") 

    class Meta:
        verbose_name = "tipo percepcion"
        verbose_name_plural = "tipos percepcion"

    def __unicode__( self ):
        return self.tipo

class DeduccionesAplicables( models.Model ):
    tipo = models.ForeignKey(TipoDeduccion, verbose_name="deduccion") 
    fecha = models.DateField()
    percent = models.DecimalField(max_digits=5, default=0.00, decimal_places=2, verbose_name="porcentaje") 
    monto = models.DecimalField(db_column="monto", default=0.00, null=True, blank=True,max_digits=8, decimal_places=2, verbose_name="monto")
    limiteinferior = models.CharField(db_column="liminferior", null=True, blank=True, max_length=100, verbose_name="limite inferior") 
    limitesuperior = models.CharField(db_column="limsuperior", null=True, blank=True, max_length=100, verbose_name="limite superior") 
    empleado = models.ForeignKey(Empleado, verbose_name="empleado") 
    
    class Meta:
        verbose_name = "deduccion aplicable"
        verbose_name_plural = "deducciones aplicables"

    def __unicode__( self ):
        return unicode(self.tipo)

class PercepcionesAplicables( models.Model ):
    tipo = models.ForeignKey(TipoPercepcion, verbose_name="percepcion") 
    fecha = models.DateField()
    percent = models.DecimalField(max_digits=5, default=0.00, decimal_places=2, verbose_name="porcentaje") 
    monto = models.DecimalField(db_column="monto", default=0.00, null=True, blank=True, max_digits=8, decimal_places=2, verbose_name="monto")
    limiteinferior = models.CharField(db_column="liminferior", null=True, blank=True, max_length=100, verbose_name="limite inferior") 
    limitesuperior = models.CharField(db_column="limsuperior", null=True, blank=True, max_length=100, verbose_name="limite superior") 
    empleado = models.ForeignKey(Empleado, verbose_name="empleado") 
    
    class Meta:
        verbose_name = "percepcion aplicable"
        verbose_name_plural = "percepciones aplicables"

    def __unicode__( self ):
        return unicode(self.tipo)

#===========================================
# Nomina
#===========================================
class Nomina( models.Model ):
    PAG0=(
        ('1','Quincenal'),
        ('2','Mensual'),
    )
    nomina = models.CharField(db_column="nomina", null=False, blank=False, max_length=300, verbose_name=u"nómina") 
    empleados = models.ManyToManyField(Empleado, related_name='empleados', verbose_name="Empleados")
    pago = models.CharField(choices=PAG0, max_length = 20, verbose_name='Forma de Pago')

    class Meta:
        verbose_name = u"nómina"
        verbose_name_plural = u"nóminas"

    def __unicode__( self ):
        return self.nomina

#===========================================
# Tabla IR
#===========================================
class IR( models.Model ):
    desde = models.DecimalField(max_digits=10, default=0.00, decimal_places=2, verbose_name="Desde") 
    hasta = models.DecimalField(max_digits=10, default=0.00, decimal_places=2, verbose_name="Hasta") 
    porcentaje =   models.IntegerField(default=0, verbose_name="Porcentaje")
    base = models.DecimalField(max_digits=10, default=0.00, decimal_places=2, verbose_name="Base") 
    exceso = models.DecimalField(max_digits=10, default=0.00, decimal_places=2, verbose_name="Exceso") 
    
    class Meta:
        verbose_name = u"IR"
        verbose_name_plural = u"IR"

    def __unicode__( self):
        return str(self.porcentaje)

#===========================================
# Estructura Organizacional
#===========================================
class Departamento( MPTTModel ):
    nombre = models.CharField(db_column="departamento", max_length=100, unique=True, verbose_name="Area" )
    parent = TreeForeignKey('self', null=True, blank=True, related_name='child', verbose_name="Dpto. Superior")
    trabajadores = models.ManyToManyField(Empleado, related_name='trabajadores', verbose_name="Empleados")

    class MPTTMeta:
        order_insertion_by = ['nombre']

    def __unicode__( self ):
        return self.nombre

#===========================================
# Vacaciones
#===========================================
class Vacaciones( models.Model ):
    Empleado = models.ForeignKey(Empleado, verbose_name="empleado") 
    fechainicio = models.DateField(db_column="fechainicio", null=False, blank=False, verbose_name="fecha inicio")
    fechafin = models.DateField(db_column="fechafin", null=False, blank=False, verbose_name="fecha fin")
    notas = models.TextField(db_column="notas", null=True, blank=True, verbose_name="notas o comentarios")

    class Meta:
        verbose_name = "vacacion"
        verbose_name_plural = "vacaciones"

    def __unicode__( self ):
        return unicode(self.Empleado)

#===========================================
# Antiguedad
#===========================================
class Antiguedad( models.Model ):
    years =   models.IntegerField(verbose_name=u"Años")
    porcentaje = models.DecimalField(max_digits=5, default=0.00, decimal_places=2, verbose_name="porcentaje") 

    class Meta:
        verbose_name = "Tabla de Antiguedad"
        verbose_name_plural = "Tabla de Antiguedad"

    def __unicode__( self ):
        return unicode(self.porcentaje)

#===========================================
# Horas Extras
#===========================================
class ParametroHE( models.Model ):
    codigodia = models.CharField(max_length=2)
    descripcion = models.CharField(max_length=100)
    hentrada = models.DecimalField(max_digits=4, decimal_places=2)
    hsalida = models.DecimalField(max_digits=4, decimal_places=2)

    def __unicode__(self):
        return self.codigodia + ' - ' + self.descripcion

    @property
    def total( self ):
        return self.hsalida - self.hentrada
    
class HorasExtras( models.Model ):
    empleado = models.ForeignKey(Empleado, verbose_name="empleado")
    fecha = models.DateField( )
    hora_entrada = models.TimeField(null=True)
    hora_salida = models.TimeField(null=True)
    hentrada = models.DecimalField(max_digits=4, decimal_places=2)
    hsalida = models.DecimalField(max_digits=4, decimal_places=2)
    lab = models.DecimalField(default=0.00, max_digits=4, decimal_places=2)
    ext = models.DecimalField(default=0.00, max_digits=4, decimal_places=2)

    def __unicode__( self ):
        return str(self.hentrada) + ' - ' + str(self.hsalida)
    
    @property
    def laboradas( self ):
        lab = math.modf(self.hsalida - self.hentrada)[1]
        
        mi =math.modf( self.hentrada)[0]
        mi2 = math.modf( self.hsalida)[0]

        if mi2 == 0:
            minu = 0.6 - mi
        else:
            minu = mi2 - mi
        if minu == 0.6:
            su = 1
        else:
            su = minu
        return int(lab)
        

    @property
    def extras( self ):
        if self.fecha.isoweekday() in range(1,6):
            hrs = ParametroHE.objects.get(codigodia='01')
            xtr = self.laboradas - hrs.total
        else:
            hrs = ParametroHE.objects.get(codigodia='02')
            xtr = self.laboradas - hrs.total
        if self.fecha.isoweekday() == 7:
            hrs = ParametroHE.objects.get(codigodia='03')
            xtr = self.laboradas - hrs.total
        return xtr

    def save(self):
        self.lab = self.laboradas
        self.ext = self.extras
        models.Model.save(self)

