# -*- coding: UTF-8 -*- 
from django.contrib import admin
from django.db import models
from django.conf import settings
from nomina.models import *
from img import AdminImageWidget
from mptt.admin import MPTTModelAdmin
from widgets import RawIdWidget

class RawIdCustomAdmin ( ):
    class Media:
        js = (  settings.MEDIA_URL + "js/RelatedEmpleadoLookups.js", settings.MEDIA_URL + "js/jquery.min.js")

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        """
        Get a form Field for a ForeignKey.
        """
        if db_field.name in self.raw_id_fields:
            if str(db_field.name) == 'empleado':
                kwargs['widget'] = RawIdWidget(self.model, 'empleado')
        return db_field.formfield(**kwargs)

#=========================================
# Inlines
#==========================================
class PrestamoInline( admin.TabularInline ):
    model = Prestamo
    extra = 1

class CuotaInline( admin.TabularInline ):
    model = Cuota
    extra = 1

class PercepcionInline ( admin.TabularInline ):
    model = PercepcionesAplicables
    extra = 1

class DeduccionInline ( admin.TabularInline ):
    model = DeduccionesAplicables
    extra = 1

#=========================================
# Admin 
#==========================================
class NominaAdmin( admin.ModelAdmin ):
    filter_horizontal = ('empleados',)

class EmpleadoAdmin( admin.ModelAdmin ):
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'foto':
            kwargs['widget'] = AdminImageWidget
            try:
                del kwargs['request']
            except KeyError:
                pass
            return db_field.formfield(**kwargs)
        return super(EmpleadoAdmin, self).formfield_for_dbfield(db_field, **kwargs)    
    inlines = [PrestamoInline, DeduccionInline, PercepcionInline]
    search_fields = ['codigoempleado','nombre', 'apellido']
    list_display = ['codigoempleado', 'nombre','apellido']

class CobradorAdmin( admin.ModelAdmin ):
    inlines = [PrestamoInline, DeduccionInline, PercepcionInline]

class VendedorAdmin( admin.ModelAdmin ):
    inlines = [PrestamoInline, DeduccionInline, PercepcionInline]

class PrestamoAdmin( admin.ModelAdmin ):
    list_display = ('empleado', 'concepto', 'monto', 'saldo')
    inlines = [CuotaInline]

class DeduccionAdmin(RawIdCustomAdmin, admin.ModelAdmin ):
    list_display = ['tipo', 'empleado', 'fecha']
    raw_id_fields = ('empleado',)

class PercepcionAdmin(RawIdCustomAdmin, admin.ModelAdmin ):
    list_display = ['tipo', 'empleado', 'fecha']
    raw_id_fields = ('empleado',)

class DepartamentoAdmin( MPTTModelAdmin ):
    filter_horizontal = ('trabajadores',)

class IRAdmin( admin.ModelAdmin ):
    list_display = ('desde','hasta','porcentaje','base','exceso')

class AntiguedadAdmin( admin.ModelAdmin ):
    list_display = ('years','porcentaje')

class ParametroHEAdmin( admin.ModelAdmin ):
    list_display = ('descripcion', 'hentrada', 'hsalida', 'total')

class HorasExtrasAdmin( admin.ModelAdmin ):
    list_display = ('empleado', 'fecha','hentrada', 'hsalida', 'laboradas', 'extras')

class SalarioAdmin( RawIdCustomAdmin, admin.ModelAdmin ):
    list_display = ('empleado', 'fecha','salario')
    raw_id_fields = ('empleado',)

class CuotaAdmin( admin.ModelAdmin ):
    list_display = ('prestamo', 'fecha', 'monto')
    search_fields = ['prestamo__empleado__codigoempleado']

admin.site.register(Departamento, DepartamentoAdmin)
admin.site.register(Nomina, NominaAdmin)
admin.site.register(Empleado, EmpleadoAdmin)
admin.site.register(Cargo)
admin.site.register(Cobrador, CobradorAdmin)
admin.site.register(Vendedor, VendedorAdmin)
admin.site.register(Prestamo, PrestamoAdmin)
admin.site.register(TipoDeduccion)
admin.site.register(TipoPercepcion)
admin.site.register(DeduccionesAplicables, DeduccionAdmin)
admin.site.register(PercepcionesAplicables, PercepcionAdmin)
admin.site.register(Vacaciones)
admin.site.register(IR, IRAdmin)
admin.site.register(Antiguedad, AntiguedadAdmin)
admin.site.register(HorasExtras, HorasExtrasAdmin)
admin.site.register(ParametroHE, ParametroHEAdmin)
admin.site.register(Cuota, CuotaAdmin)
admin.site.register(Salario, SalarioAdmin)
