from django.contrib.admin.widgets import ForeignKeyRawIdWidget
import string
from models  import DeduccionesAplicables, Empleado

class RawIdWidget(ForeignKeyRawIdWidget):

    def __init__(self, model=None, field=None, using=None):
        self.rel = model._meta.get_field(field).rel
        self.db = using
        super(RawIdWidget, self).__init__(self.rel, attrs=None, using=None)
    
    def render(self, *args, **kwargs):
        from django.utils.safestring import mark_safe
        original_render = super(RawIdWidget, self).render(*args, **kwargs)
        wid = super(RawIdWidget, self).__init__(self.rel, attrs=None, using=None)
        ADMIN_ROOT_URL = "/admin/"
        name = args[0]
        if name == 'empleado':
            model = Empleado
        value = args[1]
        codigo_cliente = ''
        nombre_cliente = ''
        if value:
            try:
                obj = model.objects.get(pk = int(value))
                if name == 'empleado':
                    codigo_cliente = obj.codigoempleado
                nombre_cliente = obj.nombrecompleto
            except (ValueError, self.rel.to.DoesNotExist):
                pass

        original_render = original_render.replace('<a',"<input id = 'codigo_%s' onchange=obtener_nombre($(this).attr('id')) type='text' value = '%s'> <a " %(name,codigo_cliente))
        original_render =  original_render + "<input id = 'nombre_%s' type='text' style='font-weight:bold; width:500px;' readonly='readonly' value = '%s'>"%(name,nombre_cliente)
        original_render = original_render.replace('name="%s"' %name,'name="%s" style="display:none;"' %name)

        return mark_safe(string.replace(original_render,"../../../", ADMIN_ROOT_URL))

    def label_for_value(self, value):
        return ''
