# -*- coding: UTF-8 -*-
from geraldo import Report, ReportBand, DetailBand, ObjectValue, SystemField, BAND_WIDTH, Label, SubReport, landscape, Line, FIELD_ACTION_SUM
from reportlab.lib.units import cm
from reportlab.lib.enums import TA_RIGHT, TA_CENTER
from reportlab.lib.pagesizes import LETTER, LEGAL
from reportlab.lib.colors import navy
from reportes.report import ReporteMembretado
from crm.models import Parametro
from datetime import date
t = Parametro.objects.get(pk=2)
###################################
# Reporte de IR
###################################
class IrReport(ReporteMembretado):
    page_size = (LETTER)
    default_style = {'fontName':'Courier'}

    class band_detail(ReportBand):
        height = 0.5*cm
        elements=(
            ObjectValue(attribute_name='codigoempleado', left=0.5*cm),
            ObjectValue(attribute_name='nombre', left=2*cm),
            ObjectValue(attribute_name='apellido', left=6*cm),
        )

    class band_page_header(ReportBand):
        height = 1.3*cm
        elements = [
            SystemField(expression='%(report_title)s', top=0.1*cm, left=0, width=BAND_WIDTH,
                style={'fontName': 'Courier', 'fontSize': 14, 'alignment': TA_CENTER,'textColor':navy}),
            Label(text=u"Codigo", top=0.8*cm, left=0.5*cm),
            Label(text=u"Nombre", top=0.8*cm, left=2*cm),
            Label(text=u"Apellido", top=0.8*cm, left=6*cm),
            Label(text=u"Nombre IR Retenido", top=0.8*cm, left=10*cm),
        ]
        borders = {'bottom': True}

###################################
# Reporte de Empleados
###################################
class EmpleadosReport(ReporteMembretado):
    page_size = landscape(LETTER)
    default_style = {'fontName':'Courier'}

    class band_detail(ReportBand):
        height = 0.5*cm
        elements=(
            ObjectValue(attribute_name='codigoempleado', left=0.5*cm),
            ObjectValue(attribute_name='nombrecompleto', left=2*cm),
            ObjectValue(attribute_name='cargo', left=6*cm),
            ObjectValue(attribute_name='sueldo', left=10*cm),
        )

    class band_page_header(ReportBand):
        height = 1.3*cm
        elements = [
            SystemField(expression='%(report_title)s', top=0.1*cm, left=0, width=BAND_WIDTH,
                style={'fontName': 'Courier', 'fontSize': 14, 'alignment': TA_CENTER,'textColor':navy}),
            Label(text=u"Codigo", top=0.8*cm, left=0.5*cm),
            Label(text=u"Nombre Completo", top=0.8*cm, left=2*cm),
            Label(text="Cargo", top=0.8*cm, left=6*cm),
            Label(text="Sueldo", top=0.8*cm, left=10*cm),
            SystemField(expression=u'Pag. %(page_number)d of %(page_count)d', top=0.1*cm,
                width=BAND_WIDTH, style={'alignment': TA_RIGHT}),
            ]
        borders = {'bottom': True}

#===============================================
#   Recibo
#===============================================
class ReciboReport(Report):
    page_size = (LETTER)
    margin_top = 0.1*cm
    margin_bottom = 0.1*cm
    margin_left = 0.1*cm
    margin_right = 0.1*cm
    default_style = {'fontName':'Courier'}

    class band_detail(DetailBand):
        height = 13*cm
        elements = [
            Label(text=t.valor, top=0.5*cm, left=1*cm),
            Label(text="Fecha : ", top=0.5*cm, left=15*cm),
            SystemField(expression='%(now:%d %b %Y)s', top=0.5*cm, left=16.8*cm),
            Label(text="Hora : ", top=1*cm, left=15.2*cm),
            SystemField(expression='%(now:%H:%M)s', top=1*cm, left=16.8*cm),
            Label(text="Recibo", top=1.5*cm, left=1*cm, width=12*cm),
            Label(text="Nombre Empleado :", top=2*cm, left=1*cm),
            ObjectValue(expression='nombrecompleto', top=2*cm, left=5*cm, width=15*cm),
            Label(text="Codigo :", top=2*cm, left=15*cm),
            ObjectValue(expression='codigoempleado', top=2*cm, left=17*cm),
            Label(text="Recibi de", top=3*cm, left=1*cm),
            Label(text=t.valor, top=3*cm, left=3.5*cm),
            Label(text="la cantidad de", top=3*cm, left=8*cm),
            Label(text="En concepto de :", top=3.5*cm, left=1*cm),
            SystemField(expression=u'%(report_title)s', top=3.5*cm, left=4.7*cm),
            Label(text="Periodo del", top=4*cm, left=1*cm),
            SystemField(expression=u'Periodo del: %(var:description)s', top=4*cm, left=1*cm, width=10*cm),
            Label(text="Segun la siguiente Liquidacion", top=5*cm, left=1*cm, width=15*cm),
            Line(left=1*cm, top=6*cm, right=19*cm, bottom=6*cm),
            Label(text="Tipo de Transaccion", top=6*cm, left=1.2*cm),
            Label(text="Tipo de Transaccion", top=6*cm, left=10*cm),
            Label(text="Valor", top=6*cm, left=7.6*cm),
            Label(text="Valor", top=6*cm, left=17.8*cm),
            Line(left=1*cm, top=6.5*cm, right=19*cm, bottom=6.5*cm),
            Label(text="Sueldo", top=7*cm, left=1.2*cm ),
            ObjectValue(expression='quincenal', top=7*cm, left=7.6*cm ),
            Label(text="Valor Antiguedad: %", top=7.5*cm, left=1.2*cm ),
            ObjectValue(expression='porcentaje', top=7.5*cm, left=5.5*cm ),
            ObjectValue(expression='antiguedad', top=7.5*cm, left=7.6*cm ),
            Label(text="INSS", top=7*cm, left=10*cm ),
            ObjectValue(expression='inss', top=7*cm, left=17.8*cm ),
            Label(text="IR", top=7.5*cm, left=10*cm ),
            ObjectValue(expression='ir', top=7.5*cm, left=17.8*cm ),
            Label(text="Prestamo", top=8*cm, left=10*cm ),
            ObjectValue(expression='prestamo', top=8*cm, left=17.8*cm ),
            Label(text="Subtotal Devengos", top=10.5*cm, left=1.2*cm, style={'fontName':'Courier-Bold'}),
            ObjectValue(expression='devengos', top=10.5*cm, left=7.6*cm ),
            Label(text="Subtotal Descuentos", top=10.5*cm, left=10*cm, style={'fontName':'Courier-Bold'}),
            ObjectValue(expression='descuentos', top=10.5*cm, left=17.8*cm ),
            Label(text="Total a Pagar", top=11*cm, left=10*cm, style={'fontName':'Courier-Bold'}),
            ObjectValue(expression='total', top=11*cm, left=17.8*cm ),
            ObjectValue(expression='nombrecompleto', top=12.5*cm, left=10*cm, width=15*cm, style={'fontName': 'Courier-Bold'}),
        ]
        borders = {'bottom': True}
    subreports = [
        SubReport(
                  queryset_string = "%(object)s.percepciones.all()",
                  band_detail = ReportBand(
                        height=0.5*cm,
                        elements=[
                            ObjectValue(attribute_name='tipo', top=-4.5*cm, left=1.2*cm),
                            ObjectValue(attribute_name='monto', top=-4.5*cm, left=7.6*cm),
                        ],
                    ),                                        
                    band_summary = ReportBand (
                        height=0.5*cm,
                        elements=[
                            ObjectValue(attribute_name='monto', left=7.6*cm, action=FIELD_ACTION_SUM),
                        ],
                    ),
        ),          
        SubReport(  
                  queryset_string = "%(object)s.deducciones.all()",
                  band_detail = ReportBand(
                        height=0.5*cm,
                        display_inline = True,
                        elements=[
                            ObjectValue(attribute_name='tipo',   top=-4*cm, left=10*cm),
                            ObjectValue(attribute_name='monto',  top=-4*cm, left=17.8*cm),
                        ],
                    ),                                        
        ),          
    ]
class PlanillaReport( Report ):
    margin_top = 0.3*cm
    margin_bottom = 0.3*cm
    margin_left = 0.3*cm
    margin_right = 0.3*cm
    page_size = landscape(LEGAL)
    default_style = {'fontName':'Courier','fontSize': 8 }
    class band_page_header(ReportBand):
        height = 2.5*cm
        elements = [
            Label(text=t.valor, top=0.5*cm, left=0.5*cm),
            SystemField(expression=u'%(report_title)s del %(var:description)s', top=1*cm, left=0.5*cm, width=10*cm),
            SystemField(expression=u'Departamento: %(var:nombre)s', top=1.5*cm, left=0.5*cm, width=10*cm),
            SystemField(expression=u'Pagina: %(page_number)d', top=0.5*cm, left=30*cm),
            Label(text="Emitido:", top=1*cm, left=30*cm),
            SystemField(expression='%(now:%d/%m/%Y)s', top=1*cm, left=31.5*cm),
            SystemField(expression='%(now:%H:%M:%S)s', top=1.5*cm, left=31.5*cm),
            Label(text="Cod.", top=2*cm, left=0.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Nombre", top=2*cm, left=1.2*cm, width=6*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Mensual", top=2*cm, left=8*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Quincenal", top=2*cm, left=10*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="% Antig.", top=2*cm, left=12*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Antig.", top=2*cm, left=14*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Otros", top=2*cm, left=16*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Devengado", top=2*cm, left=18*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="INSS", top=2*cm, left=20*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="IR", top=2*cm, left=22*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Prestamo", top=2*cm, left=24*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Deducciones", top=2*cm, left=26*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Total Desc.", top=2*cm, left=28.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Liquido", top=2*cm, left=31*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Saldo", top=2*cm, left=33.5*cm, style={'fontName': 'Courier-Bold'}),

        ]
    class band_detail(ReportBand):
        height = 0.5*cm
        auto_expand_height = True
        elements=(
            ObjectValue(attribute_name='codigoempleado', left=0.5*cm),
            ObjectValue(attribute_name='nombrecompleto', left=1.2*cm, width=6*cm ),
            ObjectValue(attribute_name='sueldo', left=8*cm),
            ObjectValue(attribute_name='quincenal', left=10*cm),
            ObjectValue(attribute_name='antiguedad', left=12*cm),
            ObjectValue(attribute_name='valor', left=14*cm),
            ObjectValue(attribute_name='pmonto', left=16*cm),
            ObjectValue(attribute_name='devengo', left=18*cm),
            ObjectValue(attribute_name='inss', left=20*cm),
            ObjectValue(attribute_name='ir', left=22*cm),
            ObjectValue(attribute_name='prestamo', left=24*cm),
            ObjectValue(attribute_name='dmonto', left=26*cm),
            ObjectValue(attribute_name='descuento', left=28.5*cm),
            ObjectValue(attribute_name='liquido', left=31*cm),
            ObjectValue(attribute_name='saldo', left=33.5*cm),
        )
    class band_summary(ReportBand):
        height = 0.5*cm
        elements = [
            Label(text='Totales Depto:', left=0.5*cm, style={'fontName': 'Courier-Bold'}),
            ObjectValue(expression='sum(sueldo)', left=8*cm ),
            ObjectValue(expression='sum(quincenal)', left=10*cm ),
            ObjectValue(expression='sum(valor)', left=14*cm ),
            ObjectValue(expression='sum(pmonto)', left=16*cm ),
            ObjectValue(expression='sum(devengo)', left=18*cm ),
            ObjectValue(expression='sum(inss)', left=20*cm ),
            #ObjectValue(expression='sum(ir)', left=20*cm ),
            ObjectValue(expression='sum(prestamo)', left=24*cm ),
            ObjectValue(expression='sum(dmonto)', left=26*cm ),
            ObjectValue(expression='sum(descuento)', left=28.5*cm ),
            ObjectValue(expression='sum(liquido)', left=31*cm ),
            ObjectValue(expression='sum(saldo)', left=33.5*cm ),
            Label(text='Elaborado Por:',left=2*cm,top=2.5*cm ),
            Label(text='Revisado Por:',left=10*cm,top=2.5*cm ),
            Label(text='Autorizado Por:',left=20*cm,top=2.5*cm ),
            ]
        borders = {'top': True}

class INSSReport( Report ):
    margin_top = 0.3*cm
    margin_bottom = 0.3*cm
    margin_left = 0.3*cm
    margin_right = 0.3*cm
    page_size = landscape(LETTER)
    default_style = {'fontName':'Courier','fontSize': 8 }
    class band_page_header(ReportBand):
        height = 2.5*cm
        elements = [
            Label(text=t.valor, top=0.5*cm, left=0.5*cm),
            SystemField(expression=u'%(report_title)s del %(var:description)s', top=1*cm, left=0.5*cm, width=10*cm),
            SystemField(expression=u'Departamento: %(var:nombre)s', top=1.5*cm, left=0.5*cm, width=10*cm),
            SystemField(expression=u'Pagina: %(page_number)d', top=0.5*cm, left=30*cm),
            Label(text="Emitido:", top=1*cm, left=30*cm),
            SystemField(expression='%(now:%d/%m/%Y)s', top=1*cm, left=31.5*cm),
            SystemField(expression='%(now:%H:%M:%S)s', top=1.5*cm, left=31.5*cm),
            Label(text="Cod.", top=2*cm, left=0.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Nombre", top=2*cm, left=1.2*cm, width=6*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Mensual", top=2*cm, left=8*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Percep.", top=2*cm, left=10*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Antig.", top=2*cm, left=12.5*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="Devengado", top=2*cm, left=15*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="A. Laboral", top=2*cm, left=18*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="A. Patronal", top=2*cm, left=21*cm, style={'fontName': 'Courier-Bold'}),
            Label(text="A. Total", top=2*cm, left=24*cm, style={'fontName': 'Courier-Bold'}),

        ]
    class band_detail(ReportBand):
        height = 0.5*cm
        auto_expand_height = True
        elements=(
            ObjectValue(attribute_name='codigoempleado', left=0.5*cm),
            ObjectValue(attribute_name='nombrecompleto', left=1.2*cm, width=6*cm ),
            ObjectValue(attribute_name='sueldo', left=8*cm),
            ObjectValue(attribute_name='pmonto', left=10*cm),
            ObjectValue(attribute_name='antiguedad', left=12.5*cm),
            ObjectValue(attribute_name='devengo', left=15*cm),
            ObjectValue(attribute_name='laboral', left=18*cm),
            ObjectValue(attribute_name='patronal', left=21*cm),
            ObjectValue(attribute_name='aporte', left=24*cm),
        )
    class band_summary(ReportBand):
        height = 0.5*cm
        elements = [
            Label(text='Totales Depto:', left=0.5*cm, style={'fontName': 'Courier-Bold'}),
            ObjectValue(expression='sum(sueldo)', left=8*cm ),
            ObjectValue(expression='sum(pmonto)', left=10*cm ),
            ObjectValue(expression='sum(antiguedad)', left=12.5*cm ),
            ObjectValue(expression='sum(devengo)', left=15*cm ),
            ObjectValue(expression='sum(laboral)', left=18*cm ),
            ObjectValue(expression='sum(patronal)', left=21*cm ),
            ObjectValue(expression='sum(aporte)', left=24*cm ),
            Label(text='Elaborado Por:',left=2*cm,top=2.5*cm ),
            Label(text='Revisado Por:',left=10*cm,top=2.5*cm ),
            Label(text='Autorizado Por:',left=20*cm,top=2.5*cm ),
            ]
        borders = {'top': True}
