# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding field 'TipoBulto.descripcion'
        db.add_column('hojac_tipobulto', 'descripcion', self.gf('django.db.models.fields.CharField')(default='', max_length=300), keep_default=False)


    def backwards(self, orm):
        
        # Deleting field 'TipoBulto.descripcion'
        db.delete_column('hojac_tipobulto', 'descripcion')


    models = {
        'hojac.aduana': {
            'Meta': {'object_name': 'Aduana'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '10', 'db_column': "'codigo'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'nombre'"})
        },
        'hojac.declarante': {
            'Meta': {'object_name': 'Declarante', '_ormbases': ['hojac.PID']},
            'identificacion': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'identificacion'"}),
            'pid_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['hojac.PID']", 'unique': 'True', 'primary_key': 'True'}),
            'referencia': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'referencia'"}),
            'representative': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'representative'"})
        },
        'hojac.departurearrivalinformation': {
            'Meta': {'object_name': 'DepartureArrivalInformation'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mode': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'nacionalidad': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'registro': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'hojac.documento': {
            'Meta': {'object_name': 'Documento'},
            'fecha': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'fecha'", 'blank': 'True'}),
            'hoja': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.HojaC']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'referencia': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'referencia'"}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.TipoDocumento']", 'null': 'True', 'blank': 'True'})
        },
        'hojac.exportcountry': {
            'Meta': {'object_name': 'ExportCountry'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'hojac.hojac': {
            'Meta': {'object_name': 'HojaC'},
            'aduana': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.Aduana']", 'null': 'True', 'blank': 'True'}),
            'aduanaentrada': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'aduanaentrada'", 'null': 'True', 'to': "orm['hojac.Aduana']"}),
            'ajuste': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'arrival': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'arrival'", 'null': 'True', 'to': "orm['hojac.DepartureArrivalInformation']"}),
            'calculation_working': ('django.db.models.fields.CharField', [], {'default': "'1'", 'max_length': '10', 'blank': 'True'}),
            'cantidad_bultos': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'db_column': "'bultos'", 'blank': 'True'}),
            'declarante': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': "orm['hojac.Declarante']", 'null': 'True', 'blank': 'True'}),
            'declaration': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.TypeOfDeclaration']", 'null': 'True', 'blank': 'True'}),
            'departure_arrival': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.DepartureArrivalInformation']", 'null': 'True', 'blank': 'True'}),
            'destiny_country': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'destiny_country'", 'null': 'True', 'to': "orm['hojac.ExportCountry']"}),
            'discharge': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'divisa': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'documento_transporte': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'export_country': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.ExportCountry']", 'null': 'True', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'fecha'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'importador': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.Importador']", 'null': 'True', 'blank': 'True'}),
            'inferior': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'manifiesto': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'number_forms': ('django.db.models.fields.CharField', [], {'default': "'1'", 'max_length': '10', 'blank': 'True'}),
            'numero': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'numero'"}),
            'pais_destino': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'pais_procedencia': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'pais_procedencia'"}),
            'peso_kilos': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'peso_kilos'"}),
            'proveedor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.Proveedor']", 'null': 'True', 'blank': 'True'}),
            'regimen': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'regimen_nacional': ('django.db.models.fields.CharField', [], {'default': "'000'", 'max_length': '10', 'blank': 'True'}),
            'sadflow': ('django.db.models.fields.CharField', [], {'default': "'I'", 'max_length': '10', 'blank': 'True'}),
            'terminoentre': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.TerminosEntrega']"}),
            'tipobulto': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.TipoBulto']", 'null': 'True', 'blank': 'True'}),
            'total_forms': ('django.db.models.fields.CharField', [], {'default': "'1'", 'max_length': '10', 'blank': 'True'}),
            'transaccioncod1': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'transaccioncod2': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'ubicacionmer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.UbicacionMercancias']"}),
            'valor_flete': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'db_column': "'flete'", 'decimal_places': '2', 'max_digits': '9'}),
            'valor_fob': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'db_column': "'fob'", 'decimal_places': '2', 'max_digits': '10'}),
            'valor_otros': ('django.db.models.fields.DecimalField', [], {'decimal_places': '2', 'db_column': "'otros'", 'default': '0.0', 'max_digits': '10', 'blank': 'True', 'null': 'True'}),
            'valor_seguro': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.ValorSeguro']", 'null': 'True', 'blank': 'True'})
        },
        'hojac.importador': {
            'Meta': {'object_name': 'Importador', '_ormbases': ['hojac.PID']},
            'identificacion': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'identificacion'"}),
            'pid_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['hojac.PID']", 'unique': 'True', 'primary_key': 'True'})
        },
        'hojac.mercancia': {
            'Meta': {'object_name': 'Mercancia'},
            'cantidad': ('django.db.models.fields.IntegerField', [], {'db_column': "'cantidad'"}),
            'caracteristicas': ('django.db.models.fields.TextField', [], {'db_column': "'caracteristicas'"}),
            'clasificacion': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'clasificacion'", 'blank': 'True'}),
            'hoja': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.HojaC']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'marca': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'db_column': "'marca'", 'blank': 'True'}),
            'modelo': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'modelo'", 'blank': 'True'}),
            'origen': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'origen'"}),
            'peso_bruto': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'peso_neto': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'posicion': ('django.db.models.fields.CharField', [], {'max_length': '3', 'db_column': "'posicion'"}),
            'serie': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'serie'", 'blank': 'True'}),
            'unidadmedida': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'unidad_medida'"}),
            'unitario': ('django.db.models.fields.DecimalField', [], {'db_column': "'unitario'", 'decimal_places': '2', 'max_digits': '10'})
        },
        'hojac.nota': {
            'Meta': {'object_name': 'Nota'},
            'descripcion': ('django.db.models.fields.TextField', [], {'db_column': "'descripcion'"}),
            'hoja': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.HojaC']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'hojac.pid': {
            'Meta': {'object_name': 'PID'},
            'ciudad': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'ciudad'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'nombre'"}),
            'pais': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'pais'"}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'db_column': "'telefono'", 'blank': 'True'})
        },
        'hojac.proveedor': {
            'Meta': {'object_name': 'Proveedor', '_ormbases': ['hojac.PID']},
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'direccion'", 'blank': 'True'}),
            'pid_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['hojac.PID']", 'unique': 'True', 'primary_key': 'True'})
        },
        'hojac.terminosentrega': {
            'Meta': {'object_name': 'TerminosEntrega'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'place': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'situation': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'})
        },
        'hojac.tipobulto': {
            'Meta': {'object_name': 'TipoBulto'},
            'bulto': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'hojac.tipodocumento': {
            'Meta': {'object_name': 'TipoDocumento'},
            'docu': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'hojac.typeofdeclaration': {
            'Meta': {'object_name': 'TypeOfDeclaration'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'hojac.ubicacionmercancias': {
            'Meta': {'object_name': 'UbicacionMercancias'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'hojac.valorseguro': {
            'Meta': {'object_name': 'ValorSeguro'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'valor': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '3'})
        }
    }

    complete_apps = ['hojac']
