# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Aduana'
        db.create_table('hojac_aduana', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('codigo', self.gf('django.db.models.fields.CharField')(max_length=10, db_column='codigo')),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=100, db_column='nombre')),
        ))
        db.send_create_signal('hojac', ['Aduana'])

        # Adding model 'TypeOfDeclaration'
        db.create_table('hojac_typeofdeclaration', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('hojac', ['TypeOfDeclaration'])

        # Adding model 'PID'
        db.create_table('hojac_pid', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='nombre')),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, db_column='telefono', blank=True)),
            ('ciudad', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, db_column='ciudad', blank=True)),
            ('pais', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='pais')),
        ))
        db.send_create_signal('hojac', ['PID'])

        # Adding model 'Proveedor'
        db.create_table('hojac_proveedor', (
            ('pid_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['hojac.PID'], unique=True, primary_key=True)),
            ('direccion', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, db_column='direccion', blank=True)),
        ))
        db.send_create_signal('hojac', ['Proveedor'])

        # Adding model 'Importador'
        db.create_table('hojac_importador', (
            ('pid_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['hojac.PID'], unique=True, primary_key=True)),
            ('identificacion', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='identificacion')),
        ))
        db.send_create_signal('hojac', ['Importador'])

        # Adding model 'Declarante'
        db.create_table('hojac_declarante', (
            ('pid_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['hojac.PID'], unique=True, primary_key=True)),
            ('identificacion', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='identificacion')),
            ('representative', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='representative')),
            ('referencia', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='referencia')),
        ))
        db.send_create_signal('hojac', ['Declarante'])

        # Adding model 'TipoBulto'
        db.create_table('hojac_tipobulto', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('bulto', self.gf('django.db.models.fields.CharField')(max_length=300)),
        ))
        db.send_create_signal('hojac', ['TipoBulto'])

        # Adding model 'ExportCountry'
        db.create_table('hojac_exportcountry', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=4)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('hojac', ['ExportCountry'])

        # Adding model 'DepartureArrivalInformation'
        db.create_table('hojac_departurearrivalinformation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('registro', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('nacionalidad', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('mode', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
        ))
        db.send_create_signal('hojac', ['DepartureArrivalInformation'])

        # Adding model 'HojaC'
        db.create_table('hojac_hojac', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('declaration', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hojac.TypeOfDeclaration'], null=True, blank=True)),
            ('aduana', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hojac.Aduana'], null=True, blank=True)),
            ('numero', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='numero')),
            ('fecha', self.gf('django.db.models.fields.DateField')(null=True, db_column='fecha', blank=True)),
            ('proveedor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hojac.Proveedor'], null=True, blank=True)),
            ('importador', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hojac.Importador'], null=True, blank=True)),
            ('declarante', self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['hojac.Declarante'], null=True, blank=True)),
            ('valor_fob', self.gf('django.db.models.fields.DecimalField')(default=0.0, db_column='fob', decimal_places=2, max_digits=10)),
            ('valor_seguro', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hojac.ValorSeguro'], null=True, blank=True)),
            ('valor_flete', self.gf('django.db.models.fields.DecimalField')(default=0.0, db_column='flete', decimal_places=2, max_digits=9)),
            ('valor_otros', self.gf('django.db.models.fields.DecimalField')(decimal_places=2, db_column='otros', default=0.0, max_digits=10, blank=True, null=True)),
            ('cantidad_bultos', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, db_column='bultos', blank=True)),
            ('peso_kilos', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='peso_kilos')),
            ('pais_procedencia', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='pais_procedencia')),
            ('export_country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hojac.ExportCountry'], null=True, blank=True)),
            ('destiny_country', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='destiny_country', null=True, to=orm['hojac.ExportCountry'])),
            ('departure_arrival', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hojac.DepartureArrivalInformation'], null=True, blank=True)),
            ('arrival', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='arrival', null=True, to=orm['hojac.DepartureArrivalInformation'])),
            ('tipobulto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hojac.TipoBulto'], null=True, blank=True)),
            ('manifiesto', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('regimen', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('inferior', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('discharge', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('aduanaentrada', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='aduanaentrada', null=True, to=orm['hojac.Aduana'])),
            ('ubicacionmer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hojac.UbicacionMercancias'])),
            ('terminoentre', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hojac.TerminosEntrega'])),
            ('divisa', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('transaccioncod1', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('transaccioncod2', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('sadflow', self.gf('django.db.models.fields.CharField')(default='I', max_length=10, blank=True)),
            ('number_forms', self.gf('django.db.models.fields.CharField')(default='1', max_length=10, blank=True)),
            ('total_forms', self.gf('django.db.models.fields.CharField')(default='1', max_length=10, blank=True)),
            ('pais_destino', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('calculation_working', self.gf('django.db.models.fields.CharField')(default='1', max_length=10, blank=True)),
            ('regimen_nacional', self.gf('django.db.models.fields.CharField')(default='000', max_length=10, blank=True)),
            ('documento_transporte', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('ajuste', self.gf('django.db.models.fields.CharField')(max_length=10, blank=True)),
        ))
        db.send_create_signal('hojac', ['HojaC'])

        # Adding model 'ValorSeguro'
        db.create_table('hojac_valorseguro', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('valor', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=3)),
        ))
        db.send_create_signal('hojac', ['ValorSeguro'])

        # Adding model 'UbicacionMercancias'
        db.create_table('hojac_ubicacionmercancias', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('location', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('hojac', ['UbicacionMercancias'])

        # Adding model 'TerminosEntrega'
        db.create_table('hojac_terminosentrega', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('place', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('situation', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
        ))
        db.send_create_signal('hojac', ['TerminosEntrega'])

        # Adding model 'Nota'
        db.create_table('hojac_nota', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hoja', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hojac.HojaC'], null=True, blank=True)),
            ('descripcion', self.gf('django.db.models.fields.TextField')(db_column='descripcion')),
        ))
        db.send_create_signal('hojac', ['Nota'])

        # Adding model 'TipoDocumento'
        db.create_table('hojac_tipodocumento', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('docu', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('hojac', ['TipoDocumento'])

        # Adding model 'Documento'
        db.create_table('hojac_documento', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hoja', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hojac.HojaC'], null=True, blank=True)),
            ('tipo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hojac.TipoDocumento'], null=True, blank=True)),
            ('referencia', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='referencia')),
            ('fecha', self.gf('django.db.models.fields.DateField')(null=True, db_column='fecha', blank=True)),
        ))
        db.send_create_signal('hojac', ['Documento'])

        # Adding model 'Mercancia'
        db.create_table('hojac_mercancia', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hoja', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hojac.HojaC'], null=True, blank=True)),
            ('posicion', self.gf('django.db.models.fields.CharField')(max_length=3, db_column='posicion')),
            ('cantidad', self.gf('django.db.models.fields.IntegerField')(db_column='cantidad')),
            ('unidadmedida', self.gf('django.db.models.fields.CharField')(max_length=50, db_column='unidad_medida')),
            ('caracteristicas', self.gf('django.db.models.fields.TextField')(db_column='caracteristicas')),
            ('marca', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, db_column='marca', blank=True)),
            ('modelo', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, db_column='modelo', blank=True)),
            ('origen', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='origen')),
            ('serie', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, db_column='serie', blank=True)),
            ('clasificacion', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, db_column='clasificacion', blank=True)),
            ('unitario', self.gf('django.db.models.fields.DecimalField')(db_column='unitario', decimal_places=2, max_digits=10)),
            ('peso_bruto', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('peso_neto', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
        ))
        db.send_create_signal('hojac', ['Mercancia'])

    def backwards(self, orm):
        # Deleting model 'Aduana'
        db.delete_table('hojac_aduana')

        # Deleting model 'TypeOfDeclaration'
        db.delete_table('hojac_typeofdeclaration')

        # Deleting model 'PID'
        db.delete_table('hojac_pid')

        # Deleting model 'Proveedor'
        db.delete_table('hojac_proveedor')

        # Deleting model 'Importador'
        db.delete_table('hojac_importador')

        # Deleting model 'Declarante'
        db.delete_table('hojac_declarante')

        # Deleting model 'TipoBulto'
        db.delete_table('hojac_tipobulto')

        # Deleting model 'ExportCountry'
        db.delete_table('hojac_exportcountry')

        # Deleting model 'DepartureArrivalInformation'
        db.delete_table('hojac_departurearrivalinformation')

        # Deleting model 'HojaC'
        db.delete_table('hojac_hojac')

        # Deleting model 'ValorSeguro'
        db.delete_table('hojac_valorseguro')

        # Deleting model 'UbicacionMercancias'
        db.delete_table('hojac_ubicacionmercancias')

        # Deleting model 'TerminosEntrega'
        db.delete_table('hojac_terminosentrega')

        # Deleting model 'Nota'
        db.delete_table('hojac_nota')

        # Deleting model 'TipoDocumento'
        db.delete_table('hojac_tipodocumento')

        # Deleting model 'Documento'
        db.delete_table('hojac_documento')

        # Deleting model 'Mercancia'
        db.delete_table('hojac_mercancia')

    models = {
        'hojac.aduana': {
            'Meta': {'object_name': 'Aduana'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '10', 'db_column': "'codigo'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'nombre'"})
        },
        'hojac.declarante': {
            'Meta': {'object_name': 'Declarante', '_ormbases': ['hojac.PID']},
            'identificacion': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'identificacion'"}),
            'pid_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['hojac.PID']", 'unique': 'True', 'primary_key': 'True'}),
            'referencia': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'referencia'"}),
            'representative': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'representative'"})
        },
        'hojac.departurearrivalinformation': {
            'Meta': {'object_name': 'DepartureArrivalInformation'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mode': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'nacionalidad': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'registro': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'hojac.documento': {
            'Meta': {'object_name': 'Documento'},
            'fecha': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'fecha'", 'blank': 'True'}),
            'hoja': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.HojaC']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'referencia': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'referencia'"}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.TipoDocumento']", 'null': 'True', 'blank': 'True'})
        },
        'hojac.exportcountry': {
            'Meta': {'object_name': 'ExportCountry'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'hojac.hojac': {
            'Meta': {'object_name': 'HojaC'},
            'aduana': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.Aduana']", 'null': 'True', 'blank': 'True'}),
            'aduanaentrada': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'aduanaentrada'", 'null': 'True', 'to': "orm['hojac.Aduana']"}),
            'ajuste': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'arrival': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'arrival'", 'null': 'True', 'to': "orm['hojac.DepartureArrivalInformation']"}),
            'calculation_working': ('django.db.models.fields.CharField', [], {'default': "'1'", 'max_length': '10', 'blank': 'True'}),
            'cantidad_bultos': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'db_column': "'bultos'", 'blank': 'True'}),
            'declarante': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': "orm['hojac.Declarante']", 'null': 'True', 'blank': 'True'}),
            'declaration': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.TypeOfDeclaration']", 'null': 'True', 'blank': 'True'}),
            'departure_arrival': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.DepartureArrivalInformation']", 'null': 'True', 'blank': 'True'}),
            'destiny_country': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'destiny_country'", 'null': 'True', 'to': "orm['hojac.ExportCountry']"}),
            'discharge': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'divisa': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'documento_transporte': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'export_country': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.ExportCountry']", 'null': 'True', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'fecha'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'importador': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.Importador']", 'null': 'True', 'blank': 'True'}),
            'inferior': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'manifiesto': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'number_forms': ('django.db.models.fields.CharField', [], {'default': "'1'", 'max_length': '10', 'blank': 'True'}),
            'numero': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'numero'"}),
            'pais_destino': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'pais_procedencia': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'pais_procedencia'"}),
            'peso_kilos': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'peso_kilos'"}),
            'proveedor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.Proveedor']", 'null': 'True', 'blank': 'True'}),
            'regimen': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'regimen_nacional': ('django.db.models.fields.CharField', [], {'default': "'000'", 'max_length': '10', 'blank': 'True'}),
            'sadflow': ('django.db.models.fields.CharField', [], {'default': "'I'", 'max_length': '10', 'blank': 'True'}),
            'terminoentre': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.TerminosEntrega']"}),
            'tipobulto': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.TipoBulto']", 'null': 'True', 'blank': 'True'}),
            'total_forms': ('django.db.models.fields.CharField', [], {'default': "'1'", 'max_length': '10', 'blank': 'True'}),
            'transaccioncod1': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'transaccioncod2': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'ubicacionmer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.UbicacionMercancias']"}),
            'valor_flete': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'db_column': "'flete'", 'decimal_places': '2', 'max_digits': '9'}),
            'valor_fob': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'db_column': "'fob'", 'decimal_places': '2', 'max_digits': '10'}),
            'valor_otros': ('django.db.models.fields.DecimalField', [], {'decimal_places': '2', 'db_column': "'otros'", 'default': '0.0', 'max_digits': '10', 'blank': 'True', 'null': 'True'}),
            'valor_seguro': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.ValorSeguro']", 'null': 'True', 'blank': 'True'})
        },
        'hojac.importador': {
            'Meta': {'object_name': 'Importador', '_ormbases': ['hojac.PID']},
            'identificacion': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'identificacion'"}),
            'pid_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['hojac.PID']", 'unique': 'True', 'primary_key': 'True'})
        },
        'hojac.mercancia': {
            'Meta': {'object_name': 'Mercancia'},
            'cantidad': ('django.db.models.fields.IntegerField', [], {'db_column': "'cantidad'"}),
            'caracteristicas': ('django.db.models.fields.TextField', [], {'db_column': "'caracteristicas'"}),
            'clasificacion': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'clasificacion'", 'blank': 'True'}),
            'hoja': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.HojaC']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'marca': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'db_column': "'marca'", 'blank': 'True'}),
            'modelo': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'modelo'", 'blank': 'True'}),
            'origen': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'origen'"}),
            'peso_bruto': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'peso_neto': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'posicion': ('django.db.models.fields.CharField', [], {'max_length': '3', 'db_column': "'posicion'"}),
            'serie': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'serie'", 'blank': 'True'}),
            'unidadmedida': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'unidad_medida'"}),
            'unitario': ('django.db.models.fields.DecimalField', [], {'db_column': "'unitario'", 'decimal_places': '2', 'max_digits': '10'})
        },
        'hojac.nota': {
            'Meta': {'object_name': 'Nota'},
            'descripcion': ('django.db.models.fields.TextField', [], {'db_column': "'descripcion'"}),
            'hoja': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hojac.HojaC']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'hojac.pid': {
            'Meta': {'object_name': 'PID'},
            'ciudad': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'ciudad'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'nombre'"}),
            'pais': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'pais'"}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'db_column': "'telefono'", 'blank': 'True'})
        },
        'hojac.proveedor': {
            'Meta': {'object_name': 'Proveedor', '_ormbases': ['hojac.PID']},
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'db_column': "'direccion'", 'blank': 'True'}),
            'pid_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['hojac.PID']", 'unique': 'True', 'primary_key': 'True'})
        },
        'hojac.terminosentrega': {
            'Meta': {'object_name': 'TerminosEntrega'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'place': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'situation': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'})
        },
        'hojac.tipobulto': {
            'Meta': {'object_name': 'TipoBulto'},
            'bulto': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'hojac.tipodocumento': {
            'Meta': {'object_name': 'TipoDocumento'},
            'docu': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'hojac.typeofdeclaration': {
            'Meta': {'object_name': 'TypeOfDeclaration'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'hojac.ubicacionmercancias': {
            'Meta': {'object_name': 'UbicacionMercancias'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'hojac.valorseguro': {
            'Meta': {'object_name': 'ValorSeguro'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'valor': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '3'})
        }
    }

    complete_apps = ['hojac']