# -*- coding: UTF-8 -*-
from django.contrib import admin
from django.db import models
from django.conf import settings

from hojac.models import *

#================================================
# Inlines
#================================================

class NotaInline( admin.TabularInline ):
    model = Nota
    extra = 1

class DocumentoInline( admin.TabularInline ):
    model = Documento
    extra = 1

class MercanciaInline( admin.StackedInline ):
    model = Mercancia
    extra = 1

class TipoBultoAdmin( admin.ModelAdmin ):
    list_display = ('bulto', 'descripcion')

class TipoDocumentoAdmin( admin.ModelAdmin ):
    list_display = ('tipo', 'docu')

#================================================
# Admin
#===============================================

class HojaCAdmin( admin.ModelAdmin ):
    model = HojaC
    raw_id_fields = ('export_country',)
    fieldsets = (
        ( None, {
            'fields':('aduana','numero','fecha','proveedor','importador','declarante')
        }),
        ('Datos Adicionales',{
            'classes': ('collapse',),
            'fields': ('sadflow', 'number_forms', 'total_forms', 'calculation_working', 'documento_transporte', 'ajuste')
        }),
        ('I-Conformacion de Valores',{
            'classes': ('collapse',),
            'fields': ('valor_fob', 'valor_flete', 'valor_seguro', 'valor_otros')
        }),
        ('H-Informacion de Bultos',{
            'classes': ('collapse',),
            'fields': ('tipobulto','cantidad_bultos','peso_kilos','pais_procedencia', 'pais_destino')
        }),
        ('Informacion de Transporte',{
            'classes': ('collapse',),
            'fields': ('departure_arrival', 'arrival', 'inferior')
        }),
        ('Informacion de Ciudades',{
            'classes': ('collapse',),
            'fields': ('export_country', 'destiny_country', 'discharge')
        }),
        ('Transacciones',{
            'classes': ('collapse',),
            'fields': ('transaccioncod1','transaccioncod2')
        }),
        ('Otras Opciones',{
            'classes': ('collapse',),
            'fields': ('manifiesto', 'regimen', 'regimen_nacional', 'declaration','aduanaentrada', 'ubicacionmer', 'terminoentre', 'divisa')
        }),
    )
    inlines = [DocumentoInline, NotaInline, MercanciaInline]

class ProveedorAdmin( admin.ModelAdmin ):
    search_fields = ['nombre']
    ordering = ['nombre']
    model = Proveedor

class ImportadorAdmin( admin.ModelAdmin ):
    model = Importador

class DeclaranteAdmin( admin.ModelAdmin ):
    model = Declarante

class DeclarationAdmin( admin.ModelAdmin ):
    model = TypeOfDeclaration

class ExportCountryAdmin( admin.ModelAdmin ):
    model = ExportCountry
    list_display = ('name','code')
    search_fields = ['name']
    ordering = ['name']

class DepartureAdmin( admin.ModelAdmin ):
    model = DepartureArrivalInformation

admin.site.register(TipoBulto, TipoBultoAdmin)
admin.site.register(TipoDocumento, TipoDocumentoAdmin)
admin.site.register(HojaC, HojaCAdmin)
admin.site.register(Proveedor, ProveedorAdmin)
admin.site.register(Importador, ImportadorAdmin)
admin.site.register(Declarante, DeclaranteAdmin)
admin.site.register(TypeOfDeclaration, DeclarationAdmin)
admin.site.register(ExportCountry, ExportCountryAdmin)
admin.site.register(DepartureArrivalInformation, DepartureAdmin)
admin.site.register(Aduana)
admin.site.register(ValorSeguro)
admin.site.register(UbicacionMercancias)
admin.site.register(TerminosEntrega)
