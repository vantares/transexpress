from django.shortcuts import render_to_response, get_object_or_404
from hojac.models import *
from reportes.views import ExportedReport
from django.views.generic import FormView
from hojac.forms import HojaForm
from hojac.reports import HojasReport
import datetime

class HojasView( ExportedReport, FormView):
    form_class = HojaForm
    title = 'Hojas C'
    template_name = 'hojac/hojas.html'
    report_class = HojasReport
    required_permissions = ('reportes.hojasc',)

    def form_valid( self, form ):
        codigo = form.cleaned_data['codigo']
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']

        data = HojaC.objects.all()

        if codigo <> '':
            data = data.filter(numero__contains=codigo)
        
        if start_date <> None and end_date <> None:
            data = data.filter(fecha__range=[start_date, end_date])
    
        context = self.get_context_data()
        context.update({
            'form':form,
            'data':data,
        })
        return self.render_to_response(context, data)

def hojac(request, hoja_id):
    hoja = get_object_or_404(HojaC, pk=hoja_id)
    notas = Nota.objects.filter( hoja = hoja_id )
    documentos = Documento.objects.filter( hoja = hoja_id )
    mercancias = Mercancia.objects.filter(hoja=hoja_id).order_by('posicion')
    posiciones = Mercancia.objects.filter(hoja=hoja_id).values_list('posicion').distinct()
    po = []
    for p in posiciones:
        po.append(p[0])
    cant = hoja.cantidad_bultos / len(po)
    return render_to_response("hojac/xml.xml", {'hoja':hoja, 'notas':notas, 'posiciones':po, 'cantidad':cant,
                    'documentos':documentos, 'mercancias':mercancias}, mimetype='application/xml')

