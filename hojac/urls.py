from django.conf.urls.defaults import patterns, url
from hojac.views import HojasView
urlpatterns = patterns('hojac.views',
    url(r'^$', HojasView.as_view(), name='hojas'),
    url('^(?P<hoja_id>\d+)/$', 'hojac' , name='hoja'),
)
