# -*- coding: utf-8 -*-
from django import forms
from django.contrib.admin import widgets
from django.contrib import admin

class HojaForm( forms.Form ):
    codigo = forms.CharField(max_length=300, required=False)
    start_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Desde', required=False)
    end_date = forms.DateField(widget=widgets.AdminDateWidget(), label='Hasta', required=False)
