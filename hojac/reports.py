# -*- coding: utf-8 -*-
from django.conf import settings

from geraldo import *
from reportlab.lib.pagesizes import LETTER
from reportlab.lib.units import cm
from reportlab.lib.enums import TA_RIGHT, TA_CENTER
from reportlab.lib.colors import navy, red
from crm.models import Parametro
from hojac.models import *
t = Parametro.objects.get(pk=2)
###################################
# Reporte de Clientes
###################################
class HojasReport(Report):
    author = t.valor
    page_size = landscape(LETTER)
    default_style = {'fontName':'Courier'}

    class band_page_header(ReportBand):
        height = 1.3*cm
        elements = [
            SystemField(expression=u'%(report_title)s', top=0.1*cm, left=0, width=BAND_WIDTH,
                style={'fontName': 'Courier', 'fontSize': 14, 'alignment': TA_CENTER, 'textColor':navy}),
            Label(text="Nombre", top=0.8*cm, left=0.5*cm),
            Label(text="Tipo", top=0.8*cm, left=8*cm),
            Label(text="Activo", top=0.8*cm, left=12*cm),
            Label(text="Exonerado", top=0.8*cm, left=15*cm),
            Label(text="Zona", top=0.8*cm, left=20*cm),
            SystemField(expression=u'Pag. %(page_number)d de %(page_count)d', top=0.1*cm,
                width=BAND_WIDTH, style={'alignment': TA_RIGHT}),
            ]
        borders = {'bottom': True}

