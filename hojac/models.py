from django.db import models
from decimal import *
###########################################################
#                       Aduana
###########################################################
class Aduana( models.Model ):
    codigo = models.CharField(db_column="codigo", null=False, blank=False, max_length=10, verbose_name="Codigo Aduana")
    nombre = models.CharField(db_column="nombre", null=False, blank=False, max_length=100, verbose_name="Aduana")

    @property
    def aduana( self ):
        return self.codigo + ' - ' + self.nombre

    def __unicode__( self ):
        return self.aduana

###########################################################
#  Tipo de Declaracion
###########################################################
class TypeOfDeclaration ( models.Model ):
    tipo = models.CharField(null=False, blank=False, max_length=50, verbose_name="Type of declaration")
    code = models.CharField(null=False, blank=False, max_length=50, verbose_name="Declaration gen procedure code")

    def __unicode__( self ):
        return self.tipo

##############################################################
#  Informacion general de Proveedor - Importador - Declarante
##############################################################
class PID( models.Model ):
    nombre = models.CharField(db_column="nombre", null=False, blank=False, max_length=300, verbose_name="Nombre o Razon Social")
    telefono = models.CharField(db_column="telefono", null=True, blank=True, max_length=50, verbose_name="telefono")
    ciudad = models.CharField(db_column="ciudad", null=True, blank=True, max_length=300, verbose_name="ciudad")
    pais = models.CharField(db_column="pais", null=False, blank=False, max_length=300, verbose_name="pais")

    def __unicode__( self ):
        return self.nombre

###########################################################
#           Proveedor - Exportador
###########################################################
class Proveedor( PID ):
    direccion = models.CharField(db_column="direccion", null=True, blank=True, max_length=300, verbose_name="direccion")

    class Meta:
        verbose_name = "Exportador"
        verbose_name_plural = "Exportadores"


###########################################################
#           Importador - Consignatario
###########################################################
class Importador( PID ):
    identificacion = models.CharField(db_column="identificacion", null=False, blank=False, max_length=300, verbose_name="identificacion")

    class Meta:
        verbose_name = "Consignatario"
        verbose_name_plural = "Consignatarios"

###########################################################
#                   Declarante
###########################################################
class Declarante( PID ):
    identificacion = models.CharField(db_column="identificacion", null=False, blank=False, max_length=300, verbose_name="identificacion")
    representative = models.CharField(db_column="representative", null=False, blank=False, max_length=300, verbose_name="representative")
    referencia = models.CharField(db_column="referencia", null=False, blank=False, max_length=300, verbose_name="Referencia del Declarante")

def get_default_aduana():
    try:
        aduana = Aduana.objects.get(codigo='0110')
    except Aduana.DoesNotExist:
        aduana = None
    return aduana

def get_default_declarante():
    try:
        declarante = Declarante.objects.get(nombre='TransExpress')
    except Declarante.DoesNotExist:
        declarante = None
    return declarante


###########################################################
#           Tipos de Bulto
###########################################################
class TipoBulto( models.Model ):
    bulto = models.CharField(null=False, blank=False, max_length=300, verbose_name="tipo de bulto")
    descripcion = models.CharField(null=False, blank=False, max_length=300)

    def __unicode__( self ):
        return self.bulto


###########################################################
#           Codigo - Pais de Exportacion
###########################################################
class ExportCountry( models.Model ):
    code = models.CharField( null=False, blank=False, max_length=4, verbose_name='Country code')
    name = models.CharField( null=False, blank=False, max_length=50, verbose_name='Country name')

    class Meta:
        verbose_name = "Export/Destiny Country"
        verbose_name_plural = "Export/Destiny Countries"

    def __unicode__( self ):
        return self.name

###########################################################
#           Departure Arrival Infomation
###########################################################
class DepartureArrivalInformation( models.Model ):
    registro = models.CharField( null=False, blank=False, max_length=50, verbose_name='Identity')
    nacionalidad = models.CharField( null=False, blank=False, max_length=50, verbose_name='Nacionalidad')
    mode = models.CharField( null=True, blank=True, max_length=50, help_text='Solo aplica al Arrival', verbose_name='Modo de Transporte')

    def __unicode__( self ):
        return self.registro

class HojaC( models.Model ):
    declaration = models.ForeignKey(TypeOfDeclaration, blank=True, null=True, verbose_name="Type Of Declaration")
    aduana = models.ForeignKey(Aduana, blank=True, null=True, default=get_default_aduana, verbose_name="Aduana")
    numero = models.CharField(db_column="numero", null=False, blank=False, max_length=300, verbose_name="Numero")
    fecha = models.DateField(db_column="fecha", null=True, blank=True, verbose_name="fecha")
    proveedor = models.ForeignKey(Proveedor, blank=True, null=True, verbose_name="B-DEL PROVEEDOR")
    importador = models.ForeignKey(Importador, blank=True, null=True, verbose_name="C-DEL IMPORTADOR")
    declarante = models.ForeignKey(Declarante, blank=True, null=True, default=get_default_declarante, verbose_name="D-DEL DECLARANTE")
    #I-Conformacion de Valores
    valor_fob = models.DecimalField(db_column="fob", null=False, blank=False, max_digits=10, decimal_places=2, default=0.00, verbose_name="Valor FOB US $")
    valor_seguro = models.ForeignKey('hojac.ValorSeguro', blank=True, null=True, verbose_name="Tipo Seguro")
    valor_flete = models.DecimalField(db_column="flete", null=False, blank=False, max_digits=9, decimal_places=2, default=0.00, verbose_name="Valor FLETE US $")
    valor_otros = models.DecimalField(db_column="otros", null=True, blank=True, max_digits=10, decimal_places=2, default=0.00, verbose_name="Valor OTROS GASTOS US $")
    #H-Informacion de bultos
    cantidad_bultos =   models.IntegerField(db_column="bultos", null=True, blank=True, default=0, verbose_name="cantidad de bultos")
    peso_kilos = models.CharField(db_column="peso_kilos", null=False, blank=False, max_length=300, verbose_name="peso en kilos")
    pais_procedencia = models.CharField(db_column="pais_procedencia", null=False, blank=False, max_length=300, verbose_name="pais de procedencia")
    export_country = models.ForeignKey(ExportCountry, blank=True, null=True, default=get_default_aduana, verbose_name="Export Country")
    destiny_country = models.ForeignKey(ExportCountry, blank=True, null=True, default=get_default_aduana, related_name= 'destiny_country',verbose_name="Destiny Country")
    departure_arrival = models.ForeignKey(DepartureArrivalInformation, blank=True, null=True, verbose_name="Departure")
    arrival = models.ForeignKey(DepartureArrivalInformation, blank=True, null=True, related_name='arrival', verbose_name="Arrival")

    #ultimos campos que voy agregando
    tipobulto = models.ForeignKey(TipoBulto, blank=True, null=True, verbose_name="TipoBulto")
    manifiesto = models.CharField(null=True, blank=True, max_length=300, verbose_name='Manifiesto')
    regimen = models.CharField(null=True, blank=True, max_length=300, verbose_name='Regimen')
    inferior = models.CharField(null=True, blank=True, max_length=50, verbose_name='Modo de Transporte Inferior')
    discharge = models.CharField(null=True, blank=True, max_length=50, verbose_name='Place of Discharge')
    aduanaentrada = models.ForeignKey(Aduana, blank=True, null=True, related_name='aduanaentrada', verbose_name="Aduana de entrada")

    ubicacionmer = models.ForeignKey('hojac.UbicacionMercancias', verbose_name="Ubicacion Mercancias")
    terminoentre = models.ForeignKey('hojac.TerminosEntrega', verbose_name="Terminos de Entrega")

    divisa = models.CharField(null=True, blank=True, max_length=50, verbose_name='Divisa Factura')
    transaccioncod1 = models.CharField(null=True, blank=True, max_length=300, verbose_name='Tipo de Transaccion Codigo 1')
    transaccioncod2 = models.CharField(null=True, blank=True, max_length=300, verbose_name='Tipo de Transaccion Codigo 2')

    #Mas campos que son valores por defecto pero se agregan en caso que cambien algun dia
    sadflow = models.CharField( default='I', null=False, blank=True, max_length=10, verbose_name='Sad flow')
    number_forms = models.CharField( default='1', null=False, blank=True, max_length=10, verbose_name='Number of the forms')
    total_forms = models.CharField( default='1', null=False, blank=True, max_length=10, verbose_name='Total number of forms')
    pais_destino = models.CharField( null=False, blank=False, max_length=300, verbose_name="pais destino ")
    calculation_working = models.CharField( default='1', null=False, blank=True, max_length=10, verbose_name="calculation working mode")
    regimen_nacional = models.CharField( default='000', null=False, blank=True, max_length=10, verbose_name="regimen nacional")
    documento_transporte = models.CharField(null=False, blank=True, max_length=20, verbose_name="Numero documento de transporte")
    ajuste = models.CharField(null=False, blank=True, max_length=10, verbose_name="Ajuste")

    class Meta:
        verbose_name = "Hoja C"
        verbose_name_plural = "Hojas C"

    def __unicode__( self ):
        return self.numero

    @property
    def vseg( self ):
        seg = Decimal(self.valor_fob * self.valor_seguro.valor ).quantize(Decimal('1.00'))
        return seg

    @property
    def valor_cif( self ):
        s = self.valor_seguro.valor if self.valor_seguro is not None else 0
        cif = Decimal( (self.valor_fob) + (self.valor_flete) + (s) + (self.valor_otros)).quantize(Decimal('1.00'))
        return cif if cif is not None else 0

    def vseguro( self ):
        return str(self.valor_seguro.valor) if self.valor_seguro is not None else 0

    def vcif( self ):
        return str( self.valor_cif ) if self.valor_cif is not None else 0

    def vfob( self ):
        return str( self.valor_fob ) if self.valor_fob is not None else 0

    def votros( self ):
        return str( self.valor_otros ) if self.valor_otros is not None else 0

class ValorSeguro( models.Model ):
    tipo = models.CharField(max_length=50, verbose_name='Tipo / Region')
    valor = models.DecimalField(null=False, blank=False, max_digits=10, decimal_places=3, verbose_name="Valor Seguro USD $")

    def __unicode__(self):
        return unicode(self.tipo+' - '+str(self.valor))

class UbicacionMercancias( models.Model ):
    location = models.CharField(max_length=50, verbose_name='Location of goods')

    def __unicode__(self):
        return unicode(self.location)

class TerminosEntrega( models.Model ):
    code = models.CharField(max_length=50, verbose_name='Codigo')
    place = models.CharField(blank=True, max_length=50, verbose_name='Place')
    situation = models.CharField(blank=True, max_length=50, verbose_name='Situation')

    def __unicode__( self ):
        return self.code

class Nota( models.Model ):
    hoja = models.ForeignKey(HojaC, blank=True, null=True, verbose_name="hoja")
    descripcion = models.TextField(db_column="descripcion", null=False, blank=False, verbose_name="descripcion")

    def __unicode__( self ):
        return self.descripcion

class TipoDocumento( models.Model ):
    tipo = models.CharField(max_length=50, verbose_name='Codigo')
    docu = models.CharField(max_length=100, verbose_name='Documento')

    def __unicode__( self ):
        return self.tipo

class Documento( models.Model ):
    hoja = models.ForeignKey(HojaC, blank=True, null=True, verbose_name="hoja")
    tipo = models.ForeignKey(TipoDocumento, blank=True, null=True, verbose_name="Tipo de Documento")
    referencia = models.CharField(db_column="referencia", null=False, blank=False, max_length=300, verbose_name="Attached Document Reference")
    fecha = models.DateField(db_column="fecha", null=True, blank=True, verbose_name="Fecha")

    def __unicode__( self):
        return self.referencia

class Mercancia( models.Model ):
    hoja = models.ForeignKey(HojaC, blank=True, null=True, verbose_name="hoja")
    posicion =   models.CharField(db_column="posicion", null=False, blank=False, max_length=3, verbose_name="Posicion")
    cantidad =   models.IntegerField(db_column="cantidad", null=False, blank=False, verbose_name="cantidad")
    unidadmedida = models.CharField(db_column="unidad_medida", null=False, blank=False, max_length=50, verbose_name="unidad de medida")
    caracteristicas = models.TextField(db_column="caracteristicas", null=False, blank=False, verbose_name="caracteristicas de las mercancias")
    marca = models.CharField(db_column="marca", null=True, blank=True, max_length=100, verbose_name="marca")
    modelo = models.CharField(db_column="modelo", null=True, blank=True, max_length=300, verbose_name="modelo y/o estilo")
    origen = models.CharField(db_column="origen", null=False, blank=False, max_length=300, verbose_name="origen")
    serie = models.CharField(db_column="serie", null=True, blank=True, max_length=300, verbose_name="Serie o Referencia")
    clasificacion = models.CharField(db_column="clasificacion", null=True, blank=True, max_length=20, verbose_name="clasificacion Arancelaria")
    unitario = models.DecimalField(db_column="unitario", null=False, blank=False, max_digits=10, decimal_places=2, verbose_name="Unitario USD $")
    peso_bruto = models.DecimalField(null=False, blank=False, max_digits=10, decimal_places=2, verbose_name="Peso bruto Kg")
    peso_neto = models.DecimalField(null=False, blank=False, max_digits=10, decimal_places=2, verbose_name="Peso neto Kg")


    class Meta:
        verbose_name = "Descripcion de las Mercancia Importada"

    def __unicode__( self ):
        return self.caracteristicas

    @property
    def total( self ):
        return round((self.cantidad * float(self.unitario)),2)
